#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
#
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Inc.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

use warnings;
use strict;
use POSIX;
use Getopt::Long;
use LWP::UserAgent;
use Data::Dumper;
use Parallel::ForkManager;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib";
#use lib "../lib/";
#use Kpan::Client;
use App::DirectCmd;
use Kpan::DefaultModule;
use Kpan::EB;
use Kpan::Install;
use Kpan::InstallTest;
use Kpan::KLog;
use Kpan::KStat;
use Kpan::ParseCmd;
use Kpan::ParseExternal;
use Kpan::Search;
use Kpan::Wiki;
#no warnings 'qw';

our $VERSION = '1.6.1';

my $app_name;
my $app_version;
my $app_install_name;
my $app_install_version;
my $app_version_cmd;
my $app_version_help;
my $app_home_url;
my $app_archive_name;
my $app_archive_url;
my $app_version_url;
my $app_install_log = "K_install.log";
my @app_version_array;
my @app_download_array;
my $app_page_url;
my $app_version_file_name;
my $brief                ="";
my $category             ="Bioinformatics";
my $channel;
my $config_client;
my $cmd_log;
my $collect_url          = 0;
my $client_name          = "kpan";
my $cluster_system;
my $debug                = 0;
my $detail               = 0;
my $direct_cmd_file;
my $dry_run;
my $parse_eb;
my $fix_url              = 0;
my $get_EB_cmd;
my $get_EB_receipt;
my $group_name;          # for receipt search
my $help                 = "";
my $help_parameter       ; # for app command test
my $install;
my $install_parsed       = undef;
my $is_kpan_dev          = 0;
my $log_file_all         = "k_all.log";
my $local_copy_file_handler;                
my $k_log;
my $module_file;
my $module_names; # seperated by space or, exclosed by ""
my $module_exp_pair; # seperated by, exclosed by "" LD_LIBRARY_PATH=lib/python2.7/site-packages/phycas/conversions
my $no_db;
my $note;
my $no_wiki;
my $org_name;            # for receipt search
my $os;                  # for receipt search
my $parse_ZU_log         = "";
my $parse_log;
my $parse_module;
my $prereq_parse_history = 0;
my $prereq_command;
my $quiet                = 0;
my $remove_install;       # remove install
my $search               = undef;
my $search_scope         = undef;
my $self_only            = undef;
my $server_stop_or_start = undef;
my $set_file_name_size   = 0;
my $setting_file_name;
my $start_level          = 0;
my $tarfile_name         ;
my $test                 = 0;
my $test_cmd_file        = undef; # file name for app test result 
my $test_command         = undef;
my $test_install         = 0;
my $test_file_core_name  = "";
my $testRPC              = 0;
my $url_check            = 0;
my $update_wiki;
my $upload_k_log         ;
my $upload_module_file   ;
my $version_file_name_ext;
my $wiki_api_url;
my $wiki_login_name;
my $wiki_login_password;
my $wiki_software_list_title;
my $wiki_test            = "";
my $write_module_file    = 0;

if ( @ARGV > 0 ) {
	GetOptions(
		"an|app_archive_name=s"             => \$app_archive_name,         
		"au|app_archive_url=s"              => \$app_archive_url,         
		"br|brief=s"                        => \$brief,         
		"cc|config_client"                  => \$config_client,         
		"cn|client_name=s"                  => \$client_name,         
		"cs|cluster_system=s"               => \$cluster_system,         
		"ct|category=s"                     => \$category,         
		"ch|channel=s"                      => \$channel, #conda         
		"cu|collect_url=i"                  => \$collect_url,         
		"d|debug=i"                         => \$debug,                # 0: simple, 1: deep         
		"df|direct_cmd_file=s"              => \$direct_cmd_file,      # dev file contains eb app name and sample command         
		"dr|dry_run=i"                      => \$dry_run,              # 0: run (default), 1: download, unpack 2. compile 3. update wiki 4. upload receipt/module         
		"dt|detail=i"                       => \$detail,               # 0: simple, 1: deep         
		"fu|fix_url"                        => \$fix_url,         
		"ge|get_EB_cmd"                     => \$get_EB_cmd,         
		"gn|group_name=s"                   => \$group_name,      
		"gr|get_EB_receipt=s"               => \$get_EB_receipt,         
		"h|help"                            => \$help,         
		"hp|help_parameter=s"               => \$help_parameter,         
		"hu|app_home_url=s"                 => \$app_home_url,           
		"in|app_install_name=s"    		      => \$app_install_name,         
		"iv|install_version=s"    		      => \$app_install_version,         
		"i|install"                         => \$install,         
		"ik|is_kpan_dev=i"                  => \$is_kpan_dev,         
		"lc|local_copy_file_handler=s"      => \$local_copy_file_handler,         
		"l|log_file=s"                      => \$log_file_all,         
		"kl|k_log=s"                        => \$k_log,         
		"me|module_exp_pair=s"              => \$module_exp_pair,         
		"mf|module_file=s"                  => \$module_file,         
		"mn|module_names=s"                 => \$module_names,         
		"nd|no_db"         						      => \$no_db,         
		"nt|note=s"         						    => \$note,         
		"nw|no_wiki"         			 		      => \$no_wiki,         
		"n|name=s"                          => \$app_name,             # string         
		"on|org_name=s"                     => \$org_name,                                      
		"os|os=s"                           => \$os,                                      
		"pe|parse_eb=s"                     => \$parse_eb,         
		"ph|prereq_parse_history"           => \$prereq_parse_history,         
		"pl|parse_ZU_log=s"                 => \$parse_ZU_log,         
		"pq|prereq"                         => \$prereq_command,         
		"ps|parse_log=s"                    => \$parse_log,         
		"ps|parse_module=s"                 => \$parse_module,         
		"q|quiet"                           => \$quiet,         
		"ri|remove_install"                 => \$remove_install,         
		"s|search"                          => \$search,         
		"sn|self_only"                      => \$self_only,         
		"sf|set_file_name_size"             => \$set_file_name_size,         
		"sl|start_level=s"                  => \$start_level,          # 0 dry run, 1, fetch, 2 extrac                
		"ss|search_scope=i"                 => \$search_scope, #receipt search scope, default 0,  0=ALL, 1=self, 2=group, 3=cluster, 4=org         
		"sn|setting_file_name=s"            => \$setting_file_name,         
		"st|server_stop_or_start"           => \$server_stop_or_start, # 1 start,0 stop               
		"t|test=i"                          => \$test,         
		"ta|tar_name=s"                     => \$tarfile_name,         
		"tc|test_cmd=s"                     => \$test_command,         
		"tf|test_cmd_file=s"                => \$test_cmd_file,         
		"ti|test_install"                   => \$test_install,         
		"tn|test_file_core_name=s"          => \$test_file_core_name,         
		"tr|test_RPC"                       => \$testRPC,         
		"uc|url_check"                      => \$url_check,         
		"uk|upload_k_log"                   => \$upload_k_log,         
		"um|upload_module_file"             => \$upload_module_file,         
		"uw|update_wiki"                    => \$update_wiki,         
		"ve|version_file_name_ext"          => \$version_file_name_ext,         
		"vn|app_version_file_name=s"        => \$app_version_file_name,         
		"vu|app_version_url=s"              => \$app_version_url,         
		"vc|app_version_cmd=s"              => \$app_version_cmd,         
		"vh|app_version_help=s"             => \$app_version_help,         
		"v|version=s"                       => \$app_version,         
	  "wa|wiki_api_url=s"                 => \$wiki_api_url,         
		"wl|wiki_software_list_title=s"     => \$wiki_software_list_title,         
		"wm|write_module_file"              => \$write_module_file,         
		"wn|wiki_login_name=s"              => \$wiki_login_name,         
		"wp|wiki_login_password=s"          => \$wiki_login_password,         
		"w|wiki_test"                       => \$wiki_test,         
  ) or usage("");
}
else {
  usage("");
}

if ($help) {
  usage("");
}

#---------------------- Main -------------------
my $client = setClientInfo(
	$setting_file_name, $no_db,$no_wiki,$config_client,$debug);	

my $_date = `date +%m/%d/%y_%T`;
$_date =~ s/\n*$//;
print __FILE__." Line ".__LINE__."\tkpan version $VERSION, $_date, ";
my $_conn_test =$client->{rc}->testConnection;
do {say "no connection to kpan server! If the problem consists, please contact kpan admin. Server:port=".$client->{rc}->{server_hostname}.":".$client->{rc}->{server_port}; exit;} if (!defined $client || !defined $client->{rc} || !defined $_conn_test );
say "connection is $_conn_test" if (defined $_conn_test);

if ($test && !defined $update_wiki ) {
	#test($debug);
	#testBrief();
	#testVersion();
	#testPattern();
	#testUrl();
	#setClientInfo($src_dir_prefix,  $install_dir_prefix,$lmod_dir_prefix, $app_install_log,$install_user_mark,$debug);
	#testColumn("app_prereq");
	testRPC($app_name, $app_version, $debug) if($test==1);
	#parseCmdFile($local_copy_file_handler,$debug);
	
	writeEB($app_name, $app_version, $debug) if($test==2);
	exit 0;
}elsif ($parse_eb) {
	parseEB($parse_eb,$debug, );
	exit 0;
}elsif ($parse_log) {
	parseLog($parse_log, $debug);
	exit 0;
}elsif ($parse_module) {
	parseModule($parse_module,$debug, );
	exit 0;
}elsif ($wiki_test) {
	testWiki();
	exit 0;
}elsif ( $parse_ZU_log ne "" ) {
	Kpan::ParseExternal->parseZULog($parse_ZU_log);
	exit 0;
}elsif ($collect_url==9 && !defined $app_name ) {
	collectUrl($debug);
	exit(0);
}elsif ($prereq_parse_history) {
	parseHistory($prereq_parse_history);
	exit(0);
}elsif ($set_file_name_size) {
	setFileNameSize($debug);
	exit(0);
}elsif ($fix_url) {
	my $p = Kpan::ParseExternal->new();
	$p->fixUrlCollection($debug);
	exit 0;
}
if($get_EB_cmd){
  getEBCmd($note, $detail, $debug);
  exit 0;
}
if($get_EB_receipt){
  getEBReceipt($get_EB_receipt, $note, $debug);
  exit 0;
}
if($direct_cmd_file){
  parseDirectCmd($direct_cmd_file, $debug);
  exit 0;
}
if(( $install && $app_name) || ($app_name && (!defined $get_EB_cmd 
  && !defined $get_EB_receipt && !defined $test && !defined $update_wiki 
  && !defined $parse_eb && !defined $parse_log && !defined $parse_module
  && !defined $parse_ZU_log && !defined $collect_url 
  && !defined $prereq_parse_history && !defined $set_file_name_size 
  && !defined $fix_url && !defined $test_install && !defined $remove_install
  && !defined $write_module_file  && !defined $upload_k_log && !defined $update_wiki
  && !defined $help )) ){
  installSoftware(
		$debug,                    $client,                $app_name,
		$app_version,              $app_install_name,      $app_install_version, 
		$app_home_url, 		         $app_archive_url,       $app_version_url,  
		$local_copy_file_handler,	 $prereq_command,        $brief,
		$category,                 $channel,               $collect_url,          
		$dry_run,                  $no_db,                 $start_level,
		$search_scope,           	 $version_file_name_ext, $tarfile_name,
		$module_names
  );
}
if (defined $test_install && $test_install>0 ) {
	if (!defined $k_log){
		usage("install log name needs to be defined at --k_log");
	}else{
		testInstall($k_log, $help_parameter, $test_command, $test_file_core_name, $debug);
	}
}
if (defined $search && $search>0 && (defined $app_name || defined $app_install_name)) {
		search(
		$client,				 $client_name,	      $app_name,	
		$app_version,	   $app_install_name,		$app_install_version,
		$cluster_system, $group_name, 	      $is_kpan_dev, 									
		$os,             $org_name,           $search_scope,
		$self_only,		   $debug
		);
	}
if( $remove_install ){
	$app_install_name    = defined $app_install_name    ? $app_install_name    : $app_name; # fix for legacy log
	$app_install_version = defined $app_install_version ? $app_install_version : $app_version;
	removeInstall($debug,$client,$app_install_name, $app_install_version, $channel );
}
if( defined $write_module_file && $write_module_file>0){
	if (!defined $k_log){
		usage("install log name needs to be defined at --k_log");
	}else{
	  writeModuleFile( $k_log, $module_exp_pair, $debug );
  }	
}
if ( defined $upload_k_log &&  $upload_k_log >0 ) {
	if (!defined $k_log){
		usage("install log name needs to be defined at --k_log");
	}else{
		my $is_update       = 1;
		my $skip_parse_test = 0;
	  parseKLog( $k_log, $is_update,$skip_parse_test, $debug );
  }
}
if(defined $update_wiki && $update_wiki>0 ){
	if (!defined $k_log && !defined $app_install_name && !defined $app_version_cmd){
		usage("install log name needs to be defined at --k_log");
	}else{
	  updateWiki( $k_log, $test, $app_install_name, $app_install_version, $app_version_cmd, $app_version_help, $debug );
  }	     
}

say __FILE__. " Line " . __LINE__ . ", job done\n";

#-------------------- subrotiune ----------------
sub usage {
    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;

    #$command =~ s#^.*/##; #remove path
    $message .= <<"END_MESSAGE";
$command [OPTIONS ... ]
version: $VERSION
OPTIONS:
General options
				-h,  --help                                help
				-t,  --thread=[Integer]                    number of threads*
User options
        -cc, --config_client      		             (re)config kpan setting																							
        -sn, --setting_file_name=[String]          setting file path and name (default: ~./kpaninfo_{hostname})																							
        -cn, --client_name=[String]		             kpan account name (default: user name)	*																						
        -cs, --cluster_system=[String]			       cluster system	name *																		
        -ip, --install_dir_prefix=[String]		     install dir prefix	*															
        -mp, --lmod_dir_prefix=[String]			       lmod dir prefix	*																	
        -nd, --no_db			      									 no database function	*																	
        -nw, --no_wiki								       			 no wiki function	*																	
        -sp, --src_dir_prefix=[String]	           src dir prefix	(build dir) *																				
        -ss, --search_scope=[int]	                 receipt search scope, default 0, 0=ALL, 1=self, 2=group, 3=cluster, 4=org) 																				
Install options
        -i,  --install			                       installation																								
        -n,  --name=[String]			                 application name																								
        -v,  --version=[String]	                   application version			 											
        -hu, --app_home_url=[String]	             application home url																							
        -vu, --app_version_url=[String]			       application version url																		
        -au, --app_archive_url=[String]			       application archive url																		
        -ch, --channel=[String]			               conda source channel	(default: bioconda)																						
        -ct, --category=[String]			             application category	(default: Bioinformatics)																						
        -dr, --dry_run=[Integer]		               0: run (default), 1: download, unpack 2: (1) and compile (default for new app) 3: (2) and update wiki 4: (1) and update wiki 5: (3) and upload receipt/module	*																												
        -iv, --install_version=[String]		         application install custom version 																	
        -l,  --log_file=[String]	                 install log file name (defailt: K_install_log)																								
        -lc, --local_copy_file_handler=[String]	   local copy file handler	(copy from local instead of download)										
        -me, --module_exp_pair=[String]	           extra export paths,seperated by , and enclosed by "". such as "LD_LIBRARY_PATH=lib/python2.7/site-packages/phycas/conversions,PATH=scripts"										
        -q,  --quiet=[Integer]                     0: display all stdout, stderr (default), 1: quiet on download 2: quiet on 1 and compile *										
        -vn, --app_version_file_name		           path to downloaded tar file																						
        -uk, --upload_k_log		                     upload receipt log																						
        -um, --upload_module_file		               upload module file																
Wiki options
        -uw, --update_wiki                         update wiki												
        -wa, --wiki_api_url=[String]		           wiki api url																						
        -wl, --wiki_software_list_title=[String]	 wiki software list page title(default: Software)										
        -wn, --wiki_login_name=[String]			       wiki login name																		
        -wp, --wiki_login_password=[String]			   wiki login password											
Dev config
        -d,  --debug=[Integer]		                 debug																													
        -cu, --collect_url=[String]      			     application collection url (for internal test)																		
        -fu, --fix_url          			             fix url (for internal test)																						
        -ik, --is_kpan_dev          			         is kpan developer, default is 0 where 0=NO, 1=Yes (for internal test)																						
        -ph, --prereq_parse_history                prereq parse history	(for internal test)										
        -pl, --parse_ZU_log		                     parse_history_log (for internal test)																						
        -pq, --prereq=[String]                     prerequisite command														
        -sf, --set_file_name_size                  set file name and size	(for internal test)												
        -t,  --test                                test																			
        -uc, --url_check                           url check (for internal test)																		
        -w,  --wiki_test         			             wiki test																				
 
example: 
install latest version gcc: $command -n gcc 
install gcc v5.4.0: $command -n gcc -v 5.4.0
search installed app or receipt: $command -s -n gcc 
try to install if no receipt available: $command -n gcc -v 4.9.2 -vu http://mirrors-usa.go-parts.com/gcc/releases/gcc-4.9.2/gcc-4.9.2.tar.gz 
install multiple versions: $command --install -n gcc -v 4.7.4 -v 4.9.2 -au http://mirrors-usa.go-parts.com/gcc/releases/ 
install gcc latest version with no receipt available: $command --install -n gcc -au http://mirrors-usa.go-parts.com/gcc/releases/ 
update wiki for EB installation: $command --update_wiki -in GLIMMER -iv 3.02b-foss-2016b -vc glimmer3 -vh "-h" 
dry install: $command --install -n gcc -au http://mirrors-usa.go-parts.com/gcc/releases/ --dry_run 1

END_MESSAGE

    say STDERR ($message);
    exit(0);
}

sub writeEB{
	my ($_app_name,$_version, $_debug)=@_;
  say __FILE__. " Line " . __LINE__ . ", write EB, n=$_app_name,v=$_version debug=$_debug";
  $_app_name = "libtool" if (!defined $_app_name);
  $_version = "2.4.6" if (!defined $_app_name && !defined $_version);
  my $_client = Kpan::Client->new;
  $_client->{debug}=$_debug;
  $_client->{kpan_client_name}="yhuang_kpan.org";
  $_client->setClientInfo;
  my ($_kpan_client_name, $_install_id, $_client_id,$_client_name, $_app_id, $_kp_name,$_set_id, $_is_all);
  my $_rc = Kpan::RestClient->new;
  $_rc->{debug}=$_debug;
  ($_kpan_client_name,           $_install_id, $_client_id,$_client_name, $_app_id, $_app_name,$_kp_name, $_version, $_set_id,$_is_all)=
  ($_client->{kpan_client_name}, undef,        undef,      undef,         undef,    $_app_name,undef,     $_version, undef,   undef);
  my $_install_hash_ref = $_rc->getInstallHash($_kpan_client_name, $_install_id, $_client_id,$_client_name, $_app_id, $_app_name,$_kp_name, $_version, $_set_id,$_is_all);
  say __FILE__. " Line " . __LINE__ . ", install hash=".Dumper($_install_hash_ref) if ( defined $_debug && $_debug > 1 );
  foreach my $_key (sort keys %{$_install_hash_ref}) {
		my $_install = $_install_hash_ref->{$_key};
		my $_software=$_install->{software};

		say __FILE__. " Line " . __LINE__ . ",writeEB,s=".Dumper($_software) if ( defined $_debug && $_debug > 0 );
		my $_eb                   = Kpan::EB->new;
		$_eb->{name}						 = $_software->{name};
		$_eb->{version}					 = $_software->{version};
		$_eb->{home_url}				 = $_software->{home_url};
		$_eb->{version_url}			 = $_software->{version_url};
		$_eb->{file_name}				 = $_software->{version_file_name};
		$_eb->{prereq_hash_ref}	 = $_software->{prereq_hash_ref};
		$_eb->{brief}						 = $_software->{brief};
		$_eb->{category}				 = $_software->{category};	  
		$_eb->{debug}            = $_debug;
		$_eb->writeEBFile;
	}
}
sub setClientInfo {
    my ( $_setting_file_name,$_no_db,$_no_wiki,$_config_client,$_debug) = @_;
    my $_c = Kpan::Client->new($_debug);
    say __FILE__." Line ".__LINE__."\tkpan new client set" if ( defined $_debug && $_debug > 0 );
    #$_debug=0;
		$_c->{setting_file_name}		=	$_setting_file_name			if (defined $_setting_file_name); 
		$_c->{no_db}								=	$_no_db									if (defined $_no_db); 
		$_c->{no_wiki}							=	$_no_wiki								if (defined $_no_wiki); 
		$_c->{config_client}				=	$_config_client					if (defined $_config_client); 
		$_c->{debug}								=	$_debug									if (defined $_debug);
    $_c->setClientInfo;
    return $_c;
}

sub installSoftware {
    my (
        $_debug,                   $_client,                $_app_name,
        $_version,                 $_kp_name,               $_kp_version,
        $_home_url,                $_archive_url,           $_version_url, 
        $_local_copy_file_handler, $_prereq_command,        $_brief, 
        $_category,                $_channel,               $_collect_url,
        $_dry_run,                 $_no_db,                 $_start_level,
        $_search_scope,            $_version_file_name_ext, $_tarfile_name,
        $_module_names,                        
    ) = @_;
    say __FILE__. " Line " . __LINE__ . ", installSoftware, $_start_level=$_start_level" if ( defined $_debug && $_debug > 3 );
    $_debug = 0 if ( !defined $_debug );
    my $_s = Kpan::Software->new;
    $_s->{version}         			= $_version;
    $_s->{name}            				= $_app_name;
    $_s->{kp_name}         				= defined $_kp_name    ? $_kp_name    : $_app_name  ;
    $_s->{kp_version}      				= defined $_kp_version ? $_kp_version : $_kp_version;
    $_s->{home_url}        				= $_home_url;
    $_s->{archive_url}     				= $_archive_url;
    $_s->{version_url}     				= $_version_url;
    $_s->{local_copy_path} 				= $_local_copy_file_handler;
    $_s->{prereq_command}  				= $_prereq_command;
    $_s->{brief}           				= $_brief;
    $_s->{category}        				= uc(substr($_category,0,1)).substr($_category,1);
    $_s->{no_db}           				= $_no_db;
    $_s->{debug}           				= $_debug;
    $_s->{start_level}     				= $_start_level;
    #$_s->{is_kpan_dev}     				= $_client->{is_kpan_dev};
    $_s->{version_file_name_ext}  = $_version_file_name_ext;

    say __FILE__. " Line " . __LINE__ . ", software=" . Dumper($_s)  if ( defined $_debug && $_debug >4 );
    my $_install = Kpan::Install->new();
    $_install->{kp_name}          = defined $_kp_name    ? $_kp_name    : $_app_name  ;
    $_install->{kp_version}       = defined $_kp_version ? $_kp_version : $_version;
    $_install->{software}         = $_s;
    $_install->{client}           = $_client;
    $_install->{channel}          = $_channel;
    $_install->{debug}            = $_debug;
    $_install->{dry_run}          = $_dry_run;
    $_install->{start_level}      = $_start_level;
    $_install->{search_scope}     = $_search_scope;
    $_install->{collect_url}      = $_collect_url;
    $_install->{tarfile_name}     = $_tarfile_name;
    $_install->{module_names}     = $_module_names;

    #say __FILE__. " Line " . __LINE__ . ", install=" . Dumper($_install) if ( defined $_debug && $_debug > 6 );
    $_install->install;
    say __FILE__. " Line " . __LINE__ . ", cd $_install->{client}->{src_dir_prefix}/".$_install->{kp_name}."/$_install->{kp_version}";
}

sub removeInstall   {
	my ($_debug,$_client,$_kp_name,$_kp_version, $_channel)=@_;
	$_kp_name    = $_kp_name;
	$_kp_version = $_kp_version;
	if (!defined $_client){
    $_client = Kpan::Client->new();
    $_client->setClientInfo;
	}
  my @_c = ("$_client->{src_dir_prefix}/$_kp_name/$_kp_version","$_client->{install_dir_prefix}/$_kp_name/$_kp_version","$_client->{lmod_dir_prefix}/$_kp_name/$_kp_version.lua");
  if( defined $_kp_name && defined $_kp_version && defined $_client ){ 
		if( defined $_channel && $_channel eq "conda"){
		  say "ml $_client->{default_conda}\n"
		     ."conda-env list\n"
  	     ."conda-env remove \"$_client->{install_dir_prefix}/$_kp_name/$_kp_version\"\n"
  	     ."sed -i 's|$_client->{install_dir_prefix}/$_kp_name/$_kp_version||' ~/.conda/environments.txt\n" 
  	     ."conda-env list\n" 
  	     ."rm -rf $_client->{install_dir_prefix}/$_kp_name/$_kp_version\n" ;
		}else{
      foreach my $_cmd (@_c){
  	   say "rm -rf $_cmd" if (-e $_cmd);	
  	  # TODO delete wiki page
      }
	  }
  }
}
sub parseKLog {
    my ( $_install_log, $_is_upload,$_skip_parse_test, $_debug, $_k ) = @_;
    return $install_parsed if (defined $install_parsed);
    $_k                           = Kpan::KLog->new if (!defined $_k);
    $_k->{debug}                  = $_debug if (!defined $_k->{debug} || defined $_debug);
    $_k->{is_upload}              = ( defined $_is_upload ) ? $_is_upload : 0;
    $_k->{checked_level}          = ( defined $_k->{is_upload} && $_k->{is_upload} >0 ) ? 3 : 0;
    $_skip_parse_test             = 1 if (!defined $_skip_parse_test);
 		my $_install                  = $_k->parseKLogFile($_install_log, $_skip_parse_test); # direct test not parse existing test result
		$_install->{debug}            = $_debug;
		say __FILE__. " Line " . __LINE__ . ", \n\n\n\nnew install=".Dumper($_install) if ( defined $_debug && $_debug > 2 );	
		$install_parsed               = $_install;
    return $_install;
}
sub testInstall {
	my ( $_install_log, $_help_parameter, $_test_command, $_test_file_core_name, $_debug ) = @_;
	
  my $_k                          = Kpan::KLog->new;
  $_k->{is_test_install}          = 1;
  my $_is_upload                  = 0;
  my $_skip_parse_test            = 1;
	my $_install                    = parseKLog($_install_log, $_is_upload,$_skip_parse_test, $_debug, $_k);	
  
	my $_t                          = $_install->{install_test};
	$_test_command                  = $_t->{command}   if (!defined $_test_command);
	$_help_parameter                = $_t->{help_para} if (!defined $_help_parameter);
	say __FILE__. " Line " . __LINE__ . ",test_command=$_test_command, help_parameter=$_help_parameter, install_test=".Dumper($_t) if ( defined $_debug && $_debug > 0 );	
	$_t->{command}                  = $_test_command if (defined $_test_command && $_test_command =~ m/\S/ );
	$_t->{help_para}                = $_help_parameter if (defined $_help_parameter);
	$_t->{file_core_name}           = $_test_file_core_name if (defined $_test_file_core_name && $_test_file_core_name =~ m/\S/);
  $_t->testCmd($_install->{client}->{lmod_app_path});   
  say "head -n 30 $_t->{file_name}";
}
sub writeModuleFile{
  my ($_install_log, $_module_exp_pair, $_debug )        = @_;
  my $_k                              = Kpan::KLog->new;
  $_k->{is_write_module}              = 1;
  my $_is_upload                      = 0;
  my $_skip_parse_test                = 1;
	my $_install                        = parseKLog($_install_log, $_is_upload,$_skip_parse_test, $_debug, $_k);	
	my $_no_log                         = 1;
	$_install->{general_module_content} = undef;
  $_install->{module_content}         = undef;
  $_install->{module_exp_pair}        = $_module_exp_pair;
	$_install->makeLmodModuleLua($_no_log);
	say __FILE__. " Line " . __LINE__ . ", \ncat $_install->{client}->{lmod_dir_prefix}/".$_install->{kp_name}."/$_install->{kp_version}.lua";
}

sub updateWiki {
	my ( $_install_log, $_test, $_kp_name, $_kp_version, $_app_cmd, $_app_help, $_debug ) = @_;
	my $_k                     = Kpan::KLog->new;
  my $_is_upload             = 0;
  my $_skip_parse_test       = 0;
	my $_install               = (defined $_install_log && -e $_install_log) ? parseKLog($_install_log, $_is_upload,$_skip_parse_test, $_debug, $_k) : Kpan::Install->new;	

	my $_client                = $_install->{client}             ;
  my $_software              = $_install->{software}           ;
  my $_app_name              = $_software->{name}              ; 
  my $_app_version           = $_software->{version}           ; 
  my $_app_home_url          = $_software->{home_url}          ; 
  my $_app_archive_url       = $_software->{archive_url}       ; 
  my $_brief                 = $_install->{brief}              ;
  my $_category              = $_install->{category}           ; 
  my $_install_test          = $_install->{install_test}       ; 
  
  if (!defined $_client || !defined $_client->{wiki_api_url}) {
 	  $_client                 = Kpan::Client->new ;
	  $_client->loadClientSetting;
  }
  my $_wiki                  = new Kpan::Wiki();
	$_wiki->{debug}            = $_debug;
	$_wiki->{api_url}          = $_client->{wiki_api_url};
	$_wiki->{cluster_system}   = $_client->{cluster_system};
	$_wiki->{login_name}       = $_client->{wiki_login_name};
	$_wiki->{login_password}   = $_client->{wiki_login_password};
	$_wiki->{template_name}    = $_client->{wiki_template_name};
	$_wiki->{wiki_list_page}   = $_client->{wiki_list_page};
  my $_app_path              = "";
	if (defined $_install_log){
	  $_app_path                 = "$_client->{install_dir_prefix}/$_install->{kp_name}/$_install->{kp_version}" ; 
	  my $_app_version_url       = $_software->{version_url}       ; 
	  my $_app_version_file_name = $_software->{version_file_name} ; 
	  # TODO set wiki->{brief} directly 
	  say __FILE__." Line ".__LINE__.", $_app_name,$_app_version,$_app_home_url,$_app_archive_url,$_test,$_category,  preset wiki =".Dumper($_wiki) if(defined $_wiki->{debug} && $_wiki->{debug}>0);
  }
	if(!defined $_install_log && defined $_kp_name ){
   	#say __FILE__." Line ".__LINE__.", $_kp_name, $_kp_version, $_app_cmd, $_app_help, $_client->{lmod_dir_eb_prefix}, $_client->{install_dir_eb_prefix}".Dumper($_client).Dumper($_wiki);
	  my $_dc           = App::DirectCmd->new();
	  $_dc->{client}    = $_client;
	  $_dc->{debug}     = $_debug;
	 	my $_eb_app_file_list   = $_dc->getAppFilePathList($_client->{install_dir_eb_prefix});
		my $_gb_app_file_list   = $_dc->getAppFilePathList($_client->{src_dir_prefix});
    my $_app_hash_ref       = $_dc->getRecipeFilePath($_kp_name, $_eb_app_file_list, $_gb_app_file_list);
    say __FILE__." Line ".__LINE__.". $_kp_name not installed or has no receipt" if (scalar keys %{$_app_hash_ref} == 0 ); # miss eb folder
    for my $_v (sort { versioncmp($a, $b) } keys %{$_app_hash_ref}){
			my $_recipe_file  = (defined $$_app_hash_ref{$_v}{recipe_file_path}) ? $$_app_hash_ref{$_v}{recipe_file_path}: undef ;
			#do { say __FILE__." Line ".__LINE__.", no such recipe $_app_name, $_cmd, $_help, ebf=$_recipe_file, v=$_v"; next;} if (! -f $_recipe_file);
			do { next;} if ((!defined $_recipe_file || !-f $_recipe_file) || (defined $_kp_version && $_kp_version ne $_v)  );
			my $_vpath    = $$_app_hash_ref{$_v}{version_path};
      if (defined $_recipe_file && -f $_recipe_file){
		    ($_app_name, $_app_version, $_app_home_url, $_category, $_brief, $_app_path, $_install_test ) = $_dc->processEBReceipt($_recipe_file, $_client->{lmod_dir_eb_prefix}, $_client->{install_dir_eb_prefix}, $_app_cmd, $_app_help);
		    $_app_archive_url = $_app_home_url;		    
			}
		}	
	}
	say __FILE__." Line ".__LINE__."$_app_name,$_app_version,$_app_home_url,$_app_archive_url,$_category, $_brief,$_app_path" if(defined $_wiki->{debug} && $_wiki->{debug}>0);
	$_wiki->updateAppWiki($_app_name,$_app_version,$_app_home_url,$_app_archive_url,$_category, $_brief,$_app_path, $_install_test);
}
sub parseHistory {
   my ( $_d, $_debug ) = @_;
   $_d = "/home/yhuang/scripts/lib_log" if ( !defined $_d || $_d == 1 );

    my $_pq = Kpan::Prereq->new();
    $_pq->{debug} = 1;
    my @_files = <$_d/*>;
    foreach my $_f ( sort @_files ) {
        $_pq->parseHistory($_f);
    }
}
sub parseModule {
   my ( $_d, $_debug ) = @_;
   $_d = "/usr/local/modulefiles_eb" if ( !defined $_d );
    my $_p = Kpan::ParseCmd->new;
    $_p->{debug} = $_debug;
    $_p->parseModule($_d, "lua",0,3,$_d,); 
    $_p->uploadModule; 
}
sub parseLog {
   my ( $_d, $_debug ) = @_;
   $_d = "/usr/local/src/gcc" if ( !defined $_d );
    my $_p = Kpan::ParseCmd->new;
    $_p->{debug} = $_debug;
    $_p->parseLog($_d, "UGA",0,3,$_d,); 
    $_p->uploadLog; 
}
sub parseEB{
	my ($_local_copy_file_handler, $_debug,)=@_;
	my $_eb=Kpan::EB->new();
	$_eb->{debug}=$_debug;
	$_eb->parseEB($_local_copy_file_handler);
}
sub parseCmdFile{
	my ($_local_copy_file_handler,$_debug)=@_;
	my $_pc           = Kpan::ParseCmd->new;
	$_pc->{debug}     = $_debug;
	$_pc->parseCmd($_local_copy_file_handler);
}
sub parseDirectCmd {
	my ($_direct_cmd_file,$_debug)=@_;

	my $_client       = Kpan::Client->new();
  $_client->{debug} = $_debug;
  $_client->setClientInfo;

	my $_dc           = App::DirectCmd->new();
	$_dc->{client}    = $_client;
	$_dc->{debug}     = $_debug;
	$_dc->parseCmd($_direct_cmd_file, $_debug);							
}

sub	search {
  my ($_client,				  $_kpan_client_name,		$_app_name,	
			$_app_version,	  $_app_install_name,		$_app_install_version,
			$_cluster_system, $_group_name, 	      $_is_kpan_dev, 
			$_os,							$_org_name,           $_search_scope,
			$_self_only,		  $_debug
		)=@_;
	$_is_kpan_dev    = undef;
	$_cluster_system = undef;
	say __FILE__." Line ".__LINE__.", $_cluster_system, $_kpan_client_name,	$_group_name,\n$_org_name,	$_is_kpan_dev, $_debug".Dumper($_client) if(defined $_debug && $_debug>0);
	my $_search      = Kpan::Search->new;
	$_search->getClients($_client, $_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev, $_debug);
}


sub getEBCmd{
   my ($_note, $_detail, $_debug ) = @_;
   my $_p = Kpan::ParseCmd->new;
   $_p->{debug} = $_debug;
   $_p->getEBCmd($_note, $_detail);
}
sub getEBReceipt{
   my ($_file, $_note, $_debug ) = @_;
   my $_p = Kpan::ParseCmd->new;
   $_p->{debug} = $_debug;
   $_p->getEBReceipt($_file, $_note);
 }
sub collectUrl {

    #http://www.mybiosoftware.com/biology-software-list
    my ($_debug) = @_;
    if ( !defined $_debug ) { $_debug = 0; }

    #say __FILE__." Line ".__LINE__.", debug=$_debug ";
    my $_db_data = Kpan::DBData->new();
    $_db_data->{debug} = $_debug;
    my $_u = Kpan::Url->new();
    $_u->{debug} = $_debug;
    my $_size     = 1;
    my @_patterns = qw(
      bitbucket.org
      github.com
      sourceforge.net
    );
    @_patterns = qw(
      conda
      boost
      cuda
      cython
      mpich2
      openmpi
      zlib
    );

    foreach my $_pattern (@_patterns) {
        my %_name_hash =
          %{ $_db_data->getNameHashFromCollection( $_pattern, $_size ) };

    #say __FILE__." Line ".__LINE__.", pattern=$_pattern ".Dumper(\%_name_hash);
        my $_i = 0;
      PAT: foreach my $_kp_name ( sort keys %_name_hash ) {
            $_i++;

            #last PAT if($_i>4);
            if ( $_kp_name eq "ocaml" ) { next; }
            my $_version = $_name_hash{$_kp_name};
            if ( $_db_data->isInUrlProduction( $_kp_name, $_version ) ) {
                say __FILE__. " Line " . __LINE__
                  . " $_kp_name,$_version in Url Prod";
            }
            else {
                my $_archive_url_hash_ref =
                  $_db_data->getUrlHashFromCollection($_kp_name);

                #say __FILE__." Line ".__LINE__.Dumper($_archive_url_hash_ref);
                $_u->spiderVersionUrl($_archive_url_hash_ref);

                #say __FILE__." Line ".__LINE__.Dumper($_archive_url_hash_ref);
                $_db_data->setVersionUrlHashToProduction(
                    $_archive_url_hash_ref);
                $_version = $$_archive_url_hash_ref{$_kp_name}{version};
            }
            my $_dir = "/usr/local/src/download";
            setFileNameSize( $_debug, $_dir, $_kp_name );
        }
    }
}


__END__

=pod

=head1 NAME

kpan - A tool to deploy applications on HPC.

=head1 SYNOPSIS

  my $object = test->new(
      foo  => 'bar',
      flag => 1,
  );
  
  $object->dummy;

=head1 DESCRIPTION

​kpan provides robust pipeline to build applications on Linux with cluster friendly structure.
It features:
1. regular user operation, no need for super privilege
2. accept multiple users, multiple cluster settings
3. standardize the document, generate install receipt and provide central repository for receipts
4. provide existing receipts, suggest receipt for update, guess draft receipt for new applications
5. reproduce the deployment by existing receipts, which can install all receipted applications automatically.
6. generate installed application documents on wiki web
7. easy use with low learning curve, the install script is minimum bash script needed to install the applications

=head1 COPYRIGHT & LICENSE

Copyright (c) 2016-2017 Omic Scientific Inc.

NOTICE:  All information contained herein is, and remains
the property of Omic Scientific Company and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Omic Scientific Company
and its suppliers and may be covered by U.S. and Foreign Patents,
patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Omic Scientific Inc.

=head1 SUPPORT

No support is available

=head1 AUTHOR

Written by Yecheng Huang, <yhuang@omicsci.com>, December 2016

=cut
