# kpan

build and deploy scientific applications on Linux with receipts 

# sample command
kpan -i -n DeepVariant -v 0.7.0 \
--module_names "Python/2.7.14-foss-2016b" \
-vu https://github.com/google/deepvariant/archive/v0.7.0.tar.gz \
--brief "DeepVariant is an analysis pipeline that uses a deep neural network to call genetic variants from next-generation DNA sequencing data."

kpan -i -n QIIME -v 1.9.1 -iv 1.9.1_conda \
-hu http://qiime.org \
--channel conda \
--brief "QIIME is an open-source bioinformatics pipeline for performing microbiome analysis from raw DNA sequencing data."

kpan -i -n SMRTLINK -v 6.0.0.47841 -iv 6.0.0.47841 \
-hu https://www.pacb.com/support/software-downloads \
--local_copy_file_handle /home/yhuang/smrtlink_6.0.0.47841.zip \
--brief "PacBio open-source SMRT Analysis software suite is designed for use with Single Molecule, Real-Time (SMRT) Sequencing data. You can analyze, visualize, and manage your data through an intuitive GUI or command-line interface. You can also integrate SMRT Analysis in your existing data workflow through the extensive set of APIs provided."

kpan -i -n DeepVariant -v 0.7.0 \
--module_names "Python/2.7.14-foss-2016b" \
-vu https://github.com/google/deepvariant/archive/v0.7.0.tar.gz \
--brief "DeepVariant is an analysis pipeline that uses a deep neural network to call genetic variants from next-generation DNA sequencing data."

kpan --update_wiki -in BLAST+ -iv 2.7.1-foss-2016b-Python-2.7.14 -vc blastn -vh "-help"  -df ~/projects/kpan/direct_cmd2.txt

kpan --write_module_file --module_exp_pair PATH=/usr/local/src/gb/juicer/1.5.6/CPU --k_log /usr/local/src/gb/juicer/1.5.6/UGA
