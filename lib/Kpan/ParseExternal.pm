#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2016

# parse UGA files
 
package Kpan::ParseExternal;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use DBData;
use KUtil;
our $VERSION = '0.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
		debug => shift,
		note => shift
	};
	if(!defined $_self->{debug}){
		$_self->{debug}=0;
	}
	return $_self;
}

sub parseZULog{
	my $_self = shift;
  my $_f=shift;
	open my $_in, "<", $_f or die  __FILE__." Line ".__LINE__.",  couldn't open file $_f for reading: $!\n";
	my $_src="Z";
	if($_f =~ /S_U/ ){
	  $_src="S";
	}elsif($_f =~ /Z_U/ ){
	  $_src="Z";
	}
	my $_fname="";
	my $_name="";
	my $_version="";
	my %_url_hash=();
	my $_cmd="";
	my $_url="";
	my $_module_path="";
	my $_module_text="";
	my $_line_count=0;
	while (<$_in>){
	  my $_line=Kpan::KUtil::Trim($_);
	  if( length($_line)==0){
	    next;
	  }
	  $_line_count++;
	  if($_=~/^---KPAN S-START---$/){
	    $_src="S";
	  }elsif($_=~/^---KPAN START---$/){
	    $_src="Z";
	  }elsif($_=~/^---KPAN END---$/){
      my $_u = Kpan::Url->new($_name);
      $_u->{name}=$_name;
      $_u->{version}=$_version;
			$_u->{home_url}  = $_url;
			$_u->{archive_url}  = $_url;
			$_u->{version_url}  = $_url;
			$_u->{src} =$_src;
			$_u->{file_name} =$_fname;
			$_u->{other_url_hash_ref}  =\%_url_hash;
			$_u->{commands}       =$_cmd;
			$_u->{module_path} = $_module_path;
			$_u->{module_text} = $_module_text;
			#say "kpan Line ".__LINE__.Dumper($_s);
			$_u->uploadToCollection();
			$_url="";
			$_src="";
			$_fname="";
			%_url_hash=();
			$_cmd="";
			$_module_path="";
			$_module_text="";
			$_line_count=0;
	  }elsif($_src ne "" && $_fname eq "" ){ #first line for file name
	    $_fname=$_line;
	    my @_e=split ('/',$_fname);
      $_name=$_e[4];
	    if($#_e == 6){ #/usr/local/src/samtools/1.1/UGA
	      $_version=$_e[5];
	    }elsif($#_e > 6){ #/usr/local/src/samtools/gcc477/1.1/UGA
	      $_version=$_e[6];
	    }elsif($#_e == 4){ #/usr/local/src/samtools/UGA
	      $_version="";
	    }else{
	      $_version="";
	    }
	    say  __FILE__." Line ".__LINE__.", name =$_name, version=$_version,fname=$_fname, $_src";
	  }elsif($_src ne "" && $_fname ne "" ){ #after frist line
	    $_cmd.="$_line\n";
	    # perl -e '$a="http://prdownloads.sourceforge.net/argtable/argtable-1.4.tar.gz"; $a=~ m/\b((http|ftp|wget|curl\s|download|\/.+\.com|\/.+\.org|\/.+\.info|\/.+\.io).*)\b?/i; print "$1,$2\n";'
	    if($_line =~ m/\b((http|ftp|git).+)\b?/i ){
	      $_line=$1;
	      $_line=Kpan::KUtil::Trim($_line);
	      if( $_url eq ""){
	        $_url=$_line;
	      }else{
	        $_url_hash{$_line}="";
	      }
	    }
	  }
	}
}

sub fixUrlCollection{
  my ($_self,$_debug)=@_;
  if(defined $_debug){
  	$_self->{debug}=$_debug;
  }
  my $_db_data=Kpan::DBData->new();
  $_db_data->{debug}=$_self->{debug};
  my($_kp_name,$_checked_level)=(undef,-2);
  my $_archive_url_hash_ref = $_db_data->getUrlHashFromCollection($_kp_name,$_checked_level);
  if(defined $_archive_url_hash_ref){
  	foreach my $_k_name (sort keys %{$_archive_url_hash_ref}){
			my %_name_hash = %{$$_archive_url_hash_ref{$_k_name}{v} };
			foreach my $_v (sort keys %_name_hash){
				my %_v_hash=%{$_name_hash{$_v}};
				say __FILE__." Line ".__LINE__.", k=$_k_name,v=$_v,".Dumper(\%_v_hash) if(defined $_self->{debug} && $_self->{debug} >2);
				if(!defined $_v_hash{commands}){ 
					next;
				};
				my $_commands= $_v_hash{commands};
				my @_all_links = ( $_commands =~ /\s((?:http|ftp).+?)\s/g );
				if(scalar @_all_links){
				  $$_archive_url_hash_ref{$_k_name}{v}{$_v}{home_url_orig_2}=$_all_links[0];
				  if(scalar @_all_links){
				  	my $_other_urls=join(',', @_all_links[1..$#_all_links]);
				    $$_archive_url_hash_ref{$_k_name}{v}{$_v}{other_urls}=$_other_urls;
				  }
				  say __FILE__." Line ".__LINE__.", k=$_k_name,v=$_v,".Dumper(\@_all_links) if(defined $_self->{debug} && $_self->{debug} >0);
				}
			}
		}
  }
  $_db_data->updateHU2ToCollection($_archive_url_hash_ref);
}
1;

