#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Wiki;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use MediaWiki::API;
use Sort::Naturally qw(ncmp); #alphanumeric
use Sort::Versions qw(versioncmp);
#use FindBin qw($Bin);
#use lib "$Bin/../lib/Kpan";
#use lib ".";
use KUtil;
our $VERSION = '1.5.9';
# bypass wiki certificate fail

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
		api_url               => shift,
		cluster_system        => shift,
		debug                 => shift,
		kpan_client_name      =>shift,
		list_header           => shift,
		list_footer           => shift,
		login_name            => shift,
		login_password        => shift,
		mediawiki             => MediaWiki::API->new(),
		note                  => shift,
		page_html             => shift,
		page_text             => shift,
		row_hash_ref          => shift,
		template_name         => shift,
		test                  => shift,
		wiki_list_page        => shift,
		column_head_order     => ["Name","Version","Category","Cluster","Top"],
		column_head_hash_ref  => shift,
	};
	return $_self;
}

sub updateAppWiki {
	#perl -e '$d=`pwd`; $d=~ s|\n(\s)*(\n)*||; $d=~ m|/usr/local/src/(.+)/(.+)|; print "$d, $1,$2\n"; '
	my ($_self, $_app_name,$_app_version,$_app_home_url,$_app_archive_url,$_app_category, $_brief,$_app_path, $_install_test, $_no_db)=@_;
	my $_cluster_system                      = $_self->{cluster_system};   
	my $_template_pagename                   = $_self->{template_name} ;
	$_self->{mediawiki}                      = MediaWiki::API->new();
 	$_self->{mediawiki}->{config}->{api_url} = $_self->{api_url};

  say __FILE__." Line ".__LINE__.", n=$_app_name, v=$_app_version,category=$_app_category,cluster_system=$_cluster_system".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>1);
	# log in to the wiki
  $_self->{mediawiki}->login( { lgname => $_self->{login_name}, lgpassword => $_self->{login_password} } ) 
			|| die __FILE__." Line ".__LINE__.", fatal error. ".$_self->{mediawiki}->{error}->{code} . ': ' . " : ".$_self->{mediawiki}->{error}->{stacktrace}."\n" . $_self->{mediawiki}->{error}->{details}.Dumper($_self);
  #say __FILE__." Line ".__LINE__.", loggeg in wiki as,  $_self->{login_name}, $_self->{login_password}";
	# get a list of articles in category
	my $_app_title_name=$_self->capitalFirst($_app_name);
  $_app_title_name="$_app_name:$_app_version:Software:$_cluster_system" if($_cluster_system =~ m/Kpan/i);
 	$_app_title_name=$_app_name."-$_cluster_system" if($_cluster_system =~ m/Sapelo2|Sapelo|Teaching/i );
  $_self->insertSoftwareList($_app_name,$_app_version,$_cluster_system, $_app_category,$_app_title_name);
  $_self->writeList;

	my $_template_page = $_self->{mediawiki}->get_page( { title => $_template_pagename } );
	
	my $_template_content = $_template_page->{'*'};
	#say __FILE__." Line ".__LINE__.",Create wiki page $_app_title_name at $_self->{wiki_list_page}\n";	

  my $_app_page_content=$_self->createAppPageContent($_template_content,$_app_name,$_app_version,$_cluster_system,$_app_home_url,$_app_archive_url, $_app_category, $_brief,$_app_path,  $_install_test);
  
 	#say __FILE__." Line ".__LINE__.",$_app_page_content\n";	

  #my $_ref = $_mw->get_page( { title => "Software" } );
  # create category page if first time
  my @_categories = ($_cluster_system, $_app_category,$_app_name);
  map {$_ ="Category:".$_;}@_categories;
  my $_ref;
  foreach my $_c (@_categories){
    $_ref = $_self->{mediawiki}->get_page( { title => $_c } );
		if ( ( defined $_ref->{missing} &&  $_ref->{missing} =~ m/\s*/ ) && $_cluster_system !~ m/^Sapelo|^Teaching/i ) {
			$_self->{mediawiki}->edit( { 
						action => 'edit',
						title => $_c,
						text => ""
			} ) || die $_self->{mediawiki}->{error}->{code} . ': ' . $_self->{mediawiki}->{error}->{details};
		}
  }
  
  $_ref = $_self->{mediawiki}->get_page( { title => $_app_title_name } );
  say __FILE__." Line ".__LINE__.",Software,".Dumper($_ref) if(defined $_self->{debug} && $_self->{debug}>1);
  if ( defined $_ref->{pageid} && $_cluster_system !~ m/^Sapelo/i ) {
		#$_mw->edit( { 
		#			action => 'delete',
		#			title => $_app_title_name,
		#} ) || die $_mw->{error}->{code} . ': ' . $_mw->{error}->{details};
  }
  #say __FILE__." Line ".__LINE__.",Software,".Dumper($_ref) if(defined $_self->{debug} && $_self->{debug}>0);
  if ( !defined $_ref->{missing} || $_ref->{missing} =~ m/^\s*$/ ) {
    say __FILE__." Line ".__LINE__.",create page $_app_title_name $_app_version" if(defined $_self->{debug} && $_self->{debug}>0);
		$_self->{mediawiki}->edit( { 
					action => 'edit',
					title => $_app_title_name,
					text => $_app_page_content
		} ) || die $_self->{mediawiki}->{error}->{code} . ': ' . $_self->{mediawiki}->{error}->{details};
  }
  my ($_wiki_root)  = $_self->{mediawiki}->{config}->{api_url} =~ m|(.+?[^/])/[^/]|;
  # perl -e '$w="http://gacrc.uga.edu/wiki/api"; ($r)=$w=~ m|(.?+(^/))/(^/)|; print "$r, $1, $2, $3, $4\n;"'
  say __FILE__." Line ".__LINE__.", app page is created/updated at $_wiki_root/wiki/".$_app_title_name;
}
sub insertSoftwareList {
  my ($_self,$_insert_app_name,$_insert_app_version, $_insert_cluster_system, $_insert_app_category,$_insert_title_name) =@_;
	my $_page                    = $_self->{mediawiki}->get_page( { title => $_self->{wiki_list_page} } );
	
	my $_content                 = $_page->{'*'};
	my @_lists                   = ();
  my ($_head_flag,$_foot_flag) = (1,0);
	my @_cont_arr                = split (/\n/,$_content);
	$_self->{list_header}        = "";
	$_self->{list_footer}        = "";
	foreach my $_line (@_cont_arr){
		if ($_line =~ m/LIST END/){
			$_foot_flag = 1;
			$_self->{list_footer} .= $_line."\n";
		}elsif(($_line =~ m/LIST BEGIN/)){
			$_head_flag = 0;
			$_self->{list_header}.= $_line."\n";
		}elsif(($_head_flag==1 && $_line =~ m/^!\s+scope="col".+\|\s*(\S+)?\s*$/)){
			$_self->{list_header}.= $_line."\n";
			my $_column  = $1;
			my $_pos     = -1;
			for my $_i (0.. scalar @{$_self->{column_head_order}} ){
				 do {$_pos=$_i;last;} if ( $_column =~ m/^\s*$_self->{column_head_order}[$_i]\s*$/i);
			}
			my $_lc_col                               = lc($_column);
			$_self->{column_head_hash_ref}{$_lc_col}  = $_pos if (defined $_column && $_pos>-1);
			say __FILE__." Line ".__LINE__.",$_self->{wiki_list_page}, $_line, $_column".Dumper($_self->{column_head_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>1);
		}elsif($_head_flag==0 && $_foot_flag==0){
			next if ($_line =~ m/^\s*\|-\s*$/);
			push(@_lists,$_line."\n");
			say __FILE__." Line ".__LINE__.",$_line" if(defined $_self->{debug} && $_self->{debug}>1);
		}elsif($_foot_flag==1){
			$_self->{list_footer} .= $_line."\n";
		}else{
			$_self->{list_header} .=$_line."\n";
		}
  }
  say __FILE__." Line ".__LINE__.",new\nlists[0,1]=$_lists[0]\n$_lists[1]\n$_insert_app_name,$_insert_app_version, $_insert_cluster_system, $_insert_app_category,$_insert_title_name\n" if(defined $_self->{debug} && $_self->{debug}>1);	
	$_self->updateSoftwareList(\@_lists, $_insert_app_name,$_insert_app_version, $_insert_cluster_system, $_insert_app_category,$_insert_title_name);
}

sub updateSoftwareList {
  my ($_self,$_arr,$_insert_app_name,$_insert_app_version, $_insert_cluster_system, $_insert_app_category,$_insert_title_name) =@_;
  my $_r_list              = "";
  my @_rows                = @{$_arr};
  my $_i                   = 0;
  my %_pos_header_hash     = reverse %{$_self->{column_head_hash_ref}};
  my $_row_key;
  my $_top;
	  
	foreach my $_r (@_rows){
		$_i++;
		$_r=Kpan::KUtil::Trim($_r);
		my @_columns            = map {$_ = Kpan::KUtil::Trim($_)} split (/\|\|/,$_r);
		$_columns[0]            =~ s/^\| //; # remove head |
		$_columns[0]            = Kpan::KUtil::Trim($_columns[0]); # remove spaces
		$_columns[0]            =~ s/^<span.+>//; # remove span
		map {$_ =~ s/\[//g;$_ =~ s/\]//g; $_ =~ s/^\s*|\s*$//g;} @_columns; # remove []
		say __FILE__." Line ".__LINE__.", ".Dumper($_self->{column_head_hash_ref}).Dumper(\@_columns) if(defined $_self->{debug} && $_self->{debug}>1);
		my $_app_name           ;
		my $_display_app_name   ;			
		if($_columns[$_self->{column_head_hash_ref}{name}] =~ m/(.+)\|(.+)/ ){
			$_app_name               = $1;
			$_app_name               = Kpan::KUtil::Trim($_app_name);
			$_display_app_name       = $2;			
			$_display_app_name       = Kpan::KUtil::Trim($_display_app_name);			
	  }else{
			$_app_name               = $_columns[$_self->{column_head_hash_ref}{name}];
			$_display_app_name       = $_columns[$_self->{column_head_hash_ref}{name}];			
		}
		if($_columns[$_self->{column_head_hash_ref}{cluster}] =~ m/(.+)\|(.+)/ ){
			$_columns[$_self->{column_head_hash_ref}{cluster}]  = $2;
			$_columns[$_self->{column_head_hash_ref}{cluster}]  = Kpan::KUtil::Trim($_columns[$_self->{column_head_hash_ref}{cluster}]);
	  }
		if($_columns[$_self->{column_head_hash_ref}{category}] =~ m/(.+)\|(.+)/ ){
			$_columns[$_self->{column_head_hash_ref}{category}]  = $2;
			$_columns[$_self->{column_head_hash_ref}{category}]  = Kpan::KUtil::Trim($_columns[$_self->{column_head_hash_ref}{category}]);
	  }
	  $_top                                               = $_columns[$_self->{column_head_hash_ref}{top}] if (!defined $_top && defined $_columns[$_self->{column_head_hash_ref}{top}]);
	  my $_cluster                                        = $_columns[$_self->{column_head_hash_ref}{cluster}];
		say __FILE__." Line ".__LINE__.", $_self->{column_head_hash_ref}{cluster} = $_cluster".Dumper(\@_columns) if(defined $_self->{debug} && $_self->{debug}>1);
		my $_ucf_display_app_name                             = $_self->capitalFirst($_display_app_name);
    $_row_key                                           = uc("$_ucf_display_app_name:$_cluster");
    $_self->{row_hash_ref}{$_row_key}{app_name}         = $_app_name; 
    $_self->{row_hash_ref}{$_row_key}{display_app_name} = $_display_app_name; 
	  for ( my $_j=0; $_j < (scalar @_columns); $_j++){
			say __FILE__." Line ".__LINE__.", app_name=$_app_name, display_app_name=$_display_app_name, columns[$_j]=$_columns[$_j]" if(defined $_self->{debug} && $_self->{debug}>1);
	    $_self->{row_hash_ref}{$_row_key}{$_pos_header_hash{$_j}} = $_columns[$_j] if(defined $_columns[$_j] && defined $_pos_header_hash{$_j}); 
		}
  }
  say __FILE__." Line ".__LINE__.", ".Dumper($_self->{row_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>1);
  if(defined $_insert_app_name && defined $_insert_cluster_system && $_insert_app_name =~ m/\S/ && $_insert_cluster_system =~ m/\S/){
  	my $_ucf_insert_app_name                             = $_self->capitalFirst($_insert_app_name);
    $_row_key                                            = uc("$_ucf_insert_app_name:$_insert_cluster_system");
    $_self->{row_hash_ref}{$_row_key}{app_name}          = $_insert_title_name;
    $_self->{row_hash_ref}{$_row_key}{display_app_name}  = $_self->capitalFirst($_insert_app_name);
    $_self->{row_hash_ref}{$_row_key}{version}           = $_self->getVersions($_self->{row_hash_ref}{$_row_key}{version}, $_insert_app_version);
    $_self->{row_hash_ref}{$_row_key}{category}          = $_insert_app_category;
    $_self->{row_hash_ref}{$_row_key}{cluster}           = $_insert_cluster_system;
    $_self->{row_hash_ref}{$_row_key}{top}               = $_top;
  }
  say __FILE__." Line ".__LINE__.", after ".Dumper($_self->{row_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>1);
  
}
sub getVersions {
  my ($_self, $_existing_versions, $_insert_app_version)=@_;
  $_existing_versions = "" if (!defined $_existing_versions);
  my @_vs =  split (/[\s,]+/, $_existing_versions);
  say __FILE__." Line ".__LINE__.", e=$_existing_versions".Dumper(\@_vs) if(defined $_self->{debug} && $_self->{debug}>1);
  push (@_vs, $_insert_app_version);
  say __FILE__." Line ".__LINE__.", 2e=$_existing_versions".Dumper(\@_vs) if(defined $_self->{debug} && $_self->{debug}>1);
  my %_version_hash_ref = map { $_ => undef }  @_vs;
  #$_version_hash_ref{$_insert_app_version}  = undef;
  say __FILE__." Line ".__LINE__.", e=$_existing_versions".Dumper(@_vs).Dumper(\%_version_hash_ref) if(defined $_self->{debug} && $_self->{debug}>1);
  my $_new_versions = "";
  for my $_v (sort { versioncmp($a, $b) } keys %_version_hash_ref){
		$_new_versions .= ($_new_versions =~ /\S/) ? ", $_v":$_v if ($_v =~ m/\S/);
	}
  return $_new_versions;				
}
sub writeList {
  my ($_self)=@_;
	my $_page                    = $_self->{mediawiki}->get_page( { title => $_self->{wiki_list_page} } );	
	my $_content                 = $_page->{'*'};
	my $_prev_first_uc_letter;
  my %_pos_header_hash         = reverse %{$_self->{column_head_hash_ref}};

	$_self->insertSoftwareList if (!defined $_self->{list_header} || !defined $_self->{list_footer});
	
	my $_updateded_list = "";
  for my $_row_key (sort keys %{$_self->{row_hash_ref}}){
		#say __FILE__." Line ".__LINE__.",new\n$_self->{list_header}\n\n$_updateded_list\n\n$_self->{list_footer}\n".Dumper(\%_pos_header_hash).Dumper($_self->{row_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>0);		
		#say __FILE__." Line ".__LINE__.",$_self->{row_hash_ref}{$_row_key}{display_app_name}\n".Dumper($_self->{row_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>0);		
		say __FILE__." Line ".__LINE__.",$_self->{row_hash_ref}{$_row_key}{app_name} || $_self->{row_hash_ref}{$_row_key}{display_app_name}\n" if(defined $_self->{debug} && $_self->{debug}>1);		
  	my $_first_letter        = substr($_self->{row_hash_ref}{$_row_key}{display_app_name},0,1);		
  	$_first_letter           = ( $_first_letter =~ m/\d/) ? 0: uc ($_first_letter);
  	my $_span                = "";
  	if (!defined $_prev_first_uc_letter || $_prev_first_uc_letter ne $_first_letter) {
  		$_span                 = "<span id =\"$_first_letter\"></span>"; #<span id="A"></span>
  		$_prev_first_uc_letter = $_first_letter;
  		
		}
		$_updateded_list        .= "| $_span";
		my $_name_cloumn         = "[[$_self->{row_hash_ref}{$_row_key}{app_name}|".$_self->{row_hash_ref}{$_row_key}{display_app_name}."]]";
		my $_version_cloumn      = "$_self->{row_hash_ref}{$_row_key}{version}";
		my $_category_cloumn     = "[[:Category:".$_self->{row_hash_ref}{$_row_key}{category}."|".$_self->{row_hash_ref}{$_row_key}{category}."]]";
		my $_cluster_cloumn     = "[[:Category:".$_self->{row_hash_ref}{$_row_key}{cluster}."|".$_self->{row_hash_ref}{$_row_key}{cluster}."]]";
		for my $_i (sort { $a <=> $b } keys %_pos_header_hash){
			my $_column_name         = $_pos_header_hash{$_i};
			$_updateded_list        .= $_name_cloumn." || "      if ($_column_name eq "name");
			$_updateded_list        .= $_version_cloumn." || "   if ($_column_name eq "version");
			$_updateded_list        .= $_category_cloumn." || "  if ($_column_name eq "category");
			$_updateded_list        .= $_cluster_cloumn." || "   if ($_column_name eq "cluster");
			$_updateded_list        .= "[[".$_self->{row_hash_ref}{$_row_key}{top}."]] ||"   if ($_column_name eq "top");
			say __FILE__." Line ".__LINE__.", n=$_name_cloumn,top=$_self->{row_hash_ref}{$_row_key}{top}, col=$_column_name".Dumper($_self->{row_hash_ref}{$_row_key}) if(defined $_self->{debug} && $_self->{debug}>0 && (!defined $_self->{row_hash_ref}{$_row_key}{top} || !defined $_column_name));
		}
		$_updateded_list =~ s/\|\|\s*$/\n\|-\n/;
	}
	#| <span id="A">[[ABySS-Sapelo|ABySS]] ||2.0.2|| [[:Category:Bioinformatics|Bioinformatics]] || [[:Category:Sapelo|Sapelo]] || [[File:Up20.png|20px|link=#top]]
  #|-
  #| [[ABySS-Sapelo2|ABySS]] ||1.9.0, 2.2.2, 2.1.0 || [[:Category:Bioinformatics|Bioinformatics]] || [[:Category:Sapelo2|Sapelo2]] || [[File:Up20.png|20px|link=#top]]
  #|-

	my $_new_content     = $_self->{list_header}.$_updateded_list.$_self->{list_footer};
  my $_file="s2.log";
  open my $_out, ">>", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for writing: $!\n";
  say $_out "$_self->{list_header}$_updateded_list$_self->{list_footer}\n";
	say __FILE__." Line ".__LINE__.",new\n$_self->{list_footer}\n" if(defined $_self->{debug} && $_self->{debug}>1);		
	$_self->{mediawiki}->edit( { 
				action => 'edit',
				title => $_self->{wiki_list_page},
				text => $_new_content
	 } ) || die $_self->{mediawiki}->{error}->{code} . ': ' . $_self->{mediawiki}->{error}->{details};    
}

sub createAppPageContent {
	my ($_self,$_template_content,$_name,$_v,$_cluster_system,$_app_home_url,$_app_archive_url,$_app_category, $_brief,$_app_path, $_install_test)=@_;
	say __FILE__." Line ".__LINE__.",template_content=$_template_content\n$_name,$_v,$_cluster_system,$_app_home_url,$_app_archive_url,$_app_path,\n".Dumper($_install_test) if(defined $_self->{debug} && $_self->{debug}>1);
	#perl -e '$a="/usr/local/apps/<!-- NAME BEGIN -->trinity<!-- NAME END -->/latest always points to last version at /usr/local/apps/<!-- NAME BEGIN -->trinity<!-- NAME END -->/<!-- VERSION BEGIN -->2.0.6<!-- VERSION END -->";$b="YH";$a=~ s/<!-- NAME BEGIN -->[^<]+<!-- NAME END -->/$b/g; print "$a\n";'
	my $_test_module_load = $_install_test->{module_load};
	my $_test_cmd         = defined $_install_test->{command} ? $_install_test->{command} : "";
	my $_test_cmd_help    = (defined $_install_test->{command} && defined $_install_test->{help_para} )? "$_install_test->{help_para}" : "";
	my $_test_cmd_result  = defined $_install_test->{test_result}? $_install_test->{test_result} : "[$_app_home_url $_name]" ;
	my $_lc_name          = lc($_name);
	$_test_module_load    =~ s/^\s*module load\s+//;
	$_test_module_load    =~ s/^\s*ml\s+//;
	my $_cmd_name         = $_test_cmd;
	if (!defined $_test_cmd){
	  $_cmd_name          = $_lc_name;
	}
	say __FILE__." Line ".__LINE__.",test_cmd=$_test_cmd\n\nwiki parse test\n".Dumper($_install_test) if(defined $_self->{debug} && $_self->{debug}>1);
	my $_new_content=$_template_content;
	$_brief           = "" if (!defined $_brief);
	$_app_archive_url = "" if (!defined $_app_archive_url);

	$_new_content =~ s/<!-- NAME BEGIN -->[^<]+<!-- NAME END -->/$_name/g;
	$_new_content =~ s/<!-- APP_PATH BEGIN -->[^<]+<!-- APP_PATH END -->/$_app_path/g;
	$_new_content =~ s/<!-- CATEGORY3 BEGIN -->[^<]+<!-- CATEGORY3 END -->/$_app_category/g;
	$_new_content =~ s/<!-- LC_NAME BEGIN -->[^<]+<!-- LC_NAME END -->/$_lc_name/g;
	$_new_content =~ s/<!-- CMD_NAME BEGIN -->[^<]+<!-- CMD_NAME END -->/$_cmd_name/g;
	$_new_content =~ s/<!-- VERSION BEGIN -->[^<]+<!-- VERSION END -->/$_v/g;
	$_new_content =~ s/<!-- HOME_URL BEGIN -->[^<]+<!-- HOME_URL END -->/[$_app_home_url $_name]/g;
	$_new_content =~ s/<!-- ARCHIVE_URL BEGIN -->[^<]+<!-- ARCHIVE_URL END -->/[$_app_archive_url $_name]/g ;
	$_new_content =~ s/<!-- APP_CATEGORY BEGIN -->[^<]+<!-- APP_CATEGORY END -->/$_app_category/g;
	$_new_content =~ s/<!-- CLUSTER_SYSTEM BEGIN -->[^<]+<!-- CLUSTER_SYSTEM END -->/$_cluster_system/g;
	$_new_content =~ s/<!-- BRIEF BEGIN -->[^<]*<!-- BRIEF END -->/$_brief/g ;
	$_new_content =~ s/<!-- TEST_MODULE BEGIN -->[^<]*<!-- TEST_MODULE END -->/$_test_module_load/g ;
	$_new_content =~ s/<!-- TEST_COMMAND BEGIN -->[^<]*<!-- TEST_COMMAND END -->/$_test_cmd/g ;
	$_new_content =~ s/<!-- TEST_COMMAND_HELP BEGIN -->[^<]*<!-- TEST_COMMAND_HELP END -->/$_test_cmd_help\n$_test_cmd_result/g;
	return $_new_content;
}
sub capitalFirst{
  my ($_self,$_s)=@_;
  if(length($_s)>0){
    $_s= uc(substr($_s,0,1)).substr($_s,1);
  }
  return $_s;
}
1;

