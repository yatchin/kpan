#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Url;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use feature qw(say);
use WWW::Mechanize;
use WWW::Mechanize::TreeBuilder;
use utf8;
use Text::Unidecode;
use Net::FTP;
use Sort::Versions qw(versioncmp);
use File::stat;
use FindBin;    
use lib "$FindBin::Bin/../lib/Kpan/";
#use KUtil qw(TAR);
use DBData;
use KUtil;
our $VERSION = '1.5.7';

use constant { TAR => [qw/ tar.gz tar.bz2 tar.xz tar.Z tar.lz bz2 cpio gz lz sht tar tgz xz zip Z sh bin pl py jar /] }; 

my %CACHE; #static cache for app

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
		name => shift,
		version  => shift, 
		module_name => shift,
		module_version  => shift, 
		home_url        => shift,
		archive_url        => shift,
		version_url        => shift,
		version_order      => shift,
		version_order      => shift,
		src        => shift, # Z,S,WZ,WS
		data_url_hash        => shift,
		other_url_hash_ref        => shift,
		version_file_name       => shift,
		version_file_name_ext  => shift, 
		commands       => shift,
		module_path => shift,
		module_text => shift,
		debug => shift,
		start_level => shift, # if >1, no need to spider
		note => shift,
	};
	if(!defined $_self->{debug}){
		$_self->{debug}=0;
	}
	return $_self;           
}

sub spiderVersionUrl { # set all apps in archive hash from in collection table
	my ($_self, $_archive_url_hash_ref) = @_;
	my $i=0; 
	my %_visited_url_hash;
	say __FILE__." Line ".__LINE__.",spider= ".Dumper($_archive_url_hash_ref).Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4 );
	foreach my $_kp_name  (sort keys %{$_archive_url_hash_ref} ){
    $$_archive_url_hash_ref{$_kp_name}{archive_url}=Kpan::KUtil::Trim($$_archive_url_hash_ref{$_kp_name}{archive_url});	
		my $_archive_url = $$_archive_url_hash_ref{$_kp_name}{archive_url};	
    $_archive_url=Kpan::KUtil::Trim($_archive_url);	
		my $_depth=0;
		say __FILE__." Line ".__LINE__.",$_kp_name, $_archive_url" if(defined $_self->{debug} && $_self->{debug}>1);
		if($_archive_url =~ m/bioconda\.github\.io/i ){
			say __FILE__." Line ".__LINE__.", $_archive_url bioconda" if(defined $_self->{debug} && $_self->{debug}>4);		
			#%_archive_url_hash = %{$_u->getGitIOBiocondaArchiveUrlHash( $_name,$_archive_url)};
			$_self->getGitIOBiocondaArchiveUrlHash($_archive_url_hash_ref,\%_visited_url_hash,$_kp_name,$_archive_url);
		}elsif($_archive_url =~ m/github\.io/i ){ #git two level /, https://github.com/CompEvol/beast2
			say __FILE__." Line ".__LINE__.", github.io, non-bioconda $_kp_name, $_archive_url " if(defined $_self->{debug} && $_self->{debug}>4);		
			$_archive_url =_convertGithubIOUrl($_archive_url);
			#say __FILE__." Line ".__LINE__.", $_kp_name, $_archive_url ";
			$_self->getGithubArchiveUrlHash($_archive_url_hash_ref,\%_visited_url_hash, $_kp_name,$_archive_url);
		}elsif($_archive_url =~ m/github\.com/i ){ #git two level /, https://github.com/CompEvol/beast2
			say __FILE__." Line ".__LINE__.", $_archive_url github" if(defined $_self->{debug} && $_self->{debug}>2);		
			$_self->getGithubArchiveUrlHash($_archive_url_hash_ref, \%_visited_url_hash,$_kp_name,$_archive_url);
		}elsif($_archive_url =~ m/sourceforge\.net/i ){ #git two level /, https://github.com/CompEvol/beast2
			say __FILE__." Line ".__LINE__.", $_kp_name,$_archive_url sourceforge" if(defined $_self->{debug} && $_self->{debug}>4);
			$_self->getSourceforgeUrlHash($_archive_url_hash_ref, \%_visited_url_hash,$_kp_name,$_archive_url);
		}elsif($_archive_url =~ m/bitbucket\.org/i ){ #git two level /, https://github.com/CompEvol/beast2
			say __FILE__." Line ".__LINE__.", $_kp_name,$_archive_url bitbucket" if(defined $_self->{debug} && $_self->{debug}>4);
			$_self->getBitbucketUrlHash($_archive_url_hash_ref, \%_visited_url_hash,$_kp_name,$_archive_url);
		}else{
			say __FILE__." Line ".__LINE__.", $_archive_url other archive urls" if(defined $_self->{debug} && $_self->{debug}>0);		
			#$$_archive_url_hash_ref{$_kp_name}{home_url}=$_home_url;
			#$$_archive_url_hash_ref{$_kp_name}{archive_url}=$_archive_url;
			$_self->drillTarFile($_kp_name,$_archive_url,$_depth,$_archive_url_hash_ref,\%_visited_url_hash);
		}
	  #say __FILE__." Line ".__LINE__.", $_archive_url archive_url_hash:".(scalar keys %_archive_url_hash);
		#say __FILE__." Line ".__LINE__.Dumper($_archive_url_hash_ref); 
	}
	say __FILE__." Line ".__LINE__.", arch hash".Dumper($_archive_url_hash_ref).Dumper(\%_visited_url_hash) if(defined $_self->{debug} && $_self->{debug} >=3);
	
	return $_archive_url_hash_ref;
}
sub getGithubArchiveUrlHash {
  my ($_self,$_archive_url_hash_ref,$_visited_url_hash_ref,$_kp_name, $_archive_url0)=@_;
  my $_archive_url=$_archive_url0;
  my %_archive_url_hash=%{$_archive_url_hash_ref};
  my %_visited_url_hash=%{$_visited_url_hash_ref};
  if(! exists $_visited_url_hash{$_archive_url0}){
		#$_visited_url_hash{$_archive_url0}=1;
	}else{
		return; #visited
	}
  my $_module_name="";
  my $_module_version="";
  my $_name=$_kp_name;
  say __FILE__." Line ".__LINE__.", $_kp_name getGithubArchiveUrlHash: $_archive_url0 " if(defined $_self->{debug} && $_self->{debug} >1);
  my $_home_url=$_self->{home_url};
  my $_version_url=$_self->{version_url};

  $_archive_url_hash{$_kp_name}{home_url_orig}=$_archive_url0;
  ($_archive_url_hash{$_kp_name}{name},$_archive_url_hash{$_kp_name}{project_name},
    $_archive_url_hash{$_kp_name}{home_url},$_archive_url_hash{$_kp_name}{archive_url}) = _guessHomeArchiveUrl($_home_url,$_archive_url,$_version_url); 
  $_self->{home_url}=$_home_url;
  $_self->{archive_url}=$_archive_url;
  $_archive_url_hash{$_kp_name}{note}="github";     
  say __FILE__." Line ".__LINE__.",$_kp_name,  $_archive_url_hash{$_kp_name}{home_url}, $_archive_url_hash{$_kp_name}{archive_url}" if(defined $_self->{debug} && $_self->{debug} >0);
  my ($_depth,$_max_depth,$_html_tag,$_tar_tag_only)=(0,1,undef,1);
  if ( $_self->hasValidLinks($_archive_url_hash{$_kp_name}{archive_url},$_html_tag,$_tar_tag_only) ){
    $_self->drillTarFile($_kp_name,$_archive_url_hash{$_kp_name}{archive_url},$_depth,\%_archive_url_hash,\%_visited_url_hash,$_max_depth,$_html_tag);
  }else{
    my $_is_git_no_release=1;
    my $_git_url=$_archive_url_hash{$_kp_name}{home_url}.".git";
    $_self->setVersionUrl($_kp_name,\%_archive_url_hash,$_git_url,$_is_git_no_release);
    $_visited_url_hash{$_archive_url_hash{$_kp_name}{archive_url}}=1;
  } 
  say __FILE__." Line ".__LINE__.",$_name,$_kp_name,  $_archive_url_hash{$_kp_name}{archive_url}\n".Dumper(\%_archive_url_hash) if(defined $_self->{debug} && $_self->{debug} >2);
}

sub getGitIOBiocondaArchiveUrlHash{
  my ($_self,$_archive_url_hash_ref,$_visited_url_hash_ref,$_name, $_archive_url0)=@_;
  my %_archive_url_hash=%{$_archive_url_hash_ref};
  my %_visited_url_hash =%{$_visited_url_hash_ref};
  if(! exists $_visited_url_hash{$_archive_url0}){
		$_visited_url_hash{$_archive_url0}=1;
	}else{
		return;
	}
  my $_archive_url=$_archive_url0;
  say __FILE__." Line ".__LINE__.",$_name, $_archive_url0";
  $_archive_url = "https://pypi.python.org/pypi/$_name"; # https://bioconda.github.io/recipes/stamp/README.html?highlight=stamp
  $_archive_url_hash{$_name}{home_url}=$_archive_url; 
  $_archive_url_hash{$_name}{home_url_orig}=$_archive_url0;
  $_archive_url_hash{$_name}{archive_url}=$_archive_url; 
  $_archive_url_hash{$_name}{note}="bioconda,pip,python"; 
  my $_depth=0;
  #say __FILE__." Line ".__LINE__.",$_name, archive_url: $_archive_url,d=$_depth";
  #say Dumper(\%_archive_url_hash);
  $_self->drillTarFile($_name,$_archive_url_hash{$_name}{archive_url},$_depth,\%_archive_url_hash,\%_visited_url_hash);
}
sub _convertGithubIOUrl {
  my $_url = shift;
  #perl -e '$a="https://daler.github.io/pybedtools/main.html"; $a=~ /https:\/\/(.+)\.github\.io\/([a-z]+)\/.+/i ; print "$1\n" '
  #perl -e '$a="http://sanger-pathogens.github.io/iva/"; $a=~ /https:\/\/(.+)\.github\.io\/([a-z]+)\/.*/i ; print "$1\n" '
  #say __FILE__." Line ".__LINE__.",original: $_url";
  if($_url =~ /:\/\/(.+)\.github\.io\/([a-z]+)\/.*/i ){
    $_url = "https://github.com/${1}/${2}";
  }
  return $_url;
}

sub getSourceforgeUrlHash {
  my ($_self,$_archive_url_hash_ref,$_visited_url_hash_ref,$_kp_name, $_archive_url0)=@_;
		#http://prdownloads.sourceforge.net/argtable/argtable-1.4.tar.gz
		#https://sourceforge.net/projects/argtable/files/argtable/argtable-2.12/argtable2-12.tar.gz/download
		#https://downloads.sourceforge.net/project/argtable/argtable/argtable-2.12/argtable2-12.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fargtable%2Ffiles%2Fargtable%2Fargtable-2.12%2F&ts=1486359067&use_mirror=svwh
    #https://downloads.sourceforge.net/project/argtable/argtable/argtable-2.12/argtable2-12.tar.gz
    #https://sourceforge.net/projects/argtable/files/argtable/
  #perl -e '@b=("sourceforge.net/projects/ae/files/argtable","sourceforge.net/projects/ae/files/argtable/a2/a.tar.gz/download","sourceforge.net/projects/ae/files/argtable=999"); foreach $a (@b) {if($a=~ m|sourceforge.net/projects/(.+?)/files/([^/=?]+)|i ) {print "match $a, 1:$1,\t2:$2,\t3:$3\n";}else{print "not match $a,\t1:$1,\t2:$2,\t3:$3\n"}}'
  my %_archive_url_hash=%{$_archive_url_hash_ref};
  my %_visited_url_hash =%{$_visited_url_hash_ref};
  if(exists $_visited_url_hash{$_archive_url0}){
		#return; #454 mira
	}
  my $_archive_url=$_archive_url0;
  say __FILE__." Line ".__LINE__.",$_kp_name, $_archive_url0" if(defined $_self->{debug} && $_self->{debug}>1);
  #my $_project_name=$_kp_name; # 454 -> mira
  #my $_name=$_kp_name;
  #if( $_archive_url0 =~ m/sourceforge\.net\/((?:(?!projects\/).)+)\//i ){
## TODO
  my $_home_url=$_self->{home_url};
  #my $_archive_url=$_self->{archive_url};
  my $_version_url=$_self->{version_url};
  ($_archive_url_hash{$_kp_name}{name},$_archive_url_hash{$_kp_name}{project_name},
    $_archive_url_hash{$_kp_name}{home_url},$_archive_url_hash{$_kp_name}{archive_url}) =_guessHomeArchiveUrl($_home_url,$_archive_url,$_version_url);
  $_self->{home_url}=$_home_url;
  $_self->{archive_url}=$_archive_url;
  $_archive_url_hash{$_kp_name}{note}="sourceforge"; 
  $_archive_url_hash{$_kp_name}{brief}=$_self->getBrief($_archive_url_hash{$_kp_name}{home_url});
  #say __FILE__." Line ".__LINE__.",$_kp_name,$_project_name,\t\t archive_url: $_archive_url" if(defined $_self->{debug} && $_self->{debug}>0);
  say "archive_url_hash=".Dumper(\%_archive_url_hash) if(defined $_self->{debug} && $_self->{debug}>0);
  say "visited_url_hash=".Dumper(\%_visited_url_hash) if(defined $_self->{debug} && $_self->{debug}>0);
	my ($_depth,$_max_depth,$_html_tag,$_tar_tag_only,$_drill)=(0,1,undef,undef,1);
	my $_u=Kpan::Url->new();
	$_u->{debug}=$_self->{debug};
	if ( $_u->hasValidLinks($_archive_url_hash{$_kp_name}{archive_url},$_html_tag,$_tar_tag_only,$_drill) ){
		$_self->drillTarFile($_kp_name,$_archive_url_hash{$_kp_name}{archive_url},$_depth,\%_archive_url_hash,\%_visited_url_hash,$_max_depth,$_html_tag);
	}else{
		my $_is_git_no_release=1;
	  # git clone https://git.code.sf.net/p/meraculous/code meraculous-code
	  my $_git_url="https://git.code.sf.net/p/$_archive_url_hash{$_kp_name}{project_name}/code";
		$_self->setVersionUrl($_kp_name,\%_archive_url_hash,$_git_url,$_is_git_no_release);
		$_visited_url_hash{$_archive_url_hash{$_kp_name}{archive_url}}=1;
	}
}
sub getBitbucketUrlHash {
  my ($_self,$_archive_url_hash_ref,$_visited_url_hash_ref,$_kp_name, $_archive_url0)=@_;
		#https://petrnovak@bitbucket.org/petrnovak/repex_tarean
		#http://bitbucket.org/eigen/eigen/get/3.0.5.tar.gz
		#hg clone https://bitbucket.org/nsegata/lefse
  my %_archive_url_hash=%{$_archive_url_hash_ref};
  my %_visited_url_hash =%{$_visited_url_hash_ref};
  if(exists $_visited_url_hash{$_archive_url0}){
		#return; #454 mira
	}
  my $_archive_url=$_archive_url0;
  say __FILE__." Line ".__LINE__.",$_kp_name, $_archive_url0" if(defined $_self->{debug} && $_self->{debug}>1);
  my $_home_url=$_self->{home_url};
  #my $_archive_url=$_self->{archive_url};
  my $_version_url=$_self->{version_url};
 ($_archive_url_hash{$_kp_name}{name},$_archive_url_hash{$_kp_name}{project_name},
    $_archive_url_hash{$_kp_name}{home_url},$_archive_url_hash{$_kp_name}{archive_url}) = _guessHomeArchiveUrl($_home_url,$_archive_url,$_version_url);
  $_self->{home_url}=$_home_url;
  $_self->{archive_url}=$_archive_url;
  $_archive_url_hash{$_kp_name}{home_url_orig}=$_archive_url0;
  $_archive_url_hash{$_kp_name}{archive_url}=$_archive_url; 
  $_archive_url_hash{$_kp_name}{note}="bitbucket"; 
  #$_archive_url_hash{$_kp_name}{brief}=$_self->getBrief($_archive_url_hash{$_kp_name}{home_url});
  say __FILE__." Line ".__LINE__.",$_kp_name,$_archive_url_hash{$_kp_name}{project_name},\t\t archive_url: $_archive_url" if(defined $_self->{debug} && $_self->{debug}>0);
  say "archive_url_hash=".Dumper(\%_archive_url_hash) if(defined $_self->{debug} && $_self->{debug}>0);
  say "visited_url_hash=".Dumper(\%_visited_url_hash) if(defined $_self->{debug} && $_self->{debug}>0);
	my ($_depth,$_max_depth,$_html_tag,$_tar_tag_only)=(0,1,undef,undef);
	my $_u=Kpan::Url->new();
	$_u->{debug}=10;
	if ( $_u->hasValidLinks($_archive_url_hash{$_kp_name}{archive_url},$_html_tag,$_tar_tag_only) ){
	 say __FILE__." Line ".__LINE__.",$_kp_name,hash-au=$_archive_url_hash{$_kp_name}{archive_url},\t\t archive_url: $_archive_url" if(defined $_self->{debug} && $_self->{debug}>0);
	$_self->drillTarFile($_kp_name,$_archive_url_hash{$_kp_name}{archive_url},$_depth,\%_archive_url_hash,\%_visited_url_hash,$_max_depth,$_html_tag);
	}else{
		if($_archive_url =~ m|(.+/downloads)/?tab=tags|){
	    #$_archive_url=$1;		
	    $_archive_url_hash{$_kp_name}{archive_url}=$1;
	 say __FILE__." Line ".__LINE__.",$_kp_name,hash-au=$_archive_url_hash{$_kp_name}{archive_url},\t\t archive_url: $_archive_url" if(defined $_self->{debug} && $_self->{debug}>0);
			if ( $_u->hasValidLinks($_archive_url_hash{$_kp_name}{archive_url},$_html_tag,$_tar_tag_only) ){
			  $_self->drillTarFile($_kp_name,$_archive_url_hash{$_kp_name}{archive_url},$_depth,\%_archive_url_hash,\%_visited_url_hash,$_max_depth,$_html_tag);
		  }else{		
			  my $_is_git_no_release=1; #TODO confirm git url
			  my $_git_url= "https://bitbucket.org/$_archive_url_hash{$_kp_name}{project_name}/$_archive_url_hash{$_kp_name}{name}";
			  $_self->setVersionUrl($_kp_name,\%_archive_url_hash,$_git_url,$_is_git_no_release);
			  $_visited_url_hash{$_archive_url_hash{$_kp_name}{archive_url}}=1;
		  }
		}
	}
}
sub drillTarFile{ # start from archive links or home from github
  my ($_self,$_kp_name,$_url,$_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag)=@_;
  #$_url=lc($_url);
  say __FILE__." Line ".__LINE__.", $_kp_name, " if(!defined $_url );
  if (!defined $_depth){
  	$_depth=0;
  }else{
  	$_depth++;
  	$_max_depth=5 if(!defined $_max_depth); 	
 		return if($_depth>$_max_depth);  	
  }  
   if( $_url =~ m|/$| ){
    $_url =~ s|/$||;
  }
 if( $_url =~ m/^(\s)*ftp:/i ){
 	  $_depth--;
    $_self->drillFTPTarFile($_kp_name,$_url,$_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag);
    return;
  }
  if( $_url =~ m/^(\s)*http(s)*:\/\/ftp\./i ){
 	  $_depth--;
    $_url =~ s/^https:/ftp:/i;
    $_self->drillFTPTarFile($_kp_name,$_url,$_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag);
    return;
  }
  say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, kp_name=$_kp_name, visited_url_hash:\n".Dumper($_visited_url_hash_ref) if(defined $_self->{debug} && $_self->{debug} >3);
  if(! exists $$_visited_url_hash_ref{$_url}){
		$$_visited_url_hash_ref{$_url}=1;
	}else{
		return;
  }
  #say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, name=$_kp_name, visited_url_hash:\n".Dumper(\%_visited_url_hash);
  my $_url_root=$_self->getRootUrl($_url);
  my $_is_tar=0;
  my @_tar_postfix_arr = @{+TAR}; # use constant arr
  my $_mech = WWW::Mechanize->new( autocheck => 0 );
  WWW::Mechanize::TreeBuilder->meta->apply($_mech);
  #$_mech->proxy(['https', 'http', 'ftp'], 'https://47.32.84.212:10443'); 
  if(defined $_url ){
    say __FILE__." Line ".__LINE__.",$_url" if(defined $_self->{debug} && $_self->{debug} >5);
    $_mech->get($_url);
    if ( $_mech->success() ) { # page available  	
      say __FILE__." Line ".__LINE__.",$_url, mech->success" if(defined $_self->{debug} && $_self->{debug} >5);
      #my @_links =$_mech->find_all_links( text => 'Download' );  
      my @_links =( );  
      $_html_tag = $_self->{version_file_name_ext} if(!defined $_html_tag && defined $_self->{version_file_name_ext});
      if (defined $_html_tag){
      	#@_links =$_mech->find_all_links( text => $_html_tag);
      }
      if((scalar @_links) ==0){
      	@_links =$_mech->find_all_links( );
      }
      if((scalar @_links) ==0 && $_url=~ m|(.+sourceforge.net/projects/.+/files)/.+| && $_depth==0 ){
      	$$_url_hash_ref{$_kp_name}{archive_url}=$1;
      	$_url=$$_url_hash_ref{$_kp_name}{archive_url};
      	$_mech->get($_url);
        if (defined $_html_tag){
					#@_links =$_mech->find_all_links( text => $_html_tag); # TODO cannot find 1.4 at https://github.com/samtools/samtools/releases
				}
				if((scalar @_links) ==0){
					@_links =$_mech->find_all_links( );
				}
      }
      if($_url=~ m|bitbucket.org| && $_depth==0){
				my $_title=$_mech->title();
				if( $_title =~ m|\b(.+?)\b/\b(.+?)\b/|i){
					my $_project_name=$1;
					my $_name=$2;
					$$_url_hash_ref{$_kp_name}{name}=$_name; 
					$$_url_hash_ref{$_kp_name}{project_name}=$_project_name; 
				}
			}
			#perl -e '$a="https://cran.cnr.berkeley.edu/src/base/R-0/"; $b="https://cran.cnr.berkeley.edu/src/base/";if(! $a=~ m/\Q$b\E/ ) {print "not match\n"} elsif ( $a!~ m/\Q$b\E/ ) {print "not match2\n"} else {print "match\n"}'
			say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url,html_tag=$_html_tag,".Dumper(\@_links)."\n " if(defined $_self->{debug} && $_self->{debug} >8);
      my @_links_all =$_mech->find_all_links( );
			say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url,html_tag=all,".Dumper(\@_links_all)."\n " if(defined $_self->{debug} && $_self->{debug} >8);
			my @_valid_links=();
			foreach my $_link_ref (@_links){
				my $_link=$_link_ref->[0];
				$_link=Kpan::KUtil::Trim($_link);
        say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, lin=$_link\n "  if(defined $_self->{debug} && $_self->{debug} >4);
				if(defined $_link &&  $_link =~ m/^\#/ ){ # link to same page
					next;
				}elsif(defined $_link && $_link =~ m/^\//  && $_link !~ m/:/ ){ # internal link start with /
					$_link=$_url_root.$_link;
				}elsif(defined $_link &&  $_link =~ m/^\?|^\/\?/ ){ # internal link with ? https://cran.cnr.berkeley.edu/src/base/ header
					next;
				}elsif(defined $_link &&  $_link =~ m/^[^\.\:\/]+/ && $_link !~ m/:/ ){ # internal link without /
					$_link=$_url."/".$_link;
				}
				my $_counts = () = $_link =~ /\//g;  
				my $_archive_url=$$_url_hash_ref{$_kp_name}{archive_url};
				$_counts=$_counts-4-$_depth;
				if( $_link =~ m|https://cran.cnr.berkeley.edu/src/base/R-[0-9]+/| # going through
				  || ($_archive_url =~ m|bitbucket\.org|i && $_link =~ m,https://bitbucket.org/.+?/.+?/get/,i)  #diff than archive url
				  || ($_archive_url =~ m|sourceforge\.net|i && $_link =~ m|sourceforge.net/projects/.+/files[^\?]|i )    
				  || ($_archive_url =~ m|sourceforge\.net|i && $_link =~ m|sourceforge.net/projects/.+/files[^\?]|i )    
			    || $_archive_url =~ m|github.com| && $_link=~ m|://github.com/.+/archive| 
			    ) {
			  }elsif( $_link !~ m|\Q$_archive_url\E|  #/${_url_root}.+${_name}/i  # external link # block
				|| exists $$_visited_url_hash_ref{$_link}  
				|| ($_archive_url =~ m|https://cran.cnr.berkeley.edu|i && ($_link =~ m/latest|\.sht|base\/\?|/i ))  
				|| ($_archive_url =~ m|sourceforge\.net|i && $_link !~ m|sourceforge.net/projects/.+/files|i )  
				|| ($_archive_url =~ m|sourceforge\.net|i && $_link =~ m,sourceforge\.net/.+(osx.tar.gz|/files/\?|latest\.|Older%20releases),i  )  #skip latest, https://sourceforge.net/projects/amos/files/?source=navbar 
				#https://sourceforge.net/projects/amos/files/amos/3.1.0/
				#https://sourceforge.net/settings/mirror_choices?projectname=amos&filename=amos/3.0.0/amos-3.0.0.tar.gz
				#https://downloads.sourceforge.net/project/amos/amos/3.0.0/amos-3.0.0.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Famos%2Ffiles%2Famos%2F3.0.0%2F&ts=1489865240&use_mirror=master
				#||$_link =~ m/:action/i #/pypi?:action=doap&name=STAMP&version=2.0.3
				|| ($_archive_url =~ m|bitbucket\.org|i && $_link =~ m,bitbucket\.org,i  )  #skip other links 
				){ 
          say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, lin=$_link not match? \t".$$_url_hash_ref{$_kp_name}{archive_url}
          ."=".($_link !~ m|\Q$_archive_url\E| )."\n"  if(defined $_self->{debug} && $_self->{debug} >5); #.Dumper($_visited_url_hash_ref)
					next;
				} 
        say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, lin=$_link\n "  if(defined $_self->{debug} && $_self->{debug} >5);
				#if( $_link =~ /\/[^\/]+\.[^\/]+$/ 
			  #perl -e '$a="https://bitbucket.org/regulatorygenomicsupf/suppa/get/v1.2.tar.gz"; if($a=~ m|bitbucket\.org|i && $a =~ m,https://bitbucket.org/.+?/.+?/get/,i ) {print "match\n";}else {print "not match 2\n"} if ( $a=~ m|/[^/]+\.[^/]+$|  ) {print "match2\n"} else {print "not match 2\n"}'
				if( $_link =~ m|/[^/]+\.[^/]+$|  # https://cran.cnr.berkeley.edu/src/base/R-3/R-3.2.2.tar.gz
				  || $_link =~ m|https://sourceforge.net/projects/.+/files/.+/download|i
				  ){ # /*.tar
          say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, lin=$_link\n "  if(defined $_self->{debug} && $_self->{debug} >5);
					my $_re_link=$_link;
					my ($_project_name,$_file_name,$_group_name)=($_kp_name,$_kp_name,$_kp_name);;
					if($_link =~ m|https://sourceforge.net/projects/(.+?)/files/((.+)/(.+)/(.+)?)/download|i ){ # https://sourceforge.net/projects/amos/files/amos/3.1.0/amos-3.1.0.tar.gz/download
					#https://sourceforge.net/projects/mira-assembler/files/MIRA/stable/mira_4.0.2_linux-gnu_x86_64_static.tar.bz2/download
			    #perl -e '$a="https://sourceforge.net/projects/mira-assembler/files/MIRA/stable/mira_4.tar.bz2/download"; if($a=~ m|https://sourceforge.net/projects/(.+)/files/((.+)/(.+)/(.+))/download|i) {print "match 1:$1,2:$2,3:$3,4:$4,5:$5\n"}'
					#match 1:mira-assembler,2:MIRA/stable/mira_4.tar.bz2,3:MIRA,4:stable,5:mira_4.tar.bz2
            $_project_name=$1;
					  $_group_name=$2;
					  $_file_name=$3;
					  if($_file_name !~ /$_project_name/i ){
					  	say __FILE__." Line ".__LINE__.",skip not matched depth=$_depth,url=$_url, re-lin= $_re_link\n "  if(defined $_self->{debug} && $_self->{debug} >7);
					  	#next;
					  }
					}elsif($_link =~ m|https://sourceforge.net/projects/(.+)/files/(.+)/download|i){ #https://sourceforge.net/projects/mira-assembler/files/mira_3rdparty_06-07-2012.tar.bz2/download
            $_project_name=$1;
					  $_group_name=$2;
					}
					$_re_link="https://sourceforge.net/settings/mirror_choices?projectname=".$_project_name."&filename=".$_group_name if($_link =~ m|https://sourceforge.net/projects/.+/files/.+/download|i); #https://sourceforge.net/settings/mirror_choices?projectname=amos&filename=amos/3.0.0/amos-3.0.0.tar.gz
					
          say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, re-lin= $_re_link\n "  if(defined $_self->{debug} && $_self->{debug} >6);
					$_is_tar=$_self->setVersionUrl($_kp_name,$_url_hash_ref,$_re_link);
					#if(grep /sourceforge.net\/projects\/$_project_name\/files.+$_kp_name\/$/i, @_links){ # https://sourceforge.net/projects/mira-assembler/files/MIRA/
					  #$_is_tar=0; #TODO
					#}
					$$_visited_url_hash_ref{$_link}=1;
					$$_visited_url_hash_ref{$_re_link}=1;
					#last if($_is_tar==1);
				}
				#say __FILE__." Line ".__LINE__.", link-$_link, valid arr: ".Dumper(\@_valid_links);
				push (@_valid_links, $_link) unless $_is_tar==1;
			}
			say __FILE__." Line ".__LINE__.",url=$_url,is_tar=$_is_tar,  valid arr: ".Dumper(\@_valid_links)   if(defined $_self->{debug} && $_self->{debug} >6);
			if($_is_tar==0 && (scalar @_valid_links)==0 && (!defined $_url_hash_ref || scalar keys %{$$_url_hash_ref{$_kp_name}{version}} ==0 ) && 
			( $_url=~ m |https://bitbucket.org/.+?/.+?/downloads| || $_url=~ m/github\.com\/.+\/releases/ ) ){ # no version release
				  my $_is_git_no_release=1;
				  $_self->setVersionUrl($_kp_name,$_url_hash_ref,$_url,$_is_git_no_release);
				  $_is_tar=1;
			}
			if($_is_tar==0 && (scalar @_valid_links)>0 ){ # if tar found, should further drill? 
				foreach my $_link (@_valid_links){          
					$_self->drillTarFile($_kp_name,$_link, $_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag);
					#say __FILE__." Line ".__LINE__.", link-$_link, valid arr: ".Dumper(\%_url_hash).Dumper($_visited_url_hash_ref);
				}
			}      
		}else{  #$_mech->success() failed
		}
  } 
}
sub drillFTPTarFile{ 
  my ($_self,$_kp_name,$_url,$_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag)=@_;
  #$_url=lc($_url);
  say __FILE__." Line ".__LINE__.", drillFTPTarFile, $_kp_name,$_url,$_depth" if(defined $_self->{debug} && $_self->{debug}>2 );  
  say __FILE__." Line ".__LINE__.", dead loop error:  $_url" if( $_url =~ /^http/i ); 
  if (!defined $_depth){
  	$_depth=0;
  }else{
  	$_depth++;
  	$_max_depth=5 if(!defined $_max_depth); 	
 		return if($_depth>$_max_depth);  	 
  }  
 	$$_url_hash_ref{$_kp_name}{note}="ftp" if( !defined $$_url_hash_ref{$_kp_name}{note});
 	$$_url_hash_ref{$_kp_name}{home_url}=$_url if( !defined $$_url_hash_ref{$_kp_name}{home_url});
 	$$_url_hash_ref{$_kp_name}{archive_url}=$_url if( !defined $$_url_hash_ref{$_kp_name}{archive_url});
  say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, name=$_kp_name, visited_url_hash:\n".Dumper($_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>2 );
  if(! exists $$_visited_url_hash_ref{$_url}){
		$$_visited_url_hash_ref{$_url}=1;
	}else{
		return;
  }
  say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, name=$_kp_name, visited_url_hash:\n".Dumper($_visited_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>3 );
  say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, name=$_kp_name, \n" if(defined $_self->{debug} && $_self->{debug}>3 );
  my $_url_root=$_self->getRootUrl($_url);
  my $_url_root_no_head=$_self->getRootUrlNoHead($_url);
  my $_ftp_dir = $_url;
  if( $_ftp_dir =~ m/$_url_root(.+)/ ){ #ftp://ftp.gnu.org/gnu/
    $_ftp_dir=$1;
  }
  my $_is_tar=0;
  my @_tar_postfix_arr = @{+TAR}; # use constant arr
  #perl -MNet::FTP -e '$_ftp=Net::FTP->new("ftp.gnu.org", Debug => 0) or die  " Cannot connect  $@";'
	my $_ftp = Net::FTP->new($_url_root_no_head, Debug => 0) or die  __FILE__." Line ".__LINE__.",  Cannot connect to $_url_root_no_head: $@";
  $_ftp->login ("anonymous","yhuang\@omicsci.com"); #todo 
  if(length($_ftp_dir)>0 && defined $_ftp->cwd($_ftp_dir) && $_ftp->cwd($_ftp_dir)){
	  $_ftp->cwd($_ftp_dir);
	}
  my @_links = $_ftp->ls("");
  say __FILE__." Line ".__LINE__.", ftp_dir=$_ftp_dir, links =\n".Dumper(\@_links) if(defined $_self->{debug} && $_self->{debug}>5);

	my @_valid_links=();
	if(scalar @_links >0){
    foreach my $_link (@_links){
      $_link=Kpan::KUtil::Trim($_link);
      say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_ftp_dir, name=$_kp_name, link=$_link\n " if(defined $_self->{debug} && $_self->{debug}>3 );
      if(defined $_link && ($_link =~ m/^[^\/]+$/ || $_link =~ m/^\//) && $_link !~ m/:/ ){ # internal link
        $_ftp_dir=~ s|/$||; # remove last / 
        $_link=$_ftp_dir."/".$_link;
        #perl -e  '$a="a//b/c//d/e"; $a=~ s|//|/|g; print "$a\n"'
        #$_link =~ s|//|/|g;
        $_link=$_url_root.$_link;
      }
      my $_counts_u = () = $_url =~ /\//g;  
      my $_counts_l = () = $_link =~ /\//g;  
      #$_counts=$_counts-4-$_depth;
      my $_ftp_archive_link = $$_url_hash_ref{$_kp_name}{archive_url};
      $_ftp_archive_link =~ s/http[s]/ftp/;
      say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, name=$_kp_name, link=$_link, counts_u=$_counts_u,counts_l=$_counts_l, ftp_archive_link=$_ftp_archive_link\n " if(defined $_self->{debug} && $_self->{debug}>3 );
 			next if( $_link !~ $_ftp_archive_link  #/${_url_root}.+${_name}/i  # external link
        || $_link =~ m/\.diff\.|readme|welcome|hpux10|(tar\.xdp\.gz$)|(\.lzma$)|(\.xz$)|(\.lz$)/i   #ftp://ftp.gnu.org/gnu/gcc/sed-2.05.bin.hpux10
        || $_link =~ m/${_url_root}.+${_kp_name}.*\.(sum|sig|asc)$/i
        || $_counts_l < $_counts_u # go up link parent dir
        || exists $$_visited_url_hash_ref{$_link}  
      );
      say __FILE__." Line ".__LINE__.", depth=$_depth,url=$_url, lin=$_link\n "  if(defined $_self->{debug} && $_self->{debug}>3 );
      #perl -MNet::FTP -e '$_link="=ftp://ftp.gnu.org/gnu/gcc/gnu-objc-issues-1.0.tar.gz";$_ftp=Net::FTP->new("ftp.gnu.org");$_ftp->login ("anonymous","yhuang\@omicsci.com");if(!defined $_ftp->size($_link )){print "pass, \n".$_ftp->size($_link )."\n"}else{print "drill\n"}'
      $$_visited_url_hash_ref{$_link}=1;
      $_is_tar=$_self->setVersionUrl($_kp_name,$_url_hash_ref,$_link);
      if( ($_is_tar==0 || ($_link =~ m/gcc-/ && $_link !~ m/\.gz$/) ) && $_ftp->cwd($_link)){ # test if it is a dir # ?stop at tar gcc has mix of tar and dirs
        push (@_valid_links, $_link);
        #$_ftp->cwd($_ftp_dir); # change back to current dir
      }
      say __FILE__." Line ".__LINE__.", link=$_link,is_tar=$_is_tar, is dir=".($_ftp->cwd($_link)).", valid arr: ".Dumper(\@_valid_links) if(defined $_self->{debug} && $_self->{debug}>3); #."\nvisited_url_hash: ".Dumper(\%_visited_url_hash);
     }
    if( (scalar @_valid_links)>0 ){ ##!! $_is_tar==1 && not true for gcc
      foreach my $_link (@_valid_links){          
      	#say __FILE__." Line ".__LINE__.", $_kp_name,$_link, $_depth ";
      	$_self->drillTarFile($_kp_name,$_link, $_depth,$_url_hash_ref,$_visited_url_hash_ref,$_max_depth,$_html_tag);
      }
    }      
  }
	$_ftp->quit;    
}
sub setVersionUrl{
	my ($_self,$_kp_name,$_url_hash_ref,$_link,$_is_git_no_release) =@_;
	my $_name=$$_url_hash_ref{$_kp_name}{name};
	my $_is_tar=0;
  my @_tar_postfix_arr = @{+TAR}; # use constant arr
  my $_url_root=$_self->getRootUrl($$_url_hash_ref{$_kp_name}{archive_url});
	#say __FILE__." Line ".__LINE__.",n=$_kp_name,link=$_link" if (!defined $_url_root);
	if(defined $_is_git_no_release &&  $_is_git_no_release>0 ){
		my $_v = "git-".`date +%Y%m%d`;#date
		my $_git_url=$_link;
	  # git clone https://git.code.sf.net/p/meraculous/code
	  # "https://git.code.sf.net/p/$_project_name/code ${_project_name}-code";
		if($_git_url !~ m/\.git$/ && $_git_url !~ m/git\.code\.sf\.net/ ){
			 $_git_url=$_git_url.".git";
		}
		$$_url_hash_ref{$_kp_name}{version}{$_v}{url}=$_git_url;
		$$_url_hash_ref{$_kp_name}{version}{$_v}{version_orig}=$_v;
		$$_url_hash_ref{$_kp_name}{version}{$_v}{file_name}=$_name;
		$$_url_hash_ref{$_kp_name}{version}{$_v}{note}=$$_url_hash_ref{$_kp_name}{note}; 
		my $i=0;
		$i=scalar keys %{$$_url_hash_ref{$_kp_name}{version}} if defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url};
		$$_url_hash_ref{$_kp_name}{version}{$_v}{order}=$i;
		say __FILE__." Line ".__LINE__.",$_git_url,$_kp_name,$_v,$i, $$_url_hash_ref{$_kp_name}{home_url}, ".Dumper($_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>2);
		$_is_tar=1;
		return $_is_tar;
	}elsif($_link =~ /\/[^\/]+\.[^\/]+$/ && $_link !~ m/^\s*$/){ # /*.tar 
		#say __FILE__." Line ".__LINE__.",n=$_kp_name,root=$_url_root, link=$_link before match ext";
		say __FILE__." Line ".__LINE__.",n=$_kp_name,root=$_url_root, link=$_link before match ext".Dumper(\@_tar_postfix_arr)  if(defined $_self->{debug} && $_self->{debug}>6);
    foreach my $_ext(@_tar_postfix_arr){
			#perl -e '$a="https://pypi.python.org/packages/87/3c/89c788e45059c5198cc2b7b1c900affe2f12bbd19cd9d9a26791faba8bd4/STAMP-2.1.3.tar.gz#md5=81a5e0df4094d65007c3c27bf1796ec8"; $a=~ m/(\/)*([^\/]+\.tar\.gz)/; print "1:$1\n2:$2\n3:$3\n" '
			say __FILE__." Line ".__LINE__.", link=$_link, $_ext\n " if(defined $_self->{debug} && $_self->{debug}>6);
			my $_http_root=$_url_root;
			if( $_http_root =~ m/^(\s)*https:\/\/ftp\./i ){
				$_http_root =~ s/^https:/ftp:/;
			}elsif($_http_root =~ m|http://sourceforge.net|i ){
				$_http_root =~ s|^http:|https:|;
			}
			say __FILE__." Line ".__LINE__.", http_root=$_http_root, lin=$_link, ext=$_ext\n " if(defined $_self->{debug} && $_self->{debug}>6);
		  #perl -e '$_link="https://sourceforge.net/settings/mirror_choices?projectname=mira-assembler&filename=MIRA/stable/mira-4.0.2.tar.bz2";$_h="";$e="tar.bz2"; $a=~ m/(\/)*([^\/]+\.tar\.gz)/; print "1:$1\n2:$2\n3:$3\n" '
		  #say __FILE__." Line ".__LINE__.",n=$_kp_name,root=$_url_root, link=$_link before match ext";
			if($_link =~ m|${_http_root}.+\.${_ext}[\W]?|  ){ # inside the root url and match tar type
				#say __FILE__." Line ".__LINE__.", lin=$_link, $_ext\n ";
				say __FILE__." Line ".__LINE__.", lin=$_link, $_ext\n " if(defined $_self->{debug} && $_self->{debug}>6);
				my $_tar_file_name=$_link;
				#perl -e '$a="https://pypi.python.org/packages/87/3c/bd4/STAMP-2.1.3.tar.gz#md5=81a5e0df4094d65007c3c27bf1796ec8"; $a=~ m/(\/)*([^\/]+\.tar\.gz)/; print "1:$1\n2:$2\n3:$3\n" '
				#perl -e '$a="ftp://ftp.gnu.org/gnu/mpfr/mpfr-3.0.1.zip"; $e="zip"; $a=~ m/(\/)*([^\/]+\.$e)/i; print "1:$1\n2:$2\n3:$3\n" '
				if($_tar_file_name=~ m|sourceforge.net/.+filename=.+/([^/]+$_ext)| ){ # https://sourceforge.net/settings/mirror_choices?projectname=amos&filename=amos/3.0.0/amos-3.0.0.tar.gz
					$_tar_file_name=$1; 
					$_is_tar=1;
				}elsif($_tar_file_name=~ m|sourceforge.net/.+filename=([^/]+$_ext)| ){ # https://sourceforge.net/settings/mirror_choices?projectname=atlas2&filename=Atlas2_v1.4.3.zip
					$_tar_file_name=$1; 
					$_is_tar=1;
				#}elsif($_tar_file_name=~ m/(\/)*([^\/]+\.$_ext)[\.]*/ ){
				}elsif($_tar_file_name=~ m/(\/)*([^\/]+\.$_ext)$/ ){ #04/23/2018 https://download.gnome.org/sources/gtk-doc/1.2/gtk-doc-1.2.tar.gz.md5
					$_tar_file_name=$2;
					$_is_tar=1;
				}elsif($_tar_file_name=~ m/(READ_ME.txt)$/ || $_tar_file_name=~ m/(READ_ME)$/ || $_tar_file_name=~ m/(README.*)\b/ ){
					$_tar_file_name=$1;
					$_is_tar=1;
				}
			  if($_tar_file_name =~ m/mirror_choices\?projectname=.+&filename=(.+)|.+%2F(.+)|\// ){
					my $_old_version_file_name=$_tar_file_name;
					$_tar_file_name=Kpan::KUtil::TrimFileName($_old_version_file_name);
			  }
			  my $_v = $_self->getVersionFromFileName($_tar_file_name,$_kp_name,$_name);
				if($_tar_file_name =~ /darwin|win32|window|macOS|solaris|irix64|xerces|OSX|org\.|latest$|\.mirrorlist$|\.sha256sum$|\.md5$|\.sha1$/i ){
				  next;
				}
			  say __FILE__." Line ".__LINE__.", lin=$_link, ext=$_ext, v=$_v, tar =$_tar_file_name\n " if(defined $_self->{debug} && $_self->{debug}>2);			
				if(!defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url} 
				  || ($_ext =~ /tar\.gz$/i && $_tar_file_name =~ /src/ && $$_url_hash_ref{$_kp_name}{version}{$_v}{url} !~ /src(.*)tar\.gz$/i)){ # give src tar.gz high priority, one version one tar file
					$$_url_hash_ref{$_kp_name}{version}{$_v}{url}=$_link;  
					$$_url_hash_ref{$_kp_name}{version}{$_v}{version_orignal}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{file_name}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{note}=$$_url_hash_ref{$_kp_name}{note};  
					my $i=0;
					$i=scalar keys %{$$_url_hash_ref{$_kp_name}{version}} if defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url};
					$$_url_hash_ref{$_kp_name}{version}{$_v}{order}=$i;
					$_is_tar=1;
			    say __FILE__." Line ".__LINE__.", lin=$_link, ext=$_ext, v=$_v, tar =$_tar_file_name\n".Dumper($_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>2);			
					return $_is_tar;
				}elsif(!defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url} 
				  || ($_ext =~ /tar\.gz/i && ($$_url_hash_ref{$_kp_name}{version}{$_v}{url} !~ /tar\.gz/i || $$_url_hash_ref{$_kp_name}{version}{$_v}{file_name} !~ /src/))){ # give tar.gz high priority, not over source , one version one tar file
					$$_url_hash_ref{$_kp_name}{version}{$_v}{url}=$_link;  
					$$_url_hash_ref{$_kp_name}{version}{$_v}{version_orignal}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{file_name}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{note}=$$_url_hash_ref{$_kp_name}{note};  
					my $i=0;
					$i=scalar keys %{$$_url_hash_ref{$_kp_name}{version}} if defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url};
					$$_url_hash_ref{$_kp_name}{version}{$_v}{order}=$i;
					$_is_tar=1;
			    say __FILE__." Line ".__LINE__.", lin=$_link, ext=$_ext, v=$_v, tar =$_tar_file_name\n".Dumper($_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>2);			
					#return $_is_tar;
				}elsif(!defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url} 
				  || ($_ext =~ /tar\.gz/i && ($$_url_hash_ref{$_kp_name}{version}{$_v}{url} !~ /tar\.gz/i || $$_url_hash_ref{$_kp_name}{version}{$_v}{file_name} !~ /src/))){ # give tar.gz high priority, not over source , one version one tar file
					$$_url_hash_ref{$_kp_name}{version}{$_v}{url}=$_link;  
					$$_url_hash_ref{$_kp_name}{version}{$_v}{version_orignal}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{file_name}=$_tar_file_name;                          	
					$$_url_hash_ref{$_kp_name}{version}{$_v}{note}=$$_url_hash_ref{$_kp_name}{note};  
					my $i=0;
					$i=scalar keys %{$$_url_hash_ref{$_kp_name}{version}} if defined $$_url_hash_ref{$_kp_name}{version}{$_v}{url};
					$$_url_hash_ref{$_kp_name}{version}{$_v}{order}=$i;
					$_is_tar=1;
			    say __FILE__." Line ".__LINE__.", lin=$_link, ext=$_ext, v=$_v, tar =$_tar_file_name\n".Dumper($_url_hash_ref) if(defined $_self->{debug} && $_self->{debug}>2);			
					#return $_is_tar;
				}
			}
	  }
	}
  return $_is_tar;
}
sub removeTarExtension{
  my $_self = shift;
  my $_fn= shift;
  $_fn=Kpan::KUtil::Trim($_fn);
  my @_tar_postfix_arr = @{+TAR};;
  EXT: foreach my $_ext(@_tar_postfix_arr){
    if($_fn =~ /(.+)\.$_ext/){
      $_fn=$1;
      last EXT;
    }
  }
  return $_fn;
}
sub getVersionFromFileName{
  my ($_self,$_fn,$_kp_name, $_name) = @_;
  #say __FILE__." Line ".__LINE__.",$_fn";
  $_fn=$_self->removeTarExtension($_fn);
  say __FILE__." Line ".__LINE__.",ext removed $_fn" if(defined $_self->{debug} && $_self->{debug}>6);		
  if(defined $_kp_name && $_fn =~ m/[_\-\.\s%]$_kp_name/i){
    $_fn =~ s/[_\-\.\s%]$_kp_name//i;
  }elsif(defined $_kp_name && $_fn =~ m/$_kp_name[_\-\.\s%]/i){
    $_fn =~ s/$_kp_name[_\-\.\s%]//i;
  }elsif(defined $_kp_name && $_fn =~ m/$_kp_name/i){
    $_fn =~ s/$_kp_name//i;  
  }elsif(defined $_name && $_fn =~ m/[_\-\.\s%]$_name/i){
    $_fn =~ s/[_\-\.\s%]$_name//i;
  }elsif(defined $_name && $_fn =~ m/$_name[_\-\.\s%]/i){
    $_fn =~ s/$_name[_\-\.\s%]//i;
  }elsif(defined $_name && $_fn =~ m/$_name/i){
    $_fn =~ s/$_name//i;    
  }
  say __FILE__." Line ".__LINE__.",ext removed name $_fn" if(defined $_self->{debug} && $_self->{debug}>6);		
  my $_v="";
  if($_fn =~ /\d/ && ($_fn =~ m|[\D]*linux[_\-](\d.+)|i)){ #"BEAST_V1_5_4","beagle_release_1_0","fuse-2.9.6", "ngsutils-0.5.3a", "OpenSSL-fips-2_0_13", "OpenSSL_1_1_0d", "smrtanalysis-4.0.0","STAR_2.5.0a"
    $_v=$1;
    say __FILE__." Line ".__LINE__.",f=$_fn, v=$_v" if(defined $_self->{debug} && $_self->{debug}>6);		
  }elsif($_fn =~ /\d/ && ($_fn =~ m/^[a-z_\-]+[\-v\.](\d.+)/i || $_fn =~ m/[\-\_]?(\d.+)/i )){ #mira_4.0.2_linux-gnu_x86_64_static.tar.bz2,beast2-1.0.tgz,"fuse-2.9.6", "ngsutils-0.5.3a", "OpenSSL-fips-2_0_13", "OpenSSL_1_1_0d", "smrtanalysis-4.0.0","STAR_2.5.0a"
    $_v=$1;
    say __FILE__." Line ".__LINE__.",f=$_fn, v=$_v" if(defined $_self->{debug} && $_self->{debug}>6);		
  }elsif($_fn =~ /\d/ && $_fn =~ /^[a-z_\-]*(\d.+)?/i){ #mira_4.0.2_linux-gnu_x86_64_static.tar.bz2,beast2-1.0.tgz,"fuse-2.9.6", "ngsutils-0.5.3a", "OpenSSL-fips-2_0_13", "OpenSSL_1_1_0d", "smrtanalysis-4.0.0","STAR_2.5.0a"
    $_v=$1;
     say __FILE__." Line ".__LINE__.",f=$_fn, v=$_v" if(defined $_self->{debug} && $_self->{debug}>6);		
  }else{ #"BEAST_prerelase"
    $_v=$_fn;  	
    say __FILE__." Line ".__LINE__.",f=$_fn, v=$_v" if(defined $_self->{debug} && $_self->{debug}>6);		
  }
  return $_v;
}
sub getRootUrl{
  my ($_self,$_url)=@_;
  my $_url_root=$_url;
  #say __FILE__." Line ".__LINE__.",url=$_url";
	#perl -e '@a=(" https://github.com/sanger-pathogens/iva","ftp://a.bc.net","ftp://a.bc.net/d/e/fg/1.0/g.tar.gz","http://a.bc.net","https://a.bc.net","https://a.bc.net/","https://a.bc.net/def/xy","abc.com","abc.com/d");  foreach $b(@a) { print"\n$b:\t"; if($b=~ m|(^(.*://)*([^/]+))/?| ) {print "1:$1,\t2:$2,\t3:$3\n" }}'
  #https://sourceforge.net/settings/mirror_choices?projectname=mira-assembler&filename=mira_3rdparty_06-07-2012.tar.bz2
  #perl -e '@a=("https://sourceforge.net/settings/mirror_choices?projectname=mira-assembler&filename=mira_3rdparty_06-07-2012.tar.bz");  foreach $b(@a) { print"\n$b:\t"; if($b=~ m|(^(.*://)*([^/]+))/?| ) {print "1:$1,\t2:$2,\t3:$3\n" }}'
  if($_url_root =~ m|(^(.*://)*([^/]+))/?| ){
    $_url_root=$1; # or $1:$3? 1:https://a.bc.net,     2:https://,     3:a.bc.net
  }
  #say __FILE__." Line ".__LINE__.",url=$_url,\turl_root=$_url_root";
  return $_url_root;
}
sub getRootUrlNoHead{
  my ($_self,$_url)=@_;
  my $_url_root=$_url;
	#perl -e '@a=(" https://github.com/sanger-pathogens/iva","ftp://a.bc.net","http://a.bc.net","https://a.bc.net","https://a.bc.net/","https://a.bc.net/def/xy","abc.com","abc.com/d"); 
	# foreach $b(@a) { print"\n$b:\t"; if($b=~ m/(^(.*:\/\/)*([^\/]+))\/?/) {print "1:$1,\t2:$2,\t3:$3\n" }}'
  if($_url_root =~ m/(^(.*:\/\/)*([^\/]+))\/?/ ){
    $_url_root=$3; # or $1:$3? 1:https://a.bc.net,     2:https://,     3:a.bc.net
  }
  return $_url_root;
}
sub setFileNameSizeToProduction{
  my ($_self,$_src_dir_prefix,$_kp_name,$_log_dir,$_log)=@_;
  my $_db_data=Kpan::DBData->new();
  $_db_data->{debug}=$_self->{debug};
  if (defined $_kp_name && ! $_db_data->isInUrlProduction($_kp_name)){
  	my $_archive_url_hash_ref = $_db_data->getUrlHashFromCollection($_kp_name);
  	say __FILE__." Line ".__LINE__.", archive_url_hash_ref=".Dumper($_archive_url_hash_ref) if(defined $_self->{debug} && $_self->{debug} >2);
  	$_self->spiderVersionUrl($_archive_url_hash_ref);
  	$_db_data->setVersionUrlHashToProduction( $_archive_url_hash_ref);
  }
  my $_size=0;
  my $_version_url_hash_ref={};
  $_db_data->getVersionUrlHashFromProduction($_kp_name,$_version_url_hash_ref,$_size);
 	say __FILE__." Line ".__LINE__.", $_src_dir_prefix,$_kp_name,$_log_dir,$_log,$_size, version_url_hash_ref=".Dumper($_version_url_hash_ref) if(defined $_self->{debug} && $_self->{debug} >0);
	my $_note="batch";
	my $_ERROR="error";
	if(!defined $_log){
		$_log="ns_".Kpan::KUtil::Trim(`date +%m_%d_%y`).".log";
	}
	if(!defined $_log_dir){
		$_log_dir=$_src_dir_prefix;
	}
	open my $_fout, "> $_log_dir/$_log" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_dir/$_log for write: $!\n";
	say __FILE__." Line ".__LINE__.", setFileNameSizeToProduction()" if(defined $_self->{debug} && $_self->{debug} >0);
	foreach my $_k_name (sort keys %{$_version_url_hash_ref}){
    my $_lc_name=lc($_k_name);
    if($_lc_name eq "r"){
    	$_lc_name="R";
    }
		my %_name_hash = %{$$_version_url_hash_ref{$_k_name}{v} };
		foreach my $_v (sort keys %_name_hash){
			my %_v_hash=%{$_name_hash{$_v}};
	    say __FILE__." Line ".__LINE__.", k=$_k_name,v=$_v,".Dumper(\%_v_hash) if(defined $_self->{debug} && $_self->{debug} >2);
			if(!defined $_v ||!defined $_v_hash{url} || (defined $_v_hash{file_size} && $_v_hash{file_size}>0) ){ #  && $$_version_url_hash_ref{$_kp_name}{$_v}{file_name} ne $_ERROR){
				next;
			};
			my $_version_url= $_v_hash{url};
			my $_get_cmd=Kpan::Install->getFetchCommand($_version_url);
			if( ((-e "$_src_dir_prefix/$_lc_name/$_v" || -e "$_src_dir_prefix/$_lc_name/*") &&  $_get_cmd =~ m/^git\b/) # git dir 
			  || (-e "$_src_dir_prefix/$_lc_name/$_v/$_v_hash{version_file_name}" ) ){ # already download
  			$_get_cmd="##$_get_cmd";
	  	}
	    say __FILE__." Line ".__LINE__.",get_cmd=$_get_cmd" if(defined $_self->{debug} && $_self->{debug} >0);
			my @_commands=( #  1# not log, run; 2# not run, log, 3# log comments
				"#if [[ ! -e $_src_dir_prefix/$_lc_name/$_v ]]; then mkdir -p $_src_dir_prefix/$_lc_name/$_v; fi"
				,"cd $_src_dir_prefix/$_lc_name/$_v"
				,$_get_cmd
			);
			Kpan::Install::_runCmd( "$_log_dir/$_log",\@_commands);
			my $_version_file_name=Kpan::KUtil::GetLastFile("$_src_dir_prefix/$_lc_name/$_v");
    	#mirror_choices?projectname=fastuniq&filename=FastUniq-1.0.tar.gz
			if(defined $_version_file_name && $_version_file_name =~ m/mirror_choices\?projectname=.+&filename=(.+)|.+%2F(.+)/ ){
			  my $_old_version_file_name=$_version_file_name;
			  $_version_file_name=Kpan::KUtil::TrimFileName($_old_version_file_name);
			  system ("mv","$_src_dir_prefix/$_lc_name/$_v/$_old_version_file_name","$_src_dir_prefix/$_lc_name/$_v/$_version_file_name");
			}
			my $_v_file_size=0;
			open my $_fout, ">> $_src_dir_prefix/$_log" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_src_dir_prefix/$_log for write: $!\n";
			if($_get_cmd=~ m/\.git$/ ||  $_get_cmd=~ m/^rsync/){
				$_version_file_name=$_k_name;
			  $_note="dir";
			  $_v_file_size=1;
			}elsif(!defined $_version_file_name){
				$_version_file_name=$_ERROR;
				$_v_file_size= -1;
				$_note="error";
			}elsif(-e "$_src_dir_prefix/$_lc_name/$_v/$_version_file_name"){
				$_note="batch";
			  $_v_file_size= stat("$_src_dir_prefix/$_lc_name/$_v/$_version_file_name")->size; 
      }else{
				$_version_file_name=$_ERROR;
				$_v_file_size= -1;
				$_note="error";
      }
      say "$_src_dir_prefix/$_lc_name/$_v/$_version_file_name\t $_v_file_size";
	    $_db_data->setFileNameSizeToProduction($_k_name,$_v,$_version_file_name,$_v_file_size,$_note);
	  }
  }  
  $_db_data->UrlCheck();
}
sub getBrief{
 my ($_self,$_url)=@_;
 my ($_content, $_brief,$_title);
 my $_mech = WWW::Mechanize->new( autocheck => 0 );
 WWW::Mechanize::TreeBuilder->meta->apply($_mech);
 if($_url =~ m|https://sourceforge.net/projects/([^\/]+)|){
 	 say __FILE__." Line ".__LINE__.",$_url" if(defined $_self->{debug} && $_self->{debug} >1);
 	 my $_project_name=$1;
	 $_mech->get($_url);
	 if ( $_mech->success() ) { # page available  	
		 say __FILE__." Line ".__LINE__.",$_url, mech->success" if(defined $_self->{debug} && $_self->{debug} >0);
		 $_title=$_mech->title();
		 $_title=~ s/\s*download\s*\|\s*SourceForge.net\s*$//i;
		 $_title=Kpan::KUtil::Trim($_title);
		 #say __FILE__." Line ".__LINE__.",$_url,content=$_content\n";
		 $_content=$_mech->content;
		 #say __FILE__." Line ".__LINE__.",$_url,content=$_content\n";
		 $_content=$_mech->text( );
		 $_content=~ s/([^[:ascii:]]+)/unidecode($1)/ge;
		 #say __FILE__." Line ".__LINE__.",$_url,content=$_content\n";
		 if($_content=~ m/(.+?)ProvidersHomeBrowse$_project_name\s$_project_name\s(.+\.?)Brought to you by/i){
		 	 $_brief=$2;
	   }elsif($_content=~ m/(.+?)Browse All FilesDescription(.+?)$_project_name\s*Web Site(Categories|Follow)/i){
		 	 $_brief=$2;
		 }elsif($_content=~ m/(.+?)Description(.+?)$_title\s*Web Site(Categories|Follow)/i){
		 	 $_brief=$2;
		 }else{
		 	 $_brief="";
		 }
	  }  
  }
  say __FILE__." Line ".__LINE__.",$_url,$_title, content=$_content" if(defined $_self->{debug} && $_self->{debug} >7);
  say __FILE__." Line ".__LINE__.",$_url,$_title, brief=\n$_brief" if(defined $_self->{debug} && $_self->{debug} >0);
  return $_brief;
}
sub hasValidLinks{
	my ($_self,$_url,$_html_tag,$_tar_tag_only, $_drill)=@_;
	my $_hasValidLinks=0;
	if(!defined $_url){
	  return $_hasValidLinks;
	}
  my @_links =( );  
  my $_end=1;
  if($_url=~ m|sourceforge| ){
    $_end=0;
  }
  my $_pattern=Kpan::KUtil::GetTarPattern($_end);
  my $_mech = WWW::Mechanize->new( autocheck => 0 );
  WWW::Mechanize::TreeBuilder->meta->apply($_mech);
  #$_mech->proxy(['https', 'http', 'ftp'], 'https://47.32.84.212:10443'); 
  if(defined $_url ){
    say __FILE__." Line ".__LINE__.",$_url" if(defined $_self->{debug} && $_self->{debug} >1);
    $_mech->get($_url);
    if ( $_mech->success() ) { # page available  	
      say __FILE__." Line ".__LINE__.",$_url, mech->success" if(defined $_self->{debug} && $_self->{debug} >4);
      if (defined $_html_tag){
      	@_links =$_mech->find_all_links( text => $_html_tag );
      }elsif(defined $_tar_tag_only && $_tar_tag_only>0){
      	@_links =$_mech->find_all_links( url_regex => qr{$_pattern});
      	foreach my $_k (@_links){
          say "t=$_tar_tag_only,n=".$_k->name.", u=".$_k->url_abs()  if(defined $_self->{debug} && $_self->{debug} >4);
        }
      }elsif(defined $_drill && $_drill>0) {
				@_links =$_mech->find_all_links(url_regex => qr{\Q$_url\E} );
      }else{
				@_links =$_mech->find_all_links();
			}      
    } 
  }
  say __FILE__." Line ".__LINE__.",url=$_url,html_tag=$_html_tag,tar_tag_only=$_tar_tag_only, drill=$_drill,".Dumper(\@_links) if(defined $_self->{debug} && $_self->{debug} >4);
  $_hasValidLinks=scalar @_links;
	return $_hasValidLinks;
}
sub processAppArchiveUrlToDB{ # set home/archive/version url into archive  hash for single app
	my ($_self) = @_; #$_app_name, $_kp_name, $_version, $_home_url, $_archive_url,$_version_url,$_ext,$_is_kpan_dev
	my $_id;
  my $_app_name    = $_self->{name};
  my $_kp_name     = $_self->{kp_name};
  my $_version     = $_self->{version};
  my $_home_url    = $_self->{home_url};
  my $_archive_url = $_self->{archive_url};
  my $_version_url = $_self->{version_url};
  my $_is_kpan_dev = $_self->{is_kpan_dev};
  my $_ext         = $_self->{version_file_name_ext};
  $_self->{src}="app_install";
  say __FILE__." Line ".__LINE__.",before upld to collection,app_name=$_app_name".Dumper($_self) if(defined $_self->{debug} && $_self->{debug} >4);
	#$_self->uploadToCollection();
  my $_db_data=Kpan::DBData->new();
  $_db_data->{debug}=$_self->{debug};
  say __FILE__." Line ".__LINE__.",app_name=$_app_name,archive_url=$_archive_url, is_kpan_dev=$_is_kpan_dev, isInUrlProduction($_app_name)= ".($_db_data->isInUrlProduction($_app_name))
    .",isInUrlProduction($_app_name,$_version)=".($_db_data->isInUrlProduction($_app_name,$_version))  if(defined $_self->{debug} && $_self->{debug} >4);
  my $_archive_url_hash_ref;
	$$_archive_url_hash_ref{$_app_name}{name}=$_app_name if(defined $_app_name);  
  $$_archive_url_hash_ref{$_app_name}{home_url}=$_home_url if (defined $_home_url);
  $$_archive_url_hash_ref{$_app_name}{archive_url}=$_archive_url if (defined $_archive_url);
  $$_archive_url_hash_ref{$_app_name}{version_url}=$_version_url if (defined $_version_url);

  if(defined $_archive_url){
		#$$_archive_url_hash_ref{$_app_name}{home_url}=$_home_url;
		#$$_archive_url_hash_ref{$_app_name}{archive_url}=$_archive_url;
		#$$_archive_url_hash_ref{$_app_name}{name}=$_app_name;  
		#$_archive_url_hash_ref={} if($_is_kpan_dev);	
	}elsif (defined $_app_name && $_db_data->isInUrlProduction($_app_name)  ){ # in prod but not that version
  	$_archive_url_hash_ref =$_db_data->getArchiveUrlHashFromProduction($_app_name);
  	say __FILE__." Line ".__LINE__.", Prou archive_url_hash_ref=".Dumper($_archive_url_hash_ref) if(defined $_self->{debug} && $_self->{debug} >5);
  }elsif (defined $_app_name && !$_db_data->isInUrlProduction($_app_name) ){
  	$_archive_url_hash_ref =$_db_data->getArchiveUrlHashFromCollection($_app_name) if(!defined $_archive_url_hash_ref);
  	say __FILE__." Line ".__LINE__.", Collection archive_url_hash_ref=".Dumper($_archive_url_hash_ref) if(defined $_self->{debug} && $_self->{debug} >5);
  }
		#$$_archive_url_hash_ref{$_app_name}{v}{$_version}{id}=$_id;
  if(defined $_archive_url_hash_ref && (!defined $_self->{start_level} || $_self->{start_level} <2 )  ){
  	say __FILE__." Line ".__LINE__.",before spider to get new url,anme=$_app_name, kp_name=$_kp_name, v=$_version, hu=$_home_url, au=$_archive_url,vu=$_version_url,".Dumper($_archive_url_hash_ref).Dumper($_self) if(defined $_self->{debug} && $_self->{debug} >3);
  	$_self->spiderVersionUrl($_archive_url_hash_ref, $_ext);
  	$_db_data->setVersionUrlHashToProduction( $_archive_url_hash_ref);
  	$_id =  $_db_data->getSoftwareID($_app_name, $_kp_name, $_version );
  }
  say __FILE__." Line ".__LINE__.",after spider and upload to DB, id=$_id, ".Dumper($_self) if(defined $_self->{debug} && $_self->{debug} >5);
  return $_id;
}

sub _guessHomeArchiveUrl{
  my ($_home_url,$_archive_url,$_version_url, $_debug)=@_;
  
  $_archive_url = defined $_home_url? $_home_url:$_version_url if (!defined $_archive_url);
  $_archive_url = $_version_url if ((!defined $_archive_url || $_archive_url=~ m/^\s*$/) && defined $_version_url);
  $_home_url    = $_archive_url if((!defined $_home_url || $_home_url=~ m/^\s*$/) && defined $_archive_url);  
  
  say __FILE__." Line ".__LINE__.",guessHomeArchiveUrl, home_url=$_home_url,archive_url=$_archive_url,vu=$_version_url" if(defined $_debug && $_debug >4);
  my ($_project_name,$_name);
  if(defined $_archive_url){
  	$_archive_url =~ s|/$|| ; # remove last /
  	# perl -e '@a=qw(https://bitbucket.org/1 http://bitbucket.org/2 http://github.com/3/m http://sourceforge.net/4/ http://se.net/5); foreach $b (@a){ $b =~ s/^http:(.+(github|sourceforge))/https:$1/; print "$b, 1:$1,2:$2,3:$3\n"}'
  	$_archive_url =~ s/^http:(.+(github\.com|github\.io|sourceforge\.net|bitbucket\.org))/https:$1/ ; # https
  	if( $_archive_url =~ m/github\./){
  		$_archive_url =_convertGithubIOUrl($_archive_url) if( $_archive_url =~ m/github\.io/);
      # perl -e '@a=("https://github.com/01org/tbb/releases","https://github.com/01org/tbb/releases/","https://github.com/01org/tbb.git"); foreach $b(@a) {$b=~ m,(.+github.com)/(.+?)/(.+?)(/|$|\.git$),i; print "1:$1,2:$2,3:$3,4:$4,5:$5,6:$6,7:$7\n";}'
      #say __FILE__." Line ".__LINE__.", hu=$_home_url,au=$_archive_url,vu=$_version_url" if(defined $_self->{debug} && $_self->{debug} >1);
      if($_archive_url =~ m,(.+github.com)/(.+?)/(.+?)(/|$|\.git$),i  ){
				my $_root         = $1;
				my $_project_name = $2;
				my $_file_name    = $3;
				my $_url_new      = "$_root/$_project_name/$_file_name";
        $_home_url        = $_url_new if(!defined $_home_url || $_home_url=~ m/^(?!http|ftp).+/ || $_home_url=~ m|github.com| ); 
        $_archive_url     = $_url_new."/releases"; 		
      }
  	}elsif($_archive_url =~ m|gitlab\.com|i){
      if($_archive_url =~ m,(.+gitlab.com)/(.+?)/(.+?\.git$),i  ){ # https://gitlab.com/ezlab/busco.git
				my $_root         = $1;
				my $_lab_name     = $2;
				my $_app_name     = $3;
        $_home_url        = "$_root/$_lab_name/$_app_name" if(!defined $_home_url || $_home_url=~ m/^(?!http|ftp).+/ || $_home_url=~ m|gitlab.com| ); 
        $_archive_url     = $_home_url; 		
			}elsif($_archive_url =~ m,(.+gitlab.com)/(.+?)/(.+?)/-/archive/(.+?)/(.+$),i  ){  # https://gitlab.com/ezlab/busco/-/archive/3.0.2/busco-3.0.2.tar.gz
				my $_root         = $1;
				my $_lab_name     = $2;
				my $_app_name     = $3;
				my $_version      = $4;
				my $_file_name    = $5;
        $_home_url        = "$_root/$_lab_name/$_app_name" if(!defined $_home_url || $_home_url=~ m/^(?!http|ftp).+/ || $_home_url=~ m|gitlab.com| ); 
        $_archive_url     = $_home_url; 		
      }
  	}elsif($_archive_url =~ m|sourceforge.net|i){ #https://downloads.sourceforge.net/project/seqclean
			my $_prefix        = $_archive_url;
			if ( $_prefix      =~ m|(https://.*sourceforge.net/project.*?)/| ){ #sourceforge.net/project(s)*
			  $_prefix         = $1;
			}
			say __FILE__." Line ".__LINE__.",prefix=$_prefix" if(defined $_debug && $_debug>4);
			if($_archive_url   =~ m|$_prefix/(.+?)/files/([^/=?&]+)|i){ #http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download http://sourceforge.net/projects/ncl/files/latest/download
				$_project_name   = $1;
				$_name           = $2;
			}elsif($_archive_url =~ m|$_prefix/(.+?)/|i || $_archive_url =~ m/:\/\/(.*)\.sourceforge\.net\//i){
				$_project_name   = $1;
				$_name           = $1;
			}
      
			my $_url_new       = (defined $_prefix && defined $_project_name) ? "$_prefix/$_project_name" : "";
			$_home_url         = $_url_new if (!defined $_home_url || $_home_url=~ m/^(?!http|ftp).+/ || $_home_url=~ m/^\s*$/ || $_home_url=~ m|sourceforge.net| );
			$_archive_url      = $_url_new."/files" if (defined $_prefix && defined $_project_name && $_archive_url !~ m|$_prefix/$_project_name/files| ); # stable link https://sourceforge.net/projects/mira-assembler/files/MIRA/stable/
    }elsif($_archive_url =~ m|bitbucket\.org/(.+?)/([^\/]+)|i){
			$_project_name     = $1;
			$_name             = $2;
			my $_url_new       = "https://bitbucket.org/$_project_name/$_name";
			#say __FILE__." Line ".__LINE__.",project_name=$_project_name, name=$_name" if(defined $_self->{debug} && $_self->{debug}>5);
			$_archive_url      = "$_url_new/downloads/?tab=tags"; # https://bitbucket.org/nsegata/lefse/downloads/?tab=tags https://bitbucket.org/dkessner/harp/downloads/?tab=downloads
			#$_archive_url = "$_url_new/downloads"; # https://bitbucket.org/nsegata/lefse/downloads/?tab=tags https://bitbucket.org/dkessner/harp/downloads/?tab=downloads
			# hg clone https://bitbucket.org/nsegata/lefse
			$_home_url        = $_url_new if (!defined $_home_url || $_home_url=~ m/^(?!http|ftp).+/ || $_home_url=~ m|bitbucket.org| );
 		}elsif($_archive_url =~ m/bioconda\.github\.io/){
				;
		}
	}
  return ($_name,$_project_name,$_home_url,$_archive_url);
}

1;

