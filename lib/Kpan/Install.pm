#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2016
 
package Kpan::Install;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use Sort::Versions qw(versioncmp);
use File::stat;
use File::Copy;
use File::Path qw(make_path rmtree);
use MIME::Types;
use Cwd qw(cwd);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use Client;
use Prereq;
use Software;
use Url;
use KUtil;

our $VERSION = '1.6.0';

use Exporter;
our @ISA = ("Exporter");
#our @EXPORT = qw(install UploadKLog parseKLog UploadModuleFile);              # Export by default
#our @EXPORT_OK = qw(leopard @llama $emu);           # Export by request
my %CACHE; #static cache for app
my $debug = undef;

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
		brief                   => shift,
		category                => shift,
		channel                 => shift,
		checked_level           => shift,
		client                  => shift,
		collect_url             => shift,
		compile_command         => shift,
		debug                   => shift,
		extract_command         => shift,
		fetch_command           => shift,
		failed_hash_ref         => shift,
		full_log                => "k_full_log",
		general_compile_command => shift,
		general_extract_command => shift,
		general_fetch_command   => shift,
		general_install_log     => shift,
		general_module_content  => shift,
		general_test_command    => shift,
		general_upload_command  => shift,
		install_hash_ref        =>shift,
		install_log_path        => shift,
		install_test            => Kpan::InstallTest->new,
		is_installed            => shift,
		log_dir                 => ".kpan_log",
		kp_name                 => shift, # smatools, 1.0_gcc447
		kp_version              => shift, # smatools, 1.0_gcc447
		kstat                   => Kpan::KStat->new, # module
		match_flag              => shift,
		make_module_command     => shift,
		module_content          => shift,
		module_exp_pair         => shift,
		module_names            => shift,
		multiline_end           => "'",
		multiline_start         => ":'Multiline comment:",
		note                    => shift,
		prereq_command          => shift,
		software                => shift,
		software_hash_ref       => shift,
		search_scope            => shift,  
		start_level             => shift, # start 0 dry run, 1 from fetch whole process, 2 from abstract, 3 from build, 4 from module, 5 test, 6 upload 
		tarfile_name            => shift,
		test_cmd_help           => shift,
		test_cmd_result         => shift,
		tmp_dir                 => "tmp",
		upload_command          => shift,
	};
	$_self->{install_hash_ref}  = undef;
	$_self->{software_hash_ref} = undef;
	return $_self;           
}

sub install{
	my ($_self, $_is_production, $_local_copy) = @_;
	if(!defined $_self->{software} || !defined $_self->{software}->{name}){
		die __FILE__. " Line " . __LINE__ ." Please provide installing application name";
	}
	my $_software                 = $_self->{software};
	my $_client                   = $_self->{client};
	my $_src_dir_prefix           = $_client->{src_dir_prefix};
	my $_install_dir_prefix       = $_client->{install_dir_prefix};
	my $_lmod_dir_prefix          = $_client->{lmod_dir_prefix};
	my $_app_install_log          = $_client->{app_install_log};
	my $_install_user_mark        = $_client->{install_user_mark};
	$_self->{start_level}         = 1                          if (!defined $_self->{start_level});
	$_software->{start_level}     = $_self->{start_level}      if (!defined $_software->{start_level});
	$_software->{debug}           = $_self->{debug};
	$_client->{debug}             = $_self->{debug};
	$_client->{rc}->{debug}       = $_client->{debug};
 	$_software->{brief}           =~ s/\n+$/\n/                if (defined $_software->{brief}); 
	$_self->{brief}               = $_software->{brief}        if (!defined $_self->{brief}      && defined $_software->{brief}); 
	$_self->{kp_name}             = $_software->{name}         if (!defined $_self->{kp_name}    && defined $_software->{name}); 
	$_self->{search_scope}        = $_client->{search_scope}   if (!defined $_self->{search_scope}); 

	$_self->getInstallFromDB;

	$_self->{kp_version}          = $_software->{version}      if (!defined $_self->{kp_version} && defined $_software->{version}); 
	my $_kp_name                  = $_self->{kp_name};
	my $_kp_version               = $_self->{kp_version};
	my $_install_path             = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path                 = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_channel                  = $_self->{channel};
	my $_kstat                    = $_self->{kstat};
	$_kstat->{app_name}           = $_software->{name};
	$_kstat->{app_version}        = $_software->{version};
	$_kstat->{kp_name}            = $_self->{kp_name};
	$_kstat->{kp_version}         = $_self->{kp_version};
	$_kstat->{client_id}          = $_client->{id};
	$_kstat->{client_name}        = $_client->{kpan_client_name};
  $_kstat->{conda}              = 1 if (defined $_self->{channel} && $_self->{channel} eq "conda" );

  my $_install_test             = $_self->{install_test};
  $_install_test->{name}                   = $_software->{name};
  $_install_test->{version}                = $_kp_version;
  $_install_test->{debug}                  = $_self->{debug};
  $_install_test->{KP}                     = $_client->{KP};
  $_install_test->{lmod_app_path}          = $_client->{lmod_app_path};
  $_install_test->{file_name_prefix}       = $_client->{install_user_mark};
  $_install_test->{file_core_name}         = $_kp_name;

  $_local_copy                             = 1 if (defined $_software->{local_copy_path} && $_software->{local_copy_path} =~ m/\S+/);
	$_self->{update_only}   = 1;
	$_self->{is_all_client} = 1;
	
	$_self->getInstallFromDB;
	#say __FILE__. " Line " . __LINE__ . ", after getInstallFromDB ihu=$_self->{software}->{home_url}, hu=$_software->{home_url}";
	$_self->{is_all_client} = undef ; 
	$_self->{update_only}   = undef;
	$_self->{is_all_client} = undef;
	
	say __FILE__. " Line " . __LINE__ . ", after getInstallFromDB install=" and  $_self->print if ( defined $_self->{debug} && $_self->{debug} >2);
	my $_is_before_installed_test  = 1;
	my $_number_of_tests           = 1;
	$_self->installTest($_is_before_installed_test, $_number_of_tests);

	#$_self->isInstall($_number_of_test_cmd);
	# TODO check install at custom version kp_name/kp+version
	say __FILE__. " Line " . __LINE__ . ", after install test install=" and  $_self->print  if ( defined $_self->{debug} && $_self->{debug} > 4 );
	say __FILE__." Line ".__LINE__.",check if installed n=$_software->{name}, v=$_software->{version},kp_name=$_kp_name, kp_v=$_kp_version, $_install_path, is_installed=".$_self->{is_installed}."\n"
		if(defined $_self->{debug} && $_self->{debug}>0); 
	if(defined $_self->{is_installed} && $_self->{is_installed} >0){ # TODo run test command get version #?
		say __FILE__." Line ".__LINE__.", $_kp_name/$_kp_version has already installed at $_install_path. " if(defined $_kp_version);
		say __FILE__." Line ".__LINE__.", $_kp_name has already installed at $_install_dir_prefix/$_kp_name " if(!defined $_kp_version);
		return;
	}else{
		if(defined $_kp_version){ 
			say __FILE__." Line ".__LINE__.",Installing $_kp_name/$_kp_version\n\ncd $_src_path\n";
		}else{
			say __FILE__." Line ".__LINE__.",Installing $_kp_name at $_install_dir_prefix/$_kp_name";
		}
	} 
  $_software->{home_url}    = "" if ($_local_copy && !defined $_software->{home_url});
  $_software->{archive_url} = "" if ($_local_copy && !defined $_software->{archive_url});
  $_software->{version_url} = "" if ($_local_copy && !defined $_software->{version_url});
	$_software->setSelf($_client->{kpan_client_name}, $_client->{rc});
	$_self->checkUpdate($_software) if (defined $_self->{collect_url} && $_self->{collect_url}>0); # check version updates
  say __FILE__. " Line " . __LINE__ . ", after setSelf ihu=$_self->{software}->{home_url}, hu=$_software->{home_url}, iau=$_self->{software}->{archive_url}, au=$_software->{archive_url}" if ( defined $_self->{debug} && $_self->{debug} > 2 );
  #exit;
  $_software->{brief}    = $_self->{brief} if (!defined $_software->{brief} ); # ready to upload if not in DB yet
	$_self->{brief}        = $_software->{brief} if(!defined $_self->{brief});
	$_software->{category} = $_self->{category} if(!defined $_software->{category});
	$_self->{category}     = $_software->{category} if(!defined $_self->{category});
	$_kstat->{app_id}      = $_software->{id};
  say __FILE__. " Line " . __LINE__ . ", after install test install=".Dumper($_software) if ( defined $_self->{debug} && $_self->{debug} > 4 );
	$_kp_version = lc ($_software->{version}) if (!defined $_kp_version ); # in case no version specified and get version from DB
	say __FILE__." Line ".__LINE__.",app name=$_software->{name},after setself install=" and  $_self->print  if(defined $_self->{debug} && $_self->{debug}>4); 
	die "There is no version from input or database for $_software->{name}. please input archive site as --app_archive_url. Use perl kpan --help to see details." if (!defined $_kp_version && !defined $_channel ); # conda could skip version
 	$_self->mkDir("$_src_dir_prefix/$_kp_name");
 	die "There is problem to make dir at $_src_dir_prefix/$_kp_name. Please check permission of that dir and re-run the command after fix" if (! -d "$_src_dir_prefix/$_kp_name" );
 	open my $_fout, "> $_src_dir_prefix/$_kp_name/$_app_install_log" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_src_dir_prefix/$_kp_name/$_app_install_log for write: $!\n";
 	print $_fout "";
 	
 	if(defined $_is_production && $_is_production>0){
  }else{ # dev
		#if($_kp_name =~ m/^anaconda|miniconda|bioconda$/i ){ #TODO conda lib m/^perl|python|anaconda|miniconda|bioconda$/
		#;
		#}
    if(1 || ! -d "$_src_path"){
      $_self->fetch;  
		  say __FILE__." Line ".__LINE__.", n=$_software->{name}, v=$_software->{version},kp_name=$_kp_name, kp_v=$_kp_version, $_install_dir_prefix,from DB size=$_software->{version_file_size},  version_file_name=$_software->{version_file_name}\n" if(defined $_self->{debug} && $_self->{debug}>4);     
    	say __FILE__." Line ".__LINE__.",n=$_software->{name}, kp_name=$_kp_name,after fetch install=" and  $_self->print  if(defined $_self->{debug} && $_self->{debug}>5); 
      if(1 || ! -f "$_install_dir_prefix/$_kp_name/$_app_install_log"){
        $_self->extract;
      }
      if(1 || ! -d "$_src_path"){
        $_self->prereq;
      }
      if(1 || ! -f "$_install_path/$_app_install_log"){
      	$_self->logDefaults();
        $_self->compile;
      }
      if(1 || !-f "$_lmod_dir_prefix/$_kp_name/$_kp_version"){
        $_self->makeLmodModuleLua;
      }
      if(1 || -f "$_install_path"){
      	my ($_is_installed_test, $_number_of_tests)=(0,1);
        $_self->installTest($_is_installed_test, $_number_of_tests);
      }
      if(1 || -f "$_app_install_log"){
        $_self->installLogUpload;
      }
    }
  }
  my @_commands=(
    "##"
		,"$_client->{KP} END#"
    ,"##"
  );
  $_self->runCmd( "$_src_path/$_app_install_log",\@_commands); 
  #$_self->uploadKLog($_app_name,$_version,"$_src_path/$_app_install_log");
  #$_self->uploadModuleFile($_app_name,$_version,"$_mod_path/$_version");
}
sub getInstallFromDB {
  my ($_self, $_kpan_client_name, $_install_id, $_client_id,$_client_name, $_app_id, $_app_name,$_kp_name, 
    $_version, $_kp_version, $_set_id, $_is_all_app, $_is_all_client, $_update_only, $_install_id_only) = @_;
	my $_software         = $_self->{software};
	my $_client           = $_self->{client};
  $_kp_name             = $_self->{kp_name}            if (!defined $_kp_name);
  $_kp_version          = $_self->{kp_version}         if (!defined $_kp_version);
  $_app_id              = $_software->{id}             if (!defined $_app_id           && defined $_software);
  $_app_name            = $_software->{name}           if (!defined $_app_name         && defined $_software);
  $_version             = $_software->{version}        if (!defined $_version          && defined $_software);
  $_kpan_client_name    = $_client->{kpan_client_name} if (!defined $_kpan_client_name && defined $_client);
  $_client_name         = $_client->{kpan_client_name} if (!defined $_client_name      && defined $_client);
  if(defined $_self->{search_scope}){
  }else{
    $_client_id           = $_client->{id}               if (!defined $_client_id        && defined $_client);
  }
  $_is_all_app          = $_self->{is_all_app}         if (!defined $_is_all_app       && defined $_self->{is_all_app});
  $_is_all_client       = $_self->{is_all_client}      if (!defined $_is_all_client    && defined $_self->{is_all_client});
  $_update_only         = $_self->{update_only}        if (!defined $_update_only      && defined $_self->{update_only});
  $_install_id_only     = $_self->{install_id_only}    if (!defined $_install_id_only  && defined $_self->{install_id_only});
  
  $_update_only          = 1 if (!defined $_update_only);	                                        
	# TODO specify client name with system
	my $_install_hash_ref = $_self->{install_hash_ref};
	$_install_hash_ref    = $_client->{rc}->getInstallHash($_kpan_client_name, $_install_id, $_client_id,$_client_name,	$_app_id, $_app_name,$_kp_name,
	  $_version, $_kp_version, $_set_id,$_is_all_app,$_is_all_client) if (!defined $_self->{install_hash_ref});
	#my $_size_of_install_hash = defined keys %{$_install_hash_ref} ? ${keys %{$_install_hash_ref}}:-1;
	#say __FILE__. " Line " . __LINE__ . ", size =$_size_of_install_hash, install hash=".Dumper($_install_hash_ref);# if ( defined $_self->{debug} && $_self->{debug} > 2 );
	say __FILE__. " Line " . __LINE__ . ", update_only=$_update_only" if ( defined $_self->{debug} && $_self->{debug} > 2);

	my $_match_flag = 0; # name, 1 version 2, system 3
	foreach my $_hash_key (sort keys %{$_install_hash_ref}) {
		my $_install            = $_install_hash_ref->{$_hash_key}; #TODO match client
  	$_install->{kp_name}    = lc($_install->{software}->{name})    if (!defined $_install->{kp_name}    && defined $_install->{software}->{name}); 
	  $_install->{kp_version} = lc($_install->{software}->{version}) if (!defined $_install->{kp_version} && defined $_install->{software}->{version}); 
		#say __FILE__. " Line " . __LINE__ . ", hash_key=$_hash_key, self=";
		#$_self->print;
		#say __FILE__. " Line " . __LINE__ . ", install=";
		#$_install->print;
		next if ($_match_flag == 1 && ($_install->{kp_name}    ne $_kp_name   ) );
		next if ($_match_flag == 2 && ($_install->{kp_version} ne $_kp_version) );
    if (defined $_install_id_only && $_install_id_only >0){
    }else{
			my $_checked_level = defined $_install->{checked_level}? $_install->{checked_level}:0 ;
			foreach my $_install_key (keys %{$_install}){
        #last if $_checked_level<3;
				if($_install_key =~ m/^(software|client|rc|id|install_test|stat)$/){
					 say __FILE__. " Line " . __LINE__ . ",match_flag=$_match_flag, skip, $_install_key"  if ( defined $_self->{debug} && $_self->{debug} > 2 );
					 next;
				}
				#if(!defined $_self->{$_install_key} && defined $_install->{$_install_key}){
				#	say __FILE__. " Line " . __LINE__ . ",match_flag=$_match_flag, update, $_update_only, $_install_key"  if ( defined $_self->{debug} && $_self->{debug} > 2 );
				#}
				#say __FILE__. " Line " . __LINE__ . ", getINstallFromDB debug=$_self->{debug}, match_flag=$_match_flag, install_key=$_install_key";
				if( defined $_update_only && $_update_only >0  ){
					$_self->{$_install_key} = $_install->{$_install_key} if ( defined $_install->{$_install_key} && ( ($_match_flag == 0 && !defined $_self->{$_install_key}) || ($_match_flag>0)));
					#$_self->{$_install_key} = $_install->{$_install_key} if (defined $_install->{$_install_key} && $_match_flag > 0);
					#say __FILE__. " Line " . __LINE__ . ",match_flag=$_match_flag, update_only, $_install_key, $_self->{$_install_key} = $_install->{$_install_key}" 
					# if ( defined $_self->{debug} && $_self->{debug} > 0 && (!defined $_self->{$_install_key} && defined $_install->{$_install_key}) );
				}elsif(!defined $_update_only || $_update_only <1){
					say __FILE__. " Line " . __LINE__ . ", WRONG update_only, $_install_key, $_self->{$_install_key} = $_install->{$_install_key}"; # if ( defined $_self->{debug} && $_self->{debug} > 0 );
					$_self->{$_install_key} = $_install->{$_install_key} ;
				}
				#say __FILE__. " Line " . __LINE__ . ", getINstallFromDB debug=$_self->{debug}, match_flag=$_match_flag, install_key=$_install_key";
			}
			#say __FILE__. " Line " . __LINE__ . ", in FromDB, install_test = ".Dumper($_install->{install_test}) if ( defined $_self->{debug} && $_self->{debug} > 0 );
			my $_general_test_cmd_result = $_install->{general_test_cmd_result};
			my $_to_personal = 1;
			my $_test_cmd_result = $_client->getGeneral($_general_test_cmd_result,$_to_personal) if (defined $_general_test_cmd_result);
			$_self->{install_test}->parseTestCmdResult($_test_cmd_result) if (defined $_test_cmd_result);
			my $_i_software = $_install->{software};
			foreach my $_s_key (keys %{ $_i_software } ) {           
				if($_s_key =~ m/^prereq_lib_hash_ref|prereq_hash_ref|rc$/){
					 say __FILE__. " Line " . __LINE__ . ",match_flag=$_match_flag, update, $_update_only, $_s_key"  if ( defined $_self->{debug} && $_self->{debug} > 4 );
					 next;
				}
				if( defined $_update_only && $_update_only >0 ){
					$_software->{$_s_key} = $_i_software->{$_s_key} if ( (!defined $_software->{$_s_key} && defined $_i_software->{$_s_key}) 
						 || (defined $_software->{$_s_key} && $_software->{$_s_key} =~ m/^\s*$/ && defined $_i_software->{$_s_key} && $_i_software->{$_s_key} =~ m/\S/));
				}elsif(!defined $_update_only || $_update_only <1){
					$_software->{$_s_key} = $_i_software->{$_s_key};
				}
			}
			#say __FILE__. " Line " . __LINE__ . ", getINstallFromDB cmp software, ".Dumper($_software) if ( defined $_self->{debug} && $_self->{debug} > 0 );
			#say __FILE__. " Line " . __LINE__ . ", getINstallFromDB cmp i_software, ".Dumper($_i_software) if ( defined $_self->{debug} && $_self->{debug} > 0 );
			$_self->{brief} = $_software->{brief} if ( (!defined $_self->{brief} || $_self->{brief} =~ m/^\s*$/ ) && defined $_software->{brief} && $_software->{brief} =~ m/^\S*$/);
			$_software->{brief} = $_self->{brief} if ( (!defined $_software->{brief} || $_software->{brief} =~ m/^\s*$/ ) && defined $_self->{brief} && $_self->{brief} =~ m/^\S*$/);
			my $_i_kstat = $_install->{kstat};
			foreach my $_k_key (keys %{ $_i_kstat } ) {
				$_self->{kstat}->{$_k_key} = $_i_kstat->{$_k_key} if ( (!defined $_self->{kstat}->{$_k_key} || ($_self->{kstat}->{$_k_key} =~ m/^[0]*$/) ) 
				  && ( defined $_i_kstat->{$_k_key} && $_i_kstat->{$_k_key} =~ m/^[\d|\S]+$/  && $_i_kstat->{$_k_key} =~ m/^[^0]$/ ) );           
	    }	
			if( defined $_update_only && $_update_only >0 ){
				my @_pl_keys = keys %{$_software->{prereq_lib_hash_ref}};
				my @_p_keys  = keys %{$_software->{prereq_hash_ref}};
				say __FILE__. " Line " . __LINE__ . ", update, #_pl_keys=$#_pl_keys,".Dumper($_software->{prereq_lib_hash_ref}) if ( defined $_self->{debug} && $_self->{debug} > 4);
				say __FILE__. " Line " . __LINE__ . ", update, #_p_keys=$#_p_keys, ".Dumper($_software->{prereq_hash_ref}) if ( defined $_self->{debug} && $_self->{debug} > 4);
				say __FILE__. " Line " . __LINE__ . ", update, ".Dumper($_i_software->{prereq_lib_hash_ref}) if ( defined $_self->{debug} && $_self->{debug} > 4);
				say __FILE__. " Line " . __LINE__ . ", update, #=".scalar (keys %{$_i_software->{prereq_hash_ref}}).", ".Dumper($_i_software->{prereq_hash_ref}) if ( defined $_self->{debug} && $_self->{debug} > 4);
				$_software->{prereq_lib_hash_ref} = $_i_software->{prereq_lib_hash_ref} if ( scalar (@_pl_keys) == 0 && scalar (keys %{$_i_software->{prereq_lib_hash_ref}}) > 0 );
				$_software->{prereq_hash_ref}     = $_i_software->{prereq_hash_ref}     if ( scalar (@_p_keys)  == 0 && scalar (keys %{$_i_software->{prereq_hash_ref}}) > 0 );
			}elsif(!defined $_update_only || $_update_only <1){
				$_software->{prereq_lib_hash_ref} = $_i_software->{prereq_lib_hash_ref};
				$_software->{prereq_hash_ref}     = $_i_software->{prereq_hash_ref};
			}
			# TODO ?
			#$_self->{client}   = $_install->{client} if ($_self->{client}->{kpan_client_name} eq $_self->{client}->{kpan_client_name});			# TODO should overwrite current client?
			#say __FILE__. " Line " . __LINE__ . ", getINstallFromDB debug=$_self->{debug}, $_install->{client}->{kpan_client_name} eq $_self->{client}->{kpan_client_name} ";#if ( defined $_self->{debug} && $_self->{debug} > 0 );
			#$_self->{software} = $_software; # TODO why software turned to hash in process
    }
  	say __FILE__. " Line " . __LINE__ . ", getInstallFromDB, client=".Dumper($_client) if ( defined $_self->{debug} && $_self->{debug} > 2);
    
		$_match_flag  = 1 if ($_match_flag < 2                                                                && $_install->{kp_name} eq $_kp_name );
		$_match_flag  = 2 if ($_match_flag < 3                                                                && $_install->{kp_name} eq $_kp_name && $_install->{kp_version} eq $_kp_version );
		$_match_flag  = 3 if ($_install->{client}->{kpan_client_name} eq $_self->{client}->{kpan_client_name} && $_install->{kp_name} eq $_kp_name && $_install->{kp_version} eq $_kp_version );
		say __FILE__. " Line " . __LINE__ . ", $_install->{client}->{kpan_client_name} eq $_self->{client}->{kpan_client_name} && $_install->{kp_name} eq $_kp_name && $_install->{kp_version} eq $_kp_version " if ( defined $_self->{debug} && $_self->{debug} > 0 );
    $_self->{id}  = $_install->{id} if ($_match_flag == 3);
		last                            if ($_match_flag == 3);		
	}
	say __FILE__. " Line " . __LINE__ . ", getInstallFromDB, client=".Dumper($_client) if ( defined $_self->{debug} && $_self->{debug} > 2);
	my $_src_dir_prefix = $_client->{src_dir_prefix};
	my $_log_dir        = "$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}";
	$_self->mkDir("$_log_dir") if ( !-e "$_log_dir");	
	my @_subjects       = qw (command general module install);
	my ($_command_log_file,  $_general_log_file, $_module_file, $_install_log_file);
  my @_command_keys   = ("prereq_command",  "fetch_command","extract_command","compile_command","make_module_command","install_test_command","test_cmd_help", "install_log_text",);
  my @_general_keys   = ("general_prereq_command","general_fetch_command","general_extract_command","general_compile_command","general_make_module_command", 
                       "general_test_command","general_test_cmd_help","general_upload_command", "general_test_cmd_result","general_install_log");	 
  my @_module_keys    = ("module_content","general_module_content",  );
  my @_install_keys   = (
      "id",                     "app_id",                      "app_name",               "app_version",             "brief",										 
      "category",               "checked_level",               "client_id",              "eb_dir",									"eb_file",                 
      "kp_name",                 "kp_version",	               "note",        );
  my %_self_hash = %{$_self};
  for my $_i (0..$#_subjects){
    my $_log_file    = "$_log_dir/$_subjects[$_i].log";	
    open my $_fout, "> $_log_file" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_file for write: $!\n";
    if ($_i == 0 ) {
		  foreach my $_key (keys @_command_keys){
			  say $_fout "###KP $_command_keys[$_key] ###\n".( (defined $_self->{$_command_keys[$_key]}) ? $_self->{$_command_keys[$_key]} : "");
		  }  
    }elsif ($_i == 1 ) {
		  foreach my $_key (keys @_general_keys){
			  say $_fout "###KP $_general_keys[$_key] ###\n".( (defined $_self->{$_general_keys[$_key]}) ? $_self->{$_general_keys[$_key]} : "");
		  }  
    }elsif ($_i == 2 ) {
		  foreach my $_key (keys @_module_keys){
			  say $_fout "###KP $_module_keys[$_key] ###\n".( (defined $_self->{$_module_keys[$_key]}) ? $_self->{$_module_keys[$_key]} : "");
		  }  
    }else{ # $i==3
      say $_fout "###KP match = $_match_flag";
      say $_fout "###KP install history log###\n";
		  foreach my $_key (keys @_install_keys){
			  say $_fout "###KP $_install_keys[$_key] ###\n".( (defined $_self->{$_install_keys[$_key]}) ? $_self->{$_install_keys[$_key]} : "");
		  }     
    }
  }
	#my $_software_hash_ref = $_client->{rc}->getSoftwareHash($_kpan_client_name,undef, $_app_name,$_client_name,	$_app_id, $_app_name,undef, undef, undef) if ($_size_of_install_hash<0);
	$_self->{install_hash_ref}=$_install_hash_ref;
	my @_keys = keys %{$_install_hash_ref};
  #say __FILE__." line ".__LINE__.", log_dir=$_log_dir, #=".($#_keys);
	return $_install_hash_ref; # set equal to slef if matched, or get all apps in hash
}

sub fetch{
  my ($_self)            = @_;
	my $_software          = $_self->{software};
	my $_client            = $_self->{client};
	my $_cmd               = $_self->{fetch_command};
	my $_src_dir_prefix    = $_client->{src_dir_prefix};
	my $_app_install_log   = $_client->{app_install_log};
	my $_install_user_mark = $_client->{install_user_mark};
	my $_local_copy_path   = $_software->{local_copy_path}; # TODO 3/1/2017
	my $_url               = $_software->{version_url} if (!defined $_local_copy_path) ;	
	my $_to_personal       = 1;
	my $_kp_name           = $_self->{kp_name};
	my $_kp_version        = $_self->{kp_version};
  my $_tmp               = $_self->{tmp_dir};#.`date +%m/%d/%y`;
	my $_fetch_command     = ( defined $_self->{general_fetch_command} &&  $_self->{checked_level}>2 ) ? $_self->{general_fetch_command} : "";
	$_fetch_command        = $_client->getGeneral($_fetch_command,$_to_personal);

	if(!defined $_local_copy_path && !defined $_self->{channel} &&  $_url =~ m |/(^/.)| && -e $_url ) {
	  $_local_copy_path=$_software->{local_copy_path}=$_url ; # vu is absolute path 
	}
	#$_self->mkDir("$_src_dir_prefix/$_lc_name");
  my $_log_header = "###$_install_user_mark ".`date +%m/%d/%y`;
	my $_get_cmd    = "";

	if( defined $_local_copy_path  ){
		$_get_cmd     = "rsync -Larvzh $_local_copy_path $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version ";
		#$_get_cmd    = "rsync -Larvzh $_local_copy_path ." if(-f $_local_copy_path);
	}elsif(defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda} ){
		$_get_cmd     = "";
	}elsif(defined $_fetch_command && $_fetch_command =~ m/\S+/){
		$_get_cmd = $_fetch_command;
	}elsif( ( !defined $_get_cmd || $_get_cmd =~ m/^\s*$/ ) && defined $_url && $_url !~ /^\s+$/ ){
		$_get_cmd= $_self->getFetchCommand($_url);
		$_get_cmd="##".$_get_cmd if(defined $_self->{start_level} && $_self->{start_level}>1);
	}else{
		$_get_cmd="";
	}
	$_self->{fetch_command} = $_get_cmd;
	$_self->{fetch_command} =~ s/#//g; # remove ##
	$_to_personal = 0;
	$_self->{general_fetch_command} = $_client->getGeneral($_self->{fetch_command},$_to_personal);
	$_self->writeKLogFile;
	say __FILE__." Line ".__LINE__.", fetch app:\nget_cmd=$_get_cmd\n" if(defined $_self->{debug} && $_self->{debug}>2);
	warn "There is no resource from input or kpan repo. please input archive site as --app_archive_url "
	."or direct download url as --app_version_url or a local dir to copy the source as --local_copy_file_handler. "
	."Use perl kpan --help to see details." if (!defined $_url && !defined $_local_copy_path && !defined $_self->{channel} );
  #perl -e '$a="\#KP FETCH#"; if($a=~ s/^\#/\$/ ) {print "match $a\n"} else {print "not match $a\n"} '
  #perl -e '$a="#get\nzero\n#on#e\n###twp #";$a=~ s/^#(?!#)+/###/gm; print "$a\n";'
  #$_cmd =~ s/^#(?!#)+/###/gm;
  if(scalar <$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/*>){
  	#unlink "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/*";
  	system "rm -rf $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/*";
  }
  my @_commands=( #  1# not log, run; 2# not run, log, 3# log comments
    "###!/bin/bash"
    ,"##"
    ,$_log_header
  );
  my @_get_commands = (
    "##mkdir -p $_src_dir_prefix/$_kp_name"
    ,"#mkdir $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version"
    ,"##"
    ,"$_client->{KP} FETCH#"
 		,"cd $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version"
		,$_get_cmd
  );
  push (@_commands, @_get_commands) if (defined $_get_cmd && $_get_cmd =~ m/\S/);
  say __FILE__." Line ".__LINE__.", p=$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version, v=$_kp_version\n".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>2);
  $_self->{failed_hash_ref}{fetch} = $_self->runCmd( "$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands);
  if (defined $_local_copy_path ){
		#$_software->{version_file_name}=Kpan::KUtil::GetLastDir($_local_copy_path);
		# remove trailing /
		$_software->{version_file_name} = $_local_copy_path;
		$_software->{version_file_name} =~ s|/$||;
		# get last dir or file name
		$_software->{version_file_name} =~ s|.*/||;
		say __FILE__." Line ".__LINE__.", p=$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version, v=$_kp_version, software->{version_file_name}=$_software->{version_file_name}\n".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>0);
	}elsif(defined $_self->{channel}){
		
	}else{
		$_software->{version_file_name}=Kpan::KUtil::GetTarFile("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version",$_kp_version);
		say __FILE__." Line ".__LINE__.", p=$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version, v=$_kp_version, software->{version_file_name}=$_software->{version_file_name}\n".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>1);
  }
  my $_version_file_size=$_software->{version_file_size};
	if(defined $_software->{version_file_name} && -e "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_software->{version_file_name}"
	  && (!defined $_software->{version_file_size} || $_software->{version_file_size} < 2 ) ){ # dir is size 1
 	  if(-f "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/".$_software->{version_file_name}){
		  my $_st = stat("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_software->{version_file_name}");
	    $_version_file_size= $_st->size; #(stat $filename)[7],  -s "$_src_dir_prefix/$_lc_name/$_software->{version_file_name}";
		  if((defined $_self->{debug} && $_self->{debug}>4) || (defined $_software->{version_file_size} && $_st->size != $_software->{version_file_size})  ) {
  		  say __FILE__." Line ".__LINE__.", $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_software->{version_file_name} from DB size=$_software->{version_file_size}, size="
  		  .$_st->size.", stat=".Dumper($_st);
		  }
		}elsif(-d "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/".$_software->{version_file_name}){
		  $_version_file_size=1;
		}else{
		  $_version_file_size=1;
		}
		$_software->{version_file_name} = "$_software->{version_file_name}" if($_software->{version_file_name} =~ m/\S\s+\S/ ); # space in file name

		#$_software->{version_file_size} = $_version_file_size if (!defined 	$_software->{version_file_size} || $_software->{version_file_size}<2);
    say __FILE__." Line ".__LINE__.", $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_software->{version_file_name}, size=$_software->{version_file_size}" if(defined $_self->{debug} && $_self->{debug}>0);
    $_software->{note}       = (-d "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/".$_software->{version_file_name}) ? "from install":"from install, dir";
    $_client->{rc}->{debug}  = $_self->{debug};
    $_client->{rc}->uploadSoftware($_client->{kpan_client_name}, $_software, $_client->{cluser_system}, $_client->{os}); 
  }
	return; 
}
sub getFetchCommand {
  my ($_self,$_url, $_simple)=@_;
	my $_get_cmd;
	# todo 
	# curl -L http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download > boost_1_56_0.tar.gz
	if(!defined $_url || $_url =~ m|^/s.$|){
		$_get_cmd="";
	}elsif($_url =~ m/\.git$/i || $_url =~ m|https://git.code.sf.net|i){
		$_get_cmd= "git clone --recursive $_url";
	}elsif($_url =~ m|^svn://| ){
		$_get_cmd= "svn co $_url";
	}elsif($_url =~ m|https://svn.code.sf.net/\S+| ){
		$_get_cmd= "svn checkout $_url";
	}elsif($_url =~ m|\Qhttp://download.oracle.com/\E.+/java/|i ){
		#$_get_cmd= "wget --no-check-certificate --no-verbose -N --no-cookies --header \"Cookie: oraclelicense=accept-securebackup-cookie\" \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
		$_get_cmd= "curl -C - -LR#OH \"Cookie: oraclelicense=accept-securebackup-cookie\" \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
	}elsif($_url =~ m/(bitbucket.org|s3.amazonaws.com)/i ){
		$_get_cmd= "wget --no-check-certificate --no-verbose --no-clobber \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
	}elsif($_url =~ m|^https:.*sourceforge.net/.+/(.+?\.tar\.gz)|i ){ # http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download 
		$_get_cmd= "wget --no-check-certificate --content-disposition --no-verbose --no-clobber \"$_url\" > $1"; #--no-clobber -N timestamp -O filename.tar.gz 
	}elsif($_url =~ m|^https:.*sourceforge.net/.+/latest/download|i  && defined $_self->{tarfile_name} && $_self->{tarfile_name} =~ m/\S/){ # http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download 
		$_get_cmd= "wget --no-check-certificate --content-disposition --no-verbose --no-clobber \"$_url\" > $_self->{tarfile_name}"; #--no-clobber -N timestamp -O filename.tar.gz 
	}elsif($_url =~ m/^https:.*sourceforge.net/i ){ # http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download 
		$_get_cmd= "wget --no-check-certificate --content-disposition --no-verbose --no-clobber \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
	}elsif($_url =~ m|github.com|i && $_url !~ m|github.com/.+/archive|i ){ # --content-disposition failed "https://github.com/ViennaRNA/ViennaRNA/releases/download/v2.4.1/ViennaRNA-2.4.1.tar.gz" 
		$_get_cmd= "wget --no-check-certificate --no-verbose --no-clobber \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
	}elsif($_url =~ m/^https:/i ){
		$_get_cmd= "wget --no-check-certificate --content-disposition --no-verbose --timestamping \"$_url\" "; #--no-clobber -N timestamp -O filename.tar.gz
	}elsif($_url =~ m/^http:/i ){
		$_get_cmd= "wget --content-disposition --no-verbose -N \"$_url\"";
	}elsif($_url =~ m/^http/i &&  $_simple==1){
		$_get_cmd= "wget \"$_url\"";
	}elsif($_url =~ m;^(/|~|./); ){
		$_get_cmd= "cp -R \"$_url\" .";
	}else{
		$_get_cmd = "wget --content-disposition  --no-verbose -N \"$_url\""; #curl -JLO 
		die __FILE__." Line ".__LINE__.", has NO url!\t $_get_cmd" if(!defined $_url || $_url=~ m/^\s*$/);
	}
	if(!defined $_self->{tarfile_name} && length($_url)>80 ){ # file name too long for OS
	  my $_tar;
	  my @_tar_arr = qw( .tar.gz .Z.gz .bz2 .gz .tgz .tar .gz .Z .zip .cpio );
	  my $_e;
	  foreach my $_e (@_tar_arr) {
			#say "$_e";
	    do {$_tar = $_e ; $_self->{tarfile_name} = "$_self->{software}->{name}.$_self->{software}->{version}$_tar"; last;} if ( $_url =~ m/$_e/);
		}		
	}
	
	if(defined $_self->{tarfile_name}){
		$_get_cmd =~ s/ -N / / if ($_get_cmd =~ m/^wget/); # remove timestamp, which not cmpatiable with -O
		$_get_cmd = $_get_cmd." -O $_self->{tarfile_name}" if ($_get_cmd =~ m/^wget/);
		$_get_cmd = $_get_cmd." -o $_self->{tarfile_name}" if ($_get_cmd =~ m/^curl/);
	}
	return $_get_cmd;
}
sub extract{ # TODO 10/13/2017
  my ($_self)               = @_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_version_file_name    = $_software->{version_file_name};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_app_install_log      = $_client->{app_install_log};
	my $_install_user_mark    = $_client->{install_user_mark};
	my $_tmp                  = $_self->{tmp_dir};
  my $_decompress_cmd       =""; #  1# not log, run; 2# not run, log, 3# log comments
	my $_to_personal          = 1;
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_extract_command      = ( defined $_self->{general_extract_command}  && $_self->{checked_level}>2 )  ? $_self->{general_extract_command} : "";
	$_extract_command         = $_client->getGeneral($_extract_command,$_to_personal);
  my $_f;
	say  __FILE__." Line ".__LINE__.",version_file_name =$_version_file_name" if(defined $_self->{debug} && $_self->{debug}>0); 

  if(defined $_version_file_name){
    $_f=$_version_file_name;
  }else{ # nested file/dir ? no use
    #my $_is_dir=0;
    #$_is_dir  =  1 if ( (defined $_software->{version_file_size} && $_software->{version_file_size}==1 ) || ( defined $_software->{version_file_name} && -d $_src_dir_prefix/${_kp_name}/${_tmp}/$_software->{version_file_name} )); #dir
    #$_f       =  $_version_file_name = Kpan::KUtil::GetTarFile("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version",$_kp_version, $_is_dir); #`$_last_dir`; #todo no extract
	  #$_f       =  $1 if(defined $_f && $_f =~ m/^mirror.+=(.+)$/ ); # sourceforge files
	  #$_f       =  $_version_file_name = Kpan::KUtil::GetLastFileHandler("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version",$_kp_version, $_is_dir) if (!defined $_f); #no tar file, nake files directly sucha gff3togenepred
	  #move ("$_version_file_name", $_f) if (defined $_f && $_version_file_name ne $_f) ; # sourceforge files
  }
	$_software->{version_file_name} = $_f; # sourceforge files
	say  __FILE__." Line ".__LINE__.",_fe = $_f\nversion_file_name =$_version_file_name" if(defined $_self->{debug} && $_self->{debug}>4); 
	if(!defined $_f || -d $_f ){ # no tar , dir exists from rsync/ 
	  $_decompress_cmd ="##";
	}elsif($_f =~ /\.bz2$/) {
	  $_decompress_cmd = "tar -xjf $_f";
	}elsif($_f =~ /\.bz$/) {
	  $_decompress_cmd = "tar -xjf $_f";
	}elsif($_f =~ /\.tar$/) {
	  $_decompress_cmd = "tar -vxf $_f";
	}elsif($_f =~ /\.zip$/) {
	  $_decompress_cmd = "unzip  $_f";
	}elsif($_f =~ /\.tar\.gz$|\.tgz$|\.gz$/) {
	  $_decompress_cmd = "tar -a -xf  $_f";
	}elsif($_f =~ /\.gz$|\.tgz$/) {
	  $_decompress_cmd = "gunzip  $_f";
	}elsif($_f =~ /\.tar\.Z$/) {
	  $_decompress_cmd = "zcat $_f | tar -xvf - ";
	}elsif($_f =~ /\.Z$/) {
	  $_decompress_cmd = "uncompress  $_f";
	}elsif($_f =~ /.cpio$/) {
	  $_decompress_cmd = " cpio -idcmv <  $_f";
	}elsif($_f =~ /\.sh$/) {
	  $_decompress_cmd = "sh  $_f"; # TODO
	}elsif($_f =~ /\.bin$/) {
	  $_decompress_cmd = "sh  $_f"; # TODO
	}elsif($_f =~ /\.xz$/) {
	  $_decompress_cmd = "tar -a -xf $_f";
	}else{
	  $_decompress_cmd = "";
	}
  my @_commands=(  #  1# not log, run; 2# not run, log, 3# log comments
    "##"
    ,"$_client->{KP} EXTRACT#"
   );
   #push (@_commands, "##rsync -avq $_src_dir_prefix/$_lc_name/$_tmp/$_kp_version/$_f $_src_dir_prefix/$_lc_name/")  if (defined $_f && -f "$_src_dir_prefix/$_lc_name/$_tmp/$_kp_version/$_f" );
   # perl -e '$f="/usr/local/src/gcc/tmp/gcc-4.7.4.tar.gz"; unlink $f or die "Unable to unlink $f: $!";' 
    # perl -e 'system ("/usr/local/apps/lmod/7.1.9/libexec/lmod", "perl", "purge")'
    # (echo "Test Out";>&2 echo "Test Err") > >(tee stdout.log) 2> >(tee stderr.log >&2) 
  my @_commands_cmd;
  if(defined $_extract_command && $_extract_command =~ m/\S/ ){ # from DB
    # KP markups todo, only need extract cmd, not moving files
    $_extract_command =~ s/^#(?!#)/###/gm; 
    @_commands_cmd = split (/\n/,$_extract_command);
    push(@_commands,@_commands_cmd);
    $_self->{failed_hash_ref}{decompress} = $_self->runCmd(  "$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands); # cmd from DB, 
    say  __FILE__." Line ".__LINE__.", extract cmd from DB $_extract_command" if(defined $_self->{debug} && $_self->{debug}>1); 
  }elsif (defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda}){
    push (@_commands_cmd, "###conda install"); 
    push(@_commands,@_commands_cmd);
    $_self->{failed_hash_ref}{decompress} = $_self->runCmd( "$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands); # cmd from DB, 
  }else{
    push (@_commands_cmd, "#cd $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version") if (cwd() ne "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version" ); 
    push (@_commands_cmd, "#rsync -avq $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_f $_src_dir_prefix/$_kp_name/")  if (defined $_f && -f "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_f" );
    push (@_commands_cmd, "#$_decompress_cmd");
    say  __FILE__." Line ".__LINE__.", #?".Dumper(\@_commands_cmd) if(defined $_self->{debug} && $_self->{debug}>0); 
    $_self->{failed_hash_ref}{decompress} = (defined $_self->{failed_hash_ref}{decompress} &&  $_self->{failed_hash_ref}{decompress}) ? $_self->{failed_hash_ref}{decompress} : $_self->runCmd("$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands_cmd); # run without logging to get untarred dir name
    map {$_ ="#".$_;} @_commands_cmd; # has run, just log, 
    say  __FILE__." Line ".__LINE__.", ##?".Dumper(\@_commands_cmd) if(defined $_self->{debug} && $_self->{debug}>0); 
    my $_extract_dir = Kpan::KUtil::GetExtractDir("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version", $_kp_name, $_kp_version, $_version_file_name);
    $_extract_dir = "$_extract_dir" if (defined $_extract_dir && $_extract_dir =~ m/\S\s+\S/ ); # space in dir name
    if (!defined  $_extract_dir){
      push (@_commands_cmd, "mkdir $_src_path"); 
		 #,"ls -1 | grep -v ^$_kp_version | xargs -I{} mv {} $_src_path"
      push (@_commands_cmd, "ls -l $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/* "); 
      push (@_commands_cmd, "rsync -avq $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/* $_src_path/"); 
    }else{
		  if(! -e "$_src_path" && -d "$_extract_dir"){
			  push (@_commands_cmd, "mv $_extract_dir $_src_path") ;
			}elsif(-e "$_src_path" && -d "$_extract_dir"){
			  push (@_commands_cmd, "rsync -avq $_extract_dir/* $_src_path/") ;
			}
	  }
 	  push(@_commands,@_commands_cmd);    
    $_self->{failed_hash_ref}{decompress} = (defined $_self->{failed_hash_ref}{decompress} &&  $_self->{failed_hash_ref}{decompress}) ? $_self->{failed_hash_ref}{decompress} : $_self->runCmd(  "$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands);  
    say  __FILE__." Line ".__LINE__.",_f = $_f,\tversion_file_name =$_version_file_name,\textract_dir=$_extract_dir".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>0); 
    @_commands=("rm -rf $_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/*"); 
    $_self->{failed_hash_ref}{decompress} = (defined $_self->{failed_hash_ref}{decompress} &&  $_self->{failed_hash_ref}{decompress}) ? $_self->{failed_hash_ref}{decompress} : $_self->runCmd(  "$_src_dir_prefix/$_kp_name/$_app_install_log",\@_commands);  
    #unlink "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_extract_dir" if (defined $_extract_dir && -e "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_extract_dir");
    #unlink ("$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_f") if (-e "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version/$_f" && -f "$_src_dir_prefix/$_kp_name/$_f");
    #rmtree "$_src_dir_prefix/$_kp_name/$_tmp/$_kp_version" if (-d "$_src_dir_prefix/$_kp_name/tmp/$_kp_version" && -f "$_src_dir_prefix/$_kp_name/$_f");
  }
  $_self->{extract_command}         =  join("\n",@_commands_cmd);
 	$_to_personal                     = 0;
  $_self->{extract_command}         =~ s/#+/\n/g;
  $_self->{extract_command}         =~ s/\n+/\n/;
	$_self->{general_extract_command} = $_client->getGeneral($_self->{extract_command},$_to_personal);
  $_self->writeKLogFile;
  # move log file
  #my $_mv_logfile = "mv $_src_dir_prefix/$_kp_name/$_app_install_log $_src_path/$_app_install_log ";	
  #say "$_mv_logfile";
  move ("$_src_dir_prefix/$_kp_name/$_app_install_log", "$_src_path/$_app_install_log");
}
sub prereq{
  my ($_self)              = @_;
	my $_software            = $_self->{software};
	my $_client              = $_self->{client};
	my $_version_file_name   = $_software->{version_file_name};
	my $_prereq_hash_ref     = $_software->{prereq_hash_ref}; # prereq from prereq table
	my $_prereq_lib_hash_ref = $_software->{prereq_lib_hash_ref}; # prereq from prereq table
	my $_src_dir_prefix=$_client->{src_dir_prefix};
	my $_app_install_log     = $_client->{app_install_log};
	my $_kp_name             = $_self->{kp_name};
	my $_kp_version          = $_self->{kp_version};
#	my $_install_path        = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path            = "$_src_dir_prefix/$_kp_name/$_kp_version";
  my $_is_log              = 0;
  my $_to_personal         = 1;
	my $_prereq_command      = defined $_self->{general_prereq_command} ? $_self->{general_prereq_command} : "";
	$_prereq_command         = $_client->getGeneral($_prereq_command,$_to_personal);
  my @_commands=(
		   "##"
      ,"$_client->{KP} PREREQ#" #  1# not log, run; 2# not run, log, 3# log comments
  );
  my @_prereq_command;
  my $_pq_hash_ref;
  if(defined $_prereq_command){
    $_prereq_command =~ s|java/jdk1.8.0_20|java/1.8.0|gi; # TODO temp fix for java module inconsistence
		@_prereq_command=split (/\n/, $_prereq_command);
	}else{
  	say __FILE__." Line ".__LINE__.", prereq_lib_hash_ref:".Dumper($_software->{prereq_lib_hash_ref} ) if(defined $_software->{prereq_lib_hash_ref} && defined $_self->{debug} && $_self->{debug}>1);
	  say __FILE__." Line ".__LINE__.", prereq_hash_ref:".Dumper($_software->{prereq_hash_ref} ) if(defined $_software->{prereq_hash_ref} && defined $_self->{debug} && $_self->{debug}>1 );

	  $_pq_hash_ref       = $_software->getCombinePrereqHashRef; # $$_pq_hash_ref{$_order} =$_pq;
    my $_pq_str         = "";
    $_prereq_command    = "";
    foreach my $_order_key (sort keys %{$_pq_hash_ref}) { # key = perl|python.*|R
      my $_pq           = $$_pq_hash_ref{$_order_key};  
      $_pq_str         .= "$_pq->{prereq_name}/$_pq->{prereq_version} ";
      $_prereq_command .= $_pq_str ;
      push (@_prereq_command, "##$_pq_str");
      print __FILE__." Line ".__LINE__.", prereq:\t$_pq_str" if(defined $_pq_str && $_pq_str ne "" && defined $_software->{prereq_hash_ref} &&  $_self->{debug} && $_self->{debug}>-1 ); 
      print "\n" if(defined $_pq_str && $_pq_str ne "" && defined $_software->{prereq_hash_ref} &&  defined $_self->{debug} && $_self->{debug}>-1 );
    }
  }
 	$_to_personal = 0;
	map {$_=~ s/^#+//; $_ ="###".$_;} @_prereq_command;
  push (@_commands,@_prereq_command);
  $_self->{failed_hash_ref}{prereq} = (defined $_self->{failed_hash_ref}{prereq} &&  $_self->{failed_hash_ref}{prereq}) ? $_self->{failed_hash_ref}{prereq} : $_self->runCmd( "$_src_path/$_app_install_log",\@_commands,$_is_log);  
	map {$_=~ s/^#+//; } @_prereq_command;
 	$_self->{prereq_command}         = \@_prereq_command;
	$_self->{general_prereq_command} = $_client->getGeneral($_self->{prereq_command},$_to_personal);
  $_self->writeKLogFile;

  my ($_pq_lib_str, $_ModuleLib_commands) = $_self->installModuleLib($_is_log); # no use?
  print "$_pq_lib_str" if(defined $_pq_lib_str && $_pq_lib_str ne "" && defined $_software->{prereq_hash_ref} &&  defined $_self->{debug} && $_self->{debug}>-1 );
  #say __FILE__." Line ".__LINE__.",L694 _pq_lib_str=$_pq_lib_str, ModuleLib_commands:".Dumper($_ModuleLib_commands);
  say __FILE__." Line ".__LINE__.", ModuleLib_commands: ".Dumper($_ModuleLib_commands ) if(defined $_self->{debug} && $_self->{debug}>1 );

  $_self->{failed_hash_ref}{prereq} = (defined $_self->{failed_hash_ref}{prereq} &&  $_self->{failed_hash_ref}{prereq}) ? $_self->{failed_hash_ref}{prereq} : $_self->runCmd( "$_src_path/$_app_install_log" ,$_ModuleLib_commands,$_is_log) if (defined $_ModuleLib_commands);  # TODO no log
  if(defined $_pq_hash_ref){
    foreach my $_order_key (sort keys %{$_pq_hash_ref}) { # key = perl|python.*|R
      my $_pq = $$_pq_hash_ref{$_order_key};  
	    say __FILE__." Line ".__LINE__.",key=$_order_key, pq:".Dumper($_pq) if(defined $_self->{debug} && $_self->{debug}>5);
  		my $_s = Kpan::Software->new();
	  	$_s->{name}= $_pq->{prereq_name};
	  	$_s->{version}=$_pq->{prereq_version};
	  	$_s->{debug}=$_self->{debug};
	  	say __FILE__." Line ".__LINE__.",trying to install prereq $_s->{name}/$_s->{version}:".Dumper($_pq) if(defined $_self->{debug} && $_self->{debug}>5);
	  	my $_install=Kpan::Install->new();
	  	$_install->{software}=$_s;
	  	$_install->{client}=$_client;
	  	$_install->{debug}=$_self->{debug};
		  $_install->install;
	  }
	}
}
sub installModuleLib{ # todo
  my ($_self,$_is_log)    = @_;
	my $_software           = $_self->{software};
	my $_client             = $_self->{client};
	my $_channel            = $_self->{channel};
	my $_install_dir_prefix = $_client->{install_dir_prefix};
	my $_src_dir_prefix     = $_client->{src_dir_prefix};
	my $_app_install_log    = $_client->{app_install_log};
	my $_install_user_mark  = $_client->{install_user_mark};
 	my $_kp_name            = $_self->{kp_name};
	my $_kp_version         = $_self->{kp_version};
 my @_commands=();
  # perl -e '@a=("perl2","apython","python"); foreach $b (@a){if ($b=~ /^perl$|^python$|^r$/) {print "$b matched\n"}}'
  @_commands=("module load $_kp_name/$_kp_version") if ($_kp_name =~ /^perl$|^python$|^r$/ );
  my %_prereq_lib_hash_pq=%{$_software->{prereq_lib_hash_ref}};
  #say __FILE__." Line ".__LINE__.", 730 pq_order_hash:".Dumper(\%_prereq_lib_hash_pq) ;
  my $_pq_lib_str = "";
 
  return $_pq_lib_str if (scalar keys %_prereq_lib_hash_pq <1); # empty hash
  foreach my $_name_key (sort keys %_prereq_lib_hash_pq) { # key = perl|python.*|R${$_prereq_lib_hash_ref}{$_prereq_name}{$_prereq_order}=$_p;
    my %_pq_order_hash = %{$_prereq_lib_hash_pq{$_name_key}};
    my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ;
	  say __FILE__." Line ".__LINE__.",keys[0]=$_keys[0], pq_order_hash:".Dumper(\%_pq_order_hash) if(defined $_self->{debug} && $_self->{debug}>1);
	  
    for my $_i (0.. scalar(@_keys)-1) {
    	my $_pq = $_pq_order_hash{$_keys[$_i]};  
	    say __FILE__." Line ".__LINE__.",keys[0]=$_keys[0], pq:".Dumper($_pq) if(defined $_self->{debug} && $_self->{debug}>5);
		  my $_app_name=$_pq->{prereq_name};
		  my $_app_version=$_pq->{prereq_version};
		  my $_module_name=$_pq->{module_name};
		  my $_module_version=$_pq->{module_version};
		  next if (!defined $_module_name);
		  #say __FILE__." Line ".__LINE__.", 746 _app_name=$_app_name:_app_version=$_app_version;_module_name=$_module_name;_module_version=$_module_version";
	    if($_i==0){
	    	push(@_commands,"module load $_app_name/$_app_version");
	    	$_pq_lib_str .= " $_app_name/$_app_version";
	    }
	    
		  if( $_kp_name eq "perl" && defined $_module_name){
        push(@_commands,"cpan install $_module_name" ); #perl -MCPAN -e 'install $_module_name'
		  }elsif( $_kp_name=~ m/python/ && defined $_module_name){
			  push(@_commands,"pip install $_module_name==$_module_version") if (defined $_module_name && defined $_module_version);
			  push(@_commands,"pip install $_module_name") if (defined $_module_name && !defined $_module_version);
		  }elsif(defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda} ){
			  my $_build_command = "conda create --prefix $_install_dir_prefix/$_module_name/$_module_version $_module_name =$_module_version"; 
			  if( defined $_channel){
				  $_build_command = $_build_command." --channel $_channel";
			  }
			  push(@_commands,$_build_command);
		  }
		  $_pq_lib_str .= ":$_module_name" if(defined $_module_name);
		  $_pq_lib_str .= ":$_module_name==$_module_version" if(defined $_module_name && defined $_module_version);
		  $_pq_lib_str .= " " if(defined $_module_name);
	  }
	}
  if(scalar @_commands >1){
    $_self->{module_install_command}=join("\n",@_commands);
  }
    say __FILE__." Line ".__LINE__.", ModuleLib_commands: ".Dumper(\@_commands ) if(defined $_self->{debug} && $_self->{debug}>1 );

  return ($_pq_lib_str, \@_commands);
}
sub compile{
  my ($_self)               = @_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_version_file_name    = $_software->{version_file_name};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_install_dir_prefix   = $_client->{install_dir_prefix};
	my $_app_install_log      = $_client->{app_install_log};
	my $_install_user_mark    = $_client->{install_user_mark};
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_build_path           = "$_src_path/build";
	my $_channel              = $_self->{channel};
	my $_to_personal          = 1;
	my $_compile_command      = defined $_self->{general_compile_command} ? $_self->{general_compile_command} : "";
	$_compile_command         = $_client->getGeneral($_compile_command,$_to_personal);
	#say "L789 compile_cmd = $_compile_command";
	my $_install_path         = "$_install_dir_prefix/$_kp_name/$_kp_version";

	say "cd $_src_path" if(-d "cd $_src_path");
	#say  "Software Line ".__LINE__.",($_kp_name,$_version,$_src_dir_prefix,$_install_dir_prefix,$_app_install_log,$_install_user_mark,$_lmod_dir_prefix,$_compile_command_array) ";
  my ($_is_perl, $_is_python)=$_self->fixHeader("$_src_path");
  $_self->{kstat}->{perl}               = 1 if ($_is_perl);
  $_self->{kstat}->{python}             = 1 if ($_is_python);
  $_self->{kstat}->{script_head_perl}   = $_is_perl;
  $_self->{kstat}->{script_head_python} = $_is_python;
 
  my @_commands =(
    "##"
 		,"$_client->{KP} BUILD#" #  1# not log, run; 2# not run, log, 3# log comments
		,"cd $_src_path"
		,"module purge"
	);
  my @_c_header = (
    "##"
	);
	my $_load_module = "module load ";
	if (defined $_self->{module_names} ){
	  my @_module_names  = split ( /[,\s]+/, $_self->{module_names} );
		#say __FILE__." Line ".__LINE__.",  _self->{module_names} = $_self->{module_names} , ".Dumper(\@_module_names);
	  foreach my $_key_value (@_module_names){
			my ($_key, $_value) = split ("=", $_key_value) if ($_key_value =~ m|=|);
			($_key, $_value) = split ("/", $_key_value) if ($_key_value =~ m|/|);
  	  $_software->addPrereq ($_key, $_value) if (defined $_key && defined $_value && $_key =~ m/\S/ && $_value =~ m/\S/);	 
			#say __FILE__." Line ".__LINE__.", k:V  $_key_value, ($_key, $_value), $_software->{name}, ".Dumper($_software->{prereq_hash_ref});
		} 
	}
	if(defined $_software->{prereq_hash_ref} ){
	  #auto load module
	  my $_prereq_hash_ref = $_software->{prereq_hash_ref};
		say __FILE__." Line ".__LINE__.", compile ml software->{prereq_hash_ref}=\n".Dumper($_software->{prereq_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>0);
		my %_ph = %{$_prereq_hash_ref};
		
		# TODO convert java/jdk1.8.0_20 to java/1.8.0
		if((scalar keys %_ph)>0){
			foreach my $_key (sort  keys %_ph) { # sort {$a <=> $b}
				$_load_module .= "$_ph{$_key}->{prereq_name}/$_ph{$_key}->{prereq_version} ";
			}
		}
	}
	my $_configure="configure";
  if(-f "$_src_path/Configure" ){
    $_configure="Configure";
  }
  #perl -e ' system("./configure", "--prefix=/usr/local/apps/libtool/2.4.6 2> >(tee YH.config.err.log >&2) 1> YH.config.log") '
  #sh -c "./configure" --prefix=/usr/local/apps/libtool/2.4.6 2> >(tee YH.config.err.log >&2) 1> YH.config.log
  #perl -e 'print "exist pl?".(-f "*.pl")."\n"'
  my $_rsync = "rsync -avq .  $_install_path  --exclude=*$_app_install_log --exclude=*.log ";
  if (-d "src"){
    $_rsync .= "--exclude=*.src";
  }
  my @_list_of_exclude_ext =("*.c","*.cpp", "*.o",);
  #perl -e 'my $a="*.o"; @f=<${a}>; print @f'
  foreach my $_k (@_list_of_exclude_ext){
  	my @_files = <${_k}>;
		if (scalar @_files > 0){
			$_rsync .= " --exclude=$_k";
		}  
	}
  my @_c_configure=(
    "###make distclean"
    ,"###mkdir -p build"
		,"###cd build"
		,"#./configure --prefix=$_install_path 1>/dev/null "
		,"##./configure --prefix=$_install_path "
		,"#make 1>/dev/null "
		,"##make"
		,"#make install  1>/dev/null "
		,"##make install "
  );
  my @_Makefile_PL=(    
    #"module load $_client->{default_perl}"
		"PREFIX=$_install_path make" #" -q >&1 | tee $_install_user_mark.make.log"
		#,"make  test  >&1 | tee $_install_user_mark.test.log"
		,"mkdir -p \"$_install_path\""
		,"#cd $_src_path"
		#,"[ -d $_install_path ] || mkdir -p $_install_path"
		,"make install " #" >&1 | tee $_install_user_mark.install.log"
		,$_rsync
  );
  my @_c_cmakefile=(
    "mkdir -p $_src_path/build"
		,"cd build"
		,"cmake -DCMAKE_INSTALL_PREFIX:PATH=$_install_path .."
		,"###make -q dist >&1 | tee $_install_user_mark.make.dist.log"
		,"make PREFIX=$_install_path" #" >&1 | tee $_install_user_mark.make.log"
		,"###make test " #" >&1 | tee $_install_user_mark.test.log"
		,"mkdir -p $_install_path"
		,"#cd $_src_path"
		,"make install " # " >&1 | tee $_install_user_mark.install.log"
		,"###$_rsync"
  );
  my @_c_makefile=(
    "###mkdir -p $_build_path"
		,"###make distclean"
		,"###cd $_build_path"
    #,"module load $_client->{default_gcc} "
		,"make PREFIX=$_install_path " #">&1 | tee $_install_user_mark.make.log"
		,"###make test "
		,"mkdir -p $_install_path"
		,"#cd $_src_path"
		,"make install  " #">&1 | tee $_install_user_mark.install.log"
		,"###$_rsync"
  );
  my @_setup_py=(
		"mkdir -p $_install_path/lib/python2.7/site-packages"
		,"export PYTHONPATH=\$PYTHONPATH:$_install_path/lib64/python2.7/site-packages:$_install_path/lib/python2.7/site-packages"
		,"#cd $_src_path"
		,"python setup.py build " #">&1 | tee $_install_user_mark.build.log"
		,"python setup.py install --prefix=$_install_path" #" >&1 | tee $_install_user_mark.install.log" --serenity 
  );
  my @_c_cp=(
		"chmod -R a+rX $_src_path $_install_path"
		#,"cp -R $_src_path $_install_path"
		#,"rm -rf $_install_path/$_app_install_log"
		,"$_rsync"
		,"chown -R $_client->{os_user_name}:$_client->{os_user_group} $_src_path $_install_path"
  );
  my $_conda_cmd = "conda create -y -c defaults -c bioconda -c anaconda -c conda-forge -p $_install_path  $_software->{name}";
  $_conda_cmd .= "=$_software->{version}" if ( defined $_software->{version} );
  my @_conda_install=(
		$_conda_cmd,
  );
  my @_others=(
		"###./configure --prefix=$_install_path   >&1 | tee $_install_user_mark.config.log "
		,"###cmake -DCMAKE_INSTALL_PREFIX:PATH=$_install_path .."
		,"###make"
		,"###mkdir -p \"$_install_path\""
		,"###make install  "
		,"###mkdir -p $_install_path/lib/python2.7/site-packages"
		,"###export PYTHONPATH=\$PYTHONPATH:$_install_path/lib64/python2.7/site-packages:$_install_path/lib/python2.7/site-packages"
		,"###python setup.py build"
		,"###python setup.py install --prefix=$_install_path " 
		,"###"
		,"###export PERL_BASE=\"$_install_path\""
		,"###export PERL_MM_OPT=\"INSTALL_BASE=\$PERL_BASE\""
		,"###export PERL_MB_OPT=\"--install_base \$PERL_BASE\""
		,"###export PERL5LIB=\"\$PERL_BASE/lib/perl5\""
		,"###export PATH=\"\$PERL_BASE/bin\${PATH:+:\$PATH}\""
		,"###export MANPATH=\"\$PERL_BASE/man\${MANPATH:+:\$MANPATH}\""
		,"###perl -MCPAN -e 'install Text::Soundex'"
		,"###ls $_install_path/lib/perl5/x86_64-linux-thread-multi"
		#,"###cp -R $_src_path $_install_path"
		#,"###rm -rf $_install_path/$_app_install_log"
		,"###$_rsync"
  );
 
  my @_c_build=();
	#say "Software Line ".__LINE__.",\t".(!defined $_compile_command_array).",\t".(-f "configure").",\t".(-f "setup.py")."\n";
	if(-f "$_src_path/configure" || -f "$_src_path/Configure"){
    $_load_module .= "$_client->{default_gcc} " if defined $_client->{default_gcc}; 
		@_c_build=@_c_configure;		
		$_self->{kstat}->{config} = 1;
	}elsif((-f "$_src_path/config.ac" || -f "$_src_path/configure.ac") && ! -e "$_src_path/configure"){
	  if(-f "$_src_path/autogen.sh"){
	    unshift (@_c_configure, "./autogen.sh");
	    $_load_module .= "$_client->{default_autoconf}" if defined $_client->{default_autoconf};
	  }else{
	    unshift (@_c_configure, "autoconf");
	    unshift (@_c_configure, "automake --force-missing --add-missing");
	    unshift (@_c_configure, "autoheader");
	    unshift (@_c_configure, "aclocal");
	    unshift (@_c_configure, "libtoolize --force");
	    $_load_module .= "$_client->{default_autoconf} " if defined $_client->{default_autoconf}; #libtool/2.4.6 automake/1.14.1 autoconf/2.69");
	  }
		@_c_build=@_c_configure;		
 		$_self->{kstat}->{config}         = 1;
 		$_self->{kstat}->{complex_build}  = 1;
	}elsif(-f "$_src_path/CMakeLists.txt" || -f "$_src_path/CMakeLists.txt"){
		@_c_build=@_c_cmakefile;		
	  $_load_module                    .= $_client->{default_cmake}  if ($_load_module !~ m/cmake/ && defined $_client->{default_cmake}); 
 		$_self->{kstat}->{config}         = 1;
 		$_self->{kstat}->{complex_build}  = 1;
	} elsif(-f "$_src_path/Makefile.PL" || -f "$_src_path/BUILD.PL"){
		@_c_build                         = @_Makefile_PL;
	  $_load_module .= $_client->{default_perl}   if ($_load_module !~ m/perl/) ; 
 		$_self->{kstat}->{perl}           = 1;
	} elsif(-f "$_src_path/Makefile" || -f "$_src_path/makefile" ){
		@_c_build                         = @_c_makefile;
	  $_load_module                    .=  $_client->{default_cmake}   if ($_load_module !~ m/cmake/ && defined $_client->{default_cmake}); 
 		$_self->{kstat}->{make}           = 1;
	} elsif(defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda} ){
		@_c_build                         = @_conda_install;
	  $_load_module                    .=  $_client->{default_conda}   if ($_load_module !~ m/$_client->{default_conda}/ && defined $_client->{default_conda}); 
	} elsif(-f "$_src_path/setup.py"){
		@_c_build                         = @_setup_py;
    $_load_module                    .= $_client->{default_python} if ($_load_module !~ m/python/ && defined $_client->{default_python}); 
 		$_self->{kstat}->{python}         = 1;
 	} else{
		@_c_build                         = @_others;
		#perl -e '@a=qw (#one zero #on#e ##two ###twp);map {if($_=~ m/^##(?!#)/ ){$_ ="#".$_;}elsif($_!~ m/^###/ ){$_ ="##".$_;} } @a; map {$_."\n"} @a; foreach $_ (@a) {print "$_\n";}'
		map {if($_=~ m/^##(?!#)/ ){$_ ="#".$_;}elsif($_!~ m/^###(?!#)/ ){$_ ="##".$_;} } @_c_build; #fill # 
	}
	if ($_load_module =~ m/^(#)*\s*(module\s+load|ml)\s*$/ ){
	  $_load_module                    = "###ml " ;
	}else{
	  #$_load_module                    = "module purge\n".$_load_module; 
	}
  say __FILE__." Line ".__LINE__."\n".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>4);
  
  if(defined $_compile_command && $_compile_command =~ m/\S/ ){
    $_compile_command =~ s/^#(?!#)/###/gm;
    $_compile_command =~ s/\bcd\s+$_src_path\b\n//;
    $_compile_command =~ s|java/jdk1.8.0_20|java/1.8.0|g; # TODO temp fix for java module inconsistence
		say __FILE__." Line ".__LINE__.", From DB _c_header,_compile_command=$_compile_command, \n".Dumper(\@_c_header) if(defined $_self->{debug} && $_self->{debug}>0);
    push(@_commands,"cd $_src_path",split (/\n/,$_compile_command));
		say __FILE__." Line ".__LINE__.", From DB cmd=$_compile_command\n".Dumper(\@_commands) if(defined $_self->{debug} && $_self->{debug}>2);
  }else{
    push(@_commands,$_load_module, @_c_header, @_c_build);
  }
  $_self->{compile_command} =  join("\n",@_c_build);
  $_self->{compile_command} =~ s|\n(#[^#]+?\n)|\n|g;
  $_self->{compile_command} =~ s|\n+|\n|g;
  $_self->{compile_command} =~ s|##||g;
  $_self->{compile_command} =~ s|###|#|g;
	$_to_personal = 0;
	$_self->{general_compile_command} = $_client->getGeneral($_self->{compile_command},$_to_personal);
  $_self->writeKLogFile;
  $_self->{failed_hash_ref}{compile} = $_self->runCmd( "$_src_path/$_app_install_log",\@_commands); 
  
  my @_c_footer =(
		 "###chmod -R a+rX $_src_path $_install_path"
		,"###chown -R $_client->{os_user_name}:$_client->{os_user_group} $_src_path $_install_path"
		);
  #push (@_c_footer,"##rm $_install_dir_prefix/$_kp_name/latest ") if (-f "$_install_dir_prefix/$_kp_name/latest") ;
  #push (@_c_footer,"##ln -s $_install_path $_install_dir_prefix/$_kp_name/latest ") ;
  if(-d "$_install_path/bin"){
    push (@_c_footer,"##ls -lth $_install_path/bin");
  }else{
    push (@_c_footer,"##ls -lth $_install_path");
  }
  $_self->{failed_hash_ref}{compile} = (defined $_self->{failed_hash_ref}{compile} &&  $_self->{failed_hash_ref}{compile}) ? $_self->{failed_hash_ref}{compile} : $_self->runCmd( "$_src_path/$_app_install_log",\@_c_footer);  
}
sub fixHeader{
  my ($_self, $_dir)=@_;
	my ($_is_perl, $_is_python)=(0,0);
  return if (! -e $_dir || ! -d $_dir);
	opendir( my $_dh, $_dir) or die __FILE__." Line ".__LINE__.",Cannot open $_dir\n";
	my @_files = readdir($_dh); 
  foreach my $_f(@_files){
    next if $_f=~ /^\.\.?$/; # exclude itself and parent
    $_f=$_dir."/".$_f;
    if (-d $_f) {
      my ($_is_perl_2, $_is_python_2)=$_self->fixHeader($_f);
      $_is_perl   += $_is_perl_2;
      $_is_python += $_is_python_2;      
    }else{
      #my $_types = MIME::Types->new;
      say __FILE__." Line ".__LINE__.", f=$_f\n" if(defined $_self->{debug} && $_self->{debug}>7);
      #my $_mime = $_types->mimeTypeOf("$_f");
      #next if ($_mime->isBinary);
      next if (! -f $_f);
      open( my $_fh, '<', $_f )  or die __FILE__." Line ".__LINE__.", Could not open $_f, $!\n"; 
      my $_data='';
      my $_is_changed=0;
      #my $_line = <$_file>;
      my $_iLines=0;
      while ( my $_line = <$_fh>) {
        $_iLines++ if ($_line !~ m/^#/ || $_line !~ m/^\s*$/ );
        last if ($_iLines>1 && $_is_changed==0); 
        if( ($_line =~ m|^#!/usr/bin/perl| || $_line =~ m|#!/usr/bin/env\s+perl\s+-w| || $_fh =~ m/\.pl$/i || $_fh =~ m/\.pm$/i) && $_is_changed==0){ # remove -w
          $_line="#!/usr/bin/env perl\n";
          $_is_perl++;
          $_is_changed=1;
          say "$_f perl file header is replaced" if(defined $_self->{debug} && $_self->{debug}>0);
        }elsif($_line =~ m|^#!/usr/bin/python| && $_is_changed==0){
          $_line="#!/usr/bin/env python\n";
          $_is_python++;
          $_is_changed=1;
          say "$_f python file header is replaced" if(defined $_self->{debug} && $_self->{debug}>0);
        }
        $_data .=$_line;
      } 
      close $_fh;
      if($_is_changed and -w $_f){
        open $_fh, '>', "$_f" or die __FILE__." Line ".__LINE__.", Could not open $_f, $!\n"; 
        print $_fh $_data; 
        close $_fh;
      }
    }
	}
	close $_dh;
  return ($_is_perl, $_is_python);
}
sub makeLmodModuleLua{
  my ($_self, $_is_run)=@_;
	my $_software           = $_self->{software};
	my $_client             = $_self->{client};
	my $_src_dir_prefix     = $_client->{src_dir_prefix};
	my $_install_dir_prefix = $_client->{install_dir_prefix};
	my $_lmod_dir_prefix    = $_client->{lmod_dir_prefix};
	my $_app_install_log    = $_client->{app_install_log};
	my $_install_user_mark  = $_client->{install_user_mark};
	my $_brief              = defined $_software->{brief}        ? $_software->{brief}        : "";
	my $_home_page          = defined $_software->{home_url}     ? $_software->{home_url}     : "";
	my $_wiki_page          = defined $_client->{wiki_list_page} ? $_client->{wiki_list_page} : "";
	my $_kp_name            = $_self->{kp_name};
	my $_kp_version         = $_self->{kp_version};
	my $_install_path       = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_mod_path           = "$_lmod_dir_prefix/$_kp_name";
	my $_src_path           = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_to_personal        = 1;
	my $_mod_file           = "$_mod_path/$_kp_version.lua";
	my $_module_content     = (defined $_self->{general_module_content} && $_self->{checked_level}>2 ) ? $_self->{general_module_content} : "";
	$_module_content        = $_client->getGeneral($_module_content,$_to_personal);
	my $_cmd                = $_self->{general_make_module_command};
	#say __FILE__." Line ".__LINE__.", DB $_cmd";
	$_cmd                   = $_client->getGeneral($_cmd,$_to_personal);
	$_brief                 =~ s/\n+$//s;
  
  my $_command_path = "prepend_path(\"PATH\", app_path) \n";
  if( -d "$_install_path/bin" ){
    $_command_path .="prepend_path(\"PATH\", pathJoin(app_path, \"bin\")) \n";
  }
  my $_include_path = "";
  if( -d "$_install_path/include" && (!defined $_self->{kstat}->{conda} || $_self->{kstat}->{conda}==0 )){
    $_include_path ="prepend_path(\"PATH\", pathJoin(app_path, \"include\")) \n";
  }
  my $_lib_path = "";
  if(-d "$_install_path/lib64" && ! -d "$_install_path/lib64/python" && (!defined $_self->{kstat}->{conda} || $_self->{kstat}->{conda}==0 )){
    $_lib_path ="prepend_path(\"LD_LIBRARY_PATH\", pathJoin(app_path, \"lib64\")) \n"; 
  }
  if(-d "$_install_path/lib" && ! -d "$_install_path/lib/python" && (!defined $_self->{kstat}->{conda} || $_self->{kstat}->{conda}==0 ) ){
    $_lib_path .="prepend_path(\"LD_LIBRARY_PATH\", pathJoin(app_path, \"lib\")) \n"; 
  }
  my $_man_path = "";
  if( -d "$_install_path/man" ){ # todo add share/*man.1? 
    $_man_path ="prepend_path(\"PATH\", pathJoin(app_path, \"man\")) \n";
  }
  #say __FILE__." Line ".__LINE__.", combined, ph_ref=\n".Dumper($_software->{prereq_lib_hash_ref}).Dumper($_software->{prereq_hash_ref});
  my $_python_lib_path = "";
  my $_py_lib_path="$_install_path/lib*/python*/site-packages";
  my @_fh=<"$_py_lib_path">;
  #perl -e '$a="/usr/local/src/mpfr/3.*"; @_f=<"$a">;  print "@_f\n";'
  if ( scalar @_fh){
		$_py_lib_path= $_fh[0];
    #say __FILE__." Line ".__LINE__.", py_lib_path=$_py_lib_path, -d =".($_py_lib_path) if(defined $_self->{debug} && $_self->{debug}>0);
		if(-d $_py_lib_path && (!defined $_self->{kstat}->{conda} || $_self->{kstat}->{conda}==0 ) ){
			$_py_lib_path=~ s|$_install_path/||;
			$_python_lib_path ="prepend_path(\"PYTHONPATH\", pathJoin(app_path,\"$_py_lib_path\")) \n"; 
			say __FILE__." Line ".__LINE__.", python_lib_path=$_python_lib_path\n" if(defined $_self->{debug} && $_self->{debug}>0);
  	}else{
		  $_python_lib_path ="unsetenv(\"PYTHONPATH\")\n"; 
		}
  	#$_python_lib_path .= "\n" if (length($_python_lib_path)>0);
  }
  
  my $_mod_perl_lib_path = "";
  my $_pl_lib_path="$_install_path/lib*"; 
  @_fh=<"$_pl_lib_path">;
  my ($_level, $_is_dir,$_is_last,$_lang)=(2,1,0,"perl");
  @_fh=@{Kpan::KUtil::GetFileHandler($_pl_lib_path, $_level, $_is_dir,$_is_last,$_lang)};
  if( scalar @_fh>0 ){ #todo
		$_mod_perl_lib_path ="";
		for my $_i (0..$#_fh){
			my $_tmp_path= $_fh[$_i];
			$_tmp_path=~ s|$_install_path||;
  		$_mod_perl_lib_path = "prepend_path(\"PERL5LIB\", pathJoin(app_path,\"$_tmp_path\")) \n"; 
		}
		#$_mod_perl_lib_path=$_mod_perl_lib_path."\n" if (length($_mod_perl_lib_path)>0);
  }
  #auto add python/perl module if not exists in prereq
  if($_python_lib_path =~ m/\S/ && !$_self->isExistModule("python") && $_self->{software}->{name} !~ m/^python$|^anaconda$|^conda$|^miniconda$/i && $_self->{software}->{name} !~ m/^(python|anaconda)[\d\.]/i){  #python2.7 python.2.7 
   	my $_default = $_client->{default_python};
  	my ($_d_name,$_d_version) = ($1,$2) if( defined $_default && $_default =~ m|(.+?)/(.+)$|); 
    my $_prereq=Kpan::Prereq->new();
    $_prereq->{name}           = $_software->{name};
    $_prereq->{version}        = ($_software->{version});
    $_prereq->{prereq_name}    = defined $_d_name ? $_d_name :"python";
    $_prereq->{prereq_version} = defined $_d_version ? $_d_version : "2.7.13";						
    $_prereq->{debug}          = $_software->{debug};
    $_prereq->addPrereq($_software->{prereq_lib_hash_ref}, $_software->{prereq_hash_ref}) if (!exists $_software->{prereq_lib_hash_ref}{$_prereq->{prereq_name}});
  }
  if($_mod_perl_lib_path !~ /^\s*$/ && !$_self->isExistModule("perl") && $_self->{software}->{name} !~ m/^perl$/i && $_self->{software}->{name} !~ m/^perl[\d\.]/i ){
   	my $_default = $_client->{default_perl};
    my ($_d_name,$_d_version) = ($1,$2) if( defined $_default && $_default =~ m|(.+?)/(.+)$|); 
    my $_prereq=Kpan::Prereq->new();
    $_prereq->{name}           = $_software->{name};
    $_prereq->{version}        = lc($_software->{version});
    $_prereq->{prereq_name}    = defined $_d_name ? $_d_name :"perl";
    $_prereq->{prereq_version} = defined $_d_version ? $_d_version : "5.24.0-thread";
    $_prereq->{debug}          = $_software->{debug};
    $_prereq->addPrereq($_software->{prereq_lib_hash_ref}, $_software->{prereq_hash_ref}) if (!exists $_software->{prereq_lib_hash_ref}{$_prereq->{prereq_name}});
  }
  my $_java_path="";
  if($_self->isExistModule("java")){
  	$_java_path="setenv(\"CLASSPATH\", app_path)\n";
  }  

  my $_other_paths= "";
  if (defined $_self->{module_exp_pair}){
		my $_module_exp_pair = $_self->{module_exp_pair};
		$_module_exp_pair =~ s/^"|"$//;

		my @_module_exp_pairs = split(/[,\s]+/, $_module_exp_pair); 
		foreach my $_key_value (@_module_exp_pairs){
			my ($_key, $_value)=split ( /=/, $_key_value );
			#say __FILE__." Line ".__LINE__.", $_key_value=($_key, $_value)";
			$_value = "pathJoin(app_path, \"$_value\")" if ($_value =~ m/\S/ && $_value !~ m|^/| ); # wrap to full path
			$_value = "\"$_value\"" if ($_value =~ m|^/| ); # wrap the full path
			my $_set_module_path  = defined $_key ?  $_self->setModulePath($_key) : ""; # prepend / setenv 
			$_other_paths .= "$_set_module_path(\"$_key\",$_value)\n" if (defined $_key && defined $_value && $_key =~ m/\S/ && $_value =~ m/\S/  );
		}
  }
  say __FILE__." Line ".__LINE__.", python_lib_path=$_python_lib_path, p_lib_hash=\n".Dumper($_software->{prereq_lib_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>0);
  say __FILE__." Line ".__LINE__.", perl_lib_path=$_mod_perl_lib_path, p_hash=\n".Dumper($_software->{prereq_hash_ref}) if(defined $_self->{debug} && $_self->{debug}>0);
  my $_ph_ref = $_software->getCombinePrereqHashRef;
  say __FILE__." Line ".__LINE__.", combined, ph_ref=\n".Dumper($_ph_ref) if(defined $_self->{debug} && $_self->{debug}>2);
  #auto load module
 	my $_load_module="";
  #say __FILE__." Line ".__LINE__.", ph_ref=\n".Dumper($_ph_ref) ;
 	if(defined $_ph_ref && (scalar keys %{$_ph_ref})!=0){
    $_load_module="load( ";
    foreach my $_key (sort {$a <=> $b} keys %{$_ph_ref}) { #sort {$a <=> $b} 
      if (defined $$_ph_ref{$_key}->{prereq_name} && $$_ph_ref{$_key}->{prereq_name} =~ m/\S/  ){
				my $_tmp_mod = "$$_ph_ref{$_key}->{prereq_name}";
        if(defined $$_ph_ref{$_key}->{prereq_version} && $$_ph_ref{$_key}->{prereq_version} =~ m/\S/){
       	  $_tmp_mod .= "/$$_ph_ref{$_key}->{prereq_version}";
        }
        if($_load_module =~  m|^load\(\s*\S| ){ # not first one
          $_load_module .=", "
        }
        $_load_module .= "\"$_tmp_mod\"" if ($_load_module !~ m/$_tmp_mod/);
      }
    }
    $_load_module .=" )\n";
  }	
  
  my $_hint = "\nDescription\n===========\n $_brief\n\nHomepage\n========\n $_home_page\n\nMore information\n================\nVisit our wiki\n";
  my $_whatis  = "whatis([[$_hint]])\n";
  my $_help = "help([[$_hint]])";
  my $_push = "-- sh $_client->{lmod_app_path}/libexec/createSystemCacheFile.sh\n\n";
 # $_push = "-- sh  /usr/local/src/lmod/Lmod-5.8/contrib/BuildSystemCacheFile/createSystemCacheFile.sh\n\n" if(defined $_client->{app_install_log} && $_client->{app_install_log} =~ /UGA/i);
  my $_family_name = $_kp_name;
  #$_family_name    = "python" if ((defined $_self->{kstat}->{conda} &&  $_self->{kstat}->{conda} )|| $_family_name =~ m/python/i ) ;
  $_family_name    = "python" if ($_family_name =~ m/^anaconda|^miniconda|^bioconda|^python/i) ;
  $_family_name    =~ s/[_-]//g ; # lmod family doesn't take '-' '_'
  my $_mod_text=
		"-- $_install_user_mark ".`date +%m/%d/%y`."\n".
		"family (\"$_family_name\")\n".
		"local base       = \"$_install_dir_prefix/$_kp_name\"\n".
		"local version    = \"$_kp_version\"\n".
    "local app_path   = pathJoin(base, version)\n".
		"\n".
		# setenv("KPAN_DATA", pathJoin(app_path, "data"))
		"$_load_module".
		"$_command_path".
		"$_include_path".
		"$_lib_path".
		"$_python_lib_path".
		"$_mod_perl_lib_path".
		"$_man_path".
		"$_java_path".
		"$_other_paths".
		"\n".
		"$_whatis".
		"\n".
		"$_help".
		"\n".
		$_push
	;
  #say __FILE__." Line ".__LINE__.", make moudle mod_text=$_mod_text" if(defined $_self->{debug} && $_self->{debug}>0);
	if (defined $_module_content && $_module_content =~ m/\S/){
  #say __FILE__." Line ".__LINE__.", make moudle module_content=$_module_content" if(defined $_self->{debug} && $_self->{debug}>0);
		$_mod_text = $_module_content;
		$_mod_text =~ s/^(.+?)\n//s; # remove first line
		$_mod_text = "-- $_install_user_mark ".`date +%m/%d/%y`.$_mod_text;
	}
	if(-d $_install_path){
		$_self->mkDir("$_mod_path");
		#qx("ls -l $_mod_path/");
		open my $_mod_lua_file, ">", $_mod_file or die __FILE__."Line ".__LINE__.",  couldn't open mod file $_mod_file for writing: $!\n";
		say $_mod_lua_file "$_mod_text";
	  close($_mod_lua_file);
  }
	$_to_personal     = 0;
	$_module_content  = $_client->getGeneral($_module_content,$_to_personal);
	$_self->{general_module_content} = $_module_content;
	
  #my $_write_module_cmd = "module load kpan\n## kpan --write_module_file --k_log $_src_path/$_app_install_log\n\n";
  #$_write_module_cmd    = "module load kpan\n##kpan --write_module_file --k_log $_src_path/$_app_install_log\n\n" if (!defined $_self->{is_failed} || $_self->{is_failed}==0);
  my @_commands=(
    "##"
		,"$_client->{KP} MODULE#"
  );	
  my $_chmod_grp = (-d $_mod_path)? "chmod 775 $_mod_path":"### chmod 775 $_mod_path";
  my $_chmod_lua = (-f $_mod_file)? "chmod 664 $_mod_file":"### chmod 664 $_mod_file";
  my @_commands_cmd=(
    "##mkdir -p $_mod_path"
    ,$_chmod_grp
    ,"##"
    ,"##module load kpan"
    ,"##kpan --write_module_file --k_log $_src_path/$_app_install_log \n#--module_exp_pair \"PATH=$_install_path/script PERL5LIB=$_install_path/lib\""
    ,"##"
    ,"### vi $_mod_file"
		,"### cat $_mod_file"
		,$_chmod_lua
  );	
  if(defined $_cmd ){
    $_cmd =~ s/^#(?!#)/###/gm;
    @_commands_cmd=split (/\n/,$_cmd) if($_cmd =~ m/\S\n/);
  }
  $_self->{make_module_command} = join("\n", @_commands_cmd);
  $_self->{general_make_module_command} = $_client->getGeneral($_self->{make_module_command},$_to_personal);
  $_self->writeKLogFile;
  push(@_commands, @_commands_cmd);  
  my $_is_log=undef;
  #$_is_run=0;
  $_self->{failed_hash_ref}{makeLmodModuleLua} = $_self->runCmd( "$_src_path/$_app_install_log" ,\@_commands, $_is_log, $_is_run);
}

sub setModulePath {
	my ($_self, $_key)=@_;

	my %_prepend_paths_hash = ( 
	    PATH            => "", 
	    LD_LIBRARY_PATH => "", 
	    PYTHONPATH      => "", 
	    PERL5LIB        => ""
	  );
  my $_str = exists $_prepend_paths_hash{$_key} ? "prepend_path" : "setenv";
  return $_str;
}

sub installTest{
  my ($_self, $_is_before_install_test, $_number_of_tests)=@_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_install_dir_prefix   = $_client->{install_dir_prefix};
	my $_lmod_dir_prefix      = $_client->{lmod_dir_prefix};
	my $_app_install_log      = $_client->{app_install_log};
	my $_install_user_mark    = $_client->{install_user_mark};
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
  my $_install_path         = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
  my $_to_personal          = 1;
	my $_install_test_command = (defined $_self->{general_test_command} && $_self->{checked_level}>2 ) ? $_self->{general_test_command} : "";
	$_install_test_command    = $_client->getGeneral($_install_test_command,$_to_personal);
	$_self->{is_installed}    = 1 ;
  my $_mod_file                = "$_client->{lmod_dir_prefix}/$_kp_name/${_kp_version}.lua";
  my $_is_exist_module      = (-f "$_mod_file" ) ? 1 : 0;
  #my $_is_exist_command     = 0 ;

  my $_install_test             = $_self->{install_test};
  # done at install begging, delete? 
  $_install_test->{name}                   = $_software->{name};
  $_install_test->{kp_name}                = $_kp_name;
  $_install_test->{version}                = $_kp_version;
  $_install_test->{kp_version}             = $_kp_version;
  $_install_test->{debug}                  = $_self->{debug};
  $_install_test->{KP}                     = $_client->{KP};
  $_install_test->{lmod_app_path}          = $_client->{lmod_app_path};
  $_install_test->{file_name_prefix}       = $_client->{install_user_mark};
  $_install_test->{file_core_name}         = $_kp_name;
  #say __FILE__." Line ".__LINE__.", install_test->{name} =$_install_test->{name} ".Dumper($_install_test);
 # collect exists    
  if ( !defined $_install_test->{exist_dirs} || $_install_test->{exist_dirs} =~ m/^\s*$/ ){
    $_install_test->{exist_dirs}          = "bin " if (-d "$_install_path/bin");
    $_install_test->{exist_dirs}         .= "lib " if (-d "$_install_path/lib");
  }
  my $_exist_files ;
  if (!defined $_install_test->{exist_files} || $_install_test->{exist_files} =~ m/^\s*$/ ){
    my @_path;
    push (@_path, "$_install_path/bin") if (-d "$_install_path/bin");
    push (@_path, $_install_path) if (-d $_install_path);
    foreach my $_p (@_path){
      if (-d $_p){
        my @_fh=<"$_p/*">;
        foreach my $_f (@_fh){
					next if (-d $_f);
          $_f                           =~ s|$_install_path||;
          $_f                           =~ s|^/+||;
          $_exist_files                .= "$_f " if (!defined $_exist_files || $_p =~ m|"$_install_path/bin"| 
                                          || ($_f =~ m/^$_software->{name}$/i) 
                                          || ($_f =~ m/$_software->{name}/i && $_exist_files !~ m/^$_software->{name}$/i)  ) ;
         $_install_test->{exist_files} = $_f if (!defined $_install_test->{exist_files} || $_install_test->{exist_files} =~ m/^\s$/);
        }
      }        
    }
  }else{
		$_exist_files = $_install_test->{exist_files};
		$_exist_files =~ s|$_install_path||g;
  }
  #say __FILE__." Line ".__LINE__.",L1319 install_test_command=$_install_test_command";
  # get test command and help para
  if ( !defined $_install_test_command || !defined $_self->{checked_level}  || (defined $_self->{checked_level} && $_self->{checked_level}<3) || !defined $_install_test->{command} || $_install_test->{command}  =~ m/^\s*$/ 
    || !defined $_install_test->{help_para} || $_install_test->{help_para} =~ m/^\s*$/  ) {
    my @_test_commands_help_para = ();
    my @_help=("-h","--help","");

    my $_run_cmd_arr_ref = _getRunCommand($_install_path, $_exist_files);
    my @_test_commands = defined $_run_cmd_arr_ref ? @{$_run_cmd_arr_ref}:();
    #say "L1318 _t->{exist_files}=$_install_test->{exist_files}, ".Dumper(\@_test_commands);
    my @_main_cmd_arr = ();
    my $_i_main_cmd_index = -1;
    my $_main_cmd="";
    foreach my $_i (0..$#_test_commands) {
      $_i_main_cmd_index  = $_i  if ($_test_commands[$_i] =~  m|^$_software->{name}$|i  || ($_i_main_cmd_index  < 0 && $_test_commands[$_i] =~  m|$_software->{name}|i));
		}
		if($_i_main_cmd_index  >= 0 ){
			$_main_cmd = $_test_commands[$_i_main_cmd_index ];
		  splice (@_test_commands,$_i_main_cmd_index ,0);  
    }
    #todo =""
    $_main_cmd = "## $_software->{name}" if($#_test_commands ==0);
	  unshift (@_test_commands,$_main_cmd); 
    #say __FILE__." Line ".__LINE__.",L1326 _i_main_cmd_index=$_i_main_cmd_index, main_cmd=$_test_commands[0], number_of_tests=$_number_of_tests/$#_test_commands, commands=".Dumper(\@_test_commands); 
    # legacy, one test only
	  $_number_of_tests = $#_test_commands if (!defined $_number_of_tests || (defined $#_test_commands && $_number_of_tests > $#_test_commands));
	  $_number_of_tests = 1;
	  
    if (defined $_is_exist_module && $_is_exist_module>0){
      my $_lmod_cmd = "$_client->{lmod_app_path}/libexec/lmod perl load $_kp_name/${_kp_version}";  ;
      eval `$_lmod_cmd`;
      for my $i ( 0 .. $_number_of_tests ){
        next if ( !defined $_test_commands[$i] || $_test_commands[$i] =~ m/^\s*$/  );
        #next if ( $_test_commands[$i] =~ m/^\s*$/ && Kpan::KUtil::IsCmdExist($_test_commands[$i]) == 0 );
        say __FILE__." Line ".__LINE__.",check installed, test_commands[$i]= ".$_test_commands[$i].", exist?=".(Kpan::KUtil::IsCmdExist($_test_commands[$i])) ;#if(defined $_self->{debug} && $_self->{debug}>0); 
        $_test_commands_help_para[$i]     = "";
        foreach ( @_help ){
          $_test_commands_help_para[$i] = Kpan::KUtil::Trim("$_");
          $_install_test->{command}                  =  $_test_commands[$i]; #$_main_cmd
          $_install_test->{command}                  =~ s/[\s\n]$//; 
          $_install_test->{help_para}                =  $_test_commands_help_para[$i];   
          #$_is_exist_command              =  1;  # exist in path and ENV                
          do {$_install_test_command      =  "$_install_test->{command} $_install_test->{help_para}"; last;} if ( system("$_install_test->{command} $_install_test->{help_para} &>/dev/null ") );
        }        
      }
    }
    $_install_test_command              =  $_main_cmd if (!defined $_install_test_command); 
    $_install_test_command              =~ s/^#+(?!#)/###/gm;
    $_install_test_command              =~ s/^\s+//;
    $_install_test_command              =~ s/\s+$//;
    $_install_test_command              = "conda activate $_install_path\n$_install_test_command\nconda deactivate\n" if(defined $_install_test_command && defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda} );
    $_install_test->{command}           = "conda activate $_install_path && $_install_test->{command}" if(defined $_install_test->{command} &&  defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda} );

    $_self->{install_test_command}      =  $_install_test_command;  
  }
  #checking existence
  if (defined $_install_test->{exist_dirs} && $_install_test->{exist_dirs} =~ m/\S+/){
    my @_exist_dirs = split (' ', $_install_test->{exist_dirs});
    foreach my $_d (@_exist_dirs){
      my $_full_d          = "$_install_path/$_d";
      $_self->{is_installed} = 0  if (!-d $_full_d); 
    }
  }
  if (defined $_install_test->{exist_files} && $_install_test->{exist_files} =~ m/\S+/){
    my @_exist_files = split (' ', $_install_test->{exist_files});
    foreach my $_f (@_exist_files){
      my $_full_f            = "$_install_path/$_f";
      $_self->{is_installed} = 0 if (!-f $_full_f); 
    }
  }   
  if ((!defined $_install_test->{exist_files} || $_install_test->{exist_files} =~ m/^\s*$/) && (!defined $_install_test->{exist_dirs} || $_install_test->{exist_dirs} =~ m/^\s*$/) ){
		$_self->{is_installed} = 0;
  }
  say __FILE__." Line ".__LINE__.",check installed, m=$_client->{lmod_dir_prefix}/$_kp_name/${_kp_version}.lua, is_exist_module=$_is_exist_module,  "
  ."{is_installed}=$_self->{is_installed}, install_test_command=$_install_test_command" if(defined $_self->{debug} && $_self->{debug}>0); 
  
  # todo no need if no lmod
  $_self->{is_installed} = 0 if (!$_is_exist_module );  
  if ($_is_before_install_test){
    return;    
  }
  #my $_ktest_cmd              =  "##kpan --test_install  --k_log $_src_path/$_app_install_log \n\n";	  
  #$_ktest_cmd                 = "## $_ktest_cmd"                         if ( $_self->{is_installed} == 0 && (!defined $_self->{is_failed} || $_self->is_failed==0) ) ;
  my $_ls_exist_files         = "";
  if (defined $_install_test->{exist_files} && $_install_test->{exist_files} =~ m/\S+/) {
    my @_files                = split (' ', $_install_test->{exist_files})        if (defined $_install_test->{exist_files} );
    my @_full_files           = map   {$_install_path."/".$_ if ($_ !~ m/^$_install_path/) } @_files; 
		$_ls_exist_files = pop  @_full_files;
	}elsif(defined $_install_test->{exist_dirs} && $_install_test->{exist_dirs} =~ m/\S+/){
    my @_dirs                 = split (' ', $_install_test->{exist_dirs})         if (defined $_install_test->{exist_dirs} );
    my @_full_dirs            = map   {$_install_path."/".$_  if ($_ !~ m/^$_install_path/)} @_dirs;
		$_ls_exist_files          = pop  @_full_dirs;
	}
  $_ls_exist_files            = (!defined $_ls_exist_files || $_ls_exist_files =~ m/^\s*$/) ?  "##ls -lh $_install_path" : "##ls -lh $_ls_exist_files";
  my $_is_dry_run             = (defined $_self->{is_installed} && $_self->{is_installed} == 0 ) ? 1 : 0 ;
 
  $_install_test->testCmd($_client->{lmod_app_path}, $_is_dry_run ) if ( $_self->{is_installed} == 0 && (!defined $_self->{is_failed} || $_self->{is_failed}==0) ) ; 
  $_install_test_command      = "##" if (!defined $_install_test_command || $_install_test_command  =~ m/^\s*$/);
  $_install_test->{file_name} = $_client->{install_user_mark}.".test.".$_kp_name.".log" if (!defined $_install_test->{file_name} );
  my @_commands   = (
       "##"
     , "$_client->{KP} TEST#"
     , "$_ls_exist_files"
     , "cd $_src_path/"
     , "##module purge" 
     , "##module load $_kp_name/$_kp_version"
     , "$_install_test_command"
     , "##module load kpan"
     , "##kpan --test_install  --k_log $_src_path/$_app_install_log"
     , "##head  -n 30 $_install_test->{file_name}"
  );
  
  say __FILE__." Line ".__LINE__." is_exist_module=$_is_exist_module,  self->{is_installed} =$_self->{is_installed}  \n".Dumper($_install_test) if(defined $_self->{debug} && $_self->{debug}>0); 

  my $_is_log = undef;
  my $_is_run = undef;

  $_self->runCmd( "$_src_path/$_app_install_log" ,\@_commands,$_is_log,$_is_run);  
  $_to_personal = 0;
  $_self->{install_test_command} =~ s|#(.+?)\n||;
  $_self->{general_test_command} = $_client->getGeneral($_self->{install_test_command},$_to_personal);
  $_self->writeKLogFile;
  # remove module if not sucessful installed

  if ( !$_self->{is_installed} ){
    system ("rm $_mod_file") if (-f "$_mod_file");
    my $_is_empty = Kpan::KUtil::IsEmptyDir("$_client->{lmod_dir_prefix}/$_kp_name");        
    system ("rm -rf $_client->{lmod_dir_prefix}/$_kp_name") if ($_is_empty==0);
  }  	
}
sub _getRunCommand{
  my ($_path, $_files) = @_;
  return if (!defined $_path || !-e $_path || !defined $_files );
  $_path  =~ s|/$||;
  my @_fs  =  split (' ', $_files);
  my @_full_fs = map {$_path."/".$_} @_fs;
  my @_commands=();
  foreach my $i ( 0..$#_fs ){
    my $_f = $_full_fs[$i];
    next if (!-f $_f);
    if($_f =~ m/\.pl$/ ){
      push (@_commands, "perl $_f"); 
    }elsif($_f =~ m/\.py$/ ){
      push (@_commands, "python $_f"); 
    }elsif($_f =~ m/\.R$/ ){
      push (@_commands, "R CMD BATCH $_f"); 
    }elsif($_f =~ m/\.sh$/ ){
      push (@_commands, "sh $_f"); 
    }elsif(-x $_f){ #binary
      $_f =~ s|(.+)/|| ;  # remove absolute path
      push (@_commands, "$_f");       
    }
  }
  return \@_commands;
}
sub installLogUpload{
  my ($_self)               = @_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_app_install_log      = $_client->{app_install_log};
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
	my $_install_dir_prefix   = $_client->{install_dir_prefix};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_lmod_dir_prefix      = $_client->{lmod_dir_prefix};
	my $_install_path         = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_mod_path             = "$_lmod_dir_prefix/$_kp_name";
  my $_log_file             = "$_src_path/$_app_install_log";
  my $_mod_file             = "$_mod_path/$_kp_version.lua";
  my $_home_url             = $_software->{home_url};
  my $_archive_url          = $_software->{archive_url};
  $_home_url                = "" if(!defined $_home_url);
  $_archive_url             = "" if(!defined $_archive_url);
  my $_brief                = $_self->{brief};
  $_brief                   = $_software->{brief} if (!defined $_brief || $_brief =~ m/^\s*$/ );
  $_brief                   = defined $_brief ? $_brief : "";
  my $_bash_brief           = $_brief;
  $_bash_brief              =~ s/'/''/g; # :' needs to excape ' during context

	my $_to_personal          = 1;
	my $_install_upload_command = (defined $_self->{general_upload_command} && $_self->{checked_level}>2) ? $_self->{general_upload_command} : "";
	$_install_upload_command  = $_client->getGeneral($_install_upload_command,$_to_personal);

  my @_commands=(
    "##"
    ,"$_client->{KP} BRIEF#"
    ,"##$_self->{multiline_start}\n$_bash_brief"
    ,"##$_self->{multiline_end}"
		,"$_client->{KP} UPLOAD#"
  );
  # kpan -ti -n cutadapt -v 1.14 --debug 1 -hp "--help"

  say __FILE__." Line ".__LINE__.", check install self, hu=$_software->{home_url}, " and $_self->print if(defined $_self->{debug} && $_self->{debug}>4);
  my $_install_upload_command_fly = $_self->getUploadCmd;
  if( defined $_install_upload_command && $_install_upload_command =~ m/\S+/ && $_self->{checked_level}<3 ) {
    $_install_upload_command_fly =  $_install_upload_command;
  	$_install_upload_command_fly =~ s/^(?!##)/##/gm; # add to avoid real run
    say __FILE__." Line ".__LINE__.", install_upload_command=$_install_upload_command, _fly=$_install_upload_command_fly" if(defined $_self->{debug} && $_self->{debug}>4);
  }
  my @_commands_cmd = split("\n",$_install_upload_command_fly);  
  push(@_commands,@_commands_cmd);
  #push(@_commands,"\n"); 
  push(@_commands,"##"); 
  push(@_commands,"$_client->{KP} NOTES#");
  push(@_commands,"##");
  push(@_commands,"$_client->{KP} STAT#");
  push(@_commands,"##".$_self->{multiline_start});
  my $_kstat            = $_self->{kstat};
  $_kstat->{install_id} = $_self->{id} if (defined $_self->{id});
  $_kstat->{app_id}     = $_software->{id} if (defined $_software->{id});
  $_kstat->{client_id}  = $_client->{id} if (defined $_client->{id});
  my %_kh               = %{$_kstat};
  foreach my $_k (sort keys %_kh){
    push(@_commands,"##$_k=$_kh{$_k}") if (defined $_kh{$_k} && $_kh{$_k} =~ m/^\d+$/ && $_k !~ m/^debug$/ );
  }
  push(@_commands,"##$_self->{multiline_end}");
  $_self->runCmd( "$_src_path/$_app_install_log" ,\@_commands);  

  $_to_personal                    = 0;
  $_install_upload_command         =~ s/\n+/\n/g;
  $_install_upload_command         =~ s/##//g;
  $_self->{uplaod_command}         = $_install_upload_command;
	$_self->{general_upload_command} = $_client->getGeneral($_install_upload_command,$_to_personal);
  $_self->writeKLogFile;
}

sub logDefaults{
  my ($_self)                 = @_;
	my $_software               = $_self->{software};
	my $_client                 = $_self->{client};
	my $_src_dir_prefix         = $_client->{src_dir_prefix};
	my $_kp_name                = $_self->{kp_name};
	my $_kp_version             = $_self->{kp_version};
	my $_log_dir                = "$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}";
	my $_log_file               = "$_log_dir/defaults.log";
	return if (-e $_log_file);
	$_self->mkDir("$_log_dir") if ( !-e "$_log_dir");
	say __FILE__." Line ".__LINE__.", log is $_log_file " if(defined $_self->{debug} && $_self->{debug}>4);
  open my $_fout, "> $_log_file" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_file for write: $!\n";
  my %_client_hash = %{$_client};
	foreach my $_key (keys %_client_hash){
		if($_key =~ m/^default_/i ){   
	    #print __FILE__.", line ". __LINE__.", ***b4 =after strip $_key, $_self->{$_key}";
		  say $_fout "$_key = ".$_client->{$_key} if (defined $_client->{$_key});
		}
	}  
}
sub getUploadCmd {
  my ($_self)                 = @_;
	my $_software               = $_self->{software};
	my $_client                 = $_self->{client};
	my $_install_dir_prefix     = $_client->{install_dir_prefix};
	my $_src_dir_prefix         = $_client->{src_dir_prefix};
	my $_lmod_dir_prefix        = $_client->{lmod_dir_prefix};
	my $_app_install_log        = $_client->{app_install_log};
	my $_kp_name                = $_self->{kp_name};
	my $_kp_version             = $_self->{kp_version};
	my $_install_path           = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path               = "$_src_dir_prefix/$_kp_name/$_kp_version";
  my $_log_file               = "$_src_path/$_app_install_log";
  my $_mod_file               = "$_lmod_dir_prefix/$_kp_name/$_kp_version.lua";
  my $_home_url               = $_software->{home_url};
  my $_archive_url            = $_software->{archive_url};
  $_home_url                  = "" if(!defined $_home_url);
  $_archive_url               = "" if(!defined $_archive_url);
  my $_brief                  = $_self->{brief};
  $_brief                     = $_software->{brief} if (!defined $_brief || $_brief =~ m/^\s*$/ );
  $_brief                     = defined $_brief ? $_brief : "";
  $_software->{name}          = defined $_software->{name} ? $_software->{name}:"tmp";
  my $_klog_cmd       = "##kpan --upload_k_log --upload_module_file --update_wiki --k_log $_log_file";
  my $_uninstall_cmd  = "### kpan --remove_install -in $_kp_name -iv $_kp_version";
  $_uninstall_cmd     .= " --channel conda" if (defined $_self->{kstat}->{conda} && $_self->{kstat}->{conda});
  my $_version_file_name = defined $_software->{version_file_name}? $_software->{version_file_name}:"";
  my $_parameters     = "###installation parameters \n"
                        ."##$_self->{multiline_start}\n"
                        ."##-n $_software->{name} \n"
                        ."##-v $_software->{version} \n"
                        ."##-in $_kp_name \n"
                        ."##-iv $_kp_version \n";
     $_parameters   .=  "##--test_cmd_file ".((defined $_self->{install_test} && $_self->{install_test}->{file_name})? "$_self->{install_test}->{file_name}":"")." \n"
                        ."##--module_file $_mod_file \n"
                        ."##--category $_software->{category} \n";
    $_parameters   .=   "##--app_home_url ".(defined $_software->{home_url}? "$_software->{home_url}":"")." \n";
    $_parameters   .=   "##--app_archive_url ".(defined $_software->{archive_url}? "$_software->{archive_url}":"")." \n";
    $_parameters   .=   "##--cluster_system $_client->{cluster_system} \n"
                        ."##--app_version_file_name "."$_src_dir_prefix/$_kp_name/$_version_file_name \n"
                        ."##$_self->{multiline_end}"
                        ;

  say __FILE__." Line ".__LINE__.", klog_cmd=$_klog_cmd" if(defined $_self->{debug} && $_self->{debug}>0);
  my @_commands_cmd=(
		"##module use ~/modulefiles"
		,"##module load kpan"
		,"##"
    ,"$_klog_cmd"
		,"##"
    ,"$_uninstall_cmd"
    ,"##"
    ,"$_parameters"
   );
   my $_install_upload_command = join ("\n", @_commands_cmd);
   return $_install_upload_command;
}
sub mkDir{
  my ($_self, $_d)=@_;
  say __FILE__." Line ".__LINE__.", trying to make dir $_d" if(defined $_self->{debug} && $_self->{debug}>0);
  my $_pwd = cwd();
  $_d =~ s/"//g;
  my @_folders = split /\/|\\/, $_d;
  say __FILE__." Line ".__LINE__.", trying to make dir $_d, b4 -p folders".Dumper(\@_folders) if(defined $_self->{debug} && $_self->{debug}>0);
  shift @_folders if (defined $_folders[0] && $_folders[0] =~ m/^\s*$/);
  $_folders[0]="/".$_folders[0]  if (defined $_folders[0] && $_folders[0] =~ m|^[^/]|);
  say __FILE__." Line ".__LINE__.", trying to make dir $_d, -p folders".Dumper(\@_folders) if(defined $_self->{debug} && $_self->{debug}>0);
  
  foreach my $_td (@_folders){
    #map { mkdir $_ if(!-d $_); chdir $_ if(-d $_); } @_folders;  
    mkdir $_td if (! -d $_td);
    #mkdir -p $_d unless -d $_d;  #mkdir -p $_d unless -d $_d;  
    die __FILE__." line ".__LINE__." at ".cwd()." $_td is not writable\n" if(!-d $_td);
    chdir $_td; 
  }
  chdir ($_pwd);
  die __FILE__." line ".__LINE__."$_d is not writable\n" if(!-d $_d); 
}
sub runCmd{
  my ($_self, $_log_file, $_cmd_array,$_is_log,$_is_run)=@_;
	my $_client                 = $_self->{client};
	my $_src_dir_prefix         = $_client->{src_dir_prefix};
	my $_kp_name                = $_self->{kp_name};
	my $_kp_version             = $_self->{kp_version};
	my $_full_install_log       = "$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}/$_self->{full_log}";
	my @_commands   = @{$_cmd_array};
  my $_is_fail    = 0;
  my $_failed_cmd = "";
  $_is_log        = 0 if (!defined $_is_log);
 	open my $_fout, ">> $_log_file" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_file for write: $!\n";
 	$_self->mkDir("$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}") if ( ! -e -d "$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}");
 	open my $_fullout, ">> $_full_install_log" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_full_install_log for write: $!\n";
  foreach my $_cmd (@_commands){
    $_cmd         = Kpan::KUtil::Trim($_cmd);
    say __FILE__." Line ".__LINE__.", $_cmd, log is $_log_file,_is_fail=$_is_fail, _is_log=$_is_log" if(defined $_self->{debug} && $_self->{debug}>0);
   	say $_fullout $_cmd;
   	if(defined $_is_log && $_is_log>0){
      say $_fout "$_cmd";
      next;
		}
		# $MODULEPATH
  	# perl -e ' eval `/usr/local/apps/lmod/lmod/libexec/lmod perl load java/1.8.0`; eval `/usr/local/apps/lmod/lmod/libexec/lmod perl list`; '
 	  # perl -e 'eval(system "/usr/local/apps/lmod/lmod/libexec/lmod perl load java/1.8.0"); system "/usr/local/apps/lmod/lmod/libexec/lmod perl list"; '
 	  # perl -e '@a=("module load gmp/4.2.1  java/1.8.0","module list"); foreach $i(@a) {print "i=$i\n";$i=~m/^module(\s+?)(.+)$/; $cmd=$2;print "cmd=$cmd\n"; eval `/usr/local/apps/lmod/lmod/libexec/lmod perl $cmd`; } '
 	  # perl -e '$a="##"; $a=~ s/^##//; print "$a\n" '
    if(defined $_is_run && $_is_run>0){ #1 not log   2# not run, log, 3# log comments
      next;
    }elsif(($_cmd =~ m/^$_client->{KP}/)  ){ ### KP
      say $_fout "$_cmd";
      next;
    }elsif(($_cmd =~ m/^\#\#/)  ){ #1 not log   2# not run, log, 3# log comments
      my $_cmd2=$_cmd;
      $_cmd2 =~ s/^##//; # remove 2 #
      say $_fout "$_cmd2";
      next;
    }elsif( $_cmd=~ m/^[^#]/) { #1 not log   2# not run, log, 3# log comments
      say $_fout "$_cmd";
    }elsif($_cmd =~ m/^\s*$/ ||  $_cmd =~ m/^kpan\s$/){ #empty line or kpan command
      next;
		}
    if(!$_is_fail){ #   1# not log, run; 
      $_cmd =~ s/^\#// if($_cmd =~ m/^\#/); # remove one #
	    #say __FILE__." Line ".__LINE__.", $_cmd, _is_fail=$_is_fail";
		  if( $_cmd =~ m/^kpan(\s+)(\S+)/ || $_cmd =~ m/^(module load|ml) kpan(\s)*$/ ){
		  }elsif( $_cmd =~ m/^cd(\s+?)(\S+)/){
		    my $_d= $2;
		    eval { chdir $_d }; warn $@ if $@;
		    if(defined $ENV{"$_d"}){
		      $ENV{PWD}="$_d:$ENV{PWD}";
		    }else{
		      $ENV{PWD}="$_d";
		    }
				say __FILE__." Line ".__LINE__.", $_cmd, $_d, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
		  }elsif($_cmd =~ m/^mkdir/){ #
		  	$_cmd =~ s/^mkdir(\s+?)-p\s+/mkdir /; # remove -p
		  	$_cmd =~ /^mkdir(\s+?)(.+)$/;
		  	my $_d=$2;
		  	$_self->mkDir($_d); 
		    say __FILE__." Line ".__LINE__.", $_cmd, $_d, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
		  }elsif($_cmd =~ m/^mv/){ #
		  	$_cmd =~ m/^mv(\s+?)(\S+?)(\s+?)(.+)$/;
		  	my $_from=$2;
		  	my $_to=$4;
		  	move ($_from,$_to) if (-e $_from); 
		    say __FILE__." Line ".__LINE__.", $_cmd, $_from, $_to, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
  	  }elsif($_cmd =~ m/^module(\s+?)(.+)$/ || $_cmd =~ m/^ml(\s+?)(.+)$/){
		  	my $_cmd2=$2;
		    eval `$_self->{client}->{lmod_app_path}/libexec/lmod perl $_cmd2`;
		  }elsif($_cmd =~ m/^export(\s+?)(\S+?)=(.+)/){
		    my $_env_name=$2;
		    my $_env_value=$3;
		    if(defined $ENV{$_env_name}){
		      $ENV{$_env_name}="$_env_value:$ENV{$_env_name}";
		    }else{
		      $ENV{$_env_name}="$_env_value";
		    }
		  }elsif(!$_is_fail && $_cmd =~ m/\S/ ){
		    say __FILE__." Line ".__LINE__.", $_cmd" if(defined $_self->{debug} && $_self->{debug}>1);
        $_is_fail = eval {system "$_cmd"}; 
        if($_is_fail){
        	my $_pwd = cwd();
          $_failed_cmd=$_cmd;
        }
      }
    }
  }  
	if($_is_fail){
		$_self->{is_failed} = 1;
   	my $_pwd = cwd();
		say $_fout "failed at $_failed_cmd, pwd is $_pwd";
		say $_fullout "failed at $_failed_cmd, pwd is $_pwd";
		#die(__FILE__." line ".__LINE__."  $_failed_cmd \nfailed at $_failed_cmd, , pwd is $_pwd\n");
	}
	return $_is_fail;
}

sub formatContent {
  my ($_self)               = @_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
	my $_install_dir_prefix   = $_client->{install_dir_prefix};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_app_install_log      = $_client->{app_install_log};
	my $_lmod_dir_prefix      = $_client->{lmod_dir_prefix};
	my $_install_path         = "$_install_dir_prefix/$_kp_name/$_kp_version";
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
	my $_mod_path             = "$_lmod_dir_prefix/$_kp_name";
	my $_to_personal          = 1;
	my $_fetch_command        = defined $_self->{general_fetch_command}   ? $_self->{general_fetch_command}   : "";
	$_fetch_command           = $_client->getGeneral($_fetch_command,$_to_personal);
	my $_extract_command      = defined $_self->{general_extract_command} ? $_self->{general_extract_command} : "";
	$_extract_command         = $_client->getGeneral($_extract_command,$_to_personal);
	my $_prereq_command       = defined $_self->{prereq_command} ? $_self->{prereq_command} : "";
	my $_compile_command      = defined $_self->{general_compile_command} ? $_self->{general_compile_command} : "";
	$_compile_command         = $_client->getGeneral($_compile_command,$_to_personal);
	my $_install_test_command = defined $_self->{general_test_command}    ? $_self->{general_test_command}    : "";
	$_install_test_command    = $_client->getGeneral($_install_test_command,$_to_personal);
  my $_upload_command       = defined $_self->{general_upload_command}  ? $_self->{general_upload_command} : "";
	$_upload_command          = $_client->getGeneral($_upload_command,$_to_personal);
  my $_note                 = defined $_self->{note}                    ? $_self->{note}                    : "";
  my $_brief                = $_self->{brief};
  $_brief                   = $_software->{brief} if (!defined $_brief || $_brief =~ m/^\s*$/ );
  $_brief                   = defined $_brief ? $_brief : "";
  if( $_upload_command      =~ /^\s*$/){
  	$_upload_command        =  $_self->getUploadCmd;
  	$_upload_command        =~ s/\n+/\n/g;
  	$_upload_command        =~ s/^##//mg;
  }
  my $_install_test         =  $_self->{install_test};
  my @_dirs                 =  split (' ', $_install_test->{exist_dirs})         if (defined $_install_test->{exist_dirs} );
  my @_full_dirs            =  map   {$_install_path."/".$_} @_dirs;
  my $_ls_exist_dirs        =  (defined $_install_test->{exist_dirs} ) ? "ls -lh ".join (" ", @_full_dirs) : "";
  my @_files                =  split (' ', $_install_test->{exist_files})        if (defined $_install_test->{exist_files} );
  my @_full_files           =  map   {$_install_path."/".$_} @_files;
  my $_ls_exist_files       =  (defined $_install_test->{exist_files} ) ? "ls -lh ".join (" ", @_full_files) : "";
  my $_h                    = defined $_install_test->{help_para} ? $_install_test->{help_para} : "-h";
  my $_c                    = defined $_install_test->{command} ? $_install_test->{command} : $_kp_name;
  my $_ktest_cmd            =  "kpan  --k_log $_src_path/$_app_install_log --test_install -in $_kp_name -iv $_kp_version -hp \"$_h\" --test_cmd  \"$_c \" --test_file_core_name $_kp_name \n\n";	                    
  my $_kstat                = $_self->{kstat};
  my %_kh                   = %{$_kstat};
  my $_kstat_str            = "";
  foreach my $_k (sort keys %_kh){
    $_kstat_str .= "$_k=$_kh{$_k}\n" if (defined $_kh{$_k} && $_kh{$_k} =~/^\d+$/ && $_k !~ m/^debug$/ );
  }
  my $_content =
		"#!/bin/bash\n"
		. "\n"
		. "#$_client->{install_user_mark} ".`date +%m/%d/%y`."\n" 
		. "mkdir -p $_install_dir_prefix/$_kp_name/tmp\n"
		. "\n"
		. "$_client->{KP} FETCH#\n"
		. "cd $_src_path/tmp\n"
		. "$_fetch_command\n"
		. "\n"
		. "$_client->{KP} EXTRACT#\n"
		#. "rsync -avq $_client->{src_dir_prefix}/$_lc_name/tmp/$_software->{version_file_name} $_client->{src_dir_prefix}/$_lc_name/ \n"
		. "$_extract_command\n"
		. "rm -rf $_src_path/tmp/*\n"
		. "\n"
		. "$_client->{KP} PREREQ# \n"
		. "$_prereq_command\n"
		. "\n"
		. "$_client->{KP} BUILD# \n"
		. "cd $_src_path\n"
		. "module purge\n"
			. "$_compile_command\n"
		. "ln -s $_install_path $_client->{install_dir_prefix}/$_kp_name/latest\n"
		. "ls -lth $_install_path\n"
		. "\n"
		. "$_client->{KP} MODULE#\n"
		#. "$_self->{make_module_command}"
		. "mkdir -p $_mod_path \n"
		. "\n"
		. "ls -lth $_mod_path/ \n"
		. "#vi $_mod_path/$_kp_version.lua\n"
		. "#cat $_mod_path/$_kp_version.lua\n"
		. "chmod 664 $_mod_path/$_kp_version.lua\n"
		. "module purge \n"
		. "module load $_kp_name/$_kp_version\n"
		. "\n"
		. "$_client->{KP} TEST#\n"
	  . "$_ls_exist_dirs\n"
	  . "$_ls_exist_files\n"
	  . "cd $_src_path\n"
	  . "module purge\n" 
	  . "module load $_kp_name/$_kp_version\n"
	  . "$_install_test_command\n"
	  . "module use ~/modulefiles\n"
	  . "module load kpan\n"
	  . "$_ktest_cmd\n"
		. "\n"
		. "$_client->{KP} BRIEF#\n"
		. "#$_brief\n"
		. "\n"
		. "$_client->{KP} UPLOAD#\n"
		. "$_upload_command"
		. "\n"
		. "$_client->{KP} NOTES#\n"
		. "$_note\n"
		. "\n"		
		. "$_client->{KP} STAT#\n"
		. $_kstat_str
		. "\n"
		. "$_client->{KP} END#\n"
  ;
 	say __FILE__." Line ".__LINE__.", \nFormated content: $_content\n" if(defined $_self->{debug} && $_self->{debug}>4);
  $_self->{commands}            = $_content;
  $_self->{install_log_text}    = $_content;
  $_self->{general_install_log} = $_content;
  $_to_personal                 = 0;
  $_self->{general_install_log} = $_client->getGeneral($_self->{general_install_log},$_to_personal);

	return$_content;	
}
sub writeKLogFile     {
  my ($_self)=@_;
	my $_software             = $_self->{software};
	my $_client               = $_self->{client};
	my $_app_install_log      = $_client->{app_install_log};
	my $_kp_name              = $_self->{kp_name};
	my $_kp_version           = $_self->{kp_version};
	my $_src_dir_prefix       = $_client->{src_dir_prefix};
	my $_src_path             = "$_src_dir_prefix/$_kp_name/$_kp_version";
  my $_content              = $_self->formatContent;
	my $_log_dir              = "$_src_dir_prefix/$_kp_name/$_kp_version/$_self->{log_dir}";
	$_self->mkDir("$_log_dir") if ( !-e "$_log_dir");	
  my $_file                 = "$_log_dir/Formatted.$_app_install_log";
	open my $_fout, ">", $_file or die __FILE__." Line ".__LINE__.",  couldn't open log file $_file for write: $!\n";
	say $_fout "$_content";
	#die(__FILE__." line ".__LINE__."  $_failed_cmd \nfailed at $_failed_cmd, , pwd is $_pwd\n"); 
}
sub uploadInstall{
  my ($_self)=@_;
  $_self->{client}->{rc}->{debug}  = $_self->{debug};
  $_self->{client}->{rc}->uploadInstall($_self->{client}->{kpan_client_name},$_self);
}
sub checkUpdate{
  my ($_self, $_software)            = @_;
  my $_archive_url                   = $_software->{archive_url};
  return if (!defined $_archive_url);
  my $_app_name                      = $_software->{name}; 
  my $_home_url                      = $_software->{home_url};
  my $_version_url                   = $_software->{version_url};
	my $_url_obj                       = Kpan::Url->new();
	$_url_obj->{debug}                 = $_self->{debug};
	$_url_obj->{name}                  = $_app_name;
	$_url_obj->{kp_name}               = $_self->{kp_name};
	$_url_obj->{archive_url}           = $_self->{archive_url};
  my $_archive_url_hash_ref;
	$$_archive_url_hash_ref{$_app_name}{name}        = $_app_name    if (defined $_app_name);  
  $$_archive_url_hash_ref{$_app_name}{home_url}    = $_home_url    if (defined $_home_url);
  $$_archive_url_hash_ref{$_app_name}{archive_url} = $_archive_url if (defined $_archive_url);
  $$_archive_url_hash_ref{$_app_name}{version_url} = $_version_url if (defined $_version_url);
  # get all urls under archive url and upload to DB
  # TODO Useful Spider
  $_url_obj->spiderVersionUrl($_archive_url_hash_ref);
  #say __FILE__." line ".__LINE__.", archive_url_hash_ref=".Dumper($_archive_url_hash_ref);
  $_self->{client}->{rc}->{debug}  = $_self->{debug};
  my $_version_hash_ref           = $$_archive_url_hash_ref{$_app_name}{version};
  for my $_version (sort { versioncmp($b, $a) } keys %{$_version_hash_ref}){
  	if(!$_self->isExistAppVersion($_version)){
  	  my $_v_url                    = $$_version_hash_ref{$_version}{url};
  	  say __FILE__." line ".__LINE__.", v=$_version, v_url=$_v_url";
  		my $_software_new             = Kpan::Software->new;
      foreach my $_key (sort keys %{$_software}) {
        $_software_new->{$_key}     = $_software->{$_key};
      }	
      $_software_new->{version}     = $_version; 		
      $_software_new->{version_url} = $_v_url; 		
      $_software_new->{id}          = undef; 		
      $_software_new->{note}        = $$_version_hash_ref{$_version}{note};		
      $_software_new->{version_file_size} = undef; 		
      #say __FILE__." line ".__LINE__.", d_new=".Dumper($_software_new);
      #$_self->{client}->{rc}->uploadSoftware($_self->{client}->{kpan_client_name},$_software_new);
  	  # todo update DB
  	}
  }
}
sub isExistAppVersion {
  my ($_self, $_version)    = @_;
  my $_install_hash_ref     = $_self->{install_hash_ref};
  my @_keys = keys %{$_install_hash_ref};
  say __FILE__." line ".__LINE__.", #=".($#_keys);
  foreach my $_hash_key (sort keys %{$_install_hash_ref}) {
		my $_install            = $_install_hash_ref->{$_hash_key}; #TODO match client
  	$_install->{kp_name}    = lc($_install->{software}->{name})    if (!defined $_install->{kp_name}    && defined $_install->{software}->{name}); 
	  $_install->{kp_version} = lc($_install->{software}->{version}) if (!defined $_install->{kp_version} && defined $_install->{software}->{version}); 
	  my $_h_software         = $_install->{software};
	  my $_h_version          = $_h_software->{version};
	  say __FILE__." line ".__LINE__.", v=$_version, hv=$_h_version";
	  return 1 if ( $_h_version =~ m/^$_version$/i ); # matched
	}
	return 0; # not match
}
sub isExistModule {
  my ($_self, $_name)=@_;
  my $_software        = $_self->{software};
#say __FILE__." Line ".__LINE__."\n".Dumper($_software) if(defined $_self->{debug} && $_self->{debug}>0);
  my $_is_exist_module = 0;
  my $_prereq_hash_ref = $_software->getCombinePrereqHashRef; #{prereq_hash_ref};
  if( defined $_prereq_hash_ref ) {
		my %_ph = %{$_prereq_hash_ref};
		foreach my $_key (sort {$a <=> $b} keys %_ph) { #
			my $_prereq = $_ph{$_key};
			if(lc($_name) eq lc($_prereq->{prereq_name})){
				$_is_exist_module=1;
				last;
			}
		}
  }
  return $_is_exist_module;
}
sub LmodModule {
  eval `/usr/local/apps/lmod/5.8/libexec/lmod perl @_`;
  if($@) {
    use Carp;
    confess "module-error: $@\n";
  }
  return 1;
}

sub print{
  my ($_self, $_flag)         = @_;
  $_flag                      = 1 if (!defined $_flag);
	my %_install                = %{$_self};
	my %_software               = %{$_install{software}};
	my %_client                 = %{$_install{client}};
	my %_install_test           = %{$_install{install_test}};
	my %_kstat                  = %{$_install{kstat}};
  my $_prereq_hash_ref        = $_software{prereq_hash_ref}; # prereq from prereq table
	my $_prereq_lib_hash_ref    = $_software{prereq_lib_hash_ref}; # prereq from prereq table
  
  $_client{rc}                    = undef;
  $_client{default_Module}        = undef;
  $_software{prereq_lib_hash_ref} = undef;
  $_software{prereq_hash_ref}     = undef;
  $_install{software}             = undef;
  $_install{client}               = undef;
  $_install{commands}             = undef;
  $_install{upload_command}       = undef;
  $_install{module_content}       = undef;
  $_install{install_test_command} = undef;
  $_install{test_cmd_result}      = undef;
  $_install{install_log_text}     = undef;
  $_install{install_test}         = undef;
  $_install{kstat}                = undef;
  
  say "prereq_lib_hash_ref\n" .Dumper($_prereq_lib_hash_ref)  if ($_flag==0 || $_flag==1 || $_flag==2 || $_flag==6);
  say "prereq_hash_ref\n"     .Dumper($_prereq_hash_ref)      if ($_flag==0 || $_flag==1 || $_flag==2 || $_flag==6);
  say "software\n"            .Dumper(\%_software)            if ($_flag==0 || $_flag==1 || $_flag==2 || $_flag==5);
  say "client\n"              .Dumper(\%_client)              if ($_flag==0 || $_flag==1 || $_flag==2 || $_flag==4);
  say "install_test\n"        .Dumper(\%_install_test)        if ($_flag==0 || $_flag==1 || $_flag==2);
  say "kstat\n"               .Dumper(\%_kstat)               if ($_flag==0 || $_flag==1 || $_flag==2);
  say "install\n"             .Dumper(\%_install)             if ($_flag==0 || $_flag==1 || $_flag==3);
  say "self\n"                .Dumper($_self)                 if ($_flag==0);
}
1;

