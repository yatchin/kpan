#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Prereq;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use DBData;
use KUtil;
our $VERSION = '1.5.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
	  id              => shift,
	  name            => shift,
		version         => shift, 
	  prereq_name     => shift,
		prereq_version  => shift, 
		module_name     => shift, # perl, python, R moudle
		module_version  => shift, 
		channel         => shift, 
		order           => shift, 
		debug           => shift,
		is_build        => shift,
		note            => shift
	 };
	return $_self;
}

sub parsePrereqCommand{ #take multi lines strings, return hash ref, not ready to use
  my ($_self, $_app_name, $_app_version, $_prereq_command,$_prereq_lib_hash_ref,$_prereq_hash_ref)=@_;
  # perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
  # perl/5.20.1:DBI/1.0 DBI File::Finder
  # python/2.7: numpy, pytorch
	say __FILE__." Line ".__LINE__.", $_prereq_command" if(defined $_self->{debug} && $_self->{debug} >1);
  if(defined $_prereq_command && $_prereq_command =~ m/\S/){
    my @_lines=split (/\n/,$_prereq_command);
 	  say __FILE__." Line ".__LINE__.", lines:".Dumper(\@_lines)   if(defined $_self->{debug} && $_self->{debug} >2) ;
		my ($_prereq_name,$_prereq_version,$_module_name,$_module_version,$_module_channel);
    foreach my $_line (@_lines){
      $_line=Kpan::KUtil::Trim($_line);
			my @_units=split(/[\s,]+/,$_line); # chop by space
			for my $_i (0..$#_units){
				($_module_name,$_module_version,$_module_channel) = ("","",""); # only reset subunit, keep prereq name and v, perl/5.20.1:DBI/1.0 :Dumper :DBD
			  $_units[$_i]=Kpan::KUtil::Trim($_units[$_i]);
			  say __FILE__." Line ".__LINE__.", units[$_i]=$_units[$_i]" if(defined $_self->{debug} && $_self->{debug} >3);
			  if($_units[$_i] =~ m/^\s*$/ || $_units[$_i] =~ m/^\(|\)|\\$/){ # skip empty line or (,),\
			    say __FILE__." Line ".__LINE__.", units[$_i]=$_units[$_i]" if(defined $_self->{debug} && $_self->{debug} >3);
			    next;
			  }if($_units[$_i] !~ m/^\(|http(s*):|ftp\.|www\./){ #not (channel or url)
					$_module_name=undef;
					$_module_version=undef;
				}
			  #my $_module_unit=$_units[$_i]; #perl/5.20.1:DBI/1.0, perl, DBI/1.0 File::Finder/0.1
	#perl -e '@a= qw(perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI); foreach $b (@a) {$b=~ m/(.+?)\/(.*?):(.*?)\/(.*?)#*/; print "$b\t\t1:$1\t2:$2\t3:$3\t4:$5\t5:$5\t6:$6\n";}'
	#perl -e '$a= "perl/5.20.1:DBI/1.0, File::Finder\tData::Dumper"; '
	#perl -e '$a= "perl/5.20.1:DBI/1.0, File::Finder\tData::Dumper #note"; $a=~ m/(.+?)\/(.+?):([^:].+?)\/(.+?)#(.+)/; print "1:$1\t2:$2\t3:$3\t4:$4\t5:$5\t6:$6\n";'
			  if($_i==0 && $_units[$_i] =~ m/(.+?)\/(.+?):([^:].+?)\/(.+?)#(.+)/){ # : perl/5.20.1:DBI/1.0 #smith (channnel)
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_prereq_version=Kpan::KUtil::Trim($2);
			    $_module_name=Kpan::KUtil::Trim($3);
			    $_module_version=Kpan::KUtil::Trim($4);
			    $_module_channel=Kpan::KUtil::Trim($5);
			  }elsif($_units[$_i] =~ m/(.+?)\/(.+?):(.+?)\/(.+)/){ # perl/5.20.1:DBI/1.0(url|channel) TODO
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_prereq_version=Kpan::KUtil::Trim($2);
			    $_module_name=Kpan::KUtil::Trim($3);
			    $_module_version=Kpan::KUtil::Trim($4);
			  }elsif($_units[$_i] =~ m/(.+?)\/(.+?):(.+)/){ # perl/5.20.1:DBI
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_prereq_version=Kpan::KUtil::Trim($2);
			    $_module_name=Kpan::KUtil::Trim($3);
			  }elsif($_units[$_i] =~ m/:(.+?)\/(.+)/){ # perl/5.20.1 
			    $_module_name=Kpan::KUtil::Trim($1);
			    $_module_version=Kpan::KUtil::Trim($2);
			  }elsif($_units[$_i] =~ m/(.+?):(.+?)\/(.+)/){ # perl:DBI/1.0 
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_module_name=Kpan::KUtil::Trim($2);
			    $_module_version=Kpan::KUtil::Trim($3);
			  }elsif($_units[$_i] =~ m/^:(.+?)\/(.+)/){ # perl/5.20.1:DBI/1.0 :DBD/2.0 
			    $_module_name=Kpan::KUtil::Trim($1);
			    $_module_version=Kpan::KUtil::Trim($2);
			  }elsif($_units[$_i] =~ m/(.+?):(.+)/){ # perl:DBI 
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_module_name=Kpan::KUtil::Trim($2);
			  }elsif($_units[$_i] =~ m/(.+?)\/(.+)/){ # gcc/4.4.7 
			    $_prereq_name=Kpan::KUtil::Trim($1);
			    $_prereq_version=Kpan::KUtil::Trim($2);
			  }elsif($_units[$_i] =~ m/(.+)/){ # gcc 
			    $_prereq_name=Kpan::KUtil::Trim($1);
			  }
			  say __FILE__." Line ".__LINE__.", prereq: $_prereq_name,$_prereq_version " if(defined $_self->{debug} && $_self->{debug} >3);
			  say __FILE__." Line ".__LINE__.", prereq: $_module_name, $_module_version, $_module_channel" if(defined $_self->{debug} && $_self->{debug} >6);
        my $_pq = Kpan::Prereq->new;
        $_pq->{name}           = $_app_name;
        $_pq->{version}        = $_app_version;
        $_pq->{prereq_name}    = $_prereq_name;
        $_pq->{prereq_version} = $_prereq_version;
        $_pq->{module_name}    = $_module_name;
        $_pq->{module_version} = $_module_version;
        $_pq->{module_channel} = $_module_channel;
        $_pq->addPrereq($_prereq_lib_hash_ref,$_prereq_hash_ref);
			  #$_self->addPrereq($_prereq_lib_hash_ref,$_prereq_hash_ref);
			}
	  }
  }
 	#say __FILE__." Line ".__LINE__.", prereq:".Dumper($_prereq_hash_ref).Dumper($_prereq_lib_hash_ref);
 	say __FILE__." Line ".__LINE__.", prereq:".Dumper($_prereq_hash_ref) if(defined $_self->{debug} && $_self->{debug} >0);
}

sub addPrereq{
  my ($_self, $_prereq_lib_hash_ref,$_prereq_hash_ref,$_app_name,$_app_version,$_prereq_name,$_prereq_version,$_module_name, $_module_version, $_module_channel,$_note)=@_;
  my $_id            = $_self->{id};
	$_app_name         = $_self->{name}           if (!defined $_app_name);
	$_app_version      = $_self->{version}        if (!defined $_app_version);
	$_prereq_name      = $_self->{prereq_name}    if (!defined $_prereq_name);
	$_prereq_version   = $_self->{prereq_version} if (!defined $_prereq_version);
	$_module_name      = $_self->{module_name}    if (!defined $_module_name);
	$_module_version   = $_self->{module_version} if (!defined $_module_version);
	$_module_channel   = $_self->{module_channel} if (!defined $_module_channel);
	$_note             = $_self->{note}           if (!defined $_note);
	return if (!defined $_prereq_name);
  #say __FILE__." Line ".__LINE__.", prereq:$_app_name,$_app_version,$_prereq_name,$_prereq_version,".Dumper($_prereq_hash_ref).Dumper($_self);
  say __FILE__." Line ".__LINE__.", prereq:$_app_name,$_app_version,$_prereq_name,$_prereq_version,".Dumper($_prereq_hash_ref) if(defined $_self->{debug} && $_self->{debug} >3);
 
		my %_pq_order_hash = %{$_prereq_hash_ref};
		my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
		#say __FILE__." Line ".__LINE__.", prereq keys before added:";
		say __FILE__." Line ".__LINE__.", prereq keys before added:".Dumper(\@_keys) if(defined $_self->{debug} && $_self->{debug} >4);
		my $_flag=0;
  	for my $_i (0.. scalar(@_keys)-1 ) { 
			my $_pq = $_pq_order_hash{$_keys[$_i]}; 
			if(defined $_pq->{prereq_name}) { 
		    do {warn "multiple python versionsL: keep $_pq->{prereq_name}, skip $_prereq_name";$_flag=1; next;} if( $_pq->{prereq_name} =~ m/python/i  && $_prereq_name =~ m/python/i );
		    do {warn "multiple python conda versions: keep $_pq->{prereq_name}, skip $_prereq_name";$_flag=1;next;} if( $_pq->{prereq_name} =~ m/^MiniConda|^Anaconda/i  && $_prereq_name =~ m/python/i );
		    do {warn "multiple python conda versions: replace $_pq->{prereq_name} by $_prereq_name"; delete $_pq_order_hash{$_keys[$_i]};  next;} if( $_pq->{prereq_name} =~ m/python/i  && $_prereq_name =~ m/^MiniConda|^Anaconda/i );
		    if( lc($_pq->{prereq_name}) eq lc($_prereq_name) 
			    && defined $_pq->{prereq_version} && lc($_pq->{prereq_version}) eq lc($_prereq_version)){
  				$_flag=1;
		    }
				last;
			} # perl/5.20.1
		}
		if( $_flag !=1 ){# not exists
		  my $_p = Kpan::Prereq->new;
		  $_p->{id}             = $_id;
		  $_p->{name}           = $_app_name;
		  $_p->{version}        = $_app_version;
		  $_p->{prereq_name}    = $_prereq_name;
		  $_p->{prereq_version} = $_prereq_version;
		  $_p->{module_name}    = $_module_name;
		  $_p->{module_version} = $_module_version;
		  $_p->{module_channel} = $_module_channel;
			foreach my $_key (keys %{$_self}){ # add note, os, system
			  $_p->{$_key}        = $_self->{$_key} if (!defined $_p->{$_key} );
			}
			my $_prereq_order     = scalar keys %{$_prereq_hash_ref};
			$_p->{order}          = $_prereq_order if (!defined $_p->{order});
			${$_prereq_hash_ref}{$_prereq_order}=$_p; 	  
		  #say __FILE__." Line ".__LINE__.", prereq_order=$_prereq_order, p=".Dumper($_p) ;
		  say __FILE__." Line ".__LINE__.", prereq_order=$_prereq_order, p=".Dumper($_p) if(defined $_self->{debug} && $_self->{debug} >2);
    }
		#say __FILE__." Line ".__LINE__.", prereq added:".Dumper($_prereq_hash_ref);
		say __FILE__." Line ".__LINE__.", prereq added:".Dumper($_prereq_hash_ref) if(defined $_self->{debug} && $_self->{debug} >1);
		
   if($_prereq_name=~ m/^perl$|^python[\d\.]*$|^R$|^MiniConda$|^conda$|^Anaconda$/i ){
		my %_prereq_hash_lib_pq = %{$_prereq_lib_hash_ref}; #prereq_lib proceed prereq_app
		my $_lib_flag=0;
		foreach my $_name_key (sort keys %_prereq_hash_lib_pq) { # key = perl|python.*|R
			my %_pq_order_hash = %{$_prereq_hash_lib_pq{$_name_key}};
			my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
			#say __FILE__." Line ".__LINE__.", prereq keys:".Dumper(\@_keys);
			say __FILE__." Line ".__LINE__.", prereq keys:".Dumper(\@_keys)  if(defined $_self->{debug} && $_self->{debug} >2);
			for my $_i ( 0..$#_keys ) { 
				my $_pq = $_pq_order_hash{$_keys[$_i]}; 
				if(defined $_pq->{prereq_name} && lc($_pq->{prereq_name}) eq lc($_prereq_name) 
				  && defined $_pq->{prereq_version} && lc($_pq->{prereq_version}) eq lc($_prereq_version) 
				  && defined $_pq->{module_name} && defined $_module_name && lc($_pq->{module_name}) eq lc($_module_name)
				  && defined $_pq->{module_version} && defined $_module_version && lc($_pq->{module_version}) eq lc($_module_version) ) { 
					$_lib_flag=1;
					last;
				} # perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
				# perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
			}
		}
		if($_lib_flag!=1){
      #say __FILE__." Line ".__LINE__.", prereq:".Dumper($_prereq_hash_ref);
		  my $_p = Kpan::Prereq->new;
		  $_p->{id}             = $_id;
		  $_p->{name}           = $_app_name;
		  $_p->{version}        = $_app_version;
		  $_p->{prereq_name}    = $_prereq_name;
		  $_p->{prereq_version} = $_prereq_version;
		  $_p->{module_name}    = $_module_name;
		  $_p->{module_version} = $_module_version;
		  $_p->{module_channel} = $_module_channel;
			foreach my $_key (keys %{$_self}){ # add note, os, system
			  $_p->{$_key}=$_self->{$_key} if (!defined $_p->{$_key} );
			}
			my %_pq_order_hash = ();
			%_pq_order_hash = %{$_prereq_hash_lib_pq{$_prereq_name}} if (defined $_prereq_hash_lib_pq{$_prereq_name});
			my $_prereq_order = scalar keys %_pq_order_hash ;
			$_p->{order}=$_prereq_order;
			${$_prereq_lib_hash_ref}{$_prereq_name}{$_prereq_order}=$_p if (defined $_module_name) ; 	 
		} 				
 		#say __FILE__." Line ".__LINE__.", self:".Dumper($_prereq_lib_hash_ref).Dumper($_prereq_hash_ref);
 }
}
#,software->{prereq_hash_ref}=$VAR1 = {
#          '2' => bless( {
#                          'debug' => undef,
#                          'module_name' => undef,
#                          'prereq_name' => 'gmp',
#                          'prereq_version' => '6.0.0a',
#                          'order' => 2,
#                          'channel' => undef,
#                          'module_version' => undef,
#                          'note' => undef,
#                          'is_build' => undef
#                        }, 'Kpan::Prereq' ),
#,software->{prereq_lib_hash_ref}=$VAR1 = {
#          'perl' => { '2' => bless( {
#                          'debug' => undef,
#                          'module_name' => 'DBI',
#                          'prereq_name' => 'perl',
#                          'prereq_version' => '5.20.1',
#                          'order' => 2,
#                          'channel' => undef,
#                          'module_version' => '1.0',
#                          'note' => undef,
#                          'is_build' => undef
#                        }, 'Kpan::Prereq' ),

# 
sub CombinePrereqHashRef {
  my ($_prereq_lib_hash_ref,$_prereq_hash_ref)=@_;
  my $_pq_hash_ref;
  #say __FILE__." Line ".__LINE__.", b4 Combine prereq hash".Dumper($_prereq_lib_hash_ref).Dumper($_prereq_hash_ref);
  if(!defined $_prereq_lib_hash_ref && !defined $_prereq_hash_ref){
    return $_pq_hash_ref;
  }
  my %_pq_name_keys =();
  foreach my $_pq_name_key (sort keys %{$_prereq_lib_hash_ref}) { # key = perl|python.*|R
    next if exists $_pq_name_keys{$_pq_name_key};
    $_pq_name_keys{$_pq_name_key}=1;
    my %_pq_order_hash = %{$$_prereq_lib_hash_ref{$_pq_name_key}};
    my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
    #say __FILE__." Line ".__LINE__.", prereq:".Dumper(\@_keys) if (scalar(@_keys)>0);
    for my $_i (0.. $#_keys) { 
      my $_pq                 = $_pq_order_hash{$_keys[$_i]};
			my $_order              = defined $_pq_hash_ref ? scalar (keys %{$_pq_hash_ref}) : 0;
			$$_pq_hash_ref{$_order} = $_pq;
		}
	}
  foreach my $_order_key (sort {$a <=> $b} keys %{$_prereq_hash_ref}) { # key = order
    # todo skip duplicates, such as perl
    my $_order              = defined $_pq_hash_ref ? scalar (keys %{$_pq_hash_ref}) : 0;
    $$_pq_hash_ref{$_order} = $$_prereq_hash_ref{$_order_key};
	}
  #say __FILE__." Line ".__LINE__.", Combine prereq hash".Dumper($_pq_hash_ref);
  
  return ReducePrereq($_pq_hash_ref);
}
sub GetPrereqCommand{
  my ($_prereq_lib_hash_ref,$_prereq_hash_ref)=@_;
  my $_prereq_command="";
  if(!defined $_prereq_lib_hash_ref&&!defined $_prereq_hash_ref){
    return $_prereq_command;
  }
  foreach my $_pq_name_key (sort keys %{$_prereq_lib_hash_ref}) { # key = perl|python.*|R
    my %_pq_order_hash = %{$$_prereq_lib_hash_ref{$_pq_name_key}};
    my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
    #say __FILE__." Line ".__LINE__.", prereq:".Dumper(\@_keys);
    for my $_i (0.. scalar(@_keys) ) { 
      return $_prereq_command if(!defined $_pq_order_hash{$_keys[$_i]}); # perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
      my $_pq = $_pq_order_hash{$_keys[$_i]};
			$_prereq_command .= $_pq->{prereq_name};
      $_prereq_command .= $_pq->{prereq_version} if (defined $_pq->{prereq_version});
      $_prereq_command .= " ";
		}
	}
  foreach my $_order_key (sort {$a <=> $b} keys %{$_prereq_hash_ref}) { # key = order
    return $_prereq_command if(!defined $$_prereq_hash_ref{$_order_key}); # perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
    my $_pq = $$_prereq_hash_ref{$_order_key};
    $_prereq_command .= $_pq->{prereq_name};
    $_prereq_command .= $_pq->{prereq_version} if (defined $_pq->{prereq_version});
    $_prereq_command .= " ";		
	}
	$_prereq_command .= "\n";
  return $_prereq_command;
}
#
# 
sub GetPrereqCommandWithLib{ # todo 
	my ($_prereq_lib_hash_ref,$_prereq_hash_ref)=@_;
  my $_prereq_command="";
  if(!defined $_prereq_lib_hash_ref||!defined $_prereq_hash_ref){
    return $_prereq_command;
  }
	my %_prereq_hash_lib_pq = %{$_prereq_lib_hash_ref}; #prereq_lib proceed prereq_app #perl/python/R/0/1/..
  #say __FILE__." Line ".__LINE__.", prereq_lib_hash_ref:".Dumper($_prereq_lib_hash_ref);
  my @_keys = sort keys %_prereq_hash_lib_pq; # key = 0..
  for my $_i (0.. scalar(@_keys) ) {
    my $_pq = $_prereq_hash_lib_pq{$_keys[$_i]};
		#say __FILE__." Line ".__LINE__.", prereq:".Dumper($_pq);
		# perl/5.20.1:DBI/1.0, perl:DBI/1.0, perl/5.20.1:DBI, perl:DBI
		$_prereq_command .= $_pq->{prereq_name};
		$_prereq_command .= $_pq->{prereq_version} if (defined $_pq->{prereq_version});
		$_prereq_command .= " ";
	}
	return $_prereq_command;
}
sub _formatCommand{
  my ($_prereq)=@_;
  my $_prereq_command="";
  if(defined $_prereq->{module_name}){
    $_prereq_command = $_prereq->{module_name};
		$_prereq_command .= "/".$_prereq->{module_version} if(defined $_prereq->{module_version});
  }
  if(defined $_prereq->{prereq_name}){
    my $_cmd = $_prereq->{prereq_name};
		$_cmd .="/".$_prereq->{prereq_version} if(defined $_prereq->{prereq_version});
    $_prereq_command=$_cmd.":".$_prereq_command if($_prereq->{prereq_name} =~ m/^perl$|^python[\d\.]*$|^R$/i );
  }
  return $_prereq_command ;
}

sub parseHistory{
  my ($_self, $_f)=@_;
  my %_lib_hash=(); #name, order, prereq obj
	my $_prereq_name="";
	my $_prereq_version="";
	my $_module_name="";
	my $_module_version="";
  open my $_in, "<", $_f or die  __FILE__." Line ".__LINE__.",  couldn't open file $_f for reading: $!\n";
	while (<$_in>){
	  my $_line=Kpan::KUtil::Trim($_);
	  if( length($_line)==0){
	    next;
	  }
	  if($_line=~/^module load (.+)\/(.+)$/){
	    $_prereq_name=$1;
	    $_prereq_version=$2;
	    next;
	  }elsif($_line=~/^==.+==/ ){
	    next;
	  }elsif($_prereq_name eq "perl" && $_line=~/(.+)\s+(.+)/ && $_line!~ /cpan \-l/ ){
	    $_module_name=$1;
	    $_module_version=$2;
	  }elsif($_prereq_name=~ m/^conda$|^python$/i  && $_line=~/(.+)==(.+)/ && $_line!~ /pip freeze/ ){
	    $_module_name=$1;
	    $_module_version=$2;
	  }elsif($_prereq_name eq "R" && $_line=~/(.+)\s+(.+)/ && $_line!~ /Rscript\s+list|Package\s+Version/ ){
	    $_module_name=$1;
	    $_module_version=$2;
	  }
		my $_prereq = Kpan::Prereq->new;
		$_prereq->{prereq_name}    = $_prereq_name;
		$_prereq->{prereq_version} = $_prereq_version;
		$_prereq->{module_name}    = $_module_name;
		$_prereq->{module_version} = $_module_version;
		my $_key="${_prereq_name}_${_prereq_version}_${_module_name}_${_module_version}";
		$_lib_hash{$_key}=$_prereq;
  }
  my $_db_data=Kpan::DBData->new();
  $_db_data->{debug}=$_self->{debug};
  $_db_data->uploadLibHash(\%_lib_hash);
}

sub ReducePrereq {
  my ($_prereq_hash_ref)=@_;
  my $_pq_hash_ref;
  
  return $_pq_hash_ref if(!defined $_prereq_hash_ref);
  
  my $_count = 0;
  #say __FILE__." Line ".__LINE__.", b4 Combine prereq hash".Dumper($_prereq_lib_hash_ref).Dumper($_prereq_hash_ref);
  my @_keys =  keys %{$_prereq_hash_ref} ; 
  do { $_count++ if ($_  =~  m/^python|^conda|^miniconda|^anaconda/i); }  foreach @_keys;
  warn __FILE__." Line ".__LINE__.", python and conda conflict at prereq " if ($_count > 0);
  
  my %_pq_name_keys =();
 
    
  foreach my $_order_key (sort {$a <=> $b} keys %{$_prereq_hash_ref}) { # key = order
    next if ($_count>0 && defined $$_prereq_hash_ref{$_order_key}->{prereq_name} && $$_prereq_hash_ref{$_order_key}->{prereq_name} =~ m/python/i ); # drop python if conda present
    next if exists $_pq_name_keys{$$_prereq_hash_ref{$_order_key}->{prereq_name}};
    $_pq_name_keys{$$_prereq_hash_ref{$_order_key}->{prereq_name}} = 1;
    my $_order              = defined $_pq_hash_ref ? scalar (keys %{$_pq_hash_ref}) : 0;
    $$_pq_hash_ref{$_order} = $$_prereq_hash_ref{$_order_key};
	}
  #say __FILE__." Line ".__LINE__.", Combine prereq hash".Dumper($_pq_hash_ref);
  return $_pq_hash_ref;     
}

1;

