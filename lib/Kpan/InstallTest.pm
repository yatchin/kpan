#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

#test installed app 
package Kpan::InstallTest;
use 5.010;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
#use FindBin qw($Bin);
#use lib "$Bin/../lib/Kpan";
#use lib ".";

use KUtil;

our $VERSION = '1.5.6';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
	     	command          => shift,
				debug            => shift,
				exist_files      => shift,
				exist_dirs       => shift,
				file_core_name   => shift,
				file_name        => shift,
				file_name_prefix => "",
				help_para        => shift,
				lmod_app_path    => shift,
				module_load      => shift,
				name             => shift,
				not_write_out    => shift,
				kp_name          => shift,
				kp_version       => shift,
				KP               => shift,
				test_result      => shift,
				version          => shift,
  };
  return $_self;		
}
sub testCmd {
	my ($_self, $_lmod_app_path, $_is_dry_run) = @_;
 	say __FILE__. " Line " . __LINE__ . ", self=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 4 );	
	return if (!defined $_self->{name});
	$_self->{kp_version}       = $_self->{version}       if (!defined $_self->{kp_version} && defined $_self->{version} );
	$_self->{kp_name}          = $_self->{name}          if (!defined $_self->{kp_name} && defined $_self->{name} );
	$_lmod_app_path            = $_self->{lmod_app_path} if (!defined $_lmod_app_path && defined $_self->{lmod_app_path});
	$_self->{command}          = lc($_self->{name})      if (!defined $_self->{command});
	$_self->{file_core_name}   = $_self->{name}          if (defined $_self->{name} && (!defined $_self->{file_core_name} || $_self->{file_core_name} =~ m/^\s*$/ ));
	$_self->{KP}               = "###KP"                 if (!defined $_self->{KP}             || $_self->{KP} =~ m/^\s*$/);
	$_self->{module_load}      = "$_self->{kp_name}/$_self->{kp_version}"  if (!defined $_self->{module_load} || $_self->{module_load} =~ m/^\s*$/);
	$_self->{file_name}        = "$_self->{file_name_prefix}.test.".$_self->{file_core_name}.".log" if (!defined $_self->{file_name} );  
	$_self->{help_para}        = ""                                                                 if (!defined $_self->{help_para} );  
	$_self->{exist_dirs}       = ""                                                                 if (!defined $_self->{exist_dirs} );  
	if (defined $_self->{exist_dirs} && $_self->{exist_dirs}=~ m/\S/ && (!defined $_self->{exist_files} || $_self->{exist_files}=~ /^\s*$/) && defined $_self->{command} && $_self->{command}=~ m/\S/  ){
	  $_self->{exist_files}      = "$_self->{exist_dirs}/$_self->{command}" if (-e "$_self->{exist_dirs}/$_self->{command}") ;
	  $_self->{exist_files}      = "$_self->{exist_dirs}/bin/$_self->{command}" if (-e "$_self->{exist_dirs}/bin/$_self->{command}") ;
	}  elsif(defined $_self->{exist_files} && $_self->{exist_files}=~ m/\S/ ){
	  #$_self->{exist_files}      = "$_self->{command}";
	}
	$_self->{exist_files}      = '' if (!defined $_self->{exist_files}); 	say __FILE__. " Line " . __LINE__ . ", L71 self->{file_name}=$_self->{file_name}=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 1 );
 	
  my $_lmod_cmd              = "$_lmod_app_path/libexec/lmod perl load $_self->{module_load}";       
  my $_cmd                   = "$_self->{command} $_self->{help_para}"; # 2>&1 | tee -a $_self->{file_name}";
  say __FILE__. " Line " . __LINE__ . ", $_lmod_cmd\n$_cmd" if ( defined $_self->{debug} && $_self->{debug} > 1 );
  eval `$_lmod_app_path/libexec/lmod perl purge`;
  eval `$_lmod_cmd`                         if ( !defined $_is_dry_run || $_is_dry_run == 0);
  my $_r                     = `$_cmd 2>&1` if ( !defined $_is_dry_run || $_is_dry_run == 0); # stderr and stdout
  #perl -e '$m="/usr/local/apps/lmod/7.4.17/libexec/lmod perl load kaiju/1.6.2"; eval `$m`;$r= `kaiju -h 2>&1`; print "$r\n"'
  $_r                        = ""           if ( !defined $_r );
  say __FILE__. " Line " . __LINE__ . ",$_cmd 2>&1,is_dry_run=$_is_dry_run, r=$_r" if ( defined $_self->{debug} && $_self->{debug} > 2 );
  if (!defined $_self->{not_write_out} || $_self->{not_write_out} ==0){
	open my $_out, ">", $_self->{file_name} or die  __FILE__." Line ".__LINE__.",  couldn't open file $_self->{file_name} for writing: $!\n";
  say $_out "$_self->{KP} Name#\n"
          . "$_self->{name}\n"
          . "$_self->{KP} Version#\n"
          . "$_self->{version}#\n"
          . "$_self->{KP} SanityCheck#\n"
          . "$_self->{KP} ExistDirs#\n"
          . "$_self->{exist_dirs}\n"
          . "$_self->{KP} ExistFiles#\n"
          . "$_self->{exist_files}\n"
          . "$_self->{KP} Module#\n"
          . "module load $_self->{module_load}\n"
          . "$_self->{KP} Command#\n"
          . "$_self->{command}\n"
          . "$_self->{KP} CmdHelp#\n"
          . "$_self->{command} $_self->{help_para}\n"
          . "$_self->{KP} TestResult#\n"
          . "$_r\n"
          . "$_self->{KP} End#\n";
	}
  $_self->{test_result} = $_r;
  #$output = `cmd 2>/dev/null`;                # stdout only
  #$output = `cmd 2>&1 1>/dev/null`;           # stderr only
  #my $_m_cmd =" module list ";
  #my $_m = `$_m_cmd`;
  #close($_out); 	#open $_out, ">>", $_self->{file_name} or die  __FILE__." Line ".__LINE__.",  couldn't open file $_self->{file_name} for writing: $!\n";
  #say "test with no output, ".Dumper($_self) if !defined $_r;        
}

sub parseTestCmdFile{ #Install Test and Wiki CreatePage
  my ($_self, $_file)=@_;
  if ( defined $_file && -f $_file){
		my $_content = "";
	  {
		  local $/ = undef;
	    open my $_in, "<", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for reading: $!\n";
		  $_content = <$_in>;
	  }
    $_self->parseTestCmdResult($_content);
    return $_content;
	}
}
sub parseTestCmdResult { #Install Test and Wiki CreatePage
  my ($_self, $_content)=@_;
  return if (!defined $_content);
	if ($_content =~ m/$_self->{KP} ExistDirs#(.+?)\n$_self->{KP}/s ){
		my $_dirs   = $1;
	  $_self->{exist_dirs} = "" if (!defined $_self->{exist_dirs});
		$_self->{exist_dirs}  = $_dirs;	
		$_self->{exist_dirs}  =~ s|\n+||g;				
		if (defined $_dirs && $_dirs =~ m/(\S+)/){
			$_self->{exist_dirs}  = $1;			
		}
	}		
	if ($_content =~ m/$_self->{KP} ExistFiles#(.+?)\n$_self->{KP}/s ){
		my $_files   = $1;
		$_files               = "" if (!defined $_files);
		$_files               =~ s|\n+||g;			
		$_self->{exist_files} = $_files;	
		if (defined $_files && $_files =~ m/^\s*(ls ?)(-\S+?)*\s+(.+)/){
			$_self->{exist_files} = $1;			
		}
	}		
	if ($_content =~ m/$_self->{KP} Module#(.+?)\n$_self->{KP}/s ){
		$_self->{module_load} = $1;
		$_self->{module_load} = "" if (!defined $_self->{module_load});
		$_self->{module_load} =~ s|\n+||g;
	}		
	 if ($_content =~ m/$_self->{KP} Command#(.+?)\n$_self->{KP}/s ){
		$_self->{command}     = $1;
		$_self->{command}     = "" if (!defined $_self->{command});
		$_self->{command}     =~ s|\n+||g;
	}		
	 if ($_content =~ m/$_self->{KP} CmdHelp#(.+?)\n$_self->{KP}/s ){
		$_self->{help_para}   = $1;
		$_self->{help_para}   = "" if (!defined $_self->{help_para});
		$_self->{help_para}   =~ s/$_self->{command}// if(defined $_self->{command});
		$_self->{help_para}   =~ s/^\s+//;
		$_self->{help_para}   =~ s/\s+$//;
		$_self->{help_para}   =~ s/\n+//;
	}		
	if ($_content =~ m/$_self->{KP} TestResult#(.+?)$_self->{KP}/s ){
	  $_self->{test_result} = $1;
	  $_self->{test_result} = "" if (!defined $_self->{test_result});
	  $_self->{test_result} =~ s/^\s+//s if(defined $_self->{test_result});
	  $_self->{test_result} =~ s/\s*\n*$//s if(defined $_self->{test_result});
  }		
  
}1;
