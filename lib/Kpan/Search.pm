#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Search;
use 5.010;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);

require RestClient;
require KUtil;

our $VERSION = '1.6.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		debug              				=> shift,
  };
  return $_self; 		
}

sub getClients{
	my ($_self, $_client, $_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev, $_debug)=@_;
	$_kpan_client_name = undef if ($_kpan_client_name eq 'kpan');
	my $_is_all_client  = ( ( (defined $_kpan_client_name && $_kpan_client_name =~ m/\S/ && $_kpan_client_name ne 'kpan' )  
	|| (defined $_cluster_system && $_cluster_system =~ m/\S/) 
	|| (defined $_group_name && $_group_name =~ m/\S/)   
	|| (defined $_org_name && $_org_name =~ m/\S/)   
	|| (defined $_is_kpan_dev && $_is_kpan_dev =~ m/\S/) 
	 ) ? 0:1);
	say __FILE__." Line ".__LINE__.", cluster=$_cluster_system, client=$_kpan_client_name,	group=$_group_name,\norg=$_org_name,	dev=$_is_kpan_dev, all_client=$_is_all_client, debug=$_debug" if(defined $_debug && $_debug>0);
	my $_client_hash_ref =  $_client->{rc}->getClient($_client, $_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev,$_is_all_client, $_debug);
	if( defined $_client_hash_ref ){
		foreach my $_client_key (keys  %{$_client_hash_ref} ){
		  my $_clientTemp = $_client_hash_ref->{$_client_key};
		  foreach my $_key (keys %{$_clientTemp}) {
			  my $_client->{$_key} = $_clientTemp->{$_key};
		  }
	  } 
  }
  say __FILE__. " Line " . __LINE__ . ", after fillClientself client=".Dumper($_client_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 2 );
  return $_client_hash_ref;
}

sub getInstallFromDB {
  my ($_self,$_client,$_client_name,$_app_name,$_app_version,$_app_install_name, $_app_install_version,
  $_group_name,$_os,$_org_name,$_self_only,$_debug,) = @_;

	# TODO specify client name with system
	my $_install_hash_ref = $_self->{install_hash_ref};
	$_install_hash_ref    = $_client->{rc}->getRecipes($_client->{name},$_client_name,$_app_name,$_app_version,$_app_install_name,$_app_install_version,
	$_group_name,$_org_name,$_os, $_debug) if (defined $_app_name || defined $_app_install_name || $_group_name || $_os );
}