#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::DefaultModule;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";

#our $VERSION = '1.0';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
	  lmod_app_path    => shift, 
	  lmod_dir_prefix    => shift, 
	  module_hash_ref  => shift, 
		debug       => shift,
		note => shift
	 };
	 #anaconda/2.2.0 R/3.1.2 gcc/4.7.4 java/jdk1.8.0_20 perl/5.24.1 automake/1.15 cmake/3.0.2 autoconf/2.69
	 $_self->{module_hash_ref} = {
     #samtools => {  name =>  "samtools",  version => "1.3.1" ,					   m_version => "1.3.1" ,						pre_defined	=> 1, version_hash_ref => {}, },
     anaconda  => {  name =>  "anaconda",  version => "2.2.0" ,					   m_version => "2.2.0" ,						pre_defined	=> 1, version_hash_ref => {}, },
     anaconda2 => {  name =>  "anaconda2", version => "2.2.0" ,					   m_version => "2.2.0" ,						pre_defined	=> 1, version_hash_ref => {}, },
     anaconda3 => {  name =>  "anaconda3", version => "4.3.1" ,					   m_version => "4.3.1" ,						pre_defined	=> 1, version_hash_ref => {}, },
     autoconf  => {  name =>  "autoconf",  version => "2.69" ,					   m_version => "2.69" ,						pre_defined	=> 1, version_hash_ref => {}, },
     autogen   => {  name =>  "autogen", 	 version => "5.11.1" ,				   m_version => "5.11.1" ,					pre_defined	=> 1, version_hash_ref => {}, },
     automake  => {  name =>  "automake",  version => "1.15" ,					   m_version => "1.15" ,						pre_defined	=> 1, version_hash_ref => {}, },
     boost     => {  name =>  "boost", 		 version => "4.7.4" ,					   m_version => "4.7.4" ,						pre_defined	=> 1, version_hash_ref => {}, },
     cmake     => {  name =>  "cmake", 		 version => "3.0.2" ,					   m_version => "3.0.2" ,						pre_defined	=> 1, version_hash_ref => {}, },
     cuda      => {  name =>  "cuda", 		 version => "8.0.61/gcc/4.4.7",  m_version => "8.0.61/gcc/4.4.7", pre_defined	=> 1, version_hash_ref => {}, },
     gcc       => {  name =>  "gcc", 			 version => "4.7.4" ,					   m_version => "4.7.4" ,           pre_defined	=> 1, version_hash_ref => {}, },
     intel     => {  name =>  "intel",     version => "14.0" ,						 m_version => "14.0" ,						pre_defined	=> 1, version_hash_ref => {}, },
     java      => {  name =>  "java", 		 version => "jdk1.8.0_20" ,		   m_version => "jdk1.8.0_20" ,			pre_defined	=> 1, version_hash_ref => {}, },
     libtool   => {  name =>  "libtool", 	 version => "2.4.6" ,					   m_version => "2.4.6" ,						pre_defined	=> 1, version_hash_ref => {}, },
     openmpi   => {  name =>  "openmpi", 	 version => "2.0.2/gcc/5.3.0" ,  m_version => "2.0.2/gcc/5.3.0" ,	pre_defined	=> 1, version_hash_ref => {}, },
     perl      => {  name =>  "perl", 		 version => "5.24.0-thread" ,	   m_version => "5.24.0-thread" ,		pre_defined	=> 1, version_hash_ref => {}, },
     pgi       => {  name =>  "pgi", 			 version => "17.3" ,					   m_version => "17.3" ,						pre_defined	=> 1, version_hash_ref => {}, },
     python    => {  name =>  "python", 	 version => "2.7.13" ,				   m_version => "2.7.13" ,					pre_defined	=> 1, version_hash_ref => {}, },
     python2   => {  name =>  "python2", 	 version => "2.7.13" ,				   m_version => "2.7.13" ,					pre_defined	=> 1, version_hash_ref => {}, },
     python3   => {  name =>  "python3", 	 version => "3.5.1" ,					   m_version => "3.5.1" ,						pre_defined	=> 1, version_hash_ref => {}, },
     r         => {  name =>  "R", 				 version => "3.1.2" ,					   m_version => "3.1.2" ,						pre_defined	=> 1, version_hash_ref => {}, },
     ruby      => {  name =>  "ruby", 		 version => "2.4.2" ,					   m_version => "2.4.2" ,						pre_defined	=> 1, version_hash_ref => {}, },
 	 };
	 #'module_hash_ref' => {
	 #	'xz' => {
	 #		'5.2.3' => {
	 #		 'version' => '5.2.3',
	 #		 'path' => '/usr/local/modulefiles',
	 #	 'm_version' => '5.2.3',
	 #		 'name' => 'xz',
	 #	 'isDefault' => 0
	 #	 }
	 #	},
	 #}

	return $_self;
}
sub setModule{ 
	my ($_self, $_name, $_version)=@_;
	$_version ="" if (!defined $_version);
	my $_lc_name = lc($_name);
	if( defined $_self->{module_hash_ref} ){
	  $_self->{module_hash_ref}{$_lc_name}{name}=$_name;
	  $_self->{module_hash_ref}{$_lc_name}{version}=$_version;
  } 
}

sub getModule{ 
	my ($_self, $_name, $_version)=@_;
	$_version ="" if (!defined $_version);
	my $_lc_name = lc($_name);
	if( defined $_self->{module_hash_ref} && exists $_self->{module_hash_ref}{$_lc_name}){
	  $_name = $_self->{module_hash_ref}{$_lc_name}{name};
	  $_version = $_self->{module_hash_ref}{$_lc_name}{version};  } 
  return ($_name, $_version);
}
# perl -e '@a=("module list"); foreach $i(@a) {print "i=$i\n";$i=~m/^module(\s+?)(.+)$/; $cmd=$2;$r=qx(/usr/local/apps/lmod/lmod/libexec/lmod perl $cmd &>tmp.log); print "r=$r\ncmd=$cmd\n";  } '
# perl -e '@a=("module avail"); foreach $i(@a) {print "i=$i\n";$i=~m/^module(\s+?)(.+)$/; $cmd=$2;$r=qx(/usr/local/apps/lmod/5.8/libexec/lmod perl $cmd &>tmp.log ); print "r=$r\ncmd=$cmd\n";  } '

sub moduleAvail{
	my ($_self, $_module_name_pattern)=@_;
	my $_log = "tmp_module_avail.log";
  $_module_name_pattern = "pcre java  xz" if (!defined $_module_name_pattern);
	say __FILE__." Line ".__LINE__.", line $_self->{lmod_app_path}/libexec/lmod". (-f "$_self->{lmod_app_path}/libexec/lmod");
  qx($_self->{lmod_app_path}/libexec/lmod perl avail $_module_name_pattern &>$_log) if (defined $_self->{lmod_app_path});
  $_self->pasreAvailLog($_log);}
sub pasreAvailLog {
	my ($_self, $_file)=@_;
	open my $_in, "<", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for reading: $!\n";
	my ($_name, $_version, $_path, $_is_default)=("","",undef,0);
	
	my $_flag=0;
	my $_visted=0;  while (my $_line=<$_in>){
	  $_line=Kpan::KUtil::Trim($_line);
	  next if( length($_line)==0); 
	  # perl -e '$a="--- /home/yhuang/modulefiles --"; if ($a =~ m/-+\s+(.+)\s+-+$/ ){print "1=$1,2=$2,3=$3\n"}' 
	  # perl -e '$a="    kpan/0.5    kpan/0.6 (L,D)   kpan/1.0  "; @m=( $a =~ m|\s([^\s]+)\s|g ); foreach (@m) {print "$_,";}print "\n";' 
	  # perl -e '$a="    kpan/0.5    kpan/0.6 (L,D)   kpan/1.0  "; @m=split(/\s+/,$a); foreach (@m) {print "$_,";}print "\n";' 
	  # perl -e '$a="   StdEnv                                    (D) "; if ($a =~ m|^\s+([^/ ]+)\s+(\S*)| ){print "1=$1,2=$2,3=$3\n"}' # OK
	  say __FILE__." Line ".__LINE__.", line=$_line" if(defined $_self->{debug} && $_self->{debug}>6);
	  
	  if($_line=~ m/-+\s+(\S+)\s+-+$/ || $_line =~ /^\s*Where:\s*$/){ #--------------------------- /home/yhuang/modulefiles ---------------------------
	    $_path=$1;
	    if (defined $_path && defined $_self->{lmod_dir_prefix} && $_path eq $_self->{lmod_dir_prefix}){
	    	$_flag = 1; # only capture offical modules, not the system or local ones
	    	next;
	    }else{
	    	$_flag = 0 ;
	    }
	    say __FILE__." Line ".__LINE__.", path=$_path, flag=$_flag" if(defined $_self->{debug} && $_self->{debug}>6); 
	  }
	  my $_m_version="";
	  if($_flag>0){
	  	my @_m = split (' ',$_line);
	  	say __FILE__." Line ".__LINE__.", path=$_path, line=$_line\n".Dumper(\@_m) if(defined $_self->{debug} && $_self->{debug}>6);
	  	
	  	for (my $_i=0; $_i<=scalar(@_m); $_i++ ){
	  		my $_unit=Kpan::KUtil::Trim($_m[$_i]);
	  		$_is_default=0;
	  		next if ($_unit =~ /^\s*$/ || $_unit =~ /moduleData/i || $_unit =~ m/^\s*\(.+\)\s*$/ );
	  		if (defined $_unit && $_unit =~ m|^(.+?)/(.+)$|){
				  $_name=$1;	    
				  $_m_version=$2;
				  $_version=$2 if(defined $_path && $_unit =~ m|^(.+)/(.+?)$| ); # agressive match last /
        }elsif (defined $_unit){
				  $_name=$_unit;	    
				  $_m_version="";
				  $_version="";
        }
        my $_j=$_i+1;
	      # perl -e '$a="(D)"; if ($a =~ m/^\s*\((L,)*D\)\s*$/ ){print "1=$1,2=$2,3=$3\n"}' 	        
	      if (defined $_m[$_j] && $_m[$_j] =~ m/^\s*\((L,)*D\)\s*$/ ){
				  $_is_default=1;
				}
	      if (defined $_m[$_j] && $_m[$_j] =~ m/^\s*\(.+\)\s*$/ ){
				  $_i++;
				}
				my $_lc_name = lc ($_name );
				my $_exist_flag = 0;
				$_exist_flag = 1 if ( defined $_self->{module_hash_ref}{$_lc_name}{name} );
				#say __FILE__." Line ".__LINE__.",i=$_i, pcre exists? ".(grep {$_==$_name} values @{$_self->{module_hash_ref}{$_lc_name}{name}})  if ($_lc_name eq "pcre"); 
				if ($_lc_name =~ m/(pcre|java)/ && $_visted<3){
					say __FILE__." Line ".__LINE__.", i=$_i, exist?=$_exist_flag,  ".Dumper($_self);				
				  $_visted++;        } 
				say __FILE__." Line ".__LINE__.",  pcre exists? ".(exists $_self->{module_hash_ref}{$_lc_name})  if ($_lc_name eq "pcre"); 
				my $_version_hash_ref = \%{$_self->{module_hash_ref}{$_lc_name}{version_hash_ref}};
			  $$_version_hash_ref{$_version} = {name => $_name, version => "$_version",m_version => "$_m_version", path => $_path, isDefault => $_is_default };
				next if ($_lc_name =~ m/^\s*d\s*/ ); # skip D or d lines missed by above
				if( $_exist_flag==1 || ($_is_default && (!defined $_self->{module_hash_ref}{pre_defined} || $_self->{module_hash_ref}{pre_defined} !=1 ))){ #default, don't over write pre-defined
				  $_self->{module_hash_ref}{$_lc_name}{name}       = $_name;
				  $_self->{module_hash_ref}{$_lc_name}{version}    = $_version;
				  $_self->{module_hash_ref}{$_lc_name}{m_version}  = $_m_version;
 				  $_self->{module_hash_ref}{$_lc_name}{path}       = $_path;
       }					
				$_self->{module_hash_ref}{$_lc_name}{version_hash_ref} = $_version_hash_ref ;
				
				say __FILE__." Line ".__LINE__.", i=$_i, u=$_unit, n-$_name, mv-$_m_version, v=$_version, d=$_is_default,exist=$_exist_flag =".($_exist_flag == 1) if(defined $_self->{debug} && $_self->{debug}>1); 
	    }
	  }
	}}

1;

