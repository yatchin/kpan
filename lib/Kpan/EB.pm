#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

# parse/write eb receipt 
package Kpan::EB;
use 5.010;
use strict;
#use warnings;
use File::Find::Rule;
use File::Basename;
#use Data::Dumper;
#use feature qw(say);
#use FindBin qw($Bin);
#use lib "$Bin/../../lib/Kpan";
#use Prereq;
#use DBData;
#use KUtil;

our $VERSION = '1.5.8';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		name               => shift,
		version            => shift, 
		home_url           => shift,
		archive_url        => shift,
		version_url        => shift,
		brief              => shift,
		category           => shift,
		#
		prereq_hash_ref    => {}, #$_hash{$_prereq_order}=$_p; $_p name, version object {}, #foreach my $i (0..$#v) {$version_url_hash{$v[$i]}=$u[$i];}
		prereq_lib_hash_ref=> {}, #R/python/perl lib
		#
		eb_hash_ref        => {},
  	content            => shift,
		file_path          => shift,
		file_name          => shift,
		debug              => shift,
		template           => "$FindBin::Bin/../eb/TEMPLATE.eb", #
		note               => shift
  };		
  return $_self;
}
sub parseEB {
	my ($_self, $_dir) = @_;  
	say __FILE__." Line ".__LINE__.", looking for receipts" if(defined $_self->{debug} && $_self->{debug}>0);  my @_files = File::Find::Rule->file()
                            ->name( '*.eb' )
                            ->in( $_dir );
 	say __FILE__." Line ".__LINE__.", found ". (scalar @_files) ." receipts" if(defined $_self->{debug} && $_self->{debug}>0);                           
  my $_db_data        =Kpan::DBData->new();
  $_db_data->{debug}  =$_self->{debug};
  my $i=0;
  for my $_f (@_files){
    my $_eb = Kpan::EB->new;    $_eb->parseEBFile($_f);
	  my %_uhash= %{$_eb};
	  #$_db_data->uploadToEBApp(\%_uhash);
    say __FILE__." Line ".__LINE__.",$_eb->{name}/$_eb->{version}" if(defined $_self->{debug} && $_self->{debug}>0 && $i<3);
    say __FILE__." Line ".__LINE__.",".Dumper(\%_uhash) if(defined $_self->{debug} && $_self->{debug}>0 && $i<2);
    $i++;
  }   
  say __FILE__." Line ".__LINE__.",total $i receipts" if(defined $_self->{debug} && $_self->{debug}>0);
                       
	return;
}

sub parseEBFile {
	my ($_self, $_file) = @_;

	open my $_in, "<", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for reading: $!\n";
	$_self->{file_path}=$_file;
	$_self->{file_name}=basename($_file);
	my %_eb_hash=();
	my $_content="";
	while (my $_line=<$_in>){
		$_content.=$_line;
	  $_line=Kpan::KUtil::Trim($_line);
	  next if( length($_line)==0); 
	  if($_line=~ m/^toolchain\s*=\s*(.+)/){ #toolchain = {\'name\': \'foss\', \'version\': \'2016a\'}
	    $_self->{toochain}  =  $1;
	    if($_self->{toochain} =~ m/name.?:(.+?),/ ){
  	    $_self->{toochain_name}  =  $1;
  	    $_self->{toochain_name}  =~ s/'//g if (defined $_self->{toochain_name}) ;
	    }
	    if($_self->{toochain} =~ m/version.?:(.+?),/ ){
  	    $_self->{toochain_version}  =  $1;
  	    $_self->{toochain_version}  =~ s/'//g if (defined $_self->{toochain_version}) ;
	    }
	  }elsif($_line=~ m/^name\s*=\s*['"]+(.+?)['"]+/){
	    $_self->{name}=$1;
	  }elsif($_line=~ m/^version\s*=\s*['"]+(.+?)['"]+/){
	    $_self->{version}=$1;
	  }elsif($_line=~ m/^homepage\b.+=\s*['"]+(.+?)['"]+/){
	    $_self->{home_url}=$1;
	  }elsif($_line=~ m/^sources.+=\s*([['"])+(.+?)([['"\]])+/){
 	    $_self->{version_file_name}=$2;
	  }elsif($_line=~ m/^source_urls.+=\s*([['"]+(.+?)[]'"])+/){ 
			# source_urls = [GNU_SOURCE], ['http://ftp.abinit.org/'], ['https://github.com/bcgsc/abyss/releases/download/%(version)s/'],
			# ['http://ftp.gnome.org/pub/GNOME/sources/%(namelower)s/%(version_major_minor)s'], [('http://sourceforge.net/projects/amos/files/%s/%s' % (name.lower(), version), 'download')]
	    $_self->{version_url_head}=$1;
      $_self->{version_url_head}=~ s/[['"\\]//g;
	    if($_line=~ m/\[homepage]\s*$/){
				 # $_self->{version_url}=$_self->{home_url};
			}elsif($_line=~ m/^source_urls.+=[^']+\'(.+%.+?)\'+/){
	      # perl -e '$a="(Autotools, 20150215, , (GNU, 4.9.3-2.25)) (Auto, 2015)"; @b= $a=~ m/\((.+?)\)/g; foreach $c(@b) {print "$c\n";}'
			  my $_tmpu=$1;			         $_tmpu=~ s/%\(version_major_minor\)s/$_self->{version}/g;
        $_tmpu=~ s/%\(version\)s/$_self->{version}/g;
        $_tmpu=~ s/%\(name\)s/$_self->{name}/g;
        my $_lc_name=lc($_self->{name});
        $_tmpu=~ s/%\(namelower\)s/$_lc_name/g;
        $_tmpu=~ s/[['"\\]//g;
        $_self->{version_url}=$_tmpu;
			}
	  }elsif($_line=~ m/^moduleclass\s*=(.+)/){
	    $_self->{category}  =  $1;
	    $_self->{category}  =~ s/['"\s]//g if (defined $_self->{category}) ;
	  }elsif($_line=~ m/^sanity_check_commands\s*=(.+)/){
	    $_self->{test_command}  = $1;
      $_self->{test_command}  =~ s/[['"\s\]]//g if (defined $_self->{test_command}) ;	  }
	  if($_line=~ m/(\S+?)\s*=\s*(.+)/ && $_line!~ m/dependencies|homepage|version|name|sources|source_urls|description|moduleclass|toolchain |sanity_check_commands|sanity_check_paths/){
	  	my $_key=Kpan::KUtil::Trim($1);
	  	my $_value=Kpan::KUtil::Trim($2); 
	  	$_value=Kpan::KUtil::RemoveHeadTail($_value) if (defined $_value);	      $_eb_hash{$_key}=$_value if (defined $_key && defined $_value);	
	  }
  }
  if($_content=~ m/builddependencies\s*=\s*\[(.+?)]/s){
	  	#use re 'debug';
	    $_self->{builddependencies}=$1;
 	    #$_self->{builddependencies}=~ s/\'//g; 
	    #say "$_self->{builddependencies}";
	    #no re 'debug';
	}
	if($_content=~ m/(?<!build)dependencies\s*=\s*\[(.+?)]/s){
	    $_self->{dependencies}=  $1;
  	  $_self->{dependencies}=~ s/[\n\r]/ /g if (defined $_self->{dependencies} ) ; 
  	  $_self->{dependencies}=~ s/^\s+//g    if (defined $_self->{dependencies} ) ; 
  	  $_self->{dependencies}=~ s/\s+$//g    if (defined $_self->{dependencies} ) ; 
	}
	if($_content=~ m/description\s*=\s*['"]+([^"]+?)['"]/s){
	    $_self->{brief} = $1;
	    $_self->{brief} =~ s/[\n\r]/ /g  if (defined $_self->{brief} );
	    $_self->{brief} =~ s/^\s+//g     if (defined $_self->{brief} );
	    $_self->{brief} =~ s/\s+$//g     if (defined $_self->{brief} );
	}
	if($_content=~ m/sanity_check_paths\s*=\s*{(.+?)}/s){
	    $_self->{sanity_check_paths} = $1;
	    $_self->{sanity_check_paths} =~ s/[\n\r]/ /g  if (defined $_self->{sanity_check_paths} );
	    $_self->{sanity_check_paths} =~ s/^\s+//g     if (defined $_self->{sanity_check_paths} );
	    $_self->{sanity_check_paths} =~ s/\s+$//g     if (defined $_self->{sanity_check_paths} );
	}
	# perl -e '$a="(Autotools, 20150215, , (GNU, 4.9.3-2.25)) (Auto, 2015)"; @b= $a=~ m/\((.+?)\)/g; foreach $c(@b) {print "$c\n";}'
  if( defined $_self->{dependencies} && $_self->{dependencies} =~ m/\((.+?)\)/g ){
  	#$_self->{dependencies} =~ s/['"]//g;
  	my @_arr= ($_self->{dependencies} =~  m/\((.+?)\)/g );
		foreach my $_e(@_arr){
			if($_e =~ m/(.+?),(.+)/ ){
				my $_pq = Kpan::Prereq->new();
				$_pq->{name}=$_self->{name};
				$_pq->{version}=$_self->{version};
				$_pq->{prereq_name}=$1;
				$_pq->{prereq_version}=$2;
				next if (!defined  $_pq->{prereq_name});
				$_pq->{prereq_name}=Kpan::KUtil::RemoveHeadTail($_pq->{prereq_name});	
				$_pq->{prereq_version}=~ s/(.+?),.+/$1/ if(defined $_pq->{prereq_version});
				$_pq->{prereq_version}=Kpan::KUtil::RemoveHeadTail($_pq->{prereq_version}) if(defined $_pq->{prereq_version});	
				$_pq->addPrereq($_self->{prereq_lib_hash_ref},$_self->{prereq_hash_ref});
			}
		}
  }
  if(defined $_self->{builddependencies} &&  $_self->{builddependencies} =~m/\((.+?)\)/g ){
 	  $_self->{builddependencies}=~ s/'//g; 
	 	my @_arr= $_self->{builddependencies} =~ m/\((.+?)\)/g  ;    say __FILE__." Line ".__LINE__.",$_self->{builddependencies}\n".Dumper(\@_arr) if(defined $_self->{debug} && $_self->{debug}>6);
		foreach my $_e(@_arr){
			if($_e =~ m/(.+?),(.+)/){
				my $_pq = Kpan::Prereq->new();
  		  $_pq->{name}=$_self->{name};
  		  $_pq->{version}=$_self->{version};
				$_pq->{prereq_name}=$1;
				$_pq->{prereq_version}=$2;
				next if (!defined  $_pq->{prereq_name});
				$_pq->{prereq_name}=Kpan::KUtil::RemoveHeadTail($_pq->{prereq_name});	
				$_pq->{prereq_version}=~ s/(.+?),.+/$1/ if(defined $_pq->{prereq_version});
				$_pq->{prereq_version}=Kpan::KUtil::RemoveHeadTail($_pq->{prereq_version}) if(defined $_pq->{prereq_version});	
				$_pq->addPrereq($_self->{prereq_lib_hash_ref},$_self->{prereq_hash_ref});
			}
		}
  }
  $_self->{eb_hash_ref}=\%_eb_hash;
  $_self->{version_file_name}=~ s/\Q%(version)\E/$_self->{version}/ if( defined $_self->{version_file_name} && $_self->{version_file_name}=~ m/\Q%(version)\E/ );
  $_self->{version_url}=$_self->{version_url_head}.$_self->{version_file_name} if (!defined  $_self->{version_url} && defined $_self->{version_url_head} && defined $_self->{version_file_name});
  $_self->{content}=$_content;
  say __FILE__." Line ".__LINE__.",f=$_file".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4);  
	return 1;
}

sub writeEBFile{
  my ($_self,)=@_;
  my $_t_file = $_self->{template};
  my $_name = $_self->{name};
  my $_lc_name = lc($_name);
  say __FILE__. " Line " . __LINE__ . ", self=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 0 );

	open my $_in,  "<", $_t_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_t_file for reading: $!\n";
	my $_content="";
	while (my $_line=<$_in>){
    my $_new_line="";
	  $_line=Kpan::KUtil::Trim($_line);
	  next if( $_line=~ m/^#/); # skip commetns 
	  if($_line=~ m/^name\s*=/){
	    $_new_line = "name = '".$_self->{name}."'";
	  }elsif($_line=~ m/^version\s*=/){
	    $_new_line = "version = '".$_self->{version}."'";
	  }elsif($_line=~ m/^homepage\s*=/){
	    $_new_line = "homepage = '".$_self->{home_url}."'";
	  }elsif($_line=~ m/^description\s*=/){
	    $_new_line = defined $_self->{brief}? "description = \"\"\"$_self->{brief}\"\"\"" :$_line ;
	  }elsif($_line=~ m/^sources\s*=/){
	    $_new_line = "sources = ['".$_self->{file_name}."']";
	  }elsif($_line=~ m/^source_urls\s*=/){ 
			# source_urls = [GNU_SOURCE], ['http://ftp.abinit.org/'], ['https://github.com/bcgsc/abyss/releases/download/%(version)s/'],
			#['http://ftp.gnome.org/pub/GNOME/sources/%(namelower)s/%(version_major_minor)s'], [('http://sourceforge.net/projects/amos/files/%s/%s' % (name.lower(), version), 'download')]
      $_new_line = "source_urls = ['".$_self->{version_url}."']";
	  }elsif($_line=~ m/^dependencies\s*=/){
	  	$_new_line = "dependencies = [";	  	
	  	#$_self->{prereq_lib_hash_ref} # todo add sub module such as perl:DBI
	  	#last if (!defined $_self->{prereq_hash_ref});
	  	#my %_pq_order_hash = %{ $_self->{prereq_hash_ref} };
	  	
       foreach my $_pq_name_key (sort keys %{$_self->{prereq_lib_hash_ref}}) { # key = perl|python.*|R
        my %_pq_order_hash = %{${$_self->{prereq_lib_hash_ref}{$_pq_name_key}}};
        my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
        say __FILE__." Line ".__LINE__.", prereq:".Dumper(\@_keys);
        for my $_i (0.. scalar(@_keys) ) { 
          my $_pq = $_pq_order_hash{$_keys[$_i]};
          $_new_line .=	"('".$_pq->{prereq_name}."','".$_pq->{prereq_version}."'),";
        }
			}
      my @_keys = sort  { $a <=> $b } keys %{$_self->{prereq_hash_ref}} ; 
		  #say __FILE__." Line ".__LINE__.", prereq:".Dumper( \%_pq_order_hash ) if(defined $_self->{debug} && $_self->{debug} >1);
    	for my $_i (0..$#_keys) { 
			  my $_pq = $$_self->{prereq_hash_ref}{$_keys[$_i]}; 
				$_new_line .=	"('".$_pq->{prereq_name}."','".$_pq->{prereq_version}."'),";
			}
			$_new_line .= "]\n";
			#dependencies = [
			#('Java', '1.8.0_92', '', True),
			#('ant', '1.9.7', '-Java-%(javaver)s', True),
			#('Bowtie', '1.1.2'),
			#('Bowtie2', '2.2.8'),
			#]
	  }elsif($_line=~ m/^moduleclass\s*=/){
	  	$_new_line = "moduleclass = '".$_self->{category}."'" if (defined $_self->{category}) ;
		} else{
		  $_new_line=$_line;
		}
		$_content.=$_new_line."\n";
  }
  # perl -e '$a="(Autotools, 20150215, , (GNU, 4.9.3-2.25)) (Auto, 2015)"; @b= $a=~ m/\((.+?)\)/g; foreach $c(@b) {print "$c\n";}'
 
  $_self->{content}=$_content;
  my $_app_file = $_self->{name};
  my $_first_letter = lc(substr($_self->{name}, 0,1));
  $_first_letter = 0 if ($_first_letter =~ /\d/);
  my $_dir = "$FindBin::Bin/../eb/$_first_letter";
  mkdir $_dir if (! -d $_dir);
	open my $_out, ">", "$_dir/$_app_file" or die  __FILE__." Line ".__LINE__.",  couldn't open file $_dir/$_app_file for writing $!\n";
	say $_out $_content;
  say __FILE__." Line ".__LINE__.",f=$_app_file, $_content" if(defined $_self->{debug} && $_self->{debug}>2);   }

1;
