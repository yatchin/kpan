#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

#test command and capture output of options
package Kpan::Man;
use 5.010;
use strict;
use warnings;
use File::Find::Rule;
use File::Basename;
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use KUtil;
our $VERSION = '0.1';

my $lmod_app_path="";
sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		path => shift,
		command => shift,
		options => shift,
		lmod_arr_ref => shift,
		other_dirs=>shift,
		command_return_hash_ref=>shift,
		install_dir_prefix => shift,
		kp_name  => shift, # smatools, 1.0_gcc447
		kp_version  => shift, # smatools, 1.0_gcc447		name               => shift,
		version            => shift, 
		main_command       => shift,
		main_lmod					 => shift,
		command_hash_ref   => {}, #$_hash{$_prereq_order}=$_p; $_p name, version object {}, #foreach my $i (0..$#v) {$version_url_hash{$v[$i]}=$u[$i];}
		extra_env_hash_ref=> {}, #export PATH=$PATH./python/perl lib
		#
		eb_hash_ref        => {},
  	content            => shift,
		file_path          => shift,
		file_name          => shift,
		debug              => shift,
		note               => shift,
		debug  => shift,
  }		
}

sub manCmd {
	my ($_self, $_command, $_option, $_lmod_arr_ref, $_path, $_program, $_is_interactive, $_log) = @_;  #$_options: null,man, -h, --help; $_path: tools, scripts; $_program: perl/python2.7; 

  my @_commands=("module purge");
  my @_lmod_arr=();
  @_lmod_arr=@$_lmod_arr_ref if (defined $_lmod_arr_ref);
  #say __FILE__." Line ".__LINE__.", prereq_lib_hash_ref:".Dumper($_prereq_lib_hash_ref);
  my $_lmod;
  foreach my $_mod (@_lmod_arr) { #     
    $_lmod .= " $_mod" if (defined $_mod);    push (@_commands, $_mod);
  }
  $_lmod ="module load ".$_lmod if (defiend $_lmod);
      my $_is_x_or_script_only=1;

  push(@_commands,$_command);
  
	say __FILE__." Line ".__LINE__.", cmd=$_command \n" if(defined $_self->{debug} && $_self->{debug}>1); 
  my ($_is_log,$_is_run)=(0,0);
  $_is_log=1;
	$_self->runCmd( $_log ,\@_commands,$_is_log,$_is_run);  
	                        
	return 1;
}

sub runCmd{
  my ($_self, $_log_file, $_cmd_array,$_is_log,$_is_run)=@_;
  my @_commands= @{$_cmd_array};
  my $_is_fail=0;
  my $_failed_cmd="";
  foreach my $_cmd (@_commands){
    #say __FILE__." Line ".__LINE__.", $_cmd, log is $_log_file,_is_fail=$_is_fail, _is_log=$_is_log";
   	open my $_fout, ">> $_log_file" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_file for write: $!\n";
   	if(defined $_is_log && $_is_log>0){
      say $_fout "$_cmd";
      next;
		}
  	# perl -e ' eval `/usr/local/apps/lmod/lmod/libexec/lmod perl avail`; '
 	  # perl -e '$a=eval {system "/usr/local/apps/lmod/lmod/libexec/lmod perl avail"}; print "a\n" '
 	  # perl -e '$a="##"; $a=~ s/^##//; print "$a\n" '
    if(defined $_is_run && $_is_run>0){ #1 not log   2# not run, log, 3# log comments
      next;
    }elsif(($_cmd =~ m/^\#\#/)  ){ #1 not log   2# not run, log, 3# log comments
      my $_cmd2=$_cmd;
      $_cmd2 =~ s/^##//; # remove 2 #
      say $_fout "$_cmd2";
      next;
    }elsif( $_cmd=~ m/^[^#]/) { #1 not log   2# not run, log, 3# log comments
      say $_fout "$_cmd";

    }elsif($_cmd =~ m/^\s*$/ ){ #empty line
      next;
		}
    if(!$_is_fail){ #   1# not log, run; 
      $_cmd =~ s/^\#// if($_cmd =~ m/^\#/); # remove one #
	    #say __FILE__." Line ".__LINE__.", $_cmd, _is_fail=$_is_fail";
		  if( $_cmd =~ /^cd(\s+?)(\S+)/){
		    my $_d= $2;
		    eval { chdir $_d }; warn $@ if $@;
		    if(defined $ENV{"$_d"}){
		      $ENV{PWD}="$_d:$ENV{PWD}";
		    }else{
		      $ENV{PWD}="$_d";
		    }
				say __FILE__." Line ".__LINE__.", $_cmd, $_d, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
		  }elsif($_cmd =~ /^\s*module\s+(purge)\s*$/){ # > /dev/null
		     my $_cmd2=$1;
		     eval { system ("$lmod_app_path/libexec/lmod", "perl", "purge") } ;
		     #eval { system ("$lmod_app_path/libexec/lmod", "perl", "purge", "&> /dev/null") } ;
  	  }elsif($_cmd =~ /^module(\s+?)(\S+)/){
		  	my $_cmd2=$2;
		    eval { system ("$lmod_app_path/libexec/lmod", "perl", "$_cmd2")};
		  }elsif($_cmd =~ /^export(\s+?)(\S+?)=(.+)/){
		    my $_env_name=$2;
		    my $_env_value=$3;
		    if(defined $ENV{$_env_name}){
		      $ENV{$_env_name}="$_env_value:$ENV{$_env_name}";
		    }else{
		      $ENV{$_env_name}="$_env_value";
		    }
		  }elsif(!$_is_fail){
		    say __FILE__." Line ".__LINE__.", $_cmd" if(defined $_self->{debug} && $_self->{debug}>1);
        $_is_fail = eval {system "$_cmd"}; 
        if($_is_fail){
        	my $_pwd = cwd();
          $_failed_cmd=$_cmd;
        }
      }
    }
  }  
	if($_is_fail){
  	open my $_fout, ">> $_log_file" or die __FILE__." Line ".__LINE__.",  couldn't open log file $_log_file for write: $!\n";
  	my $_pwd = cwd();
		say $_fout "failed at $_failed_cmd, pwd is $_pwd";
		#die(__FILE__." line ".__LINE__."  $_failed_cmd \nfailed at $_failed_cmd, , pwd is $_pwd\n");
	}
}
