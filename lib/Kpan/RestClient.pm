#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Tianyue Huang, <thuang@omicsci.com>, August 2017
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

package Kpan::RestClient;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use LWP::UserAgent;
use JSON;
our $VERSION = '1.6.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
	  server_hostname => "omicsci.com", #"75.131.28.7", # "moisci.com"
    server_port     => 13000,
    ua              => LWP::UserAgent->new,
		debug           => shift,
  };
	return $_self;
}

#sends get request to the server at $server_hostname on port $server_port
#args: method name, request content hashref
#returns: hashref containing response or undef for failure
sub server_method_call {
    my ($_self, $_server_method_call,$_request_content_hashref ) = @_;
    say __FILE__." Line ".__LINE__.", server_method_call=$_server_method_call, request_content_hashref=".Dumper($_request_content_hashref) if(defined $_self->{debug} && $_self->{debug}>4);
    my $_request = HTTP::Request->new(GET => sprintf("http://%s:%d/%s", $_self->{server_hostname},$_self->{server_port},$_server_method_call));
    $_request->header('Content-Type' => 'application/json');
    $_request->content($_self->serialize($_request_content_hashref));
    my $_response = $_self->{ua}->request($_request);
    if ($_response->is_success) {
        say __FILE__." Line ".__LINE__.", Received reply: ",$_response->decoded_content if(defined $_self->{debug} && $_self->{debug}>4);
        return $_self->deserialize($_response->decoded_content);
    } else {
        say __FILE__." Line ".__LINE__.", HTTP GET error code: ", $_response->code if(defined $_self->{debug} && $_self->{debug}>4);
        say __FILE__." Line ".__LINE__.", HTTP GET error message: ", $_response->message if(defined $_self->{debug} && $_self->{debug}>4);
        return undef;
    }
}

# 
# Kpan functions
#
sub getNameHashFromCollection {
  my ($_self, $_client, $_pattern, $_size) = @_;
  say __FILE__. " Line " . __LINE__ . ", getNameHashFromCollection=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 0 );  
  my $_kpan_client_name = $_client->{kpan_client_name};
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    pattern => $_pattern,
    size => $_size,
  };
  return $_self->server_method_call("getNameHashFromCollection",$_content);
}
sub getClientID { #http://75.131.28.7:13000/id
  my ($_self, $_client) = @_;
  say __FILE__. " Line " . __LINE__ . ", getClientID=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 0 );  
  my $_kpan_client_name = $_client->{kpan_client_name};
  my $_cluster_system   = $_client->{cluster_system};
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    kpan_client_name  => $_kpan_client_name,
    cluster_system    => $_cluster_system,
  };
  my $_ret = $_self->server_method_call("getClientID",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 0 );   
  $_client->{id} = $_ret->{content};
  return $_ret->{content};
}
sub testConnection { 
  my ($_self) = @_;
  say __FILE__. " Line " . __LINE__ . ", test connection" if ( defined $_self->{debug} && $_self->{debug} > 0 );  
  my $_content= {
  };
  my $_ret = $_self->server_method_call("testConnection",$_content);
  say __FILE__. " Line " . __LINE__ . ", conn=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 9 );   
  return $_ret->{content};
}

sub getClient        {
	my ($_self, $_client, $_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev,$_is_all_client, $_debug)=@_; 
	$_kpan_client_name    = $_client->{kpan_client_name} if (!defined $_kpan_client_name);
  $_group_name          = $_client->{group_name}       if (!defined $_group_name);
  $_org_name            = $_client->{org_name}         if (!defined $_org_name);
  $_cluster_system      = $_client->{cluster_system}   if (!defined $_cluster_system);
  my $_ip               = $_client->{ip};
  my $_os_user_name     = $_client->{os_user_name};
  my $_hostname         = $_client->{hostname};
  my $_id               = $_client->{id};
  my $_user_name        = (defined $_kpan_client_name && $_kpan_client_name =~ m/\S/)? $_kpan_client_name:"yhuang_kpan.org" ; # enable login to get client from getInstallHash

  my $_content= {
    auth => {username => $_user_name, password => undef,},
    kpan_client_name => $_kpan_client_name,
    os_user_name     => $_os_user_name,
    hostname         => $_hostname,
    cluster_system   => $_cluster_system,
    org_name         => $_org_name,
    group_name       => $_group_name,
    is_kpan_dev      => $_is_kpan_dev,
    is_all_client   => $_is_all_client,
    ip               => $_ip,
    id               => $_id,
  };
  say __FILE__. " Line " . __LINE__ . ", before DB client=".Dumper($_client) if ( defined $_self->{debug} && $_self->{debug} > 3 ); 
  my $_ret = $_self->server_method_call("getClient",$_content);
  say __FILE__. " Line " . __LINE__ . ", content=".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 3 ); 
  say __FILE__. " Line " . __LINE__ . ", ret=".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 4 ); 
  return $_ret->{content};
}

sub fillClientself        {
	my ($_self, $_client, $_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev, $_debug)=@_; 
	my $_client_hash_ref = $_self->getClient($_cluster_system, $_kpan_client_name,	$_group_name, $_org_name,	$_is_kpan_dev, $_debug);
	 # only this response needs to copy the key to keep original client integrity
	if( defined $_client_hash_ref && (keys  %{$_client_hash_ref})==1 ){
		my $_client_id = (keys %{$_client_hash_ref})[0];
		foreach my $_key (keys %{$_client_id}) {
			$_client->{$_key} = $_client_id->{$_key} if (!defined  $_client->{$_key} && defined $$_client_id->{$_key} && $$_client_id->{$_key} =~ m/\S/ ); # defined ensures method not overwritten
		}
		say __FILE__. " Line " . __LINE__ . ", after fillClientself client=".Dumper($_client_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 3 ); 
  }
}
sub uploadClient     { 
  my ($_self, $_client) = @_;
  say __FILE__. " Line " . __LINE__ . ", uploadClient=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 4 );  
  my $_kpan_client_name          = $_client->{kpan_client_name};
  my $_cluster_system            = $_client->{cluster_system};
  my %_client_hash               = %{$_client};
	$_client_hash{default_Module}  = undef;
	$_client_hash{rc}              = undef;
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    kpan_client_name  => $_kpan_client_name,
    cluster_system    => $_cluster_system,
    client						=> \%_client_hash,
  };
  say __FILE__. " Line " . __LINE__ . ", content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 4 );   
  my $_ret = $_self->server_method_call("uploadClient",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 4 );   
  $_client->{id} = $_ret->{content} if (!defined $_client->{id} && (defined $_ret->{content} && $_ret->{content}!=0 ));
  return $_ret->{content};
}
sub getSoftwareHash  { # %hash={'$_name'=>{'$_version'=>{$_software}}} 				$_software_hash_ref->{$_db_app_name}->{$_db_version}->{home_url}	
	my ($_self,$_kpan_client_name, $_app_id, $_app_name, $_version, $_set_id)=@_;  
	
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    app_id      => $_app_id,
    app_name    => $_app_name,
    version     => $_version,
    set_id      => $_set_id,
  }; 
  my %_ret_hash=();
  my $_ret = $_self->server_method_call("getSoftwareHash",$_content);
 
  #say __FILE__. " Line " . __LINE__ . ", software ret=".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 1 ); 
  foreach my $_app_name_key ( keys  %{$_ret->{content}}){
    my @_version_keys = keys %{$_ret->{content}->{$_app_name_key}};
    foreach my $_version_k (@_version_keys ){ # these are hash, not obj software
      my $_hash_software_ref=$_ret->{content}->{$_app_name_key}->{$_version_k}; # if(defined $_ret->{content}->{$_app_name_key}->{$_version_k}); # defined ensures method not overwritten
      my $_software = Kpan::Software->new;
      foreach my $_s_key (keys %$_hash_software_ref){
      	$_software->{$_s_key}=$_hash_software_ref->{$_s_key};
      }
      say __FILE__. " Line " . __LINE__ . ", software =".Dumper($_software) if ( defined $_self->{debug} && $_self->{debug} > 3 ); 
      next if(!defined $_software->{name});
			my ($_kpan_client_name3, $_prereq_id, $_app_id3, $_app_name3,       $_version3,            $_set_id3) = 
			   ($_kpan_client_name,  undef,       undef,     $_software->{name},$_software->{version}, undef);
      say __FILE__. " Line " . __LINE__ . ", software  software->{name}=$_software->{name},software->{version}=$_software->{version}" 
        if ( defined $_self->{debug} && $_self->{debug} > 4 ); 
			my ($_prereq_lib_hash_ref,$_prereq_hash_ref)=$_self->getPrereqHash($_kpan_client_name3,$_prereq_id,$_app_id3, $_app_name3, $_version3, $_set_id3);
			#	                                                         ($_self,$_kpan_client_name, $_prereq_id,$_app_id, $_app_name, $_version, $_set_id)

			#				$_software_hash_ref->{$_db_app_name}->{$_db_version}->{home_url}	
			$_software->{prereq_hash_ref}     = $_prereq_hash_ref;
			$_software->{prereq_lib_hash_ref} = $_prereq_lib_hash_ref;
			$_ret_hash{$_app_name_key}{$_version_k}=$_software;		
      say __FILE__. " Line " . __LINE__ . ", software hash=".Dumper(\%_ret_hash) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
    }
  }
  say __FILE__. " Line " . __LINE__ . ", software hash=".Dumper(\%_ret_hash) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return \%_ret_hash;
}
sub uploadSoftware   { 
	my ($_self,$_kpan_client_name, $_software,$_cluster_system,$_os)=@_;  
	my $_name									   = $_software->{name};
	my $_version								 = $_software->{version};
	my %_hash_software = %{$_software};
	$_hash_software{prereq_hash_ref}=undef;
	$_hash_software{prereq_lib_hash_ref}=undef;
	$_hash_software{version_software_hash_ref}=undef;
	$_hash_software{version_url_hash_ref}=undef;
	$_hash_software{install_test_command_result_ref}=undef;
 
  say __FILE__. " Line " . __LINE__ . ", uploadSoftware=".Dumper(\%_hash_software) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    software						=> \%_hash_software,
  };
  say __FILE__. " Line " . __LINE__ . ", content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 4 );   
  my $_ret = $_self->server_method_call("uploadSoftware",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 4 );   
  $_software->{id} = $_ret->{content} if (!defined $_software->{id} && (defined $_ret->{content} && $_ret->{content}!=0 ));
  #my $_p_hash_ref = {%{$_software->{prereq_hash_ref}},%{$_software->{prereq_lib_hash_ref}} };
  #say __FILE__. " Line " . __LINE__ . ", after merge, ".Dumper($_p_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 4 );     
  $_self->uploadPrereqHash( $_kpan_client_name,$_software->{prereq_hash_ref},$_software->{prereq_lib_hash_ref}, $_software->{id}, $_software->{name},$_software->{version},$_cluster_system,$_os ) ;
}
sub getPrereq    { 
	my ($_self,$_kpan_client_name, $_pq,$_app_id,$_set_id)=@_;  
	my ($_prereq_id, $_app_name, $_version, $_prereq_name, $_prereq_version) = 
	($_pq->{id},$_pq->{name},$_pq->{version}, $_pq->{prereq_name},$_pq->{prereq_version} ) ;  
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    prereq_id        => $_prereq_id,
    name             => $_app_name,
    version          => $_version,
    prereq_name      => $_prereq_name,
    prereq_version   => $_prereq_version,
    app_id           => $_app_id,
    set_id           => $_set_id,
  }; 
 
  #my $_pq = Kpan::Prereq->new;
  my $_ret = $_self->server_method_call("getPrereq",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", prereq ret=".Dumper($_ret->{content}) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  foreach my $_key ( keys  %{$_ret->{content}}){
 	  $_pq->{$_key} = $_ret->{content}->{$_key} if(!defined $_pq->{$_key} && defined $_ret->{content}->{$_key} );
  	say __FILE__. " Line " . __LINE__ . ", key=$_key, v=$_ret->{content}->{$_key}" if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  }
  $_pq->{name}            = $_pq->{name}; 
  $_pq->{version}         = $_pq->{version}; 
	$_pq->{prereq_name}     = $_pq->{prereq_name};
	$_pq->{prereq_version}  = $_pq->{prereq_version};
	$_pq->{module_name}     = $_pq->{module_name} if(defined $_pq->{module_name}); 
  say __FILE__. " Line " . __LINE__ . ", prereq =".Dumper($_pq) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return $_pq;
}

sub getPrereqHash    { # %hash={'$_id'=>$_pq} $_prereq_hash_ref->{$_id}->{prereq_name}							 = shift @_row;
	my ($_self,$_kpan_client_name, $_prereq_id, $_app_id, $_app_name, $_version, $_set_id)=@_;  
	
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    prereq_id   => $_prereq_id,
    name        => $_app_name,
    version     => $_version,
    app_id      => $_app_id,
    set_id      => $_set_id,
  }; 
 
  my ($_prereq_lib_hash_ref,$_prereq_hash_ref) = ({},{});
  my $_ret = $_self->server_method_call("getPrereqHash",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", prereq ret=".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  foreach my $_id_key ( keys  %{$_ret->{content}}){
  	my $_pq = Kpan::Prereq->new;
  	foreach my $_key ( keys  %{$_ret->{content}->{$_id_key}}){
  	  $_pq->{$_key} = $_ret->{content}->{$_id_key}->{$_key};
  	  say __FILE__. " Line " . __LINE__ . ", key=$_key, v=$_ret->{content}->{$_id_key}->{$_key}" if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  	}
		$_pq->{name}  = lc($_pq->{name}); # EB 
		$_pq->{prereq_name}  = lc($_pq->{prereq_name});
		$_pq->{module_name}  = lc($_pq->{module_name}) if(defined $_pq->{module_name}); 
		$_pq->{debug}=$_self->{debug};
     say __FILE__. " Line " . __LINE__ . ", prereq =".Dumper($_pq) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
	  $_pq->addPrereq($_prereq_lib_hash_ref,$_prereq_hash_ref);	  
  }
  say __FILE__. " Line " . __LINE__ . ", get prereq hash=".Dumper($_prereq_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return ($_prereq_lib_hash_ref,$_prereq_hash_ref);
}
sub uploadPrereqHash { 
	my ($_self,$_kpan_client_name,$_prereq_hash_ref, $_prereq_lib_hash_ref, $_app_id, $_app_name, $_app_version,$_cluster_system,$_os)=@_;  
#	$_self->{debug} =1;
  say __FILE__. " Line " . __LINE__ . ",prereq_hash_ref=".Dumper($_prereq_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  say __FILE__. " Line " . __LINE__ . ", prereq_lib_hash_ref=".Dumper($_prereq_lib_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  if (defined $_prereq_hash_ref){
		foreach my $_order_key (keys %{ $_prereq_hash_ref } ) {
			my $_pq = $$_prereq_hash_ref{$_order_key};
			$_self->uploadPrereq($_kpan_client_name, $_pq,$_app_id, $_app_name, $_app_version,$_cluster_system,$_os);
		}
  }
  if (defined $_prereq_lib_hash_ref){
  	foreach my $_name_key (keys %{$_prereq_lib_hash_ref} ) {
      foreach my $_order_key (keys %{ $$_prereq_lib_hash_ref{$_name_key} } ) {
  	    my $_pq = ${$$_prereq_lib_hash_ref{$_name_key}}{$_order_key};
        say __FILE__. " Line " . __LINE__ . ",order_key=$_order_key, pq=, ".Dumper($_pq) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
			  $_self->uploadPrereq($_kpan_client_name,$_pq,$_app_id, $_app_name, $_app_version,$_cluster_system,$_os);
  	  }
  	}
  }
}
sub uploadPrereq     { 
	my ($_self,$_kpan_client_name,$_pq,$_app_id, $_app_name, $_app_version,$_cluster_system,$_os)=@_;  
	my %_hash_prereq = %{$_pq};
  #say __FILE__. " Line " . __LINE__ . ", $_app_name, $_app_version, pq=, ".Dumper($_pq).", hash=".Dumper(\%_hash_prereq) if ( defined $_self->{debug} && $_self->{debug} > 1 );   
  $_app_name = $_pq->{name} if (!defined $_app_name);
  $_app_version = $_pq->{version} if (!defined $_app_version);
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    prereq						=> \%_hash_prereq,
    app_id					=> $_app_id,
    app_name				=> $_app_name,
    app_version			=> $_app_version,
    cluster_system	=> $_cluster_system,
    os					    => $_os,
  };
  say __FILE__. " Line " . __LINE__ . ", $_app_name, $_app_version, content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 1 );   
  my $_ret = $_self->server_method_call("uploadPrereq",$_content);
  $_pq->{id} = $_ret->{content} if (!defined $_pq->{id} && (defined $_ret->{content} && $_ret->{content}!=0 ));
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 1 );   
  #my $_p_hash_ref = {%{$_software->{prereq_hash_ref}},%{$_software->{prereq_lib_hash_ref}} };
  #say __FILE__. " Line " . __LINE__ . ", after merge, ".Dumper($_p_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 1 );     
}
sub getInstallHash   { # %hash={'$_name'=>{'$_version'=>{$_software}}}
	my ($_self,$_kpan_client_name, $_id, $_client_id,$_client_name, $_app_id, $_app_name,$_kp_name, $_version,  $_kp_version, $_set_id,$_is_all_app,$_is_all_client)=@_;  
	
	$_client_name=$_kpan_client_name if(!defined $_client_name && defined $_kpan_client_name);
	
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    id              => $_id,
    all_app         => $_is_all_app,
    all_client      => $_is_all_client,
    client_id       => $_client_id,
    client_name     => $_client_name,
    app_name        => $_app_name,
    kp_name         => $_kp_name,
    version         => $_version,
    kp_version      => $_kp_version,
    app_id          => $_app_id,
    set_id          => $_set_id,
  }; 
  my %_install_hash=();
  my $_ret = $_self->server_method_call("getInstallHash",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", install ret=".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 3 ); 
  foreach my $_id_key ( keys  %{$_ret->{content}}){
    my $_install = Kpan::Install->new;
    my $_hash_install_ref = $_ret->{content}->{$_id_key};
		foreach my $_i_key (keys %$_hash_install_ref){
			$_install->{$_i_key}=$_hash_install_ref->{$_i_key};
		}
		# set software
		my ($_kpan_client_name2, $_app_id2, $_app_name2, $_version2, $_set_id2) = ($_kpan_client_name, $_install->{app_id}, undef, undef, undef);
		say __FILE__. " Line " . __LINE__ . ", install=".Dumper($_install) if ( defined $_self->{debug} && $_self->{debug} > 5 );
		say __FILE__. " Line " . __LINE__ . ", install to getSoftwareHash =$_kpan_client_name2, $_app_id2, $_app_name2, $_version2" if ( defined $_self->{debug} && $_self->{debug} > 2 );
		next if(!defined $_app_id2);
		my $_software_hash_ref = $_self->getSoftwareHash($_kpan_client_name2, $_app_id2, $_app_name2, $_version2, $_set_id2);
		foreach my $_name_key ( keys  %{$_software_hash_ref} ){
			my $_version_hash_ref = $_software_hash_ref->{$_name_key}; #$_software_hash_ref->{$_db_app_name}->{$_db_version}->{id}
			foreach my $_v_key ( keys  %{$_version_hash_ref} ){
				my $_software = $_version_hash_ref->{$_v_key}; #$_software_hash_ref->{$_db_app_name}->{$_db_version}->{id}
				$_install->{software} = $_software;
				last; # only one software return
			}
	  }
		# set client
		$_install->{client}       = Kpan::Client->new;
		$_install->{client}->{id} = $_install->{client_id};
		$_self->fillClientself($_install->{client});
		say __FILE__. " Line " . __LINE__ . ", getInstallHash, client=".Dumper($_install->{client}) if ( defined $_self->{debug} && $_self->{debug} > 5);

		my $_install_test_id      = undef;
		my $_install_test         = $_self->getInstallTest($_kpan_client_name2, $_install_test_id, $_install->{id},
		                            $_install->{kp_name}, $_install->{kp_version}, $_install->{app_id}, 
		                            $_install->{software}->{name}, $_install->{software}->{version}, $_client_id, $_client_name, );
    $_install->{install_test} = $_install_test;		
		my $_kstat_id             = undef;
		my $_kstat                = $_self->getKstat($_kpan_client_name2, $_kstat_id, $_install->{id},
		                            $_install->{kp_name}, $_install->{kp_version}, $_install->{app_id}, 
		                            $_install->{software}->{name}, $_install->{software}->{version}, $_client_id, $_client_name, );
    $_install->{kstat}        = $_kstat;		
		$_install_hash{$_id_key}  = $_install;    
  }
  say __FILE__. " Line " . __LINE__ . ", install hash=".Dumper(\%_install_hash) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return \%_install_hash;
}
sub uploadInstall    { 
	my ($_self,$_kpan_client_name, $_install)=@_;  
	my $_software                = $_install->{software};
	my $_client                  = $_install->{client};
	my $_install_test            = $_install->{install_test};
	my $_kstat                   = $_install->{kstat};
	my $_app_id							     = $_software->{id};
	my $_app_name						     = $_software->{name};
	my $_app_version				     = $_software->{version};
	my $_client_id		  		     = $_client->{id};
	my $_client_name				     = $_kpan_client_name;
	my %_hash_install            = %{$_install};
	$_hash_install{software}     = undef;
	$_hash_install{client}       = undef;
	$_hash_install{kstat}        = undef;
	$_hash_install{install_test} = undef;
	$_hash_install{install_hash_ref}  = undef;
	$_hash_install{software_hash_ref} = undef;

  #say __FILE__. " Line " . __LINE__ . ", app_id=$_app_id, app_name=$_app_name, uploadInstall=".Dumper(\%_hash_install);  
  say __FILE__. " Line " . __LINE__ . ", uploadInstall=".Dumper(\%_hash_install) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    install						=> \%_hash_install,
    app_id            => $_app_id,
    app_name          => $_app_name,
    app_version       => $_app_version,
    client_id         => $_client_id,
    client_name       => $_client_name,
  };
  say __FILE__. " Line " . __LINE__ . ", content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
  my $_ret = $_self->server_method_call("uploadInstall",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
  $_install->{id} = $_ret->{content} if (!defined $_client->{id} && (defined $_ret->{content} && $_ret->{content}!=0 ));
  #my $_p_hash_ref = {%{$_software->{prereq_hash_ref}},%{$_software->{prereq_lib_hash_ref}} };
  #say __FILE__. " Line " . __LINE__ . ", after merge, ".Dumper($_p_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 1 );     
  $_self->uploadSoftware( $_kpan_client_name,$_software,$_client->{cluster_system},$_client->{os} ) ;
  $_kstat->{install_id} = $_install->{id} if (!defined $_kstat->{install_id});
  $_self->uploadInstallTest( $_kpan_client_name,$_install_test ) ;
  $_self->uploadKstat( $_kpan_client_name,$_kstat ) ;
}
sub getInstallTest    { 
	my ($_self,$_kpan_client_name, $_install_test_id, $_install_id,$_kp_name, $_kp_version, $_app_id,$_app_name, $_app_version, $_client_id, $_client_name, $_set_id)=@_;  
  my $_content= {
    auth           => {username => $_kpan_client_name, password => undef,},
    id             => $_install_test_id,
    install_id     => $_install_id,
    app_id         => $_app_id,
    kp_name        => $_kp_name,
    kp_version     => $_kp_version,
    app_name       => $_app_name,
    app_version    => $_app_version,
    client_id      => $_client_id,
    client_name    => $_client_name,
    set_id         => $_set_id,
  }; 
 
  my $_install_test = Kpan::InstallTest->new;
  my $_ret = $_self->server_method_call("getInstallTest",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", InstallTest ret=".Dumper($_ret->{content}) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  foreach my $_key ( keys  %{$_ret->{content}}){
 	  $_install_test->{$_key} = $_ret->{content}->{$_key} if(!defined $_install_test->{$_key} && defined $_ret->{content}->{$_key} );
  	say __FILE__. " Line " . __LINE__ . ", key=$_key, v=$_ret->{content}->{$_key}" if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  }
  say __FILE__. " Line " . __LINE__ . ", install_test =".Dumper($_install_test) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return $_install_test;
}
sub uploadInstallTest    { 
	my ($_self,$_kpan_client_name, $_install_test)=@_;  
	my %_hash_install_test      = %{$_install_test};
	#$_self->{debug} =3;
  say __FILE__. " Line " . __LINE__ . ", uploadInstallTest=".Dumper(\%_hash_install_test) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    install_test						  => \%_hash_install_test,
  };
  say __FILE__. " Line " . __LINE__ . ", content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
  my $_ret = $_self->server_method_call("uploadInstallTest",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
  #$_self->{debug} =0;
}
sub getKstat    { 
	my ($_self,$_kpan_client_name, $_kstat_id, $_install_id,$_kp_name, $_kp_version, $_app_id,$_app_name, $_app_version, $_client_id, $_client_name, $_set_id)=@_;  
  my $_content= {
    auth           => {username => $_kpan_client_name, password => undef,},
    id             => $_kstat_id,
    install_id     => $_install_id,
    app_id         => $_app_id,
    kp_name        => $_kp_name,
    kp_version     => $_kp_version,
    app_name       => $_app_name,
    app_version    => $_app_version,
    client_id      => $_client_id,
    client_name    => $_client_name,
    set_id         => $_set_id,
  }; 
 
  my $_kstat = Kpan::KStat->new;
  my $_ret = $_self->server_method_call("getKstat",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", kstat ret=".Dumper($_ret->{content}) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  foreach my $_key ( keys  %{$_ret->{content}}){
 	  $_kstat->{$_key} = $_ret->{content}->{$_key} if(!defined $_kstat->{$_key} && defined $_ret->{content}->{$_key} );
  	say __FILE__. " Line " . __LINE__ . ", key=$_key, v=$_ret->{content}->{$_key}" if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  }
  say __FILE__. " Line " . __LINE__ . ", kstat =".Dumper($_kstat) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
  return $_kstat;
}
sub uploadKstat    { 
	my ($_self,$_kpan_client_name, $_kstat)=@_;  
	my %_hash_kstat      = %{$_kstat};
  say __FILE__. " Line " . __LINE__ . ", uploadKstat=".Dumper(\%_hash_kstat) if ( defined $_self->{debug} && $_self->{debug} > 2 );  
  my $_content= {
    auth => {username => $_kpan_client_name, password => undef,},
    kstat						  => \%_hash_kstat,
  };
  say __FILE__. " Line " . __LINE__ . ", content=, ".Dumper($_content) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
  my $_ret = $_self->server_method_call("uploadKstat",$_content);
  say __FILE__. " Line " . __LINE__ . ", ID=$_ret->{content}, ".Dumper($_ret) if ( defined $_self->{debug} && $_self->{debug} > 2 );   
}

sub getRecipes    { 
	my ($_self,$_kpan_client_name, $_client_name,$_app_name,$_app_version,$_app_install_name, $_app_install_version,$_group_name,$_os,$_org_name,$_self_only,$_debug)=@_;  
  my $_content= {
    auth           => {username => $_kpan_client_name, password => undef,},
    client_name         => $_client_name,
    app_name            => $_app_name,
    app_version         => $_app_version,
    app_install_name    => $_app_install_name,
    app_install_version => $_app_install_version,
    group_name          => $_group_name,
    os                  => $_os,
    org_name            => $_org_name,
    self_only           => $_self_only,
  }; 
 
  my $_ret = $_self->server_method_call("getRecipes",$_content);
 
  say __FILE__. " Line " . __LINE__ . ", Receipts ret=".Dumper($_ret->{content}); 
  foreach my $_key ( keys  %{$_ret->{content}}){
  	say __FILE__. " Line " . __LINE__ . ", key=$_key, v=$_ret->{content}->{$_key}" ; 
  }
  return;
}

# utility function
# encodes perl stucture to json string
#
sub serialize {
    my ($_self, $_ref) = @_;
    return encode_json $_ref;
}

#deserializes json string to perl structure
sub deserialize {
    my ($_self, $_string) = @_;
    return decode_json $_string;
}

1;
