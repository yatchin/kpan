#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::KLog;
use 5.010;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use File::stat;
#use FindBin;    
#use lib "$FindBin::Bin/../lib/Kpan/";

use RestClient;
use KUtil;

our $VERSION = '1.5.7';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		debug              				=> shift,
  };
  return $_self; 		
}

sub parseKLogDB {
	my ($_self, $_kpan_client_name, $_install_id, $_client_id,$_client_name,	$_app_id, $_app_name,$_kp_name, $_version, $_kp_version, $_set_id,$_is_all_app,$_is_all_client)=@_;
	say __FILE__." Line ".__LINE__.", $_app_name, $_version, $_kpan_client_name;"  if(defined $_self->{debug} && $_self->{debug} >1);
  #say  __FILE__." Line ".__LINE__.",ready to parse $_install_log,  debug=$_debug, $_app_name, $_version, $_install_log, $_app_version_file_name,$_no_db, $_category, $_test_cmd_file, $_debug ";
  
	my $_install          = Kpan::Install->new();
	$_install->{update_only}=1;
	$_install->{debug}    = $_self->{debug};
	my $_install_hash_ref = $_install->getInstallFromDB( $_kpan_client_name, $_install_id, $_client_id,$_client_name,	$_app_id, $_app_name,$_kp_name, $_version, $_kp_version, $_set_id,$_is_all_app,$_is_all_client);
	say __FILE__. " Line " . __LINE__ . ", s=" and $_install->print if(defined $_self->{debug} && $_self->{debug} >1);
	
	  $_self->{is_test_install} = 0; 
  $_self->{is_write_moudle} = 0; 
  $_self->{is_uplad}        = 0; 
	foreach my $_key (sort keys %{$_install_hash_ref}) {
		my $_install_2 = $_install_hash_ref->{$_key};
		
    $_self->parseKLogContent($_install_2->{install_log}) if (defined $_install_2->{install_log});
  }
}
sub parseKLogFile {
	my ($_self,$_install_log, $_skip_parse_test)=@_;
	say __FILE__." Line ".__LINE__.", install_log=$_install_log"  if(defined $_self->{debug} && $_self->{debug} >2);
	my $_content = "";
	if($_install_log =~ m|\S| ){
		local $/ = undef;
	  open my $_in, "<", $_install_log or die  __FILE__." Line ".__LINE__.", couldn't open file $_install_log for reading: $!\n";
		$_content = <$_in>;
	}
	my $_install_log_path        = $_install_log;
	$_install_log_path           =~ s|/[^/]+$||;
	# perl -e '$a="/u/s/g/UGA"; $a=~ s|/[^/]+$||; print "$a\n"; ' 
	say __FILE__." Line ".__LINE__.", install_log_path=$_install_log_path, $_install_log" if(defined $_self->{debug} && $_self->{debug} >2);
	#$_self->{debug}=0;
  my $_install                 = $_self->parseKLogContent($_content, $_skip_parse_test,$_install_log_path);

  #$_self->{debug}                 = 1;	$_install->{update_only}         = 1;
	$_install->{debug}               = $_self->{debug};
	$_install->{is_all_client}       = 1;
	$_install->{install_id_only}     = 1;

  $_install->getInstallFromDB;
	$_install->{install_id_only}     = 0;
	$_install->{is_all_client}       = 0;
	$_install->{install_test}->{install_id} = $_install->{id};
	$_install->{kstat}->{install_id} = $_install->{id};
	say __FILE__." Line ".__LINE__.", after get DB install=" and $_install->print  if(defined $_self->{debug} && $_self->{debug} >2);

  	$_install->{checked_level}   = (defined $_self->{is_upload} && $_self->{is_upload}>0 )? 3:0;
  $_install->uploadInstall if (defined $_self->{is_upload} && $_self->{is_upload}>0);
  return $_install;
}
sub parseKLogContent {
	my ($_self,$_content, $_skip_parse_test,$_install_log_path) = @_;
 	my $_install          = Kpan::Install->new();
	$_install->{debug}    = $_self->{debug};
	my $_software         = Kpan::Software->new;;
	$_software->{debug}   = $_self->{debug};
	my $_client           = Kpan::Client->new;
	$_client->{debug}     = $_self->{debug};
  #my $_new_content      = "";	
  return if (!defined $_content);
  my @_exist_fhs;
  my $_install_test_command;
	if( $_content =~ m/(.+?)#KP FETCH#/s ){		
		my $_header = $1; # #YH 10/17/17
    if ( $_header =~ m|#(.+?)\s+(\d\d/\d\d/\d\d)\s|){
			$_client->{install_user_mark} = $1;
		}else{
			$_client->{install_user_mark} = "";
		}     
	}
	if($_content =~ m/#KP FETCH#(.+?)#+KP/s){
		$_install->{fetch_command}            = $1;
		$_install->{fetch_command}            =~ s/cd\s(.+?)\n// ; # frist cd ;
		$_install->{fetch_command}            =~ s/\n+//g;
	  #perl -e '$a="K_install.log";{local $/ = undef; open my $_in, "<", $a;$_content = <$_in>;}if($_content =~ m/#KP FETCH#(.+?)#KP /s){$c=$1; if($c =~ m/(wget (.+?)\n)/){print "1:$1\t2:$2\t3:$3\t4:$4\t5:$5\n";}}'
	  #OK perl -e '$a="K_install.log";{local $/ = undef; open my $_in, "<", $a;$_content = <$_in>;}if($_content =~ m/#KP FETCH#(.+?)#KP /s){$c=$1; if($c =~ m/((wget|git|svn|curl)(.+?)((http|ftp|git).+?))\n/){print "1:$1\t2:$2\t3:$3\t4:$4\t5:$5\n";}}'		
#1:wget --no-check-certificate --content-disposition --no-verbose --timestamping "https://github.com/marcelm/cutadapt/archive/v1.14.tar.gz"      2:wget  3: --no-check-certificate --content-disposition --no-verbose --timestamping "   4:https://github.com/marcelm/cutadapt/archive/v1.14.tar.gz"   5:http
    #git clone --recursive https://github.com/rajewsky-lab/mirdeep2.git
    # wget --no-check-certificate --content-disposition --no-verbose --timestamping "https://github.com/TGAC/KAT/archive/Release-2.3.4.tar.gz" 
    if($_install->{fetch_command}        =~ m/((wget|git|svn|curl).+((http|ftp|git).+?))\n/){
    	$_install->{general_fetch_command} = $1;
    }
    if($_install->{fetch_command}   =~ m/((http|ftp|git).+?)\n/){
     	$_software->{version_url}     =  $1; 
     	$_software->{version_url}     =  Kpan::KUtil::RemoveHeadTail($_software->{version_url});  	
    }
    if($_install->{fetch_command}  =~ m/(tar\s.+?)\n+/){
     	$_install->{extract_command} =  $1;    	# correct error, no need fo rfuture
    }
	}
	if($_content                             =~ m/#KP EXTRACT#(.+?)#KP /s){
		$_install->{extract_command}           = $1 if (!defined $_install->{extract_command}); # could from fetch misplace line
    $_install->{extract_command}           =~ s/cd\s(.+?)\n// ; # frist cd 
    $_install->{extract_command}           =~ s/rm\s(.+?)\n// ; #  rm -rf /usr/local/src/lzo/tmp/* 
    $_install->{extract_command}           =~ s/\n+$// ;  
  }
	if($_content =~ m/#KP PREREQ#(.+?)#+KP /s){
		my $_prereq_lines = $1;
		
		if( $_prereq_lines =~ m/^\s*(^#)/ && $_prereq_lines =~ m/(?<!#)(.+)/ ){
  		$_install->{prereq_command}=$1." ";
		}else{
			$_install->{prereq_command}="";
		}
		#say __FILE__." Line ".__LINE__.", prereq_lines= $_prereq_lines, software->{prereq_command}=".$_install->{prereq_command};
	}
	if($_content =~ m/#KP BUILD#(.+?)#+KP /s){
		$_install->{compile_command} = $1;
    #say __FILE__." Line ".__LINE__.", before repalce, install->{compile_command}=$_install->{compile_command}";
		$_install->{compile_command} =~ s/\n+/\n/ ; # remove extra empry lines
		$_install->{compile_command} =~ s/\n+/\n/; 
		$_install->{compile_command} =~ s/cd\s(.+?)\n[module|ml]\s+purge(.*?)\n+/[module|ml] purge\n/ ; # frist cd (? not working ) redoat bottom
		$_install->{compile_command} =~ s/(module|ml)\s+purge(.*?)\n// ;
		$_install->{compile_command} =~ s/#\s(module\s+load|ml)\s*(.*?)\n// ;
		$_install->{compile_command} =~ s/^ls\s+(.+?)\n//mg ;
		$_install->{compile_command} =~ s/^chown\s+(.+?)\n//mg ;
		$_install->{compile_command} =~ s|\bln\s+-s(.+/latest\s*?)\n||g ; # ln -s /usr/local/apps/lzo/2.10 /usr/local/apps/lzo/latest
		$_install->{compile_command} =~ s/\n+$// ; 
    #say __FILE__." Line ".__LINE__.", after repalce, install->{compile_command}=$_install->{compile_command}";
		my $_prereq_lines = $_install->{compile_command};
		# perl -e '@a=("module load python/2.7\n", "#module load  \nmake dir\n"); foreach $b (@a){if ($b=~ m/(?<!#)module\s+load\s+(.+?)\n/) {print "1=$1,2=$2\n";}}'
		if( $_prereq_lines =~ m/(?<!#)\s*(module\s+load|ml)\s+(.+?)\n/ ){ 		
  		my $_tmp_mod = $2;      $_install->{prereq_command} .= $_tmp_mod if ($_install->{prereq_command} !~ /\b\Q$_tmp_mod\E\b/ ); 
      #say __FILE__." Line ".__LINE__.", line=$_prereq_lines, \n\n1=$1, 2=$2\n";
		}
    say __FILE__." Line ".__LINE__.", software->{prereq_command}=".$_install->{prereq_command} if(defined $_self->{debug} && $_self->{debug} >2);
		# latter
	}
	if($_content =~ m/#KP MODULE#(.+?)#+KP /s){
		$_install->{make_module_command}  = $1;
		$_install->{make_module_command}  =~ s/\n+/\n/g;
		$_install->{make_module_command}  =~ s/\n##\n/\n/g;
	}
	if($_content =~ m/#KP TEST#+(.+?)#+KP /s){
		$_install_test_command               = $1;
	  my $_exist_fn                        = "";
		if($_install_test_command =~ m/(ls .+?)\n/s){
	    $_exist_fn                         = $1;
	  }
 		$_exist_fn                           =~ s|ls\s||g;
 		$_exist_fn                           =~ s|^-\S+||g;
 		$_exist_fn                           =~ s|^\s+||g;
		$_exist_fn                           =~ s|\s+$||g;
		$_exist_fn                           =~ s|\n+||g;
 
 		$_install->{install_test}->{exist_dirs}  = $_exist_fn; # if (-d $_exist_fn );
		$_install->{install_test}->{exist_files} = $_exist_fn; # if (-f $_exist_fn );
    #say __FILE__." Line ".__LINE__.", install_test_command=$_install_test_command,\n";
 		if( $_install_test_command           =~ m/(module load|ml)\s+(.+?)\n(.+)\n$/s ){
			 $_install->{install_test}->{module_load} = $2;
			 my $_test_tail = $3;
			 #say __FILE__." Line ".__LINE__.", install_test_command=$_install_test_command,\ntest_tail=$_test_tail";
			 if( defined $_test_tail && $_test_tail =~ m/(.+?)\n(module load|ml)\s+kpan\n/s ){
			   my $_test_cmd_line = $1;
         $_test_cmd_line               =~ s/\n+/\n/g;
	       if($_test_cmd_line =~ m/(.+)\s+(-h|--help|-help)/){
					 $_install->{install_test}->{command}      = $1;
	         $_install->{install_test}->{help_para}    = $2;
         }
         $_test_cmd_line                      =~ s/ (-h|--help|-help)//;
				 $_install->{install_test}->{command} = $_test_cmd_line if (!defined $_install->{install_test}->{command} && $_test_cmd_line !~ m/\n/s); # not conda active multiple lines
		   }
		}
 		$_install_test_command               =~ s|^\s*ls\s(.+?)\n||g;
		$_install_test_command               =~ s;(module|ml)\s+purge;;g;
		$_install_test_command               =~ s|module use ~/modulefiles(.*?)\n||g;
		$_install_test_command               =~ s;(module\s+load|ml)\s+kpan(.*?)\n;;g;
		$_install_test_command               =~ s|#*head (.+?)\n||g;
		$_install_test_command               =~ s|#*\s*kpan (.+?)\n||g;
		$_install_test_command               =~ s|#*cd\s(.+?)\n||g; # frist cd 
		$_install_test_command               =~ s|\s*\n+|\n|g ; # blank line 
		$_install_test_command               =~ s|\n\s+\n||g ; # blank line 
 		if( $_install_test_command           =~ m/(module load|ml)\s+(.+?)\/(.+?)\n(.+)\n$/s ){
			#$_install->{kp_name}               = $2;
			#$_install->{kp_version}            = $3;
			$_install->{install_test_command}  = $4;
		}   
		#say __FILE__." Line ".__LINE__.", install_test_command=$_install_test_command,\ninstall->{install_test_command}=$_install->{install_test_command}, install_test=".Dumper( $_install->{install_test});
    say __FILE__." Line ".__LINE__.",213 install_test_command=$_install_test_command,\ninstall->{install_test_command}=$_install->{install_test_command}, "
    ."kp_name =$_install->{kp_name}, v=$_install->{kp_version} , _exist_fn=$_exist_fn" if(defined $_self->{debug} && $_self->{debug} >2);
	}
	
	if($_content =~ m/#KP BRIEF#(.+?)#+KP /s){
		my $_bash_brief     = $1;
		if($_bash_brief     =~ m/$_install->{multiline_start}(.+)$_install->{multiline_end}/s){
		  $_bash_brief      = $1;      
		}
		$_install->{brief}  = $_bash_brief;
		$_install->{brief}  =~ s/^#//s;
		$_install->{brief}  =~ s/\n#//s;
		$_install->{brief}  =~ s/\n+$//s;
   	$_install->{brief}  = Kpan::KUtil::RemoveHeadTail($_install->{brief});  	
   	$_software->{brief} = $_install->{brief} if (!defined $_software->{brief});  	
	}
  my $_kstat = Kpan::KStat->new;
  my %_kh    = %{$_kstat};
  my $_test_cmd_file; 
	if($_content =~ m/#KP STAT#(.+?)#+KP /s){
		my $_s = $1;
		if($_s =~ m/$_install->{multiline_start}(.+)$_install->{multiline_end}/s){
		  $_s  = $1;      
		}
    foreach my $_k (sort keys %_kh){  
      if (defined $_kh{$_k} && $_kh{$_k} =~/^\d+$/ && $_s =~ m/$_k\s*=(.+?)\n/ && $_k ne 'debug'){        $_kstat->{$_k} = $1;
        $_kstat->{$_k} =~ s/[\D]//g;
		  }
    }
   	$_install->{kstat} = $_kstat;  	
	}
	if($_content =~ m/#KP UPLOAD#(.+?)#+KP/s){
		$_install->{upload_command}           = $1;
		$_install->{upload_command}           =~ s/\n\s+\n/\n/g;
		$_install->{upload_command}           =~ s/\n+/\n/g;
		$_install->{upload_command}           =~ s/\n+$//;
		if($_install->{upload_command}        =~ m/-n\s+(\S+)/){
			$_software->{name}                  = $1;
		}
		if($_install->{upload_command}        =~ m/-in\s+(\S+)/){
			$_install->{kp_name}                = $1;
		}
		if($_install->{upload_command}        =~ m/-v\s+(\S+)/){
		  $_software->{version}               = $1;
		}
		if($_install->{upload_command}        =~ m/-iv\s+(\S+)/){
			$_install->{kp_version}             = $1;
		}
		if($_install->{upload_command}        =~ m/--app_home_url\s+(\S+)/s){
			$_software->{home_url}              = $1;
		}
	  if($_install->{upload_command}        =~ m/--app_archive_url\s+(\S+)/s){
			$_software->{archive_url}           = $1;
		}
	  if($_install->{upload_command}        =~ m/--app_version_file_name\s+(\S+)/s){
			$_software->{version_file_name} = $1;
			if (-f $_software->{version_file_name} ){
				my $_st                          = stat("$_software->{version_file_name}");
		    $_software->{version_file_size}  = $_st->size; #(stat $filename)[7],  -s "$_src_dir_prefix/$_lc_name/$_software->{version_file_name}";
		  }elsif(-d $_software->{version_file_name}){
		  	$_software->{version_file_size}  = 1;
		  }
			$_software->{version_file_name}    =~ s|.+/||; # remove path
		}
	  if($_install->{upload_command}        =~ m/--module_file\s+(\S+)/){
			my $_f            = $1;
			my $_content_tmp  = "";
			if (-f $_f){
				local $/        = undef;
				open my $_in, "<", $_f or die  __FILE__." Line ".__LINE__.", couldn't open file $_f for reading: $!\n";
				$_content_tmp   = <$_in>;
				$_content_tmp   =~ s/\n+$/\n/;
			}
			$_install->{module_content}         = $_content_tmp;
		}
	  if($_install->{upload_command}        =~ m/--k_log\s+(\S+)\w/){
			$_install->{install_log_path}       = $1;
		}
	  if($_install->{upload_command}        =~ m/--cluster_system\s+(\S+)/){
			$_install->{wiki_cluster_system}    = $1;
		}
		if($_install->{upload_command}        =~ m/--category\s+(\S+)/){
      my $_category                       = $1;
      $_category                          = uc(substr($_category,0,1)).substr($_category,1);
			$_software->{category}              = $_category;
			$_install->{category}               = $_category;
		}
		
	  if($_install->{upload_command}        =~ m/--test_cmd_file\s+(\S+)/){			      $_test_cmd_file                       = $1;
      $_test_cmd_file                       = "$_install_log_path/$_test_cmd_file" if ($_test_cmd_file !~ m|^/| && defined $_install_log_path );
      
			say __FILE__." Line ".__LINE__.", test_cmd_file=$_test_cmd_file" if(defined $_self->{debug} && $_self->{debug} >2);
		}
		$_install->{kp_name}               = defined $_install->{kp_name}    ? $_install->{kp_name}    : lc($_software->{name});
		$_install->{kp_version}            = defined $_install->{kp_version} ? $_install->{kp_version} : lc($_software->{version});

	}
	if($_content =~ m/#KP NOTES#(.+?)#+KP/s){
		$_install->{note} = $1;
		$_install->{note} =~ s/\n+//g;
	}
	$_client->{rc}->{debug}=$_self->{debug};
	$_client->setClientInfo;
	
  # name and v are in place now
	if(defined $_install->{prereq_command} && $_install->{prereq_command}=~ m/\S/){
		my $_pq = Kpan::Prereq->new;
	  $_pq->parsePrereqCommand($_software->{name},$_software->{version}, $_install->{prereq_command}, $_software->{prereq_lib_hash_ref},$_software->{prereq_hash_ref} ); # todo , verify hash
	}		
  $_install->{install_test}->{name}                   = $_software->{name};
  $_install->{install_test}->{version}                = $_software->{version};
  $_install->{install_test}->{debug}                  = $_self->{debug};
  $_install->{install_test}->{KP}                     = $_client->{KP};
  $_install->{install_test}->{lmod_app_path}          = $_client->{lmod_app_path};
  $_install->{install_test}->{file_name_prefix}       = $_client->{install_user_mark};
	$_install->{install_test}->{install_id}             = $_install->{id};
	$_install->{install_test}->{client_id}              = $_client->{id};
	$_install->{install_test}->{kp_name}                = $_install->{kp_name};
	$_install->{install_test}->{kp_version}             = $_install->{kp_version};
	$_install->{install_test}->{client_id}              = $_client->{id};
	$_install->{install_test}->{client_name}            = $_client->{kpan_client_name};
	$_install->{install_test}->{module_load}            = "$_install->{kp_name}/$_install->{kp_version}" if (!defined $_install->{install_test}->{module_load});
	if (!defined $_install->{install_test}->{command}){
	  $_install->{install_test}->{command}                = $_install_test_command;
	  $_install->{install_test}->{command}                =~ s/(module |ml )(.+?)\n//;
    $_install->{install_test}->{command}                =~ s/\n+/\n/g;
  }
	if($_install->{install_test}->{command} =~ m/ (-h|--help|-help)\b/){
	  $_install->{install_test}->{help_para}              = $1 if (!defined $_install->{install_test}->{help_para});
  }
	my $_source_active                                  = undef;
	# perl -e '$a="conda activate /usr/local/apps/gb/qiime/1.9.1_conda\npython"; if ($a =~ m/(?:conda|source) activate (.+)/) {$b=$1; print "match, $b\n";}else{print "not match\n";}'
	if($_install->{install_test}->{command} =~ m/(?:conda|source) activate (.+)/){
		$_self->{channel}                                 = "conda";
		$_source_active                                   = $1;
		$_source_active                                   =~ s/\n//g;
	  $_install->{install_test}->{command}              =~ s/conda activate (.+)?\n//;
	  $_install->{install_test}->{command}              =~ s/conda deactivate(.*)?\n//;
	}
  $_install->{install_test}->{command}              =~ s/\n//g;
	$_install->{install_test}->{command}                =~ s/ (-h|--help|-help)\b/ /g;
	$_install->{install_test}->{command}                = "conda activate $_source_active && $_install->{install_test}->{command} " if (defined  $_source_active &&  $_source_active =~ m/\S+/);
	$_install->{install_test}->{help_para}              = defined $_install->{install_test}->{help_para}? "$_install->{install_test}->{help_para}  && conda deactivate" : "" if (defined  $_source_active &&  $_source_active =~ m/\S+/);
  $_install->{test_cmd_result}                        = $_install->{install_test}->parseTestCmdFile($_test_cmd_file) if (-f $_test_cmd_file && (!defined $_skip_parse_test ||  $_skip_parse_test==0) ); 
	
	$_install->{test_cmd_help}                          = (defined $_install->{install_test}->{help_para} && $_install->{install_test}->{help_para} =~ m/\S*$/ ) ? $_install->{install_test}->{help_para} : "";
	$_install->{test_cmd_help}                          =~ s/["\n]//g;
	#$_install->{install_test}->{command}                =~ s/\n//g;
	my $_install_path                                   = $_client->{install_dir_prefix}."/".$_install->{kp_name}."/".$_install->{kp_version};
	say __FILE__." Line ".__LINE__.",install_test_command=$_install_test_command, install->{install_test_command}=$_install->{install_test_command}, -f =$_test_cmd_file,".(-f $_test_cmd_file). ", sk=". (!defined $_skip_parse_test ||  $_skip_parse_test==0) 
	.",install_path=$_install_path, source_active=$_source_active, install->{install_test}=".Dumper($_install->{install_test}) if(defined $_self->{debug} && $_self->{debug} >4);  #say __FILE__." Line ".__LINE__.",".Dumper($_software->{prereq_lib_hash_ref},$_software->{prereq_hash_ref});
	$_software->setPrereqLib($_client->{kpan_client_name},$_client->{rc});  $_software->setSelf($_client->{kpan_client_name},$_client->{rc});
 
  #say __FILE__." Line ".__LINE__.",".Dumper($_software->{prereq_lib_hash_ref},$_software->{prereq_hash_ref});
 	$_install->{checked_level}    = 2                    if(!defined $_install->{checked_level});
	$_install->{note}             = "parsed install log" if(!defined $_install->{note} || $_install->{note} =~ /^\s*$/ );    
	$_install->{software}                 = $_software;
	$_install->{client}                   = $_client;
	#$_install->{install_test_command}    = lc($_software->{name}) if (!defined $_install->{install_test_command});
	$_install->{update_only}              = 1;
	$_install->{install_test}->{app_id}   = $_software->{id};
	$_install->{install_test}->{app_name} = $_software->{name};
	$_install->{install_test}->{app_version} = $_software->{version};
	$_kstat->{app_id}                     = $_software->{id};
	$_kstat->{app_name}                   = $_software->{name};
	$_kstat->{app_version}                = $_software->{version};
	$_kstat->{kp_name}                    = $_install->{kp_name};
	$_kstat->{kp_version}                 = $_install->{kp_version};
	$_kstat->{client_id}                  = $_client->{id};
	$_kstat->{client_name}                = $_client->{kpan_client_name};

	my $_c_dir = "$_client->{src_dir_prefix}/".lc($_software->{name})."/".lc($_software->{version});	$_install->{compile_command} =~ s|\bcd\s+\Q$_c_dir\E(.*?)\n||g ; # frist cd 
	#say __FILE__." Line ".__LINE__.", cd dir=$_c_dir, install->{compile_command}=$_install->{compile_command}" if(defined $_self->{debug} && $_self->{debug} > 0 );

	my $_to_personal = 0;
	$_install->{general_fetch_command}       = $_client->getGeneral($_install->{fetch_command},        $_to_personal) if(defined $_install->{fetch_command} );
	$_install->{general_extract_command}     = $_client->getGeneral($_install->{extract_command},      $_to_personal) if(defined $_install->{extract_command} );
	$_install->{general_prereq_command}      = $_client->getGeneral($_install->{prereq_command},       $_to_personal) if(defined $_install->{prereq_command} );
	$_install->{general_compile_command}     = $_client->getGeneral($_install->{compile_command},      $_to_personal) if(defined $_install->{compile_command} );
 	#say __FILE__." Line ".__LINE__.", after convert, to_personal=$_to_personal, install->{general_compile_command} = $_install->{general_compile_command}" if(defined $_self->{debug} && $_self->{debug} > 0 );
	$_install->{general_test_command}        = $_client->getGeneral($_install->{install_test_command}, $_to_personal) if(defined $_install->{install_test_command} );	$_install->{general_upload_command}      = $_client->getGeneral($_install->{upload_command},       $_to_personal) if(defined $_install->{upload_command} );  $_install->{general_module_content}      = $_client->getGeneral($_install->{module_content},       $_to_personal) if(defined $_install->{module_content} );	
  $_install->{general_make_module_command} = $_client->getGeneral($_install->{make_module_command},  $_to_personal) if(defined $_install->{make_module_command} );	  $_install->{general_install_log}         = $_client->getGeneral($_install->{install_log_text},     $_to_personal) if(defined $_install->{install_log_text} );
 	$_install->{general_test_cmd_result}     = $_client->getGeneral($_install->{test_cmd_result},      $_to_personal) if(defined $_install->{test_cmd_result} );
 	$_install->{install_test}->{general_command} = $_client->getGeneral($_install->{install_test}->{command}, $_to_personal) if(defined $_install->{install_test}->{command} );
 
	my $_content_formatted                   = $_install->formatContent;
	$_install->{install_log_text}            = $_content_formatted;
	$_install->{commands}                    = $_content_formatted;

 	say __FILE__." Line ".__LINE__.", after parse. install =" and $_install->print(0) if(defined $_self->{debug} && $_self->{debug} > 1 );
 	say __FILE__." Line ".__LINE__.", formatted\n\n\n$_content_formatted"          if(defined $_self->{debug} && $_self->{debug} > 2 );
	return $_install;
}1;
