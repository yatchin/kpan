#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Client;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use Cwd;
use Config;
use Sys::Hostname qw(hostname);
use Socket 'inet_ntoa';
use File::HomeDir;
use LWP::Simple;

use experimental 'smartmatch';
use POSIX ":sys_wait_h"; # kill hanging process, such as dig
use threads;
use threads::shared;

use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use RestClient;
use KUtil qw(Timeout);

our $VERSION = '1.5.9';
use base 'Exporter';
our @EXPORT_OK = qw(getIp);

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
		debug 									=> shift,
		app_install_log 				=> shift,
		archname 								=> shift,
		eb_depot_dir       	  	=> shift,
		eb_repo_dir        	  	=> shift,
		eb_src_dir         	  	=> shift,
		eb_template_dir    	  	=> shift,
		eb_template_name  	  	=> shift,
		cluster_system 					=> shift,
		default_Module					=> shift,
		default_R 							=> shift,
		default_autoconf				=> shift,
		default_autogen  				=> shift,
		default_boost						=> shift,
		default_cmake						=> shift,
		default_combo       		=> shift,
		default_conda       		=> shift,
		default_gcc 						=> shift,
		default_java 						=> shift,
		default_libtool 				=> shift,
		default_openmpi 				=> shift,
		default_perl 						=> shift,
		default_python 					=> shift,
		default_ruby   					=> shift,
		dry_run_level 					=> shift,
		hostname 								=> shift,
		id 						        	=> shift,
		install_dir_prefix 			=> shift,
		install_dir_eb_prefix		=> shift,
		install_user_mark 			=> shift,
		ip 											=> shift,
		is_kpan_dev 						=> shift, #kpan developer?
		kpan_client_name 				=> shift,
		KP              				=> "###KP",
		lmod_app_path 					=> shift,
		lmod_dir_prefix 				=> shift,
		lmod_dir_eb_prefix  		=> shift,
		no_db 									=> shift,
		note 										=> shift,
		org_name  							=> shift,
		os 											=> shift,
		os_user_group 					=> shift,
		os_user_name 						=> shift,
		quiet_level 						=> shift,
		rc                      => Kpan::RestClient->new, # RestClient
		real_name 							=> shift,
		setting_file_name				=> shift,
		search_scope     				=> shift, #0=ALL, 1=self, 2=group, 3=cluster, 4=org
		src_dir_prefix 					=> shift,
		wiki_api_url 						=> shift,
		wiki_list_page  				=> shift,
		wiki_login_name 				=> shift,
		wiki_login_password 		=> shift,
		wiki_template_name  		=> shift,
	};
	my @_indexes = (
        "copy_default_setting",
				"cluster_system",       "install_user_mark",    "setting_file_name",    "install_dir_prefix",   "src_dir_prefix",         
				"app_install_log",      "wiki_login_name",      "wiki_login_password",  "wiki_template_name",   "wiki_list_page",                   
				"lmod_app_path",        "lmod_dir_prefix",      "org_name",             "id",                   "kpan_client_name",              
				"default_autoconf",     "default_autogen",      "default_boost",        "default_cmake",        "default_combno",                                              
				"default_conda",        "default_gcc",          "default_java",         "default_libtool",      "default_perl",              
				"default_python",       "default_openmpi",      "default_R",            "default_ruby",         "is_kpan_dev",                           
				"eb_depot_dir",         "eb_repo_dir",          "eb_src_dir",           "eb_template_dir",      "eb_template_name",
				"wiki_api_url",        	"search_scope",			  
  ); #"preload_module",
  $_self->{indexes}  = \@_indexes;

  my $_os_user_name  = $ENV{LOGNAME}; # effective user , || $ENV{USER} || (getpwuid($<))[0]; getlogin() original user (before su)
  my $_os_user_group = (getgrgid($)))[0];
  my $_os            = $Config{osname}; #$OSNAME
  my $_archname      = $Config{archname};
  my $_hostname      = hostname();  
  my $_ip            = getIp(); #inet_ntoa(scalar gethostbyname($_hostname || 'localhost')); # failed at s104
  # public ip
  # curl ifconfig.me
  # private ip
  # hostname -I
  # 10.55.128.4 172.17.128.4 172.16.100.4
  $_hostname         =~ s/\n// if(defined $_hostname);
  $_hostname         =~ s/(\..+)// if(defined $_hostname && $_hostname eq 'build2.gacrc.uga.edu'); #=> build2
  $_ip               =~ s/\n// if(defined $_ip);
  #say __FILE__." Line ".__LINE__."\tkpan new client set self->{debug} =$_self->{debug}, $_hostname ";
  say __FILE__.", line ". __LINE__.",ip=$_ip"  if(defined $_self->{debug} && $_self->{debug}>6);  
  #$_ip=get('http://icanhazip.com/') if (!defined $_ip);
  chomp($_ip) if (defined $_ip);
  # perl -e '$a=hostname; $a =~ s/\n//; $a=~ s/(\.+)//; print "$a\n"'
  # perl -e '$a=`dig +short myip.opendns.com \@resolver1.opendns.com`; print "$a\n"'
  # say __LINE__.", user_name/group=$_os_user_name, $_os_user_group, host/ip= $_hostname, "
    # ." $_ip, cn=$_self->{kpan_client_name},cs=$_self->{cluster_system} "
    # ;
  say STDERR (__FILE__.", line ". __LINE__.", Running as root!!\n") if ( $< == 0 );
	#$< - real user id (uid); unique value
	#$> - effective user id (euid); unique value
	#$( - real group id (gid); list (separated by spaces) of groups
	#$) - effective group id (egid); list (separated by spaces) of groups
	#perl -e '$_os_user_name = getlogin(); print "$_os_user_name\n"'
  
	$_self->{os_user_name}      = $_os_user_name    unless defined $_self->{os_user_name};
	$_self->{os_user_group}     = $_os_user_group   unless defined $_self->{os_group_name};
	$_self->{os}                = $_os              unless defined $_self->{os};
	$_self->{archname}          = $_archname        unless defined $_self->{archname};
	$_self->{hostname}          = $_hostname        unless defined $_self->{hostname};
	$_self->{ip}                = $_ip              unless defined $_self->{ip};

	$_self->{app_install_log}     = "K_install.log" unless defined $_self->{app_install_log};
	$_self->{kpan_client_name}    = $_self->{os_user_name}."_".$_self->{hostname} unless defined $_self->{kpan_client_name};
	$_self->{cluster_system}      = $_self->{ip}."_".$_self->{hostname}           unless defined $_self->{cluster_system};
	$_self->{install_user_mark}   = $_self->{os_user_name} unless defined $_self->{install_user_mark};	
	
	my $_setting_file             = defined $_self->{hostname} ? "~/.kpaninfo_".$_self->{hostname} : "~/.kpaninfo";
	$_setting_file                = Kpan::KUtil::GetFullPath($_setting_file,$_self->{os_user_name});
  $_self->{setting_file_name}   = $_setting_file;
  if (!-f $_self->{setting_file_name} ){
  	my $_fh                     = "~";
    if (-d $_fh){
      opendir my $_d, $_fh or die $!;
      my @_files               = grep {/^.kpaninfo/ && -f "$_fh/$_"} readdir $_d ;
      if (scalar @_files == 1) {
      	$_self->{setting_file_name} = $_files[0]; # take the only kpaninfo file as settting
      }
    }
  }
	#say __FILE__.", line ". __LINE__.", afer init, setting=$_self->{setting_file_name}, hs=$_hostname";
#	$_self->{default_Module} = Kpan::DefaultModule->new;
	return $_self;
}

sub setClientInfo { # to do NO DB, WIKI
  my ($_self, $_setting_file_name, $_no_db ,$_no_wiki,$_config_client,$_debug)=@_;
	# __FILE__.", line ". __LINE__.", before loadClientSetting (DB and local) ".Dumper($_self)  if(defined $_self->{debug} && $_self->{debug}>0);
	$_self->loadClientSetting($_setting_file_name, $_no_db ,$_no_wiki,$_config_client,$_debug);
	#say __FILE__.", line ". __LINE__.", afer loadClientSetting (DB and local) ".Dumper($_self)  if(defined $_self->{debug} && $_self->{debug}>0);
	if( defined $_self->{lmod_app_path} && $_self->{lmod_app_path} =~ m/\S/ && !defined $_self->{config_client} ){ # existing
	  say __FILE__.", line ". __LINE__.",$_self->{kpan_client_name} loaded,"  if(defined $_self->{debug} && $_self->{debug}>2);
	  $_self->writeSetting if (! -f $_self->{setting_file_name});
	}else{
    $_self->setNewClient();	
    say __FILE__.", line ". __LINE__.", after upload, config_client=$_self->{config_client}, ".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4);
    $_self->{id} = undef if (defined $_self->{id} && $_self->{id}<0);   
    $_self->writeSetting;
 	}
 	$_self->uploadClientInfo;
  say __FILE__.", line ". __LINE__.", after getinfo, before uploading, ".Dumper($_self)  if(defined $_self->{debug} && $_self->{debug}>2);
  return;
}
sub loadClientSetting {
	my ($_self, $_setting_file_name, $_no_db ,$_no_wiki,$_config_client,$_debug)=@_;
  $_self->{debug}               = $_debug                                       if (defined $_debug);
  $_self->{no_db}               = $_no_db                                       if (defined $_no_db);
	$_self->{setting_file_name}   = $_setting_file_name                           if (defined $_setting_file_name);
  $_self->{config_client}       = $_config_client                               if (defined $_config_client);

	$_self->loadClientSettingFromFile($_self->{setting_file_name})          if ( -f $_self->{setting_file_name}); #local 
	say __FILE__.", line ". __LINE__.", client after local=".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4 );
	say __FILE__.", line ". __LINE__.", scratch new"           if(defined $_self->{debug} && $_self->{debug}>4);
	if(defined $_self->{os_user_name} && defined $_self->{hostname} && defined $_self->{ip} && (!defined $_self->{no_db} || !$_self->{no_db})){ 
		say __FILE__.", line ". __LINE__.", client before fromDB=".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4 );
		$_self->{rc}->{debug}=$_self->{debug};
		#$_self->{id}=190;
		$_self->{rc}->fillClientself($_self);
		#$_self->loadClientSettingFromFile($_self->{setting_file_name}) if(defined $_self->{setting_file_name}); # reload local overwrite DB in case different setting filename 
		say __FILE__.", line ". __LINE__.", client after fromDB=".Dumper($_self) if(defined $_self->{debug} && $_self->{debug}>4 );
	}	
	#$_self->{default_Module}->{lmod_app_path} = $_self->{lmod_app_path};
	#$_self->{default_Module}->{lmod_dir_prefix} = $_self->{lmod_dir_prefix};
	#$_self->{default_Module}->moduleAvail;
}
sub loadClientSettingFromFile{ # TODO update DB if changed
	my ($_self,$_file) = @_;

  $_file = $_self->{setting_file_name} if(!defined $_file && defined $_self->{setting_file_name});
  return if(! defined $_file || ! -f $_file);
  
  open my $_fin, "< $_file" or die __FILE__." Line ".__LINE__.",  couldn't read setting file $_file $!\n";
  # perl -e '@a=("#a","src_dir_prefix=\"/usr/local/src\""); foreach $b(@a){ next if ($b=! m/^#/); print "b=$b,1=$1\n";}'

 	while (my $_line=<$_fin>){
		$_line=Kpan::KUtil::Trim($_line);
		say __FILE__.", line ". __LINE__.",$_line" if(defined $_self->{debug} && $_self->{debug}>10);
		next if ($_line =~ m/^#*\s*$/ ); #skip empty or commnets
		#next if ($_line =~ m/^#/    ); #skip empty or commnets TODO this line doesn't work?
		my %_self_hash = %{$_self};
		foreach my $_key (keys %_self_hash){
		  if($_line =~ m/^$_key\s*=\s*(.+)/ ){
		  	my $_v = $1;
		    $_self->{$_key} = $_v if ( defined $_v && $_v =~ m/[^"]/ );
		    #print __FILE__.", line ". __LINE__.", ***b4 =after strip $_key, $_self->{$_key}";
		    $_self->{$_key} =~ s/["\s]//g if (defined $_self->{$_key}) ;
		    #say "=$_self->{$_key}"
			}
		}
  }
  say __FILE__.", line ". __LINE__.", load from file $_file, src_dir_prefix=$_self->{src_dir_prefix}" if(defined $_self->{debug} && $_self->{debug}>1);
}
sub setNewClient {
	my $_self = shift;
	my $_default_config_file = "../../conf/default_kpaninfo.conf";
	$_self->loadClientSettingFromFile($_default_config_file) if (-e $_default_config_file);
	$_self->{setting_file_name}   = "~/.kpaninfo_".$_self->{hostname}        if (!defined $_self->{setting_file_name});
	$_self->{setting_file_name}   = Kpan::KUtil::GetFullPath(	$_self->{setting_file_name},$_self->{os_user_name} );  
	$_self->{src_dir_prefix}      = "/usr/local/src"                         if (!defined $_self->{src_dir_prefix});
	$_self->{install_dir_prefix}  = "/usr/local/apps"                        if (!defined $_self->{instal_dir_prefix});
	$_self->{lmod_app_path}       = "/usr/local/apps/lmod/lmod"              if (!defined $_self->{lmod_app_path});
	$_self->{lmod_app_path}       = "/usr/local/apps/lmod/5.8"               if (defined $_self->{org_name} && $_self->{org_name} =~ /UGA/i);
	$_self->{lmod_dir_prefix}     = "/usr/local/modulefiles"                 if (!defined $_self->{lmod_dir_prefix});
	$_self->{search_scope}        = 0                                        if (!defined $_self->{search_scope});
	$_self->{install_user_mark}   = $_self->{os_user_name}                   if (!defined $_self->{install_user_mark});
	my $_ip =  `dig +short myip.opendns.com \@resolver1.opendns.com`; #inet_ntoa(scalar gethostbyname($_hostname || 'localhost')); 
	chomp($_ip);
	$_self->{cluster_system}      = hostname()                               if (!defined $_self->{cluster_system});
	$_self->{kpan_client_name}    = $_self->{os_user_name}."_".$_self->{hostname}    if (!defined $_self->{kpan_client_name});
	$_self->{wiki_api_url}        = "https://wiki.omicsci.com/api.php" if (!defined $_self->{wiki_login_name});
	$_self->{wiki_login_name}     = $_self->{os_user_name}                   if (!defined $_self->{wiki_login_name});
	$_self->{wiki_template_name}  = "YH-Template"                            if (!defined $_self->{wiki_template_name});
	$_self->{dry_run_level}       = 0                                        if (!defined $_self->{dry_run_level} );
	$_self->{quiet_level}         = 0                                        if (!defined $_self->{quiet_level}   );
	$_self->{is_kpan_dev}         = 0                                        if (!defined $_self->{is_kpan_dev}   );
	$_self->{search_scope}        = "0, #0=ALL, 1=self, 2=group, 3=cluster, 4=org" if (!defined $_self->{search_scope}   );
	
  my %_questions = (
	  copy_default_setting => "copy default setting from central configure file (y/n)",
	  setting_file_name    => "this configuration file name with full path ",
	  install_dir_prefix   => "installation prefix dir in full path",
	  src_dir_prefix       => "source code prefix dir in full path",
	  app_install_log      => "installation log file name",
	  lmod_app_path        => "Lmod application full path name",
	  lmod_dir_prefix      => "Lmod module repository prefix dir in full path",
	  cluster_system       => "cluster system name",
	  install_user_mark    => "personal marker in log file",
	  id                   => "KPAN account id, if you don't have one, leave this blank",
	  kpan_client_name     => "kpan account name, if you don't have one, take suggested",
	  org_name             => "organization name in short with no space",
	  search_scope         => "receipt search scope, default 0,  0=ALL, 1=self, 2=group, 3=cluster, 4=org",
	  
	  eb_depot_dir         => "eb receipt search dir, if there are multiple dir, seperated by ',', blank for not applicable",
	  eb_repo_dir          => "eb ready receipt dir, blank for not applicable",
	  eb_src_dir           => "eb src dir, blank for not applicable",
	  eb_template_dir      => "eb template dir, blank for not applicable",
	  eb_template_name     => "eb template name, blank for not applicable",
	  
	  wiki_api_url         => "wiki api url if you need to present docuemnt at wiki, blank for not applicable",
	  wiki_login_name      => "wiki login name, blank for not applicable",
	  wiki_login_password  => "wiki login password, blank for not applicable",
	  wiki_template_name   => "wiki template page, blank for not applicable",
	  wiki_list_page       => "wiki software index page, blank for not applicable",

	);
	
	say "\nStart setting kpan configuration. The setting can be edited later. Hit return to accept the suggested setting.\n";
	my $_is_copy = 0;
	my @_keys    = @{$_self->{indexes}};
	for my $_key (@_keys){
    my $_q = $_key;
    $_q    = "Default $1 module" if ($_key =~ m/^default_(.+)$/ );
    $_q    = $_questions{$_key} if (exists $_questions{$_key} && defined $_questions{$_key} );
    my $_h = defined $_self->{$_key} ? " ( $_self->{$_key} )" : "";
    print "\n${_q}$_h: ";
 		my $_in = <STDIN>;
		chomp $_in;
		if(defined $_in && $_in =~ m/\S/){
			$_in             =~ s/.*[^[:print:]]+//; # remove non printable input			
		  $_in             =  Kpan::KUtil::GetFullPath($_in,$_self->{os_user_name}) if($_key =~ m/_prefix|_path|_dir/);		
		  if($_key eq "copy_default_setting" ){
		  	$_default_config_file = $_in if (defined $_in && $_in =~ m/\S/);
		  	$_self->loadClientSettingFromFile($_default_config_file);		  	
		  	$_is_copy        = 1;
		  	last;
		  }else{
			  $_self->{$_key}  = Kpan::KUtil::Trim($_in);
		  }
		}
		my $_v = defined $_self->{$_key} ? $_self->{$_key} : "";
		say "\t$_v";  
  }
	$_self->writeSetting();
  ($_is_copy>0) ? say "please review the setting file $_self->{setting_file_name} and update personal credential" : say "\nSetting is completed. The setting can be edited at $_self->{setting_file_name}\n";
  #$_self->uploadClientInfo();
}
sub writeSetting {
	my $_self = shift;
  open my $_fout, "> $_self->{setting_file_name}" or die __FILE__." Line ".__LINE__.",  couldn't write setting file $_self->{setting_file_name} $!\n";
  say $_fout "###Kpan user setting file, last updated at ".`date +%Y_%m_%d_%H_%M`;#date;
  my %_client_hash               = %{$_self};
  $_client_hash{default_Module}  = undef;
	$_client_hash{rc}              = undef;
	my @_indexes                   = @{$_self->{indexes}};
	for my $_i (0..$#_indexes) {
		my $_key       = $_indexes[$_i];
		my $_v  = defined $_client_hash{$_key}? $_client_hash{$_key} : "";
    say $_fout "$_key = $_v";
    say $_fout "\n" if ($_i==10);
	}
	for my $_key (sort keys %_client_hash ){ # any pair left from the preset values
		next if ($_key =~ m/^debug|default_Module|hostname|ip|no_db|note|os|os_user_group|os_user_name|quiet_level|rc|real_name|setting_file_name$/ );
		next if ($_key ~~ @_indexes );
		my $_v  = defined $_client_hash{$_key}? $_client_hash{$_key} : "";
    say $_fout "$_key = $_v";
	}
  say $_fout "\n###Kpan end\n";   
}
sub uploadClientInfo{
	my $_self = shift;
	return if (defined $_self->{no_db} && $_self->{no_db});
  $_self->{rc}->uploadClient($_self);
}

sub getGeneral{
  my ($_self,$_str,$_to_personal)=@_;
  
  my @_set = (
				"app_install_log",
				"eb_dir",
				"eb_template_name",
				"cluster_system",
				"default_R",
				"default_basic_module",
				"default_autoconf",
				"default_autogen",
				"default_conda",
				"default_boost",
				"default_cmake",
				"default_gcc",
				"default_java",
				"default_openmpi",
				"default_perl",
				"default_python",
				"default_zlib",
				"install_user_mark",
				"lmod_app_path",
				"lmod_dir_prefix",
				"src_dir_prefix",
				"install_dir_prefix",
  );
  #		""install_dir_prefix", # has to be last to avoid overwrite others, e.g. module files
  return if (!defined $_str);
  foreach my $_key (@_set){
		next if ( !defined $_self->{$_key} || $_self->{$_key} =~ m/^\s*$/ );
		my $_general = "K_".uc($_key)."_P";
		my $_b4 = $_str;		
    if(!defined $_to_personal || $_to_personal<1){
  		if($_str =~ m/$_self->{$_key}/ ){
  			if($_key eq "cluster_system" &&  $_self->{$_key} eq "kpan" && $_str =~ m/--cluster_system/ ){ # fix for legacy
   	      $_str =~ s/ --cluster_system \s*[k|K]pan / --cluster_system $_general /g;  				
  			}else{
    	    $_str =~ s|$_self->{$_key}|$_general|g;
    	  }
  	    #say "***$_to_personal, $_key, before $_b4, \n***after: $_str, self->{$_key}=$_self->{$_key}, general=$_general " if ($_b4 =~ m/$_self->{$_key}/ && $_str =~ m/(!?bash).+gmp/i  );
  	  }
    }else{
 			if($_key eq "cluster_system" &&  $_str =~ m/--cluster_system Sapelo / ){ # fix for legacy
 	      $_str =~ s/--cluster_system Sapelo /--cluster_system $_general /g;  					
	      #say "+++$_to_personal, $_key,";		
 			}
  		if($_str =~ m/$_general/ ){
  			# perl -e '$a="#K_CLUSTER_SYSTEM_P -n abc --cluster_system K_CLUSTER_SYSTEM_P"; $a =~ s/^(#*)K_CLUSTER_SYSTEM_P /\Q$1\Ekpan/g; print "$a\n" '
      	$_str =~ s|$_general|$_self->{$_key}|g;      	
 	      #say "***$_to_personal, $_key, before $_b4, \n***after: $_str, self->{$_key}=$_self->{$_key}, general=$_general " if ($_b4 =~ m/$_self->{$_key}/ && $_str =~ m/Sapelo/i  );
      }
    }
  }
  return$_str;
}

sub getIp{
	my ($_i) =@_;
  my @_cmds = (
    "dig +short myip.opendns.com \@resolver1.opendns.com",
    "curl -s ifconfig.me",
    "hostname -I", #private IP
    #"sleep 5",
  );
 	my $_ret;
  my $_cmd_callback = sub {
  	my $_cmd = shift;
  	return `$_cmd`;
  };
  if (defined $_i && $_i > -1 && $_i < $#_cmds){
    #$_ret = runSysCmd($_cmds[$_i],$_cmd_callback)."\n";
    $_ret = Kpan::KUtil::Timeout($_cmds[$_i]);
  }else{
    foreach (@_cmds){    	
    	$_ret = Kpan::KUtil::Timeout($_,1)."\n"; # timeout 1 second
    	last if ($_ret  =~ m/(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/);    	
    }
  }
  #say $_ret;
  $_ret =~ s/(^\s+|\s+$)//g;
  #perl -e '$a="10.56.51.253 10.55.50.253 10.56.51.99"; $a =~ s/\s+.+$//;; print "$a\n";'
  $_ret =~ s/\s+.+$//; # if multiple private ip, take first one
  return $_ret;
}

sub runSysCmd{
	my ($_cmd, $_callback) =@_;
	my $_ret = $_callback->($_cmd);
  return $_ret;
}


1;

