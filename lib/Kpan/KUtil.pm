#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::KUtil;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use Sort::Versions qw(versioncmp);
use File::stat;
use File::Find::Rule;
#use File::Path qw(make_path);
use List::Util qw(max);
use File::HomeDir;
use Cwd;
use threads;
use threads::shared;

our $VERSION = '1.5.9';

#use Exporter;
#our @ISA = qw(Exporter);
use base 'Exporter';
use constant TAR => [qw/ tar.gz tar.bz2 tar.xz tar.Z tar.lz bz2 cpio gz lz sht tar tgz xz zip Z sh bin/]; 
#our @EXPORT = qw(TAR Trim GetLastFileHandler GetLatestVersion GetTarFile); #@EXPORT_OK force pkg name Kpan::KUtil?
our @EXPORT_OK = qw(TAR Trim GetLastFileHandler GetLatestVersion GetTarFile Timeout); #@EXPORT_OK force pkg name Kpan::KUtil?

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}
 
sub _init{ 
	my $_self = {
		note => shift
	};
	return $_self;
}
sub Trim { #remove two ends space and multiple new line ending \n\s*\n
  my $_s = shift; 
  if(defined $_s){
		$_s =~ s/(^\s+|\s*\n+$)//g; 
	}else{
		$_s ="";
	}  
  return $_s; 
}
sub GetLastFileHandler {
  my ($_path)=@_;
	opendir( my $_dh, $_path) or die __FILE__." Line ".__LINE__.",Cannot open $_path\n";
	my @_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ } readdir($_dh); #ctime stat 11th 
  return $_files[0];
}
sub GetLastDir {
  my ($_path)=@_;
	opendir( my $_dh, $_path) or die __FILE__." Line ".__LINE__.",Cannot open $_path\n";
	my @_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ && -d "$_path/$_"} readdir($_dh); #ctime stat 11th 
  return $_files[0];
}
sub GetExtractDir { # match name, version or last dir
  my ($_path, $_lc_name, $_version, $_version_file_name)=@_;
	opendir( my $_dh, $_path) or die __FILE__." Line ".__LINE__.",Cannot open $_path\n";
	my @_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ && -d "$_path/$_"} readdir($_dh); #ctime stat 11th 
	#say __FILE__." Line ".__LINE__." "
	#perl -MData::Dumper -e '$_path="/usr/local/src/fastme/tmp"; opendir( my $_dh, $_path);@_files =sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ } readdir($_dh); print("**\n".Dumper(\@_files));'
	my $_dir;
	my $_count = 0;
	foreach my $_f (@_files) {
		$_count++ if ( (defined $_version_file_name  && $_f eq  $_version_file_name) || -d $_f);
		my $_st=stat("$_path/$_f");
		#say "f=$_f, st=".$_st->ctime.",".Dumper($_st);
		if(-d $_f){
			$_dir = $_f if (!defined $_dir || ( defined $_lc_name && $_f =~ m/$_lc_name/ && (!defined $_dir || $_dir !~  m/$_lc_name/)) ); # name overwrite last dir if last dir not containing name as well
			do {$_dir = $_f; last; } if ( defined $_version && $_f =~ m/$_version/); # version hit, exit;			
		}
	}
	$_dir = $_path if ($_count>2); # tar file and decompressed folder, otherwise there are extracted files in the dir
  return $_dir;
}
sub GetLastFile {
  my ($_path, $_ne_file)=@_;
	opendir( my $_dh, $_path) or die __FILE__." Line ".__LINE__.",Cannot open $_path\n";
	my @_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ && -f "$_path/$_"} readdir($_dh); #ctime stat 11th 
	#say __FILE__." Line ".__LINE__.",$_path,".Dumper(\@_files);
	my $_last_file=$_files[0];
	if(defined $_ne_file && $_last_file eq $_ne_file){
	  $_last_file=$_files[1];
	}
  return $_last_file;
}
sub GetTarFile { #lastest tar or subdir if not files in dir
  my ($_path, $_version, $_is_dir)=@_;
  $_is_dir=0 if (!defined $_is_dir);
	opendir( my $_dh, $_path) or die __FILE__." Line ".__LINE__.",Cannot open $_path\n";
	my @_files = ();
	@_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ && -f "$_path/$_"} readdir($_dh) if (-d $_path); #ctime stat 11th 
	#say __FILE__." Line ".__LINE__.",p=$_path, v=$_version, d=$_is_dir \n".Dumper(\@_files);
	if(@_files ==0 || (defined $_is_dir && $_is_dir ) ){
		@_files = sort {stat("$_path/$b")->ctime <=> stat("$_path/$a")->ctime} grep {/^[^\.]/ } readdir($_dh); #git folder
	  #say __FILE__." Line ".__LINE__.",p=$_path, v=$_version, d=$_is_dir \n".Dumper(\@_files);
	}
	my $_tar_file;
  my @_tar_postfix_arr = @{+TAR}; # use constant arr
  foreach my $_f(@_files){
    #say __FILE__." Line ".__LINE__.", f=$_path/$_f\n";
		if($_is_dir){
			$_tar_file=$_f;
			last;
		}
		foreach my $_ext(@_tar_postfix_arr){
      #say __FILE__." Line ".__LINE__.", f=$_f, v=$_version, ext=$_ext;\n";
  #perl -e '$f="autoconf-2.69.tar.gz"; $v="2.69"; $e="tar.gz"; if($f =~ m|$v.*\.e$|){print $s."\n";}'
		  if($_f =~ m|\.$_ext$|){ # svm_light.tar.gz, no version 
		    $_tar_file=$_f;
			}
		  if($_f =~ m|$_version.*\.$_ext$|){
		    $_tar_file=$_f;
		    last;
		  }
		}
	}
  return $_tar_file;
}

sub GetLatestVersion{
	my ($_version_arr_ref)=@_; 
	if(defined $_version_arr_ref && scalar @{$_version_arr_ref}>0 ){
	  my @_v_arr= sort { versioncmp($b, $a) } @{$_version_arr_ref}; #max keys %_version_url_hash;
    #say __FILE__." Line ".__LINE__.", sorted version=\n".Dumper(\@_v_arr);
	  return $_v_arr[0];
	}else{
		return undef;
	}
	#opendir( my $_dh, $_src_app_dir ) or die __FILE__." Line ".__LINE__.",Cannot open $_src_app_dir\n";
  #perl  -MSort::Versions=versioncmp -e 'print versioncmp("1.7","1.8")."\n";' #-1
  #perl  -MFile::stat=stat -e '$a="/usr/local/src/mpfr/mpfr-2.4.0.tar.gz"; $s=stat($a)->size; print $s."\n";'
  #perl -MFile::stat=stat -e '$d="/home/yhuang/sbin"; opendir($_dh,$d ); @_dirs=grep {/^\./ && -f "$d/$_" } readdir $_dh;foreach $_(@_dirs){print("$_\n");}' 
	#my @_fhs = sort {stat($a)->ctime <=> stat($b)->ctime} grep { -f "$d/$_" } readdir($_dh); #ctime stat 11th, file only  (stat $b)[10]}
	#my @_fhs = sort {stat("$_src_app_dir/$b")->ctime <=> stat("$_src_app_dir/$a")->ctime} grep {/^[^\.]/ } readdir($_dh); #ctime stat 11th, file only  (stat $b)[10]} no . and ..
}
sub GetFileHandler { #path $_install_dir_prefix/$_lc_name/$_version/, full path will return full path, relative returns relative
  my ($_path, $_level, $_is_dir,$_is_last,$_lang)=@_;
  my @_fhs;
  my $_rule = File::Find::Rule->new;
  #say __FILE__." Line ".__LINE__.",$_path, $_level, $_is_dir,$_is_last,$_lang";
	if(defined $_lang){
		if($_lang=~ m/^perl$/){
		  $_rule->name("*.pl","*.PL","*.pm"); #qr{\.pl$};
			#$_rule->perl_file;
						#->name("*.pl","*.PL","*.pm");
						#->mtime("<$last_week")
						#->not( $backup )
						#->prune          # don't go into it
						#->discard;       # don't report it
						#->or( $svn, $pm ) relative
						#->relative;
		}elsif($_lang=~ m/^python$/){
			$_rule->name("*.py"); 
		}elsif($_lang=~ m/^ruby$/){
			$_rule->name("*.rb"); 
		}elsif($_lang=~ m/^tar$/){
			my $_exts = join("|",@{+TAR}); 
			$_rule->name(qr{\.(${_exts})$}); 
		}elsif($_lang=~ m/^python_lib$/){
			$_rule->name(qr{/lib/python});
		}elsif($_lang=~ m/^perl_lib$/){
			$_rule->name(qr{/lib/site_perl});
		}elsif($_lang=~ m/^lib$/){
			$_rule->name(qr{/lib});
		}
	}
  if(!defined $_is_dir || $_is_dir==0){
    $_rule->file;
  }else{
    $_rule->directory;
  }
  if(!defined $_level){
    $_level=1;
  }
  $_rule->maxdepth($_level);
  @_fhs = $_rule->in($_path);

	if(defined $_is_last && $_is_last>0){
	}
  return \@_fhs;
}

sub TranslateCommandArr{
	my ($_command_arr_ref,$_old,$_new)=@_;
	my @_commands=@{$_command_arr_ref};
  if(defined $_command_arr_ref && defined $_old && defined $_new && $_old ne $_new ){
  	#say __FILE__." Line ".__LINE__. ", ".Dumper(\@_commands);
  	map {if(defined $_ && $_=~ m/$_old/ ){$_ =~ s/$_old/$_new/g;}} @_commands;
  }
  return \@_commands;
}
sub TranslateOldNew{
	my ($_str,$_old,$_new)=@_;
	my $_new_str=$_str;
  if(defined $_str && defined $_old && defined $_new ){
  	$_new_str =~ s/\Q$_old\E/\Q$_new\E/g;
  }
  return $_new_str;
}
sub TrimFileName{
	my $_name=shift;
	#mirror_choices?projectname=fastuniq&filename=FastUniq-1.0.tar.gz
	if($_name =~ m/mirror_choices\?projectname=.+&filename=(.+)/ ){
		$_name=$1;
	}
  #perl -e '$f="glew%2F1.2.4%2Fglew-1.2.4-linux32.tgz"; if($f =~ m|.+%2F(.+)|){print $1."\n";}'
	if($_name =~ m|.+/([^\/]+)|){ #glew%2F1.2.4%2Fglew-1.2.4-linux32.tgz
		$_name=$1;
	}
	if($_name =~ m|.+%2F(.+)|){ #glew%2F1.2.4%2Fglew-1.2.4-linux32.tgz
		$_name=$1;
	}
	return $_name;
}
sub GetTarPattern{
  my $_end=shift;
  my $_pattern="";
  my @_tar_postfix_arr = @{+TAR}; # use constant arr
  foreach my $_ext(@_tar_postfix_arr){
    $_ext=~ s/\./\\\./g;
    if(defined $_end && $_end>0){
      $_ext .= '\s*$';
    }else{
      $_ext .= '\/';
    }
    if($_pattern eq ""){
      $_pattern = $_ext;
    }else{
      $_pattern .= "|".$_ext;
    }
  }
  return $_pattern;
}
sub GetFullPath{
  my ($_file, $_user)=@_;
  $_file=Trim($_file);
  # perl -MFile::HomeDir -e '$_f="~/.key"; if($_f=~ m|^~(/.+)|){print "match: ".File::HomeDir->my_home.$1."\n"}else{print "no match, $_f"}'
  if(defined $_file && $_file=~ m|^~(/.+)|){ # home
    my $_hd=$1;
    if (defined $_user){
		$_file=File::HomeDir->users_home($_user).$1;
		}else{
		 $_file=File::HomeDir->my_home.$1;
	 }
  }elsif(defined $_file && $_file !~ m|^/|){ #relative path
    $_file= cwd()."/".$_file;
  }
  return $_file;
}
sub IsEmptyDir {
	my ($_fh) =@_;
	my $_is_empty = 1;
	if (-d $_fh){
    opendir my $_d, $_fh or die $!;
    $_is_empty = grep ! /^\.\.?$/, readdir $_d ;
  }
  return $_is_empty;
}
sub IsCmdExist {
	my ($_cmd) =@_;
	my $_is_exist = 0;
	my $_full_path;
	for my $_path ( split /:/, $ENV{PATH} ) {
    if ( -f "$_path/$_cmd" && -x _ ) {
        $_full_path = "$_path/$_cmd";
        $_is_exist = 1;
        last;
    }
  }
  return $_is_exist;
}
sub RemoveHeadTail{
  my ($_str)=@_;
  my $i=0;
  #say __FILE__.", line ". __LINE__.", RemoveHeadTail s=$_str";
  # perl -Mre -e '$a="[https://github.com/trinityrnaseq/trinityrnaseq/archive/]"; if($a=~ m/^\[.+[\]]$/){print "matched\n"}else{print "NOT matched\n"}'
  while($_str =~ m/^[\s'"[].+[\s'"\]]$/ && $i<5){
  	#say __FILE__." Line ".__LINE__.",$_str";
 		$_str =~ s/^\s*(.+)\s*$/$1/ if ($_str =~ m/^\s*(.+)\s*$/);
 		$_str =~ s/^"(.+)"$/$1/ if($_str =~ m/^"(.+)"$/);
  	$_str =~ s/^'(.+)'$/$1/ if($_str =~ m/^'(.+)'$/);
  	$_str =~ s/^\[(.+)]$/$1/ if($_str =~ m/^\[(.+)]$/);
  	$i++;
  }
  return $_str;
}

sub Timeout {
	my ($_cmd,$_timeout ) = @_;
  $_timeout = 5 if (!defined $_timeout);
  
	my $_result="";
	eval {
		local $SIG{ALRM} = sub { die "$_timeout seconds timeout\n" };   
		alarm $_timeout;
		$_result = `$_cmd`;
		alarm 0;
	};
	if ($@) {
		warn "$_cmd timed out of $_timeout seconds.\n";
	}
	return $_result;
}

1;

