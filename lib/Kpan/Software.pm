#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::Software;
use strict;
use warnings;
use Sort::Versions qw(versioncmp);
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan";
use Prereq;
use DBData;
use KUtil;
our $VERSION = '0.1';

my %CACHE; #static cache for app

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
			#installing_version_array          => [],    # reference to an annonymous array      
			archive_url                        => shift,           
			brief                              => shift,                 
			category                           => shift,              
			checked_level                      => shift,              
			debug                              => shift,                 
			home_url                           => shift,               
			id                                 => shift,       
			#is_kpan_dev                        => shift, # kpan developer?                    
			kp_name                            => shift, # smatools, 1.0_gcc447 , should move to install module                      
			kp_version                         => shift, # smatools, 1.0_gcc447 , should move to install module                         
			latest_version                     => shift,              
			lc_name                            => shift,                     
			name                               => shift,                        
			no_db				                       => shift,                 
			note                               => shift,                        
			prereq_hash_ref                    => {},    # ${$_prereq_hash_ref}{$_prereq_order}=$_p; ; $_p name, version object {},    #foreach my $i (0..$#v) {$version_url_hash{$v[$i]}=$u[$i];}               
			prereq_lib_hash_ref                => {},    # R/python/perl lib ${$_prereq_lib_hash_ref}{$_prereq_name}{$_prereq_order}=$_p;         
			publisher                          => shift,                 
			sample_data_url                    => shift,         
			tag                                => shift,                      
			version                            => shift,                      
			version_file_name                  => shift,            
			version_file_name_ext              => shift,        
			version_file_size                  => shift,            
			version_url                        => shift,           
			version_url_hash_ref               => shift, # {}, #foreach my $i (0..$#@v) {$version_url_hash{$v[$i]}=$u[$i];} delete?        
			version_software_hash_ref          => shift, # {}, #foreach my $i (0..$#@v) {$_software=$version_software_hash{$v[$i]};}   delete?      
	};
	if(defined $_self->{name}){
		$_self->{lc_name} =lc($_self->{name});
	}
	if(!defined $_self->{kp_name}){
		$_self->{kp_name} =$_self->{name};
	}
	if(!defined $_self->{kp_version}){
	  $_self->{kp_version}=$_self->{version};
	}
	return $_self;  
}
sub setSelf { # todo spider siblings
  my ($_self, $_kpan_client_name, $_rc) = @_;
 	return if (!defined $_self->{name});
	$_self->{lc_name} = lc($_self->{name}) if ( defined $_self->{name} );
	$_self->{kp_name} = $_self->{name} if( !defined $_self->{kp_name}  );

  # get all version from DB
	my $_software_all_hash_ref = $_rc->getSoftwareHash($_kpan_client_name,$_self->{id}, $_self->{name}, $_self->{version});	 
	my $_software_hash_ref     = $_software_all_hash_ref->{$_self->{name}}                  if (defined $_software_all_hash_ref); 
	say __FILE__. " Line " . __LINE__ . ", software hash =".Dumper($_software_all_hash_ref) if ( defined $_self->{debug} && $_self->{debug} > 4 ); 
	if( defined $_software_hash_ref){
		my @_versions              = keys %{$_software_hash_ref};
	  $_self->{latest_version}   = Kpan::KUtil::GetLatestVersion(\@_versions); 
	  $_self->{version}          = $_self->{latest_version}                                   if (!defined $_self->{version} ); # in case only want to install last version
	  foreach my $_version_k (sort keys %{$_software_hash_ref}  ){
		  my $_software= $_software_hash_ref->{$_version_k}; # defined ensures method not overwritten
		  say __FILE__. " Line " . __LINE__ . ",_software_hash_ref, software =".Dumper($_software) if ( defined $_self->{debug} && $_self->{debug} > 2 ); 
		  foreach my $_key (keys %{$_software}){
			  $_self->{$_key}        = $_software->{$_key}  if (!defined $_self->{$_key} || ($_self->{$_key} =~ m/^\s*$/ && defined $_software->{$_key} && $_software->{$_key} =~ m/\S/)); # fill brief, url etc
		  }
		  last if($_self->{version} eq $_software->{version}); # version match, then done
    }
  }
	my $_home_url              = $_self->{home_url};
	my $_archive_url           = $_self->{archive_url};
	my $_version_url           = $_self->{version_url};
	my ($_name,$_project_name);
	say __FILE__." Line ".__LINE__.",setSelf, before guess hu name=$_self->{name}, $_home_url,$_archive_url,$_version_url,$_self->{debug}" if(defined $_self->{debug} && $_self->{debug}>2);
	($_name,$_project_name,$_home_url,$_archive_url) = Kpan::Url::_guessHomeArchiveUrl($_home_url,$_archive_url,$_version_url,$_self->{debug}); 
	say __FILE__." Line ".__LINE__.",setSelf, after guess hu name=$_self->{name}, $_home_url,$_archive_url,$_version_url,$_self->{debug}"  if(defined $_self->{debug} && $_self->{debug}>2);
	$_self->{home_url}         = $_home_url                                                 if (!defined $_self->{home_url} || ($_self->{home_url} =~ m/^\s*$/ && defined $_home_url && $_home_url =~ m/\S/));    
	$_self->{archive_url}      = $_archive_url                                              if (!defined $_self->{archive_url} || ($_archive_url =~ m/^\s*$/ && defined $_archive_url && $_archive_url =~ m/\S/));

	say __FILE__." Line ".__LINE__.",setSelf, name=$_self->{name}.".Dumper($_self)          if(defined $_self->{debug} && $_self->{debug}>2);
	# todo if (defined $_self->{archive_url}( {spider versions}; $_url_obj->processAppArchiveUrlToDB 
	#afer spider, reset last_version and fill undef version ? No, dead loopitself. VU should be presented in input  
}
sub addPrereq {
	my ($_self, $_pq_name, $_pq_version)=@_;
	Kpan::Prereq->new->addPrereq($_self->{prereq_lib_hash_ref},$_self->{prereq_hash_ref},$_self->{name},$_self->{version},$_pq_name, $_pq_version);	 
}

sub setPrereqLib{
	my ($_self, $_kpan_client_name, $_rc)=@_;
		if (defined $_self->{prereq_lib_hash_ref}){
    my $_prereq_lib_hash_ref = $_self->{prereq_lib_hash_ref};
		foreach my $_pq_name_key (sort keys %{$_prereq_lib_hash_ref}) { # key = perl|python.*|R
      my %_pq_order_hash = %{$$_prereq_lib_hash_ref{$_pq_name_key}};
      my @_keys = sort  { $a <=> $b } keys %_pq_order_hash ; 
      #say __FILE__." Line ".__LINE__.", prereq:".Dumper(\@_keys) if (scalar(@_keys)>0);
      for my $_i (0.. $#_keys) { 
        my $_pq  = $_pq_order_hash{$_keys[$_i]};
				$_pq->{debug} = $_self->{debug};
				$_rc->{debug}=$_self->{debug};
  			my $_set_id               = undef;
		    $_rc->getPrereq($_kpan_client_name, $_pq, $_self->{id},$_set_id);
		  }
		}
	}
	if (defined $_self->{prereq_hash_ref}){
    my $_pq_hash_ref = $_self->{prereq_hash_ref};
    foreach my $_order_key ( keys %{$_pq_hash_ref}) { # key = order
      my $_pq                   = $$_pq_hash_ref{$_order_key};
			$_pq->{debug}             = $_self->{debug};
			$_rc->{debug} = $_self->{debug};
			my $_set_id               = undef;
			$_rc->getPrereq($_kpan_client_name, $_pq, $_self->{id},$_set_id);
	  }
	}
}

sub getCombinePrereqHashRef {
  my ($_self)=@_;
  return Kpan::Prereq::CombinePrereqHashRef($_self->{prereq_lib_hash_ref}, $_self->{prereq_hash_ref}); # $$_pq_hash_ref{$_order} =$_pq;
}

1;

