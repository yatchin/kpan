#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

# parse kpan install cmd.sh commands 
package Kpan::ParseCmd;
use 5.010;
use strict;
use warnings;
use File::stat;
use File::Find::Rule;
use Sort::Versions qw(versioncmp);
use Data::Dumper;
use feature qw(say);
use FindBin;    
use lib "$FindBin::Bin/../lib/Kpan/";
use DBData;
use KUtil;
use Software;

use constant TAR => [qw/ tar.gz tar.bz2 tar.xz tar.Z tar.lz bz2 cpio gz lz sht tar tgz xz zip Z sh bin/]; 

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		software_hash_ref  => shift,
		log_hash_ref       => shift,
		module_hash_ref    => shift,
		debug              => shift,
		note               => shift
  }		
}
sub parseCmd {
	my ($_self, $_file) = @_;  
	$_file = "$FindBin::Bin/../cmdH2.sh/" if (!defined $_file);  $_self->parseCmdFile($_file);
	say __FILE__. " Line " . __LINE__ . ", total=".( scalar keys %{$_self->{software_hash_ref}} ) if ( defined $_self->{debug} && $_self->{debug} > 0 );
  my $_db_data       = Kpan::DBData->new;
  $_db_data->{debug} = $_self->{debug};
  foreach my $_key ( keys %{$_self->{software_hash_ref}} ) {
  	my $_s = $_self->{software_hash_ref}{$_key};
    say __FILE__." Line ".__LINE__.",$_s->{name}/$_s->{version}" if(defined $_self->{debug} && $_self->{debug}>2);
    $_db_data->uploadSoftware($_s);
  }                   
}

sub parseCmdFile {
	my ($_self, $_file) = @_;
	$_file = "$FindBin::Bin/../cmdH2.sh/" if (!defined $_file);
	open my $_in, "<", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for reading: $!\n";
  my $_kpan_start=0;
  my $_cmd="";
	while (my $_line=<$_in>){
	  $_line=Kpan::KUtil::Trim($_line);
	  next if( $_line=~ m/^#/); # skip commetns 
	  if( $_line=~ m/^\s*kpan\s+-i / && $_kpan_start==0){
	  	$_kpan_start =1;
    }elsif( $_line=~ m/^\s*$/ || $_line=~ m/^\s*kpan/ ){  #empty line or kpan, or comments? 
	  	if( $_kpan_start ==1 ){ # set cmd
	  	  say __FILE__. " Line " . __LINE__ . ", cmd=$_cmd" if ( defined $_self->{debug} && $_self->{debug} > 4 );
				my $_s = Kpan::Software->new;				        if( $_cmd =~ s/(--brief)\s+(.+)$//m){#take brief
				  $_s->{brief}=Kpan::KUtil::RemoveHeadTail($2);
				}
	  		my @_e = split (' ', $_cmd); # rest of cmd except brief
				say __FILE__. " Line " . __LINE__ . ", cmd(no brief)=$_cmd,e=".Dumper(\@_e) if ( defined $_self->{debug} && $_self->{debug} > 4 );
				for my $i (0..$#_e){
  				my $j = $i+1;
	  			if($_e[$i] eq "-n"){
	  				$_s->{name}=$_e[$j];
	  				$i++;
	  			}elsif($_e[$i] eq "-v"){
	  				$_s->{version}=$_e[$j];
	  				$i++;
	  			}elsif($_e[$i] eq "-hu"){
	  				$_s->{home_url}=$_e[$j];
	  				$i++;
	  			}elsif($_e[$i] eq "-au"){
	  				$_s->{archive_url}=$_e[$j];
	  				$i++;
	  			}elsif($_e[$i] eq "-vu"){
	  				$_s->{version_url}=$_e[$j];
	  				$i++;
	  			}elsif($_e[$i] eq "--category"){
	  				$_s->{category}=$_e[$j];
	  				$i++;
	  			}
	  		}
	  		$_s->{category}="bioinformatics" if (!defined $_s->{category});
	  		say __FILE__. " Line " . __LINE__ . ", b4 guess s=".Dumper($_s) if ( defined $_self->{debug} && $_self->{debug} > 1 );
	  		my ($_name,$_project_name,$_home_url,$_archive_url) = Kpan::Url::_guessHomeArchiveUrl($_s->{home_url},$_s->{archive_url},$_s->{version_url},$_self->{debug}); 
        $_s->{home_url}    = $_home_url if (!defined $_s->{home_url});    
        $_s->{archive_url} = $_archive_url if (!defined $_s->{archive_url});        $_s->{kp_name}     = $_s->{name}    if ( !defined $_s->{kp_name} );    
        $_s->{kp_version}  = $_s->{version} if ( !defined $_s->{kp_name} );    
	  		say __FILE__. " Line " . __LINE__ . ", self=".Dumper($_s) if ( defined $_self->{debug} && $_self->{debug} > 1 );
	  		#my %_s_hash = %{$_s};
	  		#say __FILE__. " Line " . __LINE__ . ", self=".Dumper(\%_s_hash) if ( defined $_self->{debug} && $_self->{debug} > 1 );
	  		
	  		my $_index=scalar keys %{$_self->{software_hash_ref}};
	  		$_self->{software_hash_ref}{$_index}=$_s;
	  		say __FILE__. " Line " . __LINE__ . ", self=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 4 );
	  	}
	  	# reset
	  	$_cmd="";      $_kpan_start = 0;
    }   
    if($_kpan_start==1){
    	$_line =~ s/\\$//; # remove last \
    	$_cmd .= $_line;
    }   	}
	say __FILE__. " Line " . __LINE__ . ", self=".Dumper($_self) if ( defined $_self->{debug} && $_self->{debug} > 2 );
	say __FILE__. " Line " . __LINE__ . ", parse log total=".( scalar keys %{$_self->{software_hash_ref}} ) if ( defined $_self->{debug} && $_self->{debug} > 0 );
}

sub parseDBInstallLog {
	my ($_self, $_app_name, $_app_version, $_cleint_name, )=@_;
  my $_db_data       = Kpan::DBData->new;
  $_db_data->{debug} = $_self->{debug};
	my @_install_log = $_db_data->getInstallLog($_app_name, $_app_version, $_cleint_name);
	foreach my $_f (@_install_log){
		my $_install = Kpan::Install->new;    $_install->parseKlog($_f);
	}
}

sub parseLog {
	my ($_self, $_dir, $_target_file, $_depth, $_max_depth,$_init_dir) = @_;
	$_max_depth = 3     if (!defined $_max_depth);
	my $_current_depth = ($_dir      =~ tr|/||);
	my $_init_depth    = ($_init_dir =~ tr|/||);
	$_depth     = $_current_depth - $_init_depth;
	say __FILE__." Line ".__LINE__.", dir=$_dir, t=$_target_file, d=$_depth, m=$_max_depth, i=$_init_dir"  if ( defined $_self->{debug} && $_self->{debug} > 2 );     
	return if (!defined $_dir);
	return if ($_depth>=$_max_depth);
  my @_files  = <$_dir/*>;
  foreach my $_f ( sort @_files ) {
  	say __FILE__." Line ".__LINE__.", f=$_f\n"  if ( defined $_self->{debug} && $_self->{debug} > 2 );    
  	if($_f    =~ m/$_target_file/ ){
  		my $_fh = $_f;
  		my ($_name, $_version, $_src_file_path, $_src_path_lead, $_src_path_tail, $_src_path_middle, $_version_file_name, $_version_file_size, $_log_content);
  		if( $_fh =~ m|/usr/local/src/(.+?)/(.+?)/(?!Formatted).*$_target_file.*| && -f $_fh){
				if( $_fh =~ m|/usr/local/src/(.+?)/(.+?)/(?!Formatted).*$_target_file.*| && -f $_fh){
					$_name            = $_src_path_lead = $1;
					$_version         = $_src_path_tail = $2;
				}elsif( $_fh =~ m|/usr/local/src/(.+?)/(.+?)/(.+?)/(?!Formatted).*$_target_file.*| && -f $_fh){
					$_name            = $_src_path_lead = $1;
					$_version         = $_src_path_tail = $3;
					$_src_path_middle = $2;
				}
  		  $_src_file_path   = $_fh;
  		  $_src_file_path   =~ s|/$_target_file$||;
 	      
		    $_log_content     = do { 
		    	                      local $/; 
		    	                      open my $_in, '<', "$_fh" or say  __FILE__." Line ".__LINE__.", couldn't open file $_fh for reading: $!\n";
		    	                      <$_in> 
		    	                     }; # $/ was new line, now undef
  		  my $_parent_dir   = $_fh;
  		  $_parent_dir      = "/usr/local/src/$_name";
  		  say __FILE__." Line ".__LINE__.", fh=$_fh, n=$_name, v=$_version, p=$_src_file_path, PD=$_parent_dir, \n"  if ( defined $_self->{debug} && $_self->{debug} > 0 );       
        my @_version_tars =  <$_parent_dir/*>;
        OUTER:         foreach my $_f2 ( sort @_version_tars ) {
        	next if (! -f $_f2 || $_f2 =~ m/^\.+$/);
   		    #say __FILE__." Line ".__LINE__.", f=$_f2\n"  if ( defined $_self->{debug} && $_self->{debug} > 0 );       
       	  if ($_f2 =~ /$_version/i){
            my @_tar_postfix_arr = @{+TAR}; # use constant arr
		        foreach my $_ext(@_tar_postfix_arr){
              #say __FILE__." Line ".__LINE__.", f=$_f, v=$_version, ext=$_ext;\n";
              #perl -e '$f="autoconf-2.69.tar.gz"; $v="2.69"; $e="tar.gz"; if($f =~ m|$v.*\.e$|){print $s."\n";}'
		          if($_f2 =~ m|\.$_ext$|){ # svm_light.tar.gz, no version 
		            $_version_file_name = $_f2;
		            my $_st = stat($_version_file_name);
	              $_version_file_size = $_st->size; #(stat $filename)[7],  -s $_version_file_name";
		            $_version_file_name =~ s|(.+)/||;
		            last OUTER;		          
			        }
		        }
		      }
		    }
		    say __FILE__. " Line " . __LINE__ . ", hit, d=$_depth, m=$_max_depth, fh=$_fh,n=$_name, v=$_version, " if ( defined $_self->{debug} && $_self->{debug} > 0 );               
		    $_self->{log_hash_ref}{$_name}{$_version} = {
					name									  => $_name,
					version								  => $_version,
					src_file_path					  => $_src_file_path,
					src_path_lead					  => $_src_path_lead,
					src_path_tail					  => $_src_path_tail,
					src_path_middle				  => $_src_path_middle,
					version_file_name			  => $_version_file_name,
					version_file_size			  => $_version_file_size,
					log_content						  => $_log_content,
				};
				last;
		  }
		  say __FILE__. " Line " . __LINE__ . ", hit, d=$_depth, m=$_max_depth, fh=$_fh, log hash=".Dumper($_self->{log_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 1 );               
    }elsif (-d $_f) {
      $_self->parseLog($_f, $_target_file, $_depth, $_max_depth,$_init_dir);
    }
  }
	say __FILE__. " Line " . __LINE__ . ", total=".( scalar keys %{ $_self->{log_hash_ref} } ) if ( $_depth ==0 );               
	say __FILE__. " Line " . __LINE__ . ", log hash=".Dumper($_self->{log_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 0 && $_depth ==0 );               
}

sub uploadLog {
	my ($_self) = @_;
  my $_db_data        =Kpan::DBData->new();
  $_db_data->{debug}  =$_self->{debug};
  $_db_data->uploadLog($_self->{log_hash_ref});	
}
sub downloadULogHash{
	my ($_self) = @_;
  my $_db_data        = Kpan::DBData->new();
  $_db_data->{debug}  = $_self->{debug};
  $_self->{log_hash_ref}   = $_db_data->downloadULogHash;	
	say __FILE__. " Line " . __LINE__ . ", U log downladed  total=".( scalar keys %{ $_self->{log_hash_ref} } );               
	say __FILE__. " Line " . __LINE__ . ", log hash=".Dumper($_self->{log_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 2  );  
}

sub parseModule {
	my ($_self, $_dir, $_target_file, $_depth, $_max_depth,$_init_dir) = @_;
	$_max_depth = 3     if (!defined $_max_depth);
	my $_current_depth = ($_dir      =~ tr|/||);
	my $_init_depth    = ($_init_dir =~ tr|/||);
	$_depth     = $_current_depth - $_init_depth;
	say __FILE__." Line ".__LINE__.", dir=$_dir, t=$_target_file, d=$_depth, m=$_max_depth, i=$_init_dir"  if ( defined $_self->{debug} && $_self->{debug} > 2 );     
	return if (!defined $_dir);
	return if ($_depth>=$_max_depth);
  my @_files  = <$_dir/*>;
  foreach my $_f ( sort @_files ) {
  	say __FILE__." Line ".__LINE__.", f=$_f\n"  if ( defined $_self->{debug} && $_self->{debug} > 2 );    
  	if($_f    =~ m/$_target_file/ ){
  		my $_fh = $_f;
  		$_fh    =~ s|//|/|g;
  		my ($_name, $_module_name, $_module_text, $_module_path, $_home_url, $_brief, $_src_app_path);
  		if( $_fh =~ m|/usr/local/modulefiles_eb/all/(.+?)/(.+\.$_target_file)| && -f $_fh){
				$_name          = $1;
				$_module_name   = $2;
  		  $_module_path   = $_fh;
  		  $_module_path   =~ s|/[^/]+\.$_target_file$||;
 	      
  		  ($_module_text, $_home_url, $_brief, $_src_app_path) = $_self->parseModuleFile($_fh);
  		  say __FILE__." Line ".__LINE__.", fh=$_fh, n=$_name, m=$_module_name, src_app_path=$_src_app_path, \n"  if ( defined $_self->{debug} && $_self->{debug} > 2 );       
 
		    $_self->{module_hash_ref}{$_name."_".$_module_name} = {
								name							  => $_name,
								module_name				  => $_module_name,
								module_text				  => $_module_text,
								module_path				  => $_module_path,
								home_url					  => $_home_url,
								brief							  => $_brief,
								src_app_path			  => $_src_app_path,
				};
				last;
		  }
		  say __FILE__. " Line " . __LINE__ . ", hit, d=$_depth, m=$_max_depth, fh=$_fh module hash=".Dumper($_self->{module_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 2 );               
    }elsif (-d $_f) {
      $_self->parseModule($_f, $_target_file, $_depth, $_max_depth,$_init_dir);
    }
  }
	say __FILE__. " Line " . __LINE__ . ", total=".( scalar keys %{ $_self->{module_hash_ref} } ) if ( $_depth ==0 );               
	say __FILE__. " Line " . __LINE__ . ", log hash=".Dumper($_self->{module_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 2 && $_depth == 0 );               
}

sub parseModuleFile {
  my ($_self, $_file) = @_;
	my ($_module_text, $_home_url, $_brief, $_src_app_path) ;
	$_module_text    = do { 
										      local $/; 
													open my $_in, '<', "$_file" or say  __FILE__." Line ".__LINE__.", couldn't open file $_file for reading: $!\n";
													<$_in> 
												}; # $/ was new line, now undef		$_module_text    .= $_line;
	if($_module_text =~ m/\n\s*local\s+root\s+=(.+?)\n/s){
		$_src_app_path =  $1;
		$_src_app_path =~ s|["\s]||g;
	}
	if($_module_text =~ m/Description(.+)More\s+information/s){
		$_brief        =  $1;
		$_brief        =~ s/==+//g;
		$_brief        =~ s/\n//g;
	}
	if($_module_text =~ m/\n\s*-\s+Homepage:\s+(.+?)\n/s){
		$_home_url     =  $1;
		$_home_url     =~ s/[\s\n]//g;
	}
	
	return ($_module_text, $_home_url, $_brief, $_src_app_path);}

sub uploadModule {
	my ($_self) = @_;
  my $_db_data        =Kpan::DBData->new();
  $_db_data->{debug}  =$_self->{debug};
  $_db_data->uploadUModule($_self->{module_hash_ref});	
}sub downloadModuleHash{
	my ($_self) = @_;
  my $_db_data        = Kpan::DBData->new();
  $_db_data->{debug}  = $_self->{debug};
  $_self->{module_hash_ref}   = $_db_data->downloadUModuleHash;	
	say __FILE__. " Line " . __LINE__ . ", module downloaded, total name=".( scalar keys %{ $_self->{module_hash_ref} } );               
	say __FILE__. " Line " . __LINE__ . ", module hash=".Dumper($_self->{module_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 2  );  
}

sub parseEB {
	my ($_self, $_dir, $_upload)    = @_;
	#my $_eb        = Kpan::EB->new;
	#$_eb->{debug}  = $_self->{debug};
	#$_eb->parseEB($_dir);
	say __FILE__." Line ".__LINE__.", looking for receipts";
  my @_files = File::Find::Rule->file()
                            ->name( '*.eb' )
                            ->in( $_dir );
 	say __FILE__." Line ".__LINE__.", found ". (scalar @_files) ." receipts" ;                           
  my $_db_data        =Kpan::DBData->new();
  $_db_data->{debug}  =$_self->{debug};
  my $_i = 0;
  for $_i (0..$#_files){
  	my $_eb        = Kpan::EB->new;
  	$_eb->{debug}  = $_self->{debug};
    $_eb->parseEBFile($_files[$_i]);
	  my %_uhash     = %{$_eb};
	  $_db_data->uploadToEBApp(\%_uhash, $_upload) if (defined $_upload && $_upload>0);
    say __FILE__." Line ".__LINE__.",$_eb->{name}/$_eb->{version}" if( defined $_self->{debug} && $_self->{debug}>0 );
    say __FILE__." Line ".__LINE__.", i=$_i ".Dumper(\%_uhash)     if( defined $_self->{debug} && $_self->{debug}>0 && ($_i<2 || $_i%1000 ==0) );
    #$_i++;
    #last if $_i>10;
  }   
  say __FILE__." Line ".__LINE__.",total ". (scalar @_files) ." receipts done" ;           }sub downloadEBHash{
	my ($_self,$_note) = @_;
  my $_db_data        = Kpan::DBData->new();
  $_db_data->{debug}  = $_self->{debug};
  $_self->{eb_hash_ref}   = $_db_data->downloadEBHash($_note);	
	say __FILE__. " Line " . __LINE__ . ", eb receipt downloaded total name=".( scalar keys %{ $_self->{eb_hash_ref} } );               
	say __FILE__. " Line " . __LINE__ . ", eb hash=".Dumper($_self->{eb_hash_ref} ) if ( defined $_self->{debug} && $_self->{debug} > 2  );  
}
sub getEBCmd{
	my ($_self, $_note,  $_detail) = @_;
	$_detail = 0 if (!defined $_detail);
	$_note   = "E1" if (!defined $_note);
	my $_d = "/usr/local/modulefiles_eb/all";
	my $_init_depth_level = 0; 
	my $_max_depth = 3; 
	$_self->parseModule($_d, "lua",$_init_depth_level,$_max_depth,$_d,);
	
  $_self->uploadModule;
  $_d = "/usr/local/apps/EasyBuild/3.4.1/lib/python2.7/site-packages/easybuild_easyconfigs-3.4.1-py2.7.egg/easybuild/easyconfigs";
  my $_upload = 1;
	#$_self->parseEB($_d, $_upload);
	$_upload = 2;
  $_d = "/usr/local/src/easybuild/gacrc-easyconfigs/raj76/easybuild-life-sciences/easybuild/easyconfigs/";
	#$_self->parseEB($_d, $_upload);  
	$_self->downloadULogHash;
  $_self->downloadModuleHash;
  $_self->downloadEBHash($_note);
   say __FILE__. " Line " . __LINE__ ;
     
  my %_eb_installed_hash    = map { exists  $_self->{module_hash_ref}{$_}                               ? ($_ => 1)  : ()        } keys  %{ $_self->{log_hash_ref} };  my %_need_eb_install_hash = map { !exists $_eb_installed_hash{$_} && exists $_self->{eb_hash_ref}{$_} ? ($_ => 1 ) : ()        } keys  %{ $_self->{log_hash_ref} };
  my %_need_gb_install_hash = map { exists  $_self->{eb_hash_ref}{$_}                                   ? ()         : ($_ => 1) } keys  %{ $_self->{log_hash_ref} };

  say __FILE__. " Line " . __LINE__;
  say "apps installed on sapelo, total=".( scalar keys  %{ $_self->{log_hash_ref} } );
  say "eb has installed overlap sapelo's, total=".( scalar keys  %_eb_installed_hash );
  say "eb could install overlap sapelo'S, total=".( scalar keys  %_need_eb_install_hash );
  say "gb could install overlap sapelo, total=".( scalar keys  %_need_gb_install_hash );

  say __FILE__. " Line " . __LINE__ . ", eb_installed_hash="   .Dumper(\%_eb_installed_hash )    if ( defined $_self->{debug} && $_self->{debug} > 4 );
  say __FILE__. " Line " . __LINE__ . ", need_eb_install_hash=".Dumper(\%_need_eb_install_hash ) if ( defined $_self->{debug} && $_self->{debug} > 4 );
  say __FILE__. " Line " . __LINE__ . ", need_gb_install_hash=".Dumper(\%_need_gb_install_hash ) if ( defined $_self->{debug} && $_self->{debug} > 4 );  
   say __FILE__. " Line " . __LINE__ . ", built-in receipt" if ($_note eq "1117E1");
 
  my $_i = 0;  foreach my $_nkey ( sort keys %_need_eb_install_hash ) {
  	my $_v_hash = $_self->{eb_hash_ref}{$_nkey} ;
  	my $_eb_cmd = "";
  	#say "* $_nkey";
  	my $_last_v;
  	foreach my $_vkey ( sort { versioncmp($b, $a) } keys %{$_v_hash} ) {
  		my $_app_name     = $$_v_hash{$_vkey}{name};
  		my $_version_name = $$_v_hash{$_vkey}{version};
  		my $_file_name    = $$_v_hash{$_vkey}{file_name};
  		$_last_v          = $_vkey;
  		if ($_detail==0){ #last version
				$_eb_cmd =  "$_app_name $_version_name $_file_name";
  	  }elsif($_detail==2017 && $_file_name =~ m/foss-2017/){ #foss_2017 last version
  	  	$_eb_cmd =  "$_app_name $_version_name $_file_name\n";
				last ;
  	  }elsif($_detail==2016 && $_file_name =~ m/foss-201[6|7]/){ #foss_2017 2016 last version
  	  	$_eb_cmd =  "$_app_name $_version_name $_file_name\n";
				last;
  	  }elsif ($_detail>0 && $_detail != 2016 && $_detail != 2017){ # all 
  	  	$_eb_cmd .=  "$_app_name $_version_name $_file_name\n";
  		} 
    }
    if ( $_eb_cmd =~ /\S/ ){
      $_i++;
      say $$_v_hash{$_last_v}{home_url};
      say $_eb_cmd;
    }  }
  say "Total $_i eb apps pending";
}

sub getEBReceipt{
	my ($_self, $_file, $_note) = @_;
	$_note   = "E1" if (!defined $_note);
  $_self->downloadEBHash($_note);
 
 	open my $_in, '<', "$_file" or say  __FILE__." Line ".__LINE__.", couldn't open file $_file for reading: $!\n";

 	my $_eb_cmd = "";
	while (my $_line=<$_in>){
		$_line=Kpan::KUtil::Trim($_line);
	  next if( length($_line)==0 || $_line =~/^#/);
	  my ($_app_name, $_old_version, $_receipt_name, $_new_version);
	  if($_line =~ m/(.+?)\s+(.+?)\s+(\S+)/){
			$_app_name          = $1;
			$_old_version       = $2;
			$_receipt_name      = $3;
			$_new_version       = $_old_version;
			$_new_version       = $4 if($_line =~ m/(.+?)\s+(.+?)\s+(.+?)\s+(.+)/);
			my $_lc_app_name    = lc($_app_name);
			my $_lc_old_version = lc($_old_version);
			if(exists $_self->{eb_hash_ref}{$_lc_app_name}){
			  my $_v_hash       = $_self->{eb_hash_ref}{$_lc_app_name} ;
			  if(exists $$_v_hash{$_lc_old_version}){
          my $_content = $$_v_hash{$_lc_old_version}->{content};
					#say Dumper($$_v_hash{$_lc_old_version});
					$_content =~ s/checksums\s+=(.+?)\n//; 
					$_content =~ s/version\s+=\s+'$_old_version'\s*\n/version = '$_new_version'\n/; # 
					my $_new_file_name = $_receipt_name;
					$_new_file_name =~ s/$_old_version/$_new_version/i;
					my $_ed = "/usr/local/src/easybuild/gacrc-easyconfigs/yhuang/robot/".substr($_lc_app_name, 0,1)."/$_app_name";
					say "mkdir -p $_ed/" if (! -d $_ed );					
					system "mkdir -p $_ed/" if (! -d $_ed );					
					my $_new_receipt = "$_ed/$_new_file_name";
					open my $_out, '>', "$_new_receipt" or say  __FILE__." Line ".__LINE__.", couldn't open file $_new_receipt for writing: $!\n";
					say $_out $_content;
					say "eb $_new_file_name --inject-checksums" if (-f $_new_receipt);
					system "eb $_new_file_name --inject-checksums" if (-f $_new_receipt);
					#say "ls -l $_new_receipt";
					#say "head $_new_receipt" if (-f $_new_receipt);
					#say "eb $_new_file_name --inject-checksums";
					say "vi $_new_receipt";
					say "eb $_new_file_name";					
				}			    	}	  		     } 
	}
  say __FILE__. " Line " . __LINE__ ;
	my $_content="";
  my @cmd_arr =(  );}

sub runCmd{
  my ($_self, $_cmd_array_ref)=@_;
  foreach my $_cmd (@{$_cmd_array_ref}){
    $_cmd=Kpan::KUtil::Trim($_cmd);
    say __FILE__." Line ".__LINE__.", $_cmd " if(defined $_self->{debug} && $_self->{debug}>0);
    next if(($_cmd =~ m/^\#\#/)  );
		if( $_cmd =~ m/^cd(\s+?)(\S+)/){
		  my $_d= $2;
		  eval { chdir $_d }; warn $@ if $@;
		  if(defined $ENV{"$_d"}){
		    $ENV{PWD}="$_d:$ENV{PWD}";
		  }else{
		    $ENV{PWD}="$_d";
		  }
			say __FILE__." Line ".__LINE__.", $_cmd, $_d, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
		}elsif($_cmd =~ m/^mkdir/){ #
		  $_cmd =~ s/^mkdir(\s+?)-p\s+/mkdir /; # remove -p
		  $_cmd =~ /^mkdir(\s+?)(.+)$/;
		  my $_d=$2;
		  $_self->mkDir($_d); 
		   say __FILE__." Line ".__LINE__.", $_cmd, $_d, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
		}elsif($_cmd =~ m/^mv/){ #
		 	$_cmd =~ m/^mv(\s+?)(\S+?)(\s+?)(.+)$/;
		 	my $_from=$2;
		 	my $_to=$4;
		 	move ($_from,$_to) if (-e $_from); 
		  say __FILE__." Line ".__LINE__.", $_cmd, $_from, $_to, pwd is ".cwd() if(defined $_self->{debug} && $_self->{debug}>3);
  	}elsif($_cmd =~ m/^module(\s+?)(.+)$/){
		 	my $_cmd2=$2;
		  eval `$_self->{client}->{lmod_app_path}/libexec/lmod perl $_cmd2`;
		}elsif($_cmd =~ m/^export(\s+?)(\S+?)=(.+)/){
		  my $_env_name=$2;
		  my $_env_value=$3;
		  if(defined $ENV{$_env_name}){
		    $ENV{$_env_name}="$_env_value:$ENV{$_env_name}";
		  }else{
		    $ENV{$_env_name}="$_env_value";
		  }
		}else{
		  say __FILE__." Line ".__LINE__.", $_cmd" if(defined $_self->{debug} && $_self->{debug}>0);
      my $_is_fail = eval {system "$_cmd"}; 
      say "!failed, $_cmd" if($_is_fail);
    }
  }  
}
sub mkDir{
  my ($_self, $_d)=@_;
  say __FILE__." Line ".__LINE__.", trying to make dir $_d" if(defined $_self->{debug} && $_self->{debug}>0);
  my $_pwd = cwd();
  $_d =~ s/"//g;
  my @_folders = split /\/|\\/, $_d;
  #say __FILE__." Line ".__LINE__.", trying to make dir $_d, b4 -p folders".Dumper(\@_folders) if(defined $_self->{debug} && $_self->{debug}>0);
  shift @_folders if (defined $_folders[0] && $_folders[0] =~ m/^\s*$/);
  $_folders[0]="/".$_folders[0]  if (defined $_folders[0] && $_folders[0] =~ m|^[^/]|);
  #say __FILE__." Line ".__LINE__.", trying to make dir $_d, -p folders".Dumper(\@_folders) if(defined $_self->{debug} && $_self->{debug}>0);
  
  foreach my $_td (@_folders){
    mkdir $_td if (! -d $_td);
    die __FILE__." line ".__LINE__."at ".cwd()." $_td is not writable\n" if(!-d $_td);
    chdir $_td; 
  }
  chdir ($_pwd);
  die __FILE__." line ".__LINE__."$_d is not writable\n" if(!-d $_d); 
}1;
