#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017
 
package Kpan::KStat;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../lib/Kpan/";
use KUtil;

our $VERSION = '1.5.4';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class;
	return $_self;
}

sub _init{ 
	my $_self = {
				 id														=> shift,
				 install_id										=> shift,
				 app_id												=> shift,
				 app_name											=> shift,
				 app_version									=> shift,
				 kp_name											=> shift,
				 kp_version										=> shift,
				 client_id										=> shift,
				 client_name									=> shift,
				 binary_only									=> 0,
				 config												=> 0,
				 complex_build								=> 0,
				 dependency										=> 0,
				 debug    										=> 0,
				 hard_coded										=> 0,
				 conda												=> 0,
				 java													=> 0,
				 make													=> 0,
				 note													=> shift,
				 note2												=> shift,
				 perl													=> 0,
				 python												=> 0,
				 script_head_perl							=> 0,
				 script_head_python						=> 0,
				 script_head_others						=> 0,
				 time_created									=> shift,
				 time_updated									=> shift,
				 tmp													=> 0,
	};
	return $_self;
}

1;

