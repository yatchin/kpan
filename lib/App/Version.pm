package App::Version;

use 5.010;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);
our $VERSION = '0.1';

my %CACHE; #static cache for app

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
			archive_url                        => shift,           
			brief                              => shift,                 
			debug                              => shift,                 
			home_url                           => shift,               
			id                                 => shift,       
			name                               => shift,                        
			note                               => shift,                        
			version_url                        => shift,           
			# license
			# author             
	};
	return $_self;  
}

1;
