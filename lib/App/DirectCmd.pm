#!/usr/bin/env perl

# Copyright (c) 2017 Omic Scientific Co.
# 
# NOTICE:  All information contained herein is, and remains
# the property of Omic Scientific Company and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Omic Scientific Company
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Omic Scientific Company.
# Written by Yecheng Huang, <yhuang@omicsci.com>, December 2017

# parse apps installed by eb or directly by cmd
 
package App::DirectCmd;
use 5.010;
use strict;
use warnings;
#use File::stat;
#use File::Find::Rule;
use Data::Dumper;
use Sort::Versions qw(versioncmp);
use feature qw(say);

our $VERSION = '1.5.9';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{		
	my $_self = {
		client             => shift,
		debug              => shift,
		direct_app_help    => shift,
		direct_app_name    => shift,
		direct_app_version => shift,
		direct_module      => shift,
		is_overwrite       => shift, # (1,0)
		eb                 => shift,
		note               => shift,
		warning            => '',
		wiki               => Kpan::Wiki->new(),
  }		
}

sub parseCmd {
	my ($_self, $_file) = @_;  
	return if !defined $_file;
  open my $_in, "<", $_file or die  __FILE__." Line ".__LINE__.",  couldn't open file $_file for reading: $!\n";
  my $_warnings             = "";
  my $_count_all            = 0;
  my $_count_valid          = 0;
  my $_count_done           = 0;
  my $_directive_start      = 0;
  my $_app_start            = 0;
  my $_src_dir_prefix       = $_self->{client}->{src_dir_prefix};
	my $_install_dir_prefix   = $_self->{client}->{install_dir_prefix};
	my $_lmod_dir_prefix      = $_self->{client}->{lmod_dir_prefix};
	my $_wiki_list_page       = $_self->{client}->{wiki_list_page};
  my $_eb_app_file_list        ;
  my $_gb_app_file_list        ;
  my @_miss_app_list        ;
  my @_miss_app_cmd_list    ;
	while (my $_line = <$_in>){
	  $_line = Kpan::KUtil::Trim($_line);
		next if( $_line =~ m/^#/ && $_line !~ m/^$_self->{client}->{KP}/); # skip commetns 
	  next if( $_line =~ m/^\s*$/); # skip blank 
 	  if( $_line =~ m/^$_self->{client}->{KP} Directive/){
			$_directive_start = 1;
			next;
		}
	  if( $_line =~ m/^$_self->{client}->{KP} App/){
			$_app_start       = 1;
			$_directive_start = 0;
			$_eb_app_file_list   = $_self->getAppFilePathList($_install_dir_prefix);
			$_gb_app_file_list   = $_self->getAppFilePathList($_src_dir_prefix);
			next;
		}
	  if( $_line =~ m/^$_self->{client}->{KP} Interactive/){
			$_app_start       = 0;
			$_directive_start = 0;
			next;
		}
  	#say __FILE__." Line ".__LINE__.", l=$_line, d=$_directive_start, a=$_app_start";
		if($_directive_start == 1 ){
			my $_temp            = $_line;
			$_temp               =~ s/.+=\s*// if ( $_line =~ m/=/ ) ; 
	    $_src_dir_prefix     = $_temp if ( $_line =~ m/src_dir_prefix/     ) ; 
	    $_install_dir_prefix = $_temp if ( $_line =~ m/install_dir_prefix/ ) ; 
	    $_lmod_dir_prefix    = $_temp if ( $_line =~ m/lmod_dir_prefix/    ) ; 
	    $_wiki_list_page     = $_temp if ( $_line =~ m/wiki_list_page/    ) ; 
	    say __FILE__." Line ".__LINE__.", l=$_line, d=$_install_dir_prefix; w=$_wiki_list_page" if(defined $_self->{debug} && $_self->{debug}>2);
		}
		if($_app_start == 1){
      my $_temp = $_line;
      $_count_all++;
      my ($_app_name, $_cmd, $_help) = split(",",$_temp);
      $_cmd         = '' if (!defined $_cmd);
      $_help        = '' if (!defined $_help);
      $_app_name    = $_self->trimStr($_app_name);
      $_cmd         = $_self->trimStr($_cmd);
      $_help        = $_self->trimStr($_help);
      say __FILE__." Line ".__LINE__.", process $_app_name;$_cmd; $_help" if(defined $_self->{debug} && $_self->{debug}>0);
      my ($_software_version, $_software_name, $_kp_name, $_kp_version, $_category, $_brief, $_home_url,$_install_test,$_app_path);
      my $_src_gb_dir_prefix = "$_self->{client}->{src_dir_prefix}";
      my $_app_hash_ref  = $_self->getRecipeFilePath($_app_name, $_eb_app_file_list, $_gb_app_file_list);
      do { say __FILE__." Line ".__LINE__.". skip $_app_name scalar=".(scalar keys %{$_app_hash_ref}).Dumper($_app_hash_ref); push (@_miss_app_list, $_app_name); next;} if (scalar keys %{$_app_hash_ref} == 0 ); # miss eb folder
      for my $_v (sort { versioncmp($a, $b) } keys %{$_app_hash_ref}){
				my $_recipe_file  = (defined $$_app_hash_ref{$_v}{recipe_file_path}) ? $$_app_hash_ref{$_v}{recipe_file_path}: undef ;
				#do { say __FILE__." Line ".__LINE__.", no such recipe $_app_name, $_cmd, $_help, ebf=$_recipe_file, v=$_v"; next;} if (! -f $_recipe_file);
				do { next;} if (!defined $_recipe_file || !-f $_recipe_file);
				my $_vpath    = $$_app_hash_ref{$_v}{version_path};
        if (defined $_recipe_file && -f $_recipe_file){
					if ($_recipe_file =~ m/\.eb$/){
						$_cmd                             = "which $_cmd" if ($_cmd eq "baseml");
			      ($_software_name, $_software_version, $_home_url, $_category, $_brief, $_app_path, $_install_test ) = $_self->processEBReceipt($_recipe_file, $_lmod_dir_prefix, $_install_dir_prefix, $_cmd, $_help);
						$_self->{warning}                .= "manually fix wiki $_software_name, / $_software_version for command \n" if ($_cmd eq "" || $_cmd =~ m/^\s*NA\s*$/i || $_cmd eq "baseml");
				  }else{					
						say __FILE__." Line ".__LINE__.", $_app_name,$_src_gb_dir_prefix,  vpath=$_vpath; $_recipe_file exists?".(-e $_recipe_file) if(defined $_self->{debug} && $_self->{debug}>0);
					  my $_k                          = Kpan::KLog->new;
					  $_k->{is_test_install}          = 1;
					  $_k->{debug}                    = $_self->{debug};
					  my $_is_upload                  = 0;
					  my $_skip_parse_test            = 0;
					  $_k->{checked_level}            = 3;
  					my $_install                    = $_k->parseKLogFile($_recipe_file, $_skip_parse_test); # parse existing test result
			      $_software_name                 = $_install->{software}->{name};
			      $_software_version              = $_install->{software}->{version};
			      $_home_url                      = $_install->{software}->{home_url};
			      $_brief                         = $_install->{brief};
			      $_category                      = $_install->{category};
			      $_app_path                      = "$_self->{client}->{install_dir_prefix}/$_install->{kp_name}/$_install->{kp_version}";
			      $_install_test                  = $_install->{install_test};

						say __FILE__." Line ".__LINE__.", $_app_name,$_src_gb_dir_prefix,  vpath=$_vpath; " if(defined $_self->{debug} && $_self->{debug}>0);
					}
				}       		  
  		  say  __FILE__." Line ".__LINE__.", $_app_path, install_test=".Dumper($_install_test) if(defined $_self->{debug} && $_self->{debug}>1);
			  say  __FILE__." Line ".__LINE__.", $_app_name, $_v will update wiki $_software_name, $_software_version, $_home_url, $_category, $_app_path" if(defined $_self->{debug} && $_self->{debug}>0);
			  $_self->updateWiki($_software_name, $_software_version, $_home_url, $_category, $_brief, $_app_path, $_install_test, $_wiki_list_page ) if (defined $_software_name && $_software_name =~ m/\S/ && defined $_software_version);				          
			}
      $_count_valid++;
		}
		#last if ($_count_valid>2);
	}		
	say  __FILE__." Line ".__LINE__.", Total ".(scalar @_miss_app_list )." no installed".Dumper(\@_miss_app_cmd_list);
	say  __FILE__." Line ".__LINE__.", Total ".(scalar @_miss_app_cmd_list )." have no main command".Dumper(\@_miss_app_cmd_list);
	say  __FILE__." Line ".__LINE__.", Warnings:\n\n$_self->{warning}";
}
sub trimStr {
	my ($_self, $_s) = @_;
	my $_r = $_s;
	$_r    =~ s/"//g;			
	$_r    =~ s/,//g;			
	$_r    =~ s/^\s*//;			
	$_r    =~ s/\s*$//;			
	return $_r;			
}
sub processEBReceipt {
	my ($_self, $_recipe_file, $_lmod_dir_prefix, $_install_dir_prefix, $_cmd, $_help) = @_;
  my $_eb       = Kpan::EB->new;
  $_eb->parseEBFile($_recipe_file); 
  my $_category = defined $_eb->{category} ? $_eb->{category}:"Other";
  my $_version  = defined $_eb->{version}  ? $_eb->{version} : "";
  if ($_category eq "bio") {
    $_category    = "Bioinformatics" ;
  } elsif ($_category eq "chem") {
    $_category    = "Chemistry" ;
  } elsif ($_eb->{name} =~ m/^\s*(java|R|perl|bioperl|python|biopython)\s*$/i) {
    $_category    = "Programming" ;
  } else{
    $_category    = "Other";
  }
  say __FILE__." Line ".__LINE__.",  ebf=$_recipe_file,  $_eb->{name}, $_eb->{version}, $_eb->{home_url}, $_eb->{brief};" if(defined $_self->{debug} && $_self->{debug}>1);

  my $_kp_name        = $_eb->{name};
  my $_kp_version     = $_eb->{version};
  my $_app_path       = "$_install_dir_prefix/$_eb->{name}/$_eb->{version}" ; 
  #my $_module_load    = "$_kp_name/$_kp->version";
  # to do
  my $_module_app_path = "$_lmod_dir_prefix/$_eb->{name}";
  my @_module_files = <$_module_app_path/*.lua>;
  if(scalar @_module_files != 1 ) {
     #say  __FILE__." Line ".__LINE__.", not one lua.".Dumper(@_module_files);
  }
  my ($_module_lua_file)   = grep { $_ =~ m/$_eb->{version}/  } @_module_files;
  do {say  __FILE__." Line ".__LINE__.", $_eb->{version} not such lua: $_module_lua_file".Dumper(\@_module_files); next; } if (!defined $_module_lua_file && !-f $_module_lua_file) ;
  if ($_module_lua_file      =~ m|$_lmod_dir_prefix/(\S+?)/(\S+).lua|){
    $_kp_name                = $1;
    $_kp_version             = $2;
  }
  my $_install_test   = $_self->getInstallTest($_cmd,$_eb->{name}, $_eb->{version}, $_kp_name, $_kp_version, $_help); 				
	return ($_eb->{name}, $_eb->{version}, $_eb->{home_url}, $_category, $_eb->{brief}, $_app_path, $_install_test );
}

sub getDirectCmd {
}sub getInstallTest{
  my ($_self, $_cmd,$_app_name, $_app_version, $_kp_name, $_kp_version, $_help, )= @_;
  my $_install_test                 = Kpan::InstallTest->new;
  return $_install_test if (!defined $_app_name || !defined $_app_version);
  $_kp_name                         = $_app_name if (!defined $_kp_name);
  $_kp_version                      = $_app_version if (!defined $_kp_version);
	$_self->{warning}                .= "manually fix wiki $_app_name, /$_kp_version for command \n" if ($_cmd eq "" || $_cmd =~ m/^\s*NA\s*$/i) || $_cmd eq "baseml";
	$_cmd                             = "which $_cmd" if ($_cmd eq "baseml");
	$_install_test->{command}         = $_cmd if (defined $_cmd);
	$_install_test->{debug }          = $_self->{debug};
	$_install_test->{file_name_prefix}= $_self->{client}->{install_user_mark};
	$_install_test->{help_para}       = $_help;
	$_install_test->{module_load}     = "$_kp_name/$_kp_version";
	$_install_test->{name}            = $_app_name;
	$_install_test->{kp_name}         = $_kp_name;
	$_install_test->{KP}              = $_self->{client}->{KP};
	$_install_test->{version}         = $_app_version;
	$_install_test->{kp_version}      = $_kp_version;
	$_install_test->{not_write_out}   = 1;
	$_install_test->{lmod_app_path}   = $_self->{client}->{lmod_app_path};
	my $_is_dry_run                   =  0 ;
	$_install_test->testCmd($_self->{client}->{lmod_app_path}, $_is_dry_run );   
  return $_install_test;}
sub getAppFilePathList {
	my ($_self, $_install_dir_prefix) = @_;
	say __FILE__." Line ".__LINE__.", looking for installed apps at $_install_dir_prefix " if(defined $_self->{debug} && $_self->{debug}>1);
	opendir my $_dir, "$_install_dir_prefix" or die "Cannot open directory: $!";
  my @_app_file_list = <$_install_dir_prefix/*> ; # /usr/local/apps/apache
  #my @_app_file_list = grep {$_ ne '.' and $_ ne '..'} readdir $_dir; # apache
  closedir $_dir;
 	say __FILE__." Line ".__LINE__.", found ". (scalar @_app_file_list +1) ." apps\n".Dumper(\@_app_file_list) if(defined $_self->{debug} && $_self->{debug}>2);                           
	return \@_app_file_list;	
}

sub getRecipeFilePath {
	my ($_self, $_app_name, $_eb_app_file_list,, $_gb_app_file_list) = @_;
	my %_hash; #(app_install_name, version, eb_file_path) ;
	my @_eb_app_version_dir = grep { $_ =~ m/\/\b\Q$_app_name\E$/i } @{$_eb_app_file_list};
	say __FILE__." Line ".__LINE__.", $_app_name has ".(scalar @_eb_app_version_dir)." apps \n".Dumper(\@_eb_app_version_dir)  if(defined $_self->{debug} && $_self->{debug}>4);
	@_eb_app_version_dir = grep { $_ =~ m/\/\Q$_app_name\E/i } @{$_eb_app_file_list} if (scalar @_eb_app_version_dir == 0 );
	say __FILE__." Line ".__LINE__.", $_app_name has ".(scalar @_eb_app_version_dir)." apps \n".Dumper(\@_eb_app_version_dir)  if(defined $_self->{debug} && $_self->{debug}>4);
	my @_gb_app_version_dir = grep { $_ =~ m/\/\b\Q$_app_name\E$/i } @{$_gb_app_file_list};
	@_gb_app_version_dir = grep { $_ =~ m/\/\Q$_app_name\E/i } @{$_gb_app_file_list} if (scalar @_gb_app_version_dir == 0 );

	my $_eb_size = scalar @_eb_app_version_dir;
	my $_gb_size = scalar @_gb_app_version_dir;
	if ( $_eb_size == 0 && $_gb_size == 0 ) {
		$_self->{warning} .= "no such app $_app_name at eb or gb\n";
		return;
	}elsif ($_eb_size > 1 || $_gb_size >1 ) {
		say __FILE__." Line ".__LINE__.", multiple match app $_app_name\n".Dumper(\@_eb_app_version_dir).Dumper(\@_gb_app_version_dir)  if(defined $_self->{debug} && $_self->{debug}>0);
		return;
	}else{ # only one match
	  my $_install_app_name = ($_eb_size == 1)? $_eb_app_version_dir[0]:$_gb_app_version_dir[0]; 
	  say __FILE__." Line ".__LINE__.", app $_app_name is at $_install_app_name\n"  if(defined $_self->{debug} && $_self->{debug}>1);
	  do {$_self->{warning} .= "$_app_name doesn't not exist $_install_app_name\n"; next} if (!-e "$_install_app_name");
  	opendir my $_fdir, "$_install_app_name" or die "Cannot open $_app_name directory: $_install_app_name ";
    my @_version_list = grep {$_ ne '.' and $_ ne '..'} readdir $_fdir;
	  say __FILE__." Line ".__LINE__.", app $_app_name at $_install_app_name has vsersions: \n".Dumper(@_version_list)  if(defined $_self->{debug} && $_self->{debug}>1);
    closedir $_fdir;
    foreach my $_v (@_version_list) {
			my $_vpath = "$_install_app_name/$_v";
			$_hash{$_v}{version_path}= $_vpath;
			next if (-f $_vpath || $_vpath =~ /tmp$/ );
		  my $_target_dir = ($_eb_size == 1)? "$_vpath/easybuild/" : "$_vpath";
			do {say __FILE__." Line ".__LINE__.", app $_app_name doesn't not exist $_target_dir "; next;} if (!-e "$_target_dir");
  	  opendir my $_vdir, $_target_dir or die "Cannot open $_app_name directory: $_target_dir";
  	  my $_target_pattern = ($_eb_size == 1)? ".eb":"$_self->{client}->{app_install_log}";
      my @_e_files = ($_eb_size == 1) ? grep {$_ =~ m/\Q$_target_pattern\E$/} readdir $_vdir : grep {$_  eq $_target_pattern} readdir $_vdir;
      closedir $_vdir;
      say __FILE__." Line ".__LINE__.", $_vpath, $_target_dir has recipe files? ".(scalar @_e_files)." \n".Dumper(@_e_files)  if(defined $_self->{debug} && $_self->{debug}>1 ) ;
      if ( scalar @_e_files > 1 ){
	      say __FILE__." Line ".__LINE__.", $_target_dir has multiple $_target_pattern files \n".Dumper(@_e_files)  if(defined $_self->{debug} && $_self->{debug}>0 ) ;
	      $_self->{warning} .= "$_target_dir has multiple $_target_pattern files\n"; 
	      next;
			}else{
				my $_f = ($_eb_size == 1)? "$_vpath/easybuild/$_e_files[0]":"$_vpath/$_self->{client}->{app_install_log}";
				$_hash{$_v}{recipe_file_path}= $_f;
				say __FILE__." Line ".__LINE__.", $_app_name ($_install_app_name/$_v) recipe is at $_f " if(defined $_self->{debug} && $_self->{debug}>1 ) ;
			}    			
		}
	}
	return \%_hash;						 
}

sub updateWiki{
	my ( $_self, $_app_name, $_version, $_url, $_category, $_brief, $_app_path, $_install_test, $_wiki_list_page ) = @_;

	my $_client                = $_self->{client};
	my $_app_home_url          = $_url; 
	my $_app_archive_url       = $_url; 

	$_self->{wiki}->{debug}            = $_self->{debug};
	$_self->{wiki}->{api_url}          = $_client->{wiki_api_url};
	$_self->{wiki}->{cluster_system}   = $_client->{cluster_system};
	$_self->{wiki}->{login_name}       = $_client->{wiki_login_name};
	$_self->{wiki}->{login_password}   = $_client->{wiki_login_password};
	$_self->{wiki}->{template_name}    = $_client->{wiki_template_name};
	$_self->{wiki}->{wiki_list_page}   = $_wiki_list_page;

	say __FILE__." Line ".__LINE__.", $_app_name,$_version,$_app_home_url,$_app_archive_url,$_category,  $_wiki_list_page, preset wiki =".Dumper($_self->{wiki}) if(defined $_self->{wiki}->{debug} && $_self->{wiki}->{debug}>2);
	$_self->{wiki}->updateAppWiki($_app_name,$_version,$_app_home_url,$_app_archive_url,$_category, $_brief,$_app_path, $_install_test);
}
1;
