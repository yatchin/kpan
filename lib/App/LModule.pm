package App::LModule;

use 5.010;
use strict;
use warnings;
use Data::Dumper;
use feature qw(say);

our $VERSION = '0.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
			archive_url                        => shift,           
			brief                              => shift,                 
			category                           => shift,              
			checked_level                      => shift,              
			content                            => shift,                 
			debug                              => shift,                 
			home_url                           => shift,               
			id                                 => shift,       
			kp_name                            => shift, # smatools, 1.0_gcc447                       
			kp_version                         => shift, # smatools, 1.0_gcc447                    
			lc_name                            => shift,                     
			name                               => shift,                        
			node                               => shift,                        
			note                               => shift,                        
			path                               => shift,                        
			load_hash_ref                      => shift, # gcc/4.47       
			path_hash_ref                      => shift, # path, LD_PATH, PERL5LIB, PYTHON, etc. >{}, #foreach my $i (0..$#@v) {$_software=$version_software_hash{$v[$i]};}         
	};
	return $_self;  
}

1;
