package App::Software;

=pod

=head1 NAME

InstalledApp - My author was too lazy to write an abstract

=head1 SYNOPSIS

  my $object = InstalledApp->new(
      foo  => 'bar',
      flag => 1,
  );
  
  $object->dummy;

=head1 DESCRIPTION

The author was too lazy to write a description.

=head1 METHODS

=cut

use 5.010;
use strict;
use warnings;
use Sort::Versions qw(versioncmp);
use Data::Dumper;
use feature qw(say);
use Version;
our $VERSION = '0.1';

sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
			archive_url                        => shift,           
			brief                              => shift,                 
			debug                              => shift,                 
			home_url                           => shift,               
			id                                 => shift,       
			name                               => shift,                        
			note                               => shift,                        
			version_hash_ref                   => shift,                        
	};
	return $_self;  
}

1;
