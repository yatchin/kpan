package App::InstalledApp;

=pod

=head1 NAME

InstalledApp - My author was too lazy to write an abstract

=head1 SYNOPSIS

  my $object = InstalledApp->new(
      foo  => 'bar',
      flag => 1,
  );
  
  $object->dummy;

=head1 DESCRIPTION

The author was too lazy to write a description.

=head1 METHODS

=cut

use 5.010;
use strict;
use warnings;
use Sort::Versions qw(versioncmp);
use Data::Dumper;
use feature qw(say);
use FindBin qw($Bin);
use lib "$Bin/../../lib/Kpan";
use DBData;
use lib "$Bin/../../lib/App";
use LModule;
use Software;
our $VERSION = '0.1';

my %CACHE; #static cache for app


sub new {
	my ($_class, @_args) = @_;
	my $_self = _init(@_args);
	bless $_self, $_class; # Reference to empty hash
	return $_self;
}

sub _init{
	my $_self = {
			#installing_version_array          => [],    # reference to an annonymous array      
			category                           => shift,              
			checked_level                      => shift,              
			cluster                            => shift,              
			debug                              => shift,                 
			id                                 => shift,       
			kp_name                            => shift, # smatools, 1.0_gcc447                       
			kp_version                         => shift, # smatools, 1.0_gcc447                    
			latest_version                     => shift,              
			lc_name                            => shift,                     
			name                               => shift,                        
			note                               => shift,                        
			software                           => shift,                        
			installed_hash_ref                 => shift, # {}, #foreach my $i (0..$#@v) {$_software=$version_software_hash{$v[$i]};}         
	};
	return $_self;  
}

sub addVersion {
	my ($_self, $_installed_version, $_version, $_debug) = @_;
	if(defined $_installed_version && defined $_version){
  	my $_installed_hash_ref                             = $_self->{installed_hash_ref};
	  $$_installed_hash_ref{$_installed_version}{version} = $_version;
  }
	return 1;
}

sub addModule {
	my ($_self, $_installed_version, $_module_file, $_debug)   = @_;
	if(defined $_installed_version && defined $_module_file && -e $_module_file){
	  my $_module_version                                        = $_module_file;
		$_module_version                                           =~ s|.*/||; # only base name
		$_module_version                                           =~ s|.lua$||; # only base name
		my $_installed_hash_ref                                    =  $_self->{installed_hash_ref};
		$$_installed_hash_ref{$_installed_version}{module_file}    =  $_module_file;
		$$_installed_hash_ref{$_installed_version}{module_version} =  $_module_version;
  }
	return 1;
}
1;

=pod

=head1 SUPPORT

No support is available

=head1 AUTHOR

Copyright 2012 Anonymous.

=cut
